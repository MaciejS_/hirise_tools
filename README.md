# hirise-tools

CLI utils used for experimentation with [RISE](https://github.com/eclique/RISE/) and [HiRISE](https://github.com/macsakow/hirise)

# Utilities
* `bin/rise` - RISE CLI tools
    * `simple_mapgen` - RISE Saliency Map generator
    * `ins_del_runner` - Insertion/Deletion game runner - a saliency map quality metric proposed by [RISE: Randomized Input Sampling for Explanation of Black-box Models](https://arxiv.org/abs/1806.07421)
    * `combiner` - Combines archives created by simple_mapgen. 
    * `converter` - used to convert old simple_mapgen's result archives to between format versions
    * `map_info` - result archive viewer - displays the contents of .npz files created with `ins_del_runner` and `simple_mapgen`. numpy.ndarrays are presented via their shapes
* `bin/hirise` - HiRISE CLI tools
    * `run_hirise` - HiRISE saliency map generators. Optimized to detect overlapping parameter configurations and prevents redundant computations.  
    * `alt_game` - Alteration Game (causal metric) runner for map archives created by `run_hirise` and `run_hirise_optimized`
    * `hirise_refs` - generates reference RISE saliency maps for map archives created by `run_hirise`
* `bin/vrise` - VRISE CLI tools
  * core
    * `mapgen` - saliency maps generator 
    * `refs` - generates reference RISE saliency maps for map archives created by `mapgen`
    * `game` - runs Alteraation Game on maps in archives created by `mapgen`
    * `merge` - merges identically-shaped archive "shards" along a selected dimension (e.g. several archives containing repetitions of a single experiment ran by `mapgen`). Capable of creating both "physical" and "[virtual](https://docs.h5py.org/en/stable/vds.html)" merges. Virtual merges are particularly well suited to large shards, as they create linking datasets and do not duplicate data from shards.
  * extras
    * `change_imgroot` - alters the images root path stored in `mapgen`/`game`/`refs` archive, allowing computations to be resumed e.g. if the results archive has been moved to another machine
    * `f32_arx_to_fp16` - lossy compressor of map archives created by `mapgen` and `refs` - downcasts saliency map data from FP32 to FP16, reducing archive size by 50%
    * `list_hdf` - summarizes contents of the archive - parameter dimensions their contents, fixed parameters, paths and archive metadata
    * `sort_dim` - sorts the specified 
  * `cfg` - example YAML configurations for `mapgen`
RISE/HiRISE/VRISE tools **are not compatible with tools outside of their suite!** E.g. VRISE tools and the archives they create are compatible with other VRISE tools, but not with RISE or HiRISE tools, and vice-versa. 


# Requirements files (`reqs/`)
There is no single requirements file. This repository contains three configurations:
* `cuda10` - dedicated to GTX970 (and other GPUs which require CUDA >= 10.1)
* `cuda11` - dedicated to RTX3060Ti (and other GPUs which require CUDA >= 11.0)
* `dgx` - used on DGX-Station equipped with Tesla V100 GPUs, running under CUDA10.1 driver
