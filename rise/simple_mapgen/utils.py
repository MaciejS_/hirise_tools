import threading
from math import floor, log10
from typing import Union

import numpy as np
import torch
from retrying import retry
from hirise.structures import CombinedMask


def human_friendly_number(number):
    suffixes = ["", "K", "M", "G"]

    order_of_magnitude = floor(log10(number) / 3)

    return f"{number // 10**(3*order_of_magnitude)}{suffixes[order_of_magnitude]}"


def is_out_of_memory_exception(e: Exception):
    return isinstance(e, MemoryError) or (isinstance(e, RuntimeError) and "Cannot allocate memory" in str(e))


@retry(retry_on_exception=is_out_of_memory_exception, stop_max_delay=30 * 1000, wait_fixed=1000)
def combine_masks(combined_mask: CombinedMask, masks: np.ndarray, scores: torch.Tensor, explained_class: Union[list, tuple]) -> None:
    scores = scores.cpu()[:, explained_class]
    combined_mask.add_many(masks.cpu().squeeze(), scores)


def combine_masks_async(combined_mask: CombinedMask, masks: np.ndarray, scores: torch.Tensor, explained_class: Union[list, tuple]) -> threading.Thread:
    """
    Creates and starts a worker thread which executes the mask combination task.

    :return: worker thread object
    """
    mask_combiner_thread = threading.Thread(target=combine_masks,
                                            name="MaskCombinerThread",
                                            kwargs={"scores": scores,
                                                    "masks": masks,
                                                    "combined_mask": combined_mask,
                                                    "explained_class": explained_class})
    mask_combiner_thread.start()
    return mask_combiner_thread