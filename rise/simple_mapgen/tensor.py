import collections

import numpy as np


def rebuild_experiment_tensor(list_of_checkpoint_tuples, model_input_size):
    """
    Rebuild the the multidimensional tensor of saliency maps from a list of (checkpoint_idx, map) tuples
    Expected input structure: list of runs, each being a list containing checkpoint states as (num_masks, saliency_map) tuples
    The resulting tensor will have a shape of (num_runs, num_checkpoints, *map_shape)

    :return: tuple, (result_tensor, checkpoint_idxs)
    """
    runs = len(list_of_checkpoint_tuples)
    checkpoint_idxs = [n_masks for n_masks, map_state in list_of_checkpoint_tuples[0]]
    exp_tensor = np.zeros((runs, len(checkpoint_idxs), *model_input_size))
    for run_idx, run in enumerate(list_of_checkpoint_tuples):
        for chkp_idx, (checkpoint_idx, mask) in enumerate(run):
            exp_tensor[run_idx, chkp_idx] = mask

    return exp_tensor, checkpoint_idxs


def create_tensor_layout(parsed_args, checkpoints_lambda, input_img_shape):
    args = parsed_args
    checkpoints = list(filter(lambda i: checkpoints_lambda(i, None, None), range(1, args.masks + 1)))

    if not all(map(lambda x: isinstance(x, (list, tuple)),
                   (args.grid_size, args.occlusion_prob))):
        raise TypeError("`grid_size` and `occlusion_prob` returned by parser must be either tuple or list")

    tensor_layout = collections.OrderedDict(runs=1,
                                            s=args.grid_size,
                                            p1=args.occlusion_prob,
                                            checkpoints=checkpoints,
                                            map_y=input_img_shape[0],
                                            map_x=input_img_shape[1])


    return tensor_layout


def create_tensor(tensor_layout):
    """
    Creates a tensor from tensor layout.
    Number of dimensions will be equal to the number of items in tensor_layout.
    Dimension lengths will be equal to the value of corresponding item in tensor_layout, or its length, in case of a collection

    :param tensor_layout: OrderedDict instance
    :return:
    """
    return np.zeros([len(v) if isinstance(v, (list, tuple)) else v
                     for k, v
                     in tensor_layout.items()])