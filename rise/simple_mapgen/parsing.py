def parse_value_or_range(runs_param):
    if '-' in runs_param:
        lower_bound, upper_bound = map(int, runs_param.split('-'))
        if not (0 <= lower_bound < upper_bound):
            raise ValueError("Boundary values must be positive integers. Lower bound must be greater than lower")
        return range(lower_bound, upper_bound)
    else:
        upper_bound = int(runs_param)
        if upper_bound <= 0:
            raise ValueError("Run count ust be a positive integer")
        return range(upper_bound)