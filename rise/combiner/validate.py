import copy
from typing import List, Dict

import numpy as np


def validate_map_tensors(map_tensors: List[np.ndarray]):
    reference_map = map_tensors[0]
    if not all(map_tensor.shape[1:] == reference_map.shape[1:] for map_tensor in map_tensors[1:]):
        shapes = '\n'.join(str(tensor.shape) for tensor in map_tensors)
        raise ValueError(f"Map tensor shapes didn't match, got: {shapes}")


def validate_tensor_layouts(tensor_layouts: List[Dict]):
    ignored_keys = {"runs"}
    tensor_layouts = copy.deepcopy(tensor_layouts)
    for key in ignored_keys:
        for layout in tensor_layouts:
            layout.pop(key)

    reference_tensor = copy.deepcopy(tensor_layouts[0])
    for tensor_layout in tensor_layouts[1:]:
        if reference_tensor != tensor_layout:
            raise ValueError(f"Tensor layouts don't match:\n reference: {reference_tensor} differing: {tensor_layout}")


def all_part_keysets_equal(parts):
    result = True
    first_keyset = set(parts[0].keys())
    for part in parts[1:]:
        result &= len(first_keyset.difference(part.keys())) == 0
    return result