import copy
from typing import List, Dict


def combine_tensor_layouts(tensor_layouts: List[Dict]):
    total_run_count = sum(layout["runs"] for layout in tensor_layouts)
    new_layout = copy.deepcopy(tensor_layouts[0])
    new_layout["runs"] = total_run_count
    return new_layout