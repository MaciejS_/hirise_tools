import numpy as np
import pandas as pd
from unittest import TestCase

from parameterized import parameterized

from hirise_tools.annotation.utils import (
    add_bbox_arrays,
    scale_bboxes,
    render_bbox,
    bbox_union,
    bbox_intersection,
)


class TestScaleBBoxes(TestCase):
    def setUp(self) -> None:
        self.img_df = pd.DataFrame.from_records(
            data=[
                {"img": "A", "width": 50, "height": 20},
                {"img": "B", "width": 100, "height": 40},
                {"img": "C", "width": 50, "height": 100},
            ]
        )

        self.test_df = pd.DataFrame.from_records(
            data=[
                {"img": "A", "xmin": 35, "xmax": 45, "ymin": 4, "ymax": 12},
                {"img": "B", "xmin": 26, "xmax": 88, "ymin": 10, "ymax": 30},
            ]
        )
        self.expected_result = pd.DataFrame.from_records(
            data=[
                {"img": "A", "xmin": .7, "xmax": .9, "ymin": .2, "ymax": .6},
                {"img": "B", "xmin": .26, "xmax": .88, "ymin": .25, "ymax": .75},
            ]
        )

        self.test_dfs = {
            "bbox_df": self.test_df,
            "img_df": self.img_df
        }

    def test_result(self):
        result = scale_bboxes(self.test_df, self.img_df)
        self.assertTrue(
            result.equals(self.expected_result),
            msg=f"Expected:\n{self.expected_result},\n got:{result}"
        )

    @parameterized.expand([
        ((2, 2), ),
        ((3, 5), )
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.scale_yx = {p.args[-1]}")
    def test_scaling_to_custom_size(self, scale_shape):
        # arrange
        self.expected_result = (
            self.expected_result
            .assign(
                xmin=lambda df: df['xmin'] * scale_shape[1],
                xmax=lambda df: df['xmax'] * scale_shape[1],
                ymin=lambda df: df['ymin'] * scale_shape[0],
                ymax=lambda df: df['ymax'] * scale_shape[0],
            )
        )

        result = scale_bboxes(self.test_df, self.img_df, scale_shape)
        self.assertTrue(
            result.equals(self.expected_result),
            msg=f"Expected:\n{self.expected_result},\n got:{result}"
        )

    @parameterized.expand([
        ("bbox_df", "xmin"),
        ("bbox_df", "xmax"),
        ("bbox_df", "ymin"),
        ("bbox_df", "ymax"),

        ("img_df", "height"),
        ("img_df", "width"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_on_missing_column(self, df_name, colname):
        self.test_dfs[df_name].drop(colname, axis=1, inplace=True)

        # act & assert
        with self.assertRaises(KeyError):
            scale_bboxes(self.test_df, self.img_df)


class TestAddBboxInformation(TestCase):
    def setUp(self) -> None:
        self.test_df = pd.DataFrame.from_records(
            data=[
                {"img": "A", "xmin": 10, "xmax": 25, "ymin": 4, "ymax": 12},
                {"img": "B", "xmin": 26, "xmax": 88, "ymin": 8, "ymax": 23},
            ]
        )
        self.expected_result = pd.DataFrame.from_records(
            data=[
                {"img": "A", "xmin": 10, "xmax": 25, "ymin": 4, "ymax": 12, "bbox": np.array([[4, 12], [10, 25]])},
                {"img": "B", "xmin": 26, "xmax": 88, "ymin": 8, "ymax": 23, "bbox": np.array([[8, 23], [26, 88]])},
            ]
        )

    def test_result(self):
        result = add_bbox_arrays(self.test_df)
        self.assertTrue(
            result.equals(self.expected_result),
            msg=f"Expected:\n{self.expected_result},\n got:{result}"
        )

    def test_original_array_isnt_altered(self):
        add_bbox_arrays(self.test_df)
        self.assertNotIn('bbox', self.test_df)


class TestRenderBbox(TestCase):
    # def test_render_bbox_without_scaling(self):

    @parameterized.expand([
        (
            (10, 10),
            False,
            np.array(
                [
                    [[0, 2], [7, 9]],
                    [[3, 7], [4, 7]],
                    [[8, 9], [1, 4]],
                ]
            ),
            (
                (slice(0, 2), slice(7, 9)),
                (slice(3, 7), slice(4, 7)),
                (slice(8, 9), slice(1, 4))
            ),
            "do not scale.2d.map shape (10, 10)"
        ),
        (
            (10, 10),
            True,
            np.array(
                [
                    [[0, .2], [.7, .9]],
                    [[.3, .7], [.4, .7]],
                    [[.8, .9], [.1, .4]],
                ]
            ),
            (
                (slice(0, 2), slice(7, 9)),
                (slice(3, 7), slice(4, 7)),
                (slice(8, 9), slice(1, 4))
            ),
            "scale.2d.map shape (10, 10)"
        ),
        (
            (200, 200),
            True,
            np.array(
                [
                    [[0, .2], [.7, .9]],
                    [[.3, .7], [.4, .7]],
                    [[.8, .9], [.1, .4]],
                ]
            ),
            (
                (slice(0, 40), slice(140, 180)),
                (slice(60, 140), slice(80, 140)),
                (slice(160, 180), slice(20, 80))
            ),
            "scale.2d.map shape (200, 200)"
        ),
        (
            (10, 20, 30),
            True,
            np.array(
                [
                    [[0, .2], [.7, .9], [.3, .6]],
                    [[.3, .7], [.4, .7], [.5, .8]],
                    [[.8, .9], [.1, .4], [.2, .5]],
                ]
            ),
            (
                (slice(0, 2), slice(14, 18), slice(9, 18)),
                (slice(3, 7), slice(8, 14), slice(15, 24)),
                (slice(8, 9), slice(2, 8), slice(6, 15))
            ),
            "scale.3d.map shape (10, 20, 30)"
        ),
    ], 
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result(self, map_shape, scale, bboxes, expected_result_selectors, desc):

        # act
        bboxes = render_bbox(bboxes, map_shape, scale_bbox=scale)

        # assert
        self.assertEqual(bboxes.dtype, np.bool)
        self.assertSequenceEqual(bboxes.shape, (len(bboxes), *map_shape))
        for render, selector in zip(bboxes, expected_result_selectors):
            # make sure that the
            self.assertTrue(np.all(render[selector] == 1), msg='Not all pixels in the selector were set to 1')
            render[selector] = 0
            self.assertTrue(np.all(render == 0), msg="Some pixels outside of selector were set to 1")


    @parameterized.expand([
        ((2, 2), (10, 10), NotImplementedError, "bbox tensor is 2dim"),
        ((3, 4, 2, 2), (10, 10), NotImplementedError, "bbox tensor is 4dim"),

        ((3, 2, 2), (10, 10, 10), ValueError, "bbox has less dims that declared by map_shape"),
        ((3, 3, 2), (10, 10), ValueError, "bbox has more dims that declared by map_shape"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_when(self, bbox_tensor_shape, map_shape, expected_exc, desc):
        dummy_bbox = np.arange(bbox_tensor_shape[-2]).reshape(bbox_tensor_shape[-2], 1)
        bbox_tensor = np.tile(
            np.concatenate(
                [dummy_bbox, dummy_bbox + 2],
                axis=-1
            ),
            reps=(*bbox_tensor_shape[:-2], 1, 1)
        )

        # act & assert
        with self.assertRaises(expected_exc):
            render_bbox(bbox_tensor, map_shape, scale_bbox=False)


class TestBBoxUnion(TestCase):

    @parameterized.expand([
        ([[[85, 175], [21, 215]]], (224, 224), False, 0.346, "single bbox.real bug case #1"),
        ([[[187, 218], [79, 122]]], (224, 224), False, 0.026, "single bbox.real bug case #2"),

        ([[[.1, .5], [.3, .8]]], (20, 20), True, 0.2, "single bbox.scaling"),
        ([[[2, 10], [6, 16]]], (20, 20), False, 0.2, "single bbox.no scaling"),

        ([[[.1, .5], [.3, .8]], [[.3, .7], [.1, .4]]], (20, 20), True, .3, "overlapping bboxes.scaling"),
        ([[[2, 10], [6, 16]], [[6, 14], [2, 8]]], (20, 20), False, .3, "overlapping bboxes.no scaling"),

        ([[[.1, .3], [.2, .5]], [[.4, .7], [.1, .4]]], (20, 20), True, .15, "disjoint bboxes.scaling"),
        ([[[2, 6], [4, 10]], [[8, 14], [2, 8]]], (20, 20), False, .15, "disjoint bboxes.no scaling"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result(self, bbox_coords, map_shape, needs_scaling, expected_area, desc):
        bbox_tensor = np.array(bbox_coords)

        # act
        result = bbox_union(bbox_tensor, map_shape, scale_bbox=needs_scaling)
        union_area = np.mean(result, axis=None)

        # assert
        self.assertTupleEqual(result.shape, map_shape)
        self.assertAlmostEqual(union_area, expected_area, places=2)


class TestBBoxIntersection(TestCase):

    @parameterized.expand([
        ([[[.1, .5], [.3, .8]]], (20, 20), True, 0.2, "single bbox.scaling"),
        ([[[2, 10], [6, 16]]], (20, 20), False, 0.2, "single bbox.no scaling"),

        ([[[.1, .5], [.3, .8]], [[.3, .7], [.1, .4]]], (20, 20), True, 0.02, "overlapping bboxes.scaling"),
        ([[[2, 10], [6, 16]], [[6, 14], [2, 8]]], (20, 20), False, 0.02, "overlapping bboxes.no scaling"),

        ([[[.1, .3], [.2, .5]], [[.4, .7], [.1, .4]]], (20, 20), True, 0, "disjoint bboxes.scaling"),
        ([[[2, 6], [4, 10]], [[8, 14], [2, 8]]], (20, 20), False, 0, "disjoint bboxes.no scaling"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result(self, bbox_coords, map_shape, needs_scaling, expected_area, desc):
        bbox_tensor = np.array(bbox_coords)

        # act
        result = bbox_intersection(bbox_tensor, map_shape, scale_bbox=needs_scaling)
        union_area = np.mean(result, axis=None)

        # assert
        self.assertTupleEqual(result.shape, map_shape)
        self.assertAlmostEqual(union_area, expected_area, places=2)