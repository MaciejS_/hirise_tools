from unittest import TestCase
import pandas

from hirise_tools.annotation.pascal_voc import (
    load_bbox_as_dict,
    load_bbox_as_rows,
    load_to_dataframes
)

# region [test data]
from parameterized import parameterized

TEST_ANNOTATION_XML = """
<annotation>
	<folder>ILSVRC2013_val</folder>
	<filename>ILSVRC2012_val_00008726</filename>
	<source>
		<database>ILSVRC_2013</database>
	</source>
	<size>
		<width>500</width>
		<height>357</height>
	</size>
	<object>
		<name>n02118333</name>
		<bndbox>
			<xmin>255</xmin>
			<xmax>454</xmax>
			<ymin>142</ymin>
			<ymax>329</ymax>
		</bndbox>
	</object>
	<object>
		<name>n02118322</name>
		<bndbox>
			<xmin>44</xmin>
			<xmax>322</xmax>
			<ymin>21</ymin>
			<ymax>295</ymax>
		</bndbox>
	</object>
</annotation>
"""
ANOTHER_ANNOTATION_XML = """
<annotation>
	<folder>val</folder>
	<filename>ILSVRC2012_val_00000008</filename>
	<source>
		<database>ILSVRC_2012</database>
	</source>
	<size>
		<width>500</width>
		<height>375</height>
		<depth>3</depth>
	</size>
	<segmented>0</segmented>
	<object>
		<name>n02776631</name>
		<pose>Unspecified</pose>
		<truncated>0</truncated>
		<difficult>0</difficult>
		<bndbox>
			<xmin>14</xmin>
			<ymin>163</ymin>
			<xmax>181</xmax>
			<ymax>328</ymax>
		</bndbox>
	</object>
	<object>
		<name>n02776631</name>
		<pose>Unspecified</pose>
		<truncated>0</truncated>
		<difficult>0</difficult>
		<bndbox>
			<xmin>176</xmin>
			<ymin>81</ymin>
			<xmax>331</xmax>
			<ymax>223</ymax>
		</bndbox>
	</object>
	<object>
		<name>n02776631</name>
		<pose>Unspecified</pose>
		<truncated>0</truncated>
		<difficult>0</difficult>
		<bndbox>
			<xmin>77</xmin>
			<ymin>2</ymin>
			<xmax>236</xmax>
			<ymax>155</ymax>
		</bndbox>
	</object>
	<object>
		<name>n02776631</name>
		<pose>Unspecified</pose>
		<truncated>0</truncated>
		<difficult>0</difficult>
		<bndbox>
			<xmin>163</xmin>
			<ymin>219</ymin>
			<xmax>355</xmax>
			<ymax>374</ymax>
		</bndbox>
	</object>
</annotation>
"""
# endregion


class TestLoadingImgnetBoundingBoxesToDicts(TestCase):

    EXPECTED_BBOX_DICTS = [
        {"img": "ILSVRC2012_val_00008726",
         "synset": "n02118333",
         "xmin": 255, "xmax": 454, "ymin": 142, "ymax": 329},
        {"img": "ILSVRC2012_val_00008726",
         "synset": "n02118322",
         "xmin": 44, "xmax": 322, "ymin": 21, "ymax": 295}
    ]

    def test_load_bbox_as_dict(self):
        results = load_bbox_as_dict(TEST_ANNOTATION_XML)
        self.assertTrue(all(expected in results for expected in self.EXPECTED_BBOX_DICTS))


class TestLoadingImgnetBoundingBOxesToRows(TestCase):
    EXPECTED_BBOX_ROWS = [
        ("ILSVRC2012_val_00008726", "n02118333", 255, 142, 454, 329),
        ("ILSVRC2012_val_00008726", "n02118322", 44, 21, 322, 295)
    ]

    def test_load_bbox_as_dict(self):
        results = load_bbox_as_rows(TEST_ANNOTATION_XML)
        self.assertTrue(all(expected in results for expected in self.EXPECTED_BBOX_ROWS))


class TestLoadToDataframe(TestCase):

    DATA = [
        TEST_ANNOTATION_XML,
        ANOTHER_ANNOTATION_XML
    ]

    EXPECTED_DFS = {
        "img_df": pandas.DataFrame.from_dict(
            data={
                "folder": ["ILSVRC2013_val", "val"],
                "dataset": ["ILSVRC_2013", "ILSVRC_2012"],
                "img": ["ILSVRC2012_val_00008726", "ILSVRC2012_val_00000008"],
                "width": [500, 500],
                "height": [357, 375]
            }
        ),
        "bbox_df": pandas.DataFrame.from_records(
            data=[
                {
                    "img": "ILSVRC2012_val_00008726",
                    "synset": "n02118333",
                    "xmin": 255, "xmax": 454, "ymin": 142, "ymax": 329
                },
                {
                    "img": "ILSVRC2012_val_00008726",
                    "synset": "n02118322",
                    "xmin": 44, "xmax": 322, "ymin": 21, "ymax": 295
                },
                {
                    "img": "ILSVRC2012_val_00000008",
                    "synset": "n02776631",
                    "xmin": 14, "xmax": 181, "ymin": 163, "ymax": 328
                },
                {
                    "img": "ILSVRC2012_val_00000008",
                    "synset": "n02776631",
                    "xmin": 176, "xmax": 331, "ymin": 81, "ymax": 223
                },
                {
                    "img": "ILSVRC2012_val_00000008",
                    "synset": "n02776631",
                    "xmin": 77, "xmax": 236, "ymin": 2, "ymax": 155
                },
                {
                    "img": "ILSVRC2012_val_00000008",
                    "synset": "n02776631",
                    "xmin": 163, "xmax": 355, "ymin": 219, "ymax": 374
                },
            ]
        )
    }

    def setUp(self) -> None:
        self.test_img_df, self.test_bbox_df = load_to_dataframes(self.DATA)
        self.test_dfs = {
            "img_df": self.test_img_df,
            "bbox_df": self.test_bbox_df
        }

    @parameterized.expand([
        ('img_df',),
        ('bbox_df',)
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result_df(self, df_name):
        self.assertTrue(self.test_dfs[df_name].equals(self.EXPECTED_DFS[df_name]))


    @parameterized.expand([
        ('img_df', 'img', str),
        ('img_df', 'folder', str),
        ('img_df', 'dataset', str),
        ('img_df', 'height', int),
        ('img_df', 'width', int),

        ('bbox_df', 'img', str),
        ('bbox_df', 'synset', str),
        ('bbox_df', 'xmin', int),
        ('bbox_df', 'xmax', int),
        ('bbox_df', 'ymin', int),
        ('bbox_df', 'ymax', int),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-3]}.{p.args[-2]} is {p.args[-1].__name__}")
    def test_column_type(self, tested_df, colname, expected_type):
        self.assertTrue(all([
                isinstance(entry, expected_type)
             for entry
             in self.test_dfs[tested_df][colname]
        ]))