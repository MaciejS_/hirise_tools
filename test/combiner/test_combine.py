import copy
from collections import OrderedDict
from unittest import TestCase

from hirise_tools.rise.combiner.combine import combine_tensor_layouts

class TestCombineTensorLayouts(TestCase):

    def setUp(self) -> None:
        self.sample_tensor_layout = OrderedDict(runs=3,
                                                s=[1, 2, 3])

    def test_combine_applies_only_to_runs_field(self):
        other_layout = copy.deepcopy(self.sample_tensor_layout)
        other_layout["runs"] = 1

        result_layout = combine_tensor_layouts([self.sample_tensor_layout, other_layout])
        self.assertEqual(result_layout["runs"], other_layout["runs"] + self.sample_tensor_layout["runs"])
        self.assertTrue(all(result_layout[key] == self.sample_tensor_layout[key]
                            for key
                            in result_layout.keys()
                            if key != "runs"))