import copy
from collections import OrderedDict
from unittest import TestCase

import numpy as np
from combiner.validate import validate_map_tensors, validate_tensor_layouts, all_part_keysets_equal
from parameterized import parameterized


class TestMapShapeValidation(TestCase):

    @parameterized.expand([
        ([np.random.rand(2, 3, 4) for _ in range(4)], "all_tensors_have_the_same_shape"),
        ([np.random.rand(2, 3, 4), np.random.rand(1, 3, 4)], "first_dimension_doesn't_match"),
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_validation_succeeds_if(self, tensors, message):
        validate_map_tensors(tensors)

    @parameterized.expand([
        ([np.random.rand(2, 3, 4), np.random.rand(2, 3), np.random.rand(2, 3)], "number_of_dimensions_dosn_not_match"),
        ([np.random.rand(2, 4, 4), np.random.rand(2, 3, 4)], "at_least_one_dimension_doesn't_match"),
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_validation_fails_if(self, tensors, message):
        with self.assertRaises(ValueError):
            validate_map_tensors(tensors)


class TestTensorLayoutValidation(TestCase):

    def setUp(self) -> None:
        self.fake_tensor_layout = OrderedDict(runs=5,
                                              s=[5, 10],
                                              p1=[0.2, 0.4, 0.6],
                                              checkpoints=[1, 10, 100],
                                              map_x=224,
                                              map_y=224)

    def test_runs_field_is_ignored(self):
        tensor_with_different_number_of_runs = copy.deepcopy(self.fake_tensor_layout)
        tensor_with_different_number_of_runs["runs"] = None
        validate_tensor_layouts([self.fake_tensor_layout, tensor_with_different_number_of_runs])

    @parameterized.expand([
        ("s",),
        ("p1"),
        ("checkpoints",),
        ("map_y",),
        ("map_x",),
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_validation_fails_when_field_differs(self, field_name):
        other = copy.deepcopy(self.fake_tensor_layout)
        other[field_name] = None
        with self.assertRaises(ValueError):
            validate_tensor_layouts([self.fake_tensor_layout, other])

    @parameterized.expand([
        ("s",),
        ("p1"),
        ("checkpoints",),
        ("map_y",),
        ("map_x",),
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_validation_fails_when_any_field_is_missing(self, field_name):
        other = copy.deepcopy(self.fake_tensor_layout)
        other.pop(field_name)
        with self.assertRaises(ValueError):
            validate_tensor_layouts([self.fake_tensor_layout, other])


class Test(TestCase):
    def setUp(self) -> None:
        self.sample_dict = {"a": [1,2,4],
                     "b": ["a", "b", "c"]}

    def test_keyset_validation_doesnt_check_values_associated_with_keys(self):
        other = copy.deepcopy(self.sample_dict)
        other["a"] = None
        self.assertTrue(all_part_keysets_equal([self.sample_dict, other]))

    def test_keyset_validation_succeeds_when_keysets_equal(self):
        other = copy.deepcopy(self.sample_dict)
        self.assertTrue(all_part_keysets_equal([self.sample_dict, other]))

    def test_keyset_validation_fails_when_keysets_dont_match(self):
        other = copy.deepcopy(self.sample_dict)
        other.popitem()
        self.assertFalse(all_part_keysets_equal([self.sample_dict, other]))
