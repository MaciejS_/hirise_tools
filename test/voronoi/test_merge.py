import typing as t
import math
from unittest import TestCase, skip

import h5py
import numpy as np
from hirise_tools.voronoi.merge import (
    get_combined_ds_shape,
    merge_scales,
    merge_datasets,
    merge_datasets_virtual,
    adjust_chunksize,
)
from hirise_tools.voronoi.hdf import create_attributes, create_dataset, create_scales, label_dataset
from hirise_tools.voronoi.param_def import FIXED_PARAM_ATTR_PREFIX

from hirise_tools.test.voronoi.test_hdf import HDFArchiveFixture
from parameterized import parameterized, parameterized_class


class TestGetCombinedDatasetShape(HDFArchiveFixture, TestCase):
    def setUp(self):
        HDFArchiveFixture.setUp(self)

    def tearDown(self) -> None:
        HDFArchiveFixture.tearDown(self)

    @parameterized.expand([
        (
            [
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 3, 'bar': 5, 'baz': 2},
            ],
            'foo',
            (3*3, 5, 2),
            "equal part sizes"
        ),
        (
            [
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 2, 'bar': 5, 'baz': 2},
                {'foo': 4, 'bar': 5, 'baz': 2},
            ],
            'foo',
            (3+2+4, 5, 2),
            "variable part sizes"
        ),
        (
            [
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 3, 'bar': 5, 'baz': 2},
            ],
            'bar',
            (3, 5*3, 2),
            "joining along any dimension"
        ),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shape(self, part_schemes, merged_dim, expected_shape, desc):
        # arrange
        parts = []
        for i, (scheme) in enumerate(part_schemes):
            ds = self.hdf_file.create_dataset(
                name=f"ds_part_{i}",
                shape=tuple(scheme.values()),
                dtype=np.float32,
                data=np.arange(int(math.prod(scheme.values()))).reshape(tuple(scheme.values()))
            )
            label_dataset(ds, scheme.keys())
            parts.append(ds)

        # act
        shape, _ = get_combined_ds_shape(merged_dim, *parts)

        # assert
        self.assertSequenceEqual(shape, expected_shape)

    @parameterized.expand([
        (
            [{'foo': 3, 'bar': 5, 'baz': 2}] * 2,
            [{"foo": None, 'bar': None, 'baz': 2}] * 2,
            'foo',
            (None, None, 2),
            "merged dim is unlimited if this dim was unlimited in all parts"
        ),
        (
            [
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 4, 'bar': 5, 'baz': 2},
            ],
            [
                {"foo": 3, 'bar': None, 'baz': 2},
                {"foo": 4, 'bar': None, 'baz': 2},
            ],
            'foo',
            (3 + 4, None, 2),
            "merged dim is limited if it was limited in all parts"
        ),
        (
            [
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 4, 'bar': 5, 'baz': 2},
            ],
            [
                {"foo": 3, 'bar': 5, 'baz': 2},
                {"foo": None, 'bar': None, 'baz': 2},
            ],
            'foo',
            (3 + 4, 5, 2),
            "maxdims are limited if they were limited in first of parts"
        ),
        (
            [
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 4, 'bar': 5, 'baz': 2},
            ],
            [
                {"foo": None, 'bar': None, 'baz': 2},
                {"foo": 4, 'bar': 5, 'baz': 2},
            ],
            'foo',
            (None, None, 2),
            "maxdims are unlimited if they were unlimited in first of parts"
        ),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_maxshape(self, part_schemes, maxshape_schemes, merged_dim, expected_maxshape, desc):
        # arrange
        parts = []
        for i, (scheme, maxshape_scheme) in enumerate(zip(part_schemes, maxshape_schemes)):
            ds = self.hdf_file.create_dataset(
                name=f"ds_part_{i}",
                shape=tuple(scheme.values()),
                dtype=np.float32,
                data=np.arange(int(math.prod(scheme.values()))).reshape(tuple(scheme.values())),
                maxshape=tuple(maxshape_scheme.values())
            )
            label_dataset(ds, scheme.keys())
            parts.append(ds)

        # act
        _, maxshape = get_combined_ds_shape(merged_dim, *parts)

        # assert
        self.assertSequenceEqual(maxshape, expected_maxshape)

    #
    @parameterized.expand([
        (
            [
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 4,           'baz': 2},
            ],
            'bar',
            "in one of the parts"
        ),
        (
            [
                {'foo': 3, 'bar': 5, 'baz': 2},
                {'foo': 3, 'bar': 5, 'baz': 2},
            ],
            'quux',
            "in all of the parts"
        ),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_if_dim_doesnt_exist(self, part_schemes, merged_dim, desc):
        # arrange
        parts = []
        for i, (scheme) in enumerate(part_schemes):
            ds = self.hdf_file.create_dataset(
                name=f"ds_part_{i}",
                shape=tuple(scheme.values()),
                dtype=np.float32,
                data=np.arange(int(math.prod(scheme.values()))).reshape(tuple(scheme.values()))
            )
            label_dataset(ds, scheme.keys())
            parts.append(ds)

        # act & assert
        with self.assertRaises(Exception):
            get_combined_ds_shape(merged_dim, *parts)


class TestMergeScales(HDFArchiveFixture, TestCase):

    def setUp(self) -> None:
        HDFArchiveFixture.setUp(self)

        # The fixture places off of the parts in a single file, but from the perspective of the
        # merging logic, that doesn't make any difference.

        self.n_parts = 3
        self.dummy_scales = {
            "B": [np.arange(100, 103)] * self.n_parts,
            "C": [np.arange(200, 204)] * self.n_parts,
        }

        self.target_group = self.hdf_file.create_group('_scales')

        part_scales_group = self.hdf_file.create_group('part_scales_group')
        self.source_scale_groups = [
            part_scales_group.create_group(f'_scales_part_{i}')
            for i
            in range(self.n_parts)
        ]

        for name, scale_values in self.dummy_scales.items():
            for i, group in enumerate(self.source_scale_groups):
                _values = scale_values[i]
                group.create_dataset(name, shape=_values.shape, dtype=_values.dtype, data=_values)

    def test_merge_scaled_dim(self):
        # preconditions
        self.assertEqual(len(self.target_group), 0)

        # arrange
        for i, group in enumerate(self.source_scale_groups):
            _values = np.arange(i * 5, (i + 1) * 5) # each part of this scale will have a different set of values
            group.create_dataset(name="A", shape=_values.shape, dtype=_values.dtype, data=_values)

        # act
        merge_scales(self.target_group, 'A', self.source_scale_groups)

        # assert
        self.assertSequenceEqual(self.target_group.keys(), self.source_scale_groups[0].keys())
        # assert that all unmerged groups are the same as in part files
        self.assertTrue(all(
            np.equal(self.target_group['B'], source_group['B']).all()
            for source_group
            in self.source_scale_groups
        ))
        # assert that merged dim is a concatenation of all source groups, in order of appearance
        self.assertTrue(np.equal(
            self.target_group['A'],
            np.concatenate([scales_group['A'][:] for scales_group in self.source_scale_groups])
        ).all())


    def test_merging_a_range_dim_doesnt_merge_any_scales(self):
        dim_name = "runs" # currently the only range dimension
        # act
        merge_scales(self.target_group, dim_name, self.source_scale_groups)

        # assert
        self.assertSequenceEqual(self.target_group.keys(), self.source_scale_groups[0].keys())
        # the scale datasets in target_group are identical to those in all of the source groups
        self.assertTrue(
            all(
                all(
                    # exclude A from check
                    np.equal(self.target_group[scale_name], source_group[scale_name]).all()
                    for scale_name
                    in source_group
                )
                for source_group
                in self.source_scale_groups
            )
        )

    def test_merging_coupled_scales(self):
        # arrange
        merged_dim = "img+cls"
        for ix, src_group in enumerate(self.source_scale_groups):
            _ds = src_group.create_dataset(
                name=merged_dim,
                shape=(4, 2),
                dtype=h5py.string_dtype(encoding='utf-8'), # creates a string-compatible HDF5 dataset
            )

            _ds[:] = np.array([
                    [f"img_{ix}_{i}", str((i+1)**(ix+1))] # just some fake str + str(int) array
                    for i
                    in range(4)
                ]).astype(str)

        # act
        merge_scales(self.target_group, merged_dim, self.source_scale_groups)

        # assert
        self.assertSequenceEqual(self.target_group.keys(), self.source_scale_groups[0].keys())
        # assert that all unmerged groups are the same as in part files
        self.assertTrue(all(
            np.equal(self.target_group['B'], source_group['B']).all()
            for source_group
            in self.source_scale_groups
        ))
        # assert that merged dim is a concatenation of all source groups, in order of appearance
        self.assertTrue(np.equal(
            self.target_group['img+cls'],
            np.concatenate([scales_group['img+cls'][:] for scales_group in self.source_scale_groups])
        ).all())


    def test_raises_on_attempt_to_merge_a_non_existent_scale(self):
        dim_name = "X" # scale using this name doesn't exist in the fixture
        with self.assertRaises(Exception):
            merge_scales(self.target_group, dim_name, self.source_scale_groups)


    def tearDown(self) -> None:
        HDFArchiveFixture.tearDown(self)

@skip # prevent unparameterized instance from being ran
@parameterized_class([
    {"merge_function_capsule": lambda self: merge_datasets,         "merge_type": "real"},
    {"merge_function_capsule": lambda self: merge_datasets_virtual, "merge_type": "virtual"},
],
class_name_func=lambda c,n,d: f"{c.__name__}.{d['merge_type']} merge")
class TestMergeDataset(TestCase, HDFArchiveFixture):

    def setUp(self) -> None:
        HDFArchiveFixture.setUp(self)

        self.merge_function = self.merge_function_capsule()
        self.n_parts = 3
        self.layout = {"runs": tuple(range(3)), "foo": tuple(range(3)), "bar": tuple(range(5))}
        self.layout_order = ['runs', 'foo', 'bar']

        self.attributes = {"attr1": 5, "attr2": "test", "attr3": 3.14}
        self.range_params = {'runs': 3}
        self.scale_params = {"foo": [4, 8, 12]}
        # set up dataset
        self.parts = [
            self.__initialize_dataset(
                self.hdf_file,
                f"part_{i}",
                layout=self.layout,
                param_order=self.layout_order
            )
            for i
            in range(self.n_parts)
        ]

        create_scales(self.hdf_file, scale_params=self.scale_params)

        # add attributes:
        for part in self.parts:
            create_attributes(part, self.attributes, self.range_params)

    def tearDown(self) -> None:
        HDFArchiveFixture.tearDown(self)

    def __initialize_dataset(self,
        base_group: h5py.Group,
        ds_name: str,
        layout: t.Dict[str, t.Sequence[t.Any]],
        param_order: t.Sequence[str],
        data=None
    ) -> h5py.Dataset:
        dataset = create_dataset(
            base_group,
            ds_name,
            dimension_params=layout,
            param_order=param_order
        )

        # set up attributes
        label_dataset(dataset, param_order)

        # assign data
        if data is None:
            data = np.random.rand(*[len(layout[name]) for name in param_order])
        dataset[:] = data

        return dataset

    def test_merge_creates_the_target_dataset(self):
        # precondition
        target_ds_name = "__test_merged"
        self.assertNotIn(target_ds_name, self.hdf_file)

        # act
        self.merge_function(self.hdf_file, target_ds_name, 'runs', self.parts)

        # assert
        self.assertIn(target_ds_name, self.hdf_file)

    @parameterized.expand([
        (
            [{"runs": np.arange(3), "foo": np.arange(5), "bar": np.arange(2)}] * 3,
            "runs",
            (3*3, 5, 2),
            "identical size parts"
        ),
        (
            [
                {"runs": np.arange(3), "foo": np.arange(5), "bar": np.arange(2)},
                {"runs": np.arange(2), "foo": np.arange(5), "bar": np.arange(2)},
                {"runs": np.arange(4), "foo": np.arange(5), "bar": np.arange(2)},
            ],
            "runs",
            (3+2+4, 5, 2),
            "variable size parts"
        ),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result_contains_all_parts_in_order_of_appearance(self, part_layouts, merged_dim, expected_shape, desc):
        # arrange
        _parts = [
            self.__initialize_dataset(self.hdf_file, f"_test_part_{i}", layout, list(layout.keys()))
            for i, layout
            in enumerate(part_layouts)
        ]

        # act
        merged_dataset = self.merge_function(self.hdf_file, '_merged', merged_dim, _parts)

        # assert
        self.assertSequenceEqual(merged_dataset.shape, expected_shape)
        expected_data = np.concatenate([x[:] for x in _parts], axis=0)
        self.assertTrue(np.allclose(merged_dataset[:], expected_data))


    def test_runs_attribute_is_updated_on_merge(self):
        # act
        ds = self.merge_function(self.hdf_file, "__merged", 'runs', self.parts)

        # assert
        self.assertEqual(ds.attrs['param.runs'], sum(part.attrs['param.runs'] for part in self.parts))

    def test_result_dims_are_labelled_identically_to_parts(self):
        # act
        merged = self.merge_function(self.hdf_file, "__merged", 'runs', self.parts)

        # assert
        self.assertTrue(all(
            [dim.label for dim in merged.dims] == [dim.label for dim in part.dims]
            for part
            in self.parts
        ))

    def test_attributes_from_first_part_are_attached_to_the_merged_dataset(self):
        # arrange
        # deliberately remove one key from part #2
        new_key = "new_test_key"
        new_key_attr = f'{FIXED_PARAM_ATTR_PREFIX}.{new_key}'
        self.parts[1].attrs[new_key_attr] = 42.0

        # act
        ds = self.merge_function(self.hdf_file, "__merged", 'foo', self.parts)

        copied_attributes = {k: v for k, v in ds.attrs.items() if k.startswith(FIXED_PARAM_ATTR_PREFIX)}
        self.assertGreater(len(copied_attributes), 0)
        self.assertDictEqual(
            copied_attributes,
            {k: v for k, v in self.parts[0].attrs.items() if k.startswith(FIXED_PARAM_ATTR_PREFIX)}
        )
        # key present only in part 2 will not be present in the merged attributes because they're
        # copied entirely from part 1
        self.assertNotIn(new_key_attr, ds.attrs)

    def test_raises_when_non_merged_dimension_varies_in_size(self):
        # arrange
        _parts = [
            self.__initialize_dataset(
                base_group=self.hdf_file,
                ds_name=f'__part_{i}',
                layout=layout,
                param_order=list(layout.keys())
            )
            for i, layout
            in enumerate([
                {"runs": np.arange(3), "foo": np.arange(5), "bar": np.arange(2)},
                {"runs": np.arange(3), "foo": np.arange(3), "bar": np.arange(2)}
            ])
        ]

        with self.assertRaises(Exception):
            self.merge_function(self.hdf_file, "__merged", "runs", _parts)

    def test_merged_archive_is_virtual_only_on_virtual_merge(self):
        merged_ds = self.merge_function(self.hdf_file, 'test', 'runs', self.parts)

        if self.merge_type == "real":
            self.assertFalse(merged_ds.is_virtual)
        elif self.merge_type == "virtual":
            self.assertTrue(merged_ds.is_virtual)
        else:
            raise NotImplementedError(f"Didn't implement case for merge_type=='{self.merge_type}'")


class TestAdjustChunksize(TestCase):

    @parameterized.expand([
        ((224,), (3, 5, 224), (1, 1, 224), "1D chunk in 3D dataset"),
        ((3, 224), (3, 5, 224), (1, 3, 224), "2D chunk in 3D dataset"),
        ((2, 3, 224), (3, 5, 224), (2, 3, 224), "3D chunk in 3D dataset"),

        ((3, 2, 4, 224), (3, 5, 224), (2, 4, 224), "extra dims are trimmed"),

        ((8, 224), (3, 5, 224), (1, 5, 224), "chunksize values exceeding shape are reduced to dataset dim sizes"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result(self, user_chunksize, arx_shape, expected_result, desc):
        self.assertSequenceEqual(
            adjust_chunksize(user_chunksize, arx_shape),
            expected_result
        )