from unittest import TestCase, mock, skip
from parameterized import parameterized

import numpy as np
import h5py
from hirise_tools.voronoi.hdf import (
    create_attributes,
    create_dataset,
    create_scales,
    extract_dimension_scales,
    initialize_result_dataset,
    label_dataset,
    open_hdf_archives,
)
from hirise_tools.voronoi.param_def import LIMITED_DIM_RANGE_PARAMS


class HDFArchiveFixture():

    def setUp(self) -> None:
        self.hdf_file = h5py.File('hdf5testfile', driver='core', backing_store=False, mode='w')

    def tearDown(self) -> None:
        self.hdf_file.close()


class TestCreateScales(TestCase, HDFArchiveFixture):

    def setUp(self) -> None:
        HDFArchiveFixture.setUp(self)

    def tearDown(self) -> None:
        HDFArchiveFixture.tearDown(self)

    def test_scales_group_is_created_in_base_group(self):
        # precondition
        self.assertNotIn('_scales', self.hdf_file)

        # act
        test_params = {"quux": [41, 42]}
        scales_group = create_scales(self.hdf_file, test_params)

        # assert
        self.assertIn('_scales', self.hdf_file)
        self.assertEqual(scales_group, self.hdf_file['_scales'])

    def test_create_many_scales(self):
        test_params = {
            "quux": [41, 42],
            "bar": (332, 11),
            "foo": np.asarray([.5, .6])
        }

        # act
        scales_group = create_scales(self.hdf_file, test_params)

        # assert
        self.assertTrue(
            all(
                (
                    param_name in scales_group
                    and np.allclose(param_values, scales_group[param_name])
                )
                for param_name, param_values
                in test_params.items()
            )
        )

    def test_vlen_string_scale_is_stored_as_bytes(self):
        param_name, param_values = "foo", ("bar", 'quux', 'ft')

        # act
        scales_group = create_scales(self.hdf_file, scale_params={param_name: param_values})

        # assert
        scale_values = scales_group['foo'][:]
        self.assertIsInstance(scale_values, np.ndarray)
        self.assertTrue(all(isinstance(param_value, bytes) for param_value in scale_values))
        self.assertSequenceEqual([x.decode('utf-8') for x in scale_values], param_values)

    @parameterized.expand([
        ('a', [1,2,3], (3,), 'single param - single dim'),
        ('a+b', [(1,2), (2,3), (3,4)], (3,2), 'two params - two dims'),
        ('a+b+c', [(1,2,3), (2,3,4), (3,4,5)], (3,3), 'three params - three dims'),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_multidim_scale_creation(self, param_name, param_values, expected_shape, desc):
        # act
        scale_group = create_scales(self.hdf_file, scale_params={param_name: param_values})

        self.assertEqual(scale_group[param_name].shape, expected_shape)

    @parameterized.expand([
        ('a+b', [1,2,3], 'two params in name but only one in values'),
        ('a_b', [(1,2), (3,4), (5,6)], "param name doesn't contain plus"),
        ('a+b', [(1,2,3), (3,4,5), (5,6,7), (6,7,8)], "two params in name but three in values"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_multidim_scale_creation_raises_when(self, param_name, param_values, desc):
        with self.assertRaises(ValueError):
            create_scales(self.hdf_file, scale_params={param_name: param_values})


    @parameterized.expand([
        ('a+b', [(1, .5), (3, .4), (6, .2)], (float, float), 'numerical values are cast to the most generic type '),
        ('a+b', [('a', 1), ('b', 2), ('c', 3)], (bytes, bytes), 'if one of params is string, all are converted to bytestrings')
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_type_conversion_in_multidim_scale(self, param_name, param_values, expected_types, desc):
        # act
        scale_group = create_scales(self.hdf_file, scale_params={param_name: param_values})

        self.assertIsInstance(scale_group[param_name][:], np.ndarray)
        self.assertTrue(all(
            all(isinstance(pv, expected_type) for pv in values)
            for values, expected_type
            in zip(scale_group[param_name][:], expected_types)
        ))

    def test_non_sequence_param_cannot_be_used_to_create_a_scale(self):
        params = {"param": 7}
        with self.assertRaises(TypeError):
            create_scales(self.hdf_file, scale_params=params)


class TestCreateDataset(TestCase, HDFArchiveFixture):
    DEFAULT_DTYPE = np.float32
    DEFAULT_FILLVALUE = np.nan

    def setUp(self) -> None:
        HDFArchiveFixture.setUp(self)

        self.PARAM_DEFAULTS = dict(
            dtype=TestCreateDataset.DEFAULT_DTYPE,
            fillvalue=TestCreateDataset.DEFAULT_FILLVALUE,
        )

    def tearDown(self) -> None:
        HDFArchiveFixture.tearDown(self)

    @parameterized.expand([
        (None, None, DEFAULT_DTYPE, DEFAULT_FILLVALUE, "test default args"),
        (np.int64, 0, np.int64, 0, "test custom args"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_default_call(self, dtype, fillvalue, expected_dtype, expected_fillvalue, desc):
        hdf_group_mock = mock.Mock()

        dataset_name = "NAME"
        dim_params = {"a": [2, 3], "b": 5}
        param_order = ['b', 'a']

        expected_shape = [5, 2]
        expected_maxshape = [None, None]

        call_kwargs = dict(
            group=hdf_group_mock,
            name=dataset_name,
            dimension_params=dim_params,
            param_order=param_order,
        )
        # if param value is None, then don't include it in kwargs
        call_kwargs.update({
            k: v
            for k, v
            in (("dtype", dtype), ("fillvalue", fillvalue))
            if v is not None
        })

        # act
        create_dataset(**call_kwargs)

        # assert
        expected_kwargs = dict(
            name=dataset_name,
            shape=expected_shape,
            dtype=expected_dtype,
            fillvalue=expected_fillvalue,
            maxshape=expected_maxshape
        )
        hdf_group_mock.create_dataset.assert_called_once_with(**expected_kwargs)

    def test_compliance_with_param_order(self):
        params = {
            "a": 3,
            "b": 4,
            "c": 6,
            "d": 2,
        }
        param_order = ["c", "b", "d", "a"]
        expected_shape = [6, 4, 2, 3]

        # act
        group_mock = mock.Mock()
        create_dataset(group_mock, name="foo", dimension_params=params, param_order=param_order)

        # assert
        expected_kwargs = dict(
            name="foo",
            shape=expected_shape,
            maxshape=[None for _ in expected_shape],
            **self.PARAM_DEFAULTS
        )
        group_mock.create_dataset.assert_called_once_with(**expected_kwargs)

    @parameterized.expand([
        ([3, 4, 1], 3, "list "),
        ((3, 4, 1), 3, "tuple "),
        (np.asarray([3, 4, 1]), 3, "1d ndarray"),
        (np.asarray([[3, 4, 1], [1, 2, 3]]), 2, "2D ndarray"),
        (5, 5, "single integer value"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shape_inference(self, param_value, expected_dim, desc):
        params = {"param": param_value}
        param_order = list(params.keys())
        group_mock = mock.Mock()

        # act
        create_dataset(group_mock, name="foo", dimension_params=params, param_order=param_order)

        # assert
        expected_kwargs = dict(
            name="foo",
            shape=[expected_dim],
            maxshape=[None],
            **self.PARAM_DEFAULTS
        )
        group_mock.create_dataset.assert_called_once_with(**expected_kwargs)

    @parameterized.expand([
        ({'a': 7}, [None], "generic param (single int value) creates unlimited dimensions"),
        ({'a': [3, 2]}, [None], "generic param (sequence of values) creates unlimited dimensions"),
        *(
            ({param_name: 7}, [7], f"listed param name: {param_name} creates a limited dimension")
            for param_name
            in LIMITED_DIM_RANGE_PARAMS
        ),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_maxshape_rules(self, params, expected_maxshape, desc):
        param_order = list(params.keys())
        group_mock = mock.Mock()

        # act
        create_dataset(group_mock, name="foo", dimension_params=params, param_order=param_order)

        # assert
        args, kwargs = group_mock.create_dataset.call_args_list[0]
        self.assertEqual(kwargs['maxshape'], expected_maxshape)


class TestLabelDataset(TestCase, HDFArchiveFixture):

    def setUp(self) -> None:
        HDFArchiveFixture.setUp(self)

        self.test_params = {
            "foo": [1, 2],
            "quux": [3,4,5],
            "bar": [3,4,5,7],
            "baz": 5,
            "extra": 7,
        }
        self.test_scale_params = {k: v for k, v in self.test_params.items() if isinstance(v, (tuple, list, np.ndarray))}
        self.test_params_without_scale = {k: v for k, v in self.test_params.items() if k not in self.test_scale_params}

        # note that params have been reordered
        self.test_param_order = ["foo", "bar", "quux", "extra", "baz"]
        self.test_scales_group = create_scales(self.hdf_file, self.test_scale_params)
        self.test_dataset = create_dataset(
            self.hdf_file,
            name="test_dataset",
            dimension_params=self.test_params,
            param_order=self.test_param_order,
        )

    def tearDown(self) -> None:
        HDFArchiveFixture.tearDown(self)

    @staticmethod
    def gather_labels(dataset):
        return [dim.label for dim in dataset.dims]

    def test_correctness_of_labelling(self):
        label_dataset(
            self.test_dataset,
            param_order=self.test_param_order,
        )

        self.assertSequenceEqual(self.gather_labels(self.test_dataset), self.test_param_order)

    @skip("Scales are no longer being attached to datasets due to 64K size limit for attributes (and therefore scales)")
    def test_scales_are_attached(self):
        label_dataset(
            self.test_dataset,
            scales_group=self.test_scales_group,
            param_order=self.test_param_order,
            params_without_scale=tuple(self.test_params_without_scale.keys())
        )

        # assert
        scales_checked = 0
        for i, dim in enumerate(self.test_dataset.dims):
            if dim.label in self.test_scales_group:
                scales_checked += 1
                self.assertEqual(len(dim), 1, msg="Expected only one dimension to be attached to this dim") # only one scale attached
                first_scale_of_this_dim = dim[0]
                self.assertTrue(np.allclose(
                    first_scale_of_this_dim[:],
                    self.test_scales_group[dim.label][:]
                ))

        self.assertNotEqual(scales_checked, 0, msg="Expected at least one scale to be attached to the test dataset")


    def test_raises_if_provided_with_param_not_present_in_scales(self):
        # act & assert
        with self.assertRaises(ValueError):
            label_dataset(
                self.test_dataset,
                param_order=self.test_param_order + ["novel_param"],
            )

    def test_raises_if_dataset_ndim_doesnt_match_the_len_of_param_order(self):
        params = {
            'sequence_param': [10, 20],
            'range_param': 7,
            'range_param_2': 3,
        }
        scale_params = {k:v for k, v in params.items() if isinstance(v, (tuple, list, np.ndarray))}
        param_order = ['range_param', 'sequence_param', 'range_param_2']

        dataset = create_dataset(
            self.hdf_file,
            name="test2_dataset",
            dimension_params=params,
            param_order=param_order,
        )
        scales = create_scales(self.hdf_file, scale_params=scale_params)

        # act & assert
        with self.assertRaises(ValueError):
            label_dataset(
                dataset,
                param_order=['sequence_param', 'range_param_2'], # notice that 'range_param' is missing
            )


class TestCreateAttributes(TestCase, HDFArchiveFixture):

    DEFAULT_TEST_PARAMS = {'a': 42, 'b': (12, 30), 'c': 'foo'}

    def setUp(self) -> None:
        HDFArchiveFixture.setUp(self)

        self.test_dataset = self.hdf_file.create_dataset(name='foo', shape=(10, 20))

    def tearDown(self) -> None:
        HDFArchiveFixture.tearDown(self)

    def test_attr_names_are_prefixed_with_param(self):
        hdf_object = mock.Mock()
        params = self.DEFAULT_TEST_PARAMS
        expected_calls = [mock.call(f"param.{name}", data=value) for name, value in params.items()]

        # act
        create_attributes(hdf_object, params, {})

        # assert
        self.assertEqual(len(hdf_object.attrs.create.call_args_list), len(params))
        hdf_object.attrs.create.assert_has_calls(expected_calls)


    def test_all_provided_params_are_added_as_attrs(self):
        fixed_params = {'fixed1': 2, 'fixed2': 3}
        range_params = {'range1': 5, 'range2': 6}
        other_params = {'other1': 1, 'other2': 8}
        hdf_object = mock.Mock()

        # act
        create_attributes(
            hdf_object,
            fixed_params=fixed_params,
            range_params=range_params,
            **other_params
        )

        # assert
        for name, value in {**fixed_params, **range_params, **other_params}.items():
            hdf_object.attrs.create.assert_any_call(f'param.{name}', data=value)


class TestInitializeArchive(TestCase, HDFArchiveFixture):

    def setUp(self) -> None:
        HDFArchiveFixture.setUp(self)
        self.test_grouped_params = {
            'cartesian': {
                "cartesianA": [9, 16, 25, 36, 64],
                "cartesianB": [0.25, 0.5, 0.75],
            },
            'coupled': {
                "coupledA+coupledB": [
                    ('/path/imgA', 2),
                    ('/path/imgB', 4),
                    ('/path/imgA', 3),
                ],
            },
            'range': {
                "rangeA": 7,
            },
            'fixed': {
                "fixedA": 4,
                "fixedB": 224,
            },
        }
        self.ungrouped_params = dict(sum(map(lambda x: list(x.items()), self.test_grouped_params.values()), []))
        self.test_param_order = ['rangeA', 'coupledA+coupledB', 'cartesianA', 'cartesianB', 'fixedB']

        # act
        self.test_dataset = initialize_result_dataset(
            self.hdf_file,
            grouped_params=self.test_grouped_params,
            dim_params_order=self.test_param_order,
            dataset_name='test_dataset'
        )

    def tearDown(self) -> None:
        HDFArchiveFixture.tearDown(self)

    def test_scales_consist_of_cartesian_and_coupled_params(self):
        scales = self.hdf_file['_scales'].items()
        expected_scale_params = {
            **self.test_grouped_params['cartesian'],
            **self.test_grouped_params['coupled'],
        }

        # assert
        self.assertDictEqual(
            {k: len(v) for k, v in scales},
            {k: len(v) for k, v in expected_scale_params.items()}
        )

    def test_dataset_is_labelled_according_to_param_order(self):
        self.assertSequenceEqual(
            self.test_param_order,
            [dim.label for dim in self.test_dataset.dims]
        )

    def test_dataset_dimensions(self):
        dataset_shape = self.test_dataset.shape
        expected_shape = [
            v if isinstance(v, int) else len(v)
            for v
            in [self.ungrouped_params[k] for k in self.test_param_order]
        ]

        # assert
        self.assertSequenceEqual(dataset_shape, expected_shape)

    def test_added_attributes_consist_of_fixed_and_range_params(self):
        fixed_params = self.test_grouped_params['fixed']
        range_params = self.test_grouped_params['range']

        inserted_attributes = {
            k.split('.', maxsplit=1)[-1]: v
            for k, v
            in self.test_dataset.attrs.items()
            if k.startswith('param.')
        }

        # assert
        self.assertDictEqual(
            inserted_attributes,
            {
                **fixed_params,
                **range_params,
            }
        )


class TestExtractDimensionScales(TestCase, HDFArchiveFixture):

    def setUp(self) -> None:
        HDFArchiveFixture.setUp(self)

    def tearDown(self) -> None:
        HDFArchiveFixture.tearDown(self)

    @parameterized.expand([
        (
            {'a': [10, 20], 'b': [3, 1, 2], 'steps': 7},
            ['b', 'a', 'steps'],
            False,
            {'b': [3, 1, 2], 'a': [10, 20]},
            "scale params only"
        ),
        (
            {'a': [10, 20], 'b': [3, 1, 2], 'steps': 7},
            ['b', 'a', 'steps'],
            True,
            {'b': [3, 1, 2], 'a': np.asarray([10, 20]), 'steps': np.arange(7)},
            "include range params"
        ),
    ], testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_scale_retrieval(self, params, param_order, include_fixed, expected, desc):
        scale_params = {k: v for k, v in params.items() if isinstance(v, (tuple, list, np.ndarray))}
        range_params = {k: v for k, v in params.items() if k not in scale_params}

        create_scales(self.hdf_file, scale_params)
        test_dataset = create_dataset(self.hdf_file, name='testds', dimension_params=params, param_order=param_order)
        create_attributes(test_dataset, fixed_params={}, range_params=range_params)
        label_dataset(test_dataset, param_order)

        # act
        scales = extract_dimension_scales(test_dataset, include_fixed=include_fixed)

        # assert
        self.assertSetEqual(set(scales.keys()), set(expected.keys()))
        for k, v in expected.items():
            self.assertTrue(np.allclose(scales[k], v))

    def test_raises_if_scales_group_not_present_in_dataset_parent_group(self):
        self.assertNotIn('_scales', self.hdf_file)

        # arrange
        test_ds = self.hdf_file.create_dataset(name='test', shape=(10, 20), fillvalue=np.nan)

        # act & assert
        with self.assertRaises(ValueError):
            extract_dimension_scales(test_ds)


class TestOpenTwoArchives(TestCase):
    # TODO: mock out h5py.File and validate params

    def setUp(self) -> None:
        self.hdf5_module_patcher = mock.patch('hirise_tools.voronoi.hdf.h5py')
        self.hdf5_module_mock = self.hdf5_module_patcher.start()

        # each call returns a different mock
        self.hdf5_module_mock.File.side_effect = [mock.Mock(), mock.Mock()]

    def tearDown(self) -> None:
        self.hdf5_module_patcher.stop()

    def test_source_file_is_open_in_SWMR_mode_if_both_paths_are_provided(self):
        maps_path = "maps_path"
        results_path = "results_path"

        # act
        with open_hdf_archives(results_path, maps_path):
            self.hdf5_module_mock.File.assert_any_call(maps_path, 'r', swmr=True)

    @parameterized.expand([
        (True, 'w-', 'create results if file does not exist'),
        (False, 'r+', 'require results file to exist'),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_opening_modes_when_maps_and_results_paths_differ(self, create_if_missing, expected_results_mode, desc):
        maps_path = "maps_path"
        results_path = "results_path"
        expected_calls = [
            mock.call(maps_path, 'r', swmr=True),
            mock.call(results_path, expected_results_mode)
        ]

        # act
        with open_hdf_archives(results_path, maps_path, create_if_missing) as (results_file, maps_file):
            self.hdf5_module_mock.File.assert_has_calls(expected_calls)
            self.assertNotEqual(results_file, maps_file)

    @parameterized.expand([
        ('common_path', 'common_path', 'maps and results point to the same file'),
        (None, 'results_path', 'maps path is omitted'),
        ('maps_path', None, 'results path is omitted'),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_open_in_read_write_mode_and_expect_file_to_exist_when(self, maps_path, results_path, desc):
        with open_hdf_archives(results_path, maps_path) as (results_file, maps_file):
            self.hdf5_module_mock.File.assert_called_once_with(maps_path or results_path, 'r+')
            self.assertEqual(results_file, maps_file)

    def test_raises_if_both_paths_are_none(self):
        with self.assertRaises(ValueError):
            with open_hdf_archives(None, None):
                pass