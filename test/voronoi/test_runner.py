from unittest import TestCase

import torch
from hirise_tools.voronoi.runner import VoronoiHDFMapGen, RiseHDFMapGen

class TestVoronoiHDFMapGen(TestCase):

    def setUp(self) -> None:
        self.paramset = {
            'renderer.fill_polygons': True,
            'renderer.outline': "halftone",
            'renderer.blur_sigma': 0.1,

            'meshgen.seedgen': "CheckerboardPatternSeeds",
            'gridgen.type': "perm",
            'gridgen.fixing_threshold': 0.0,

            'meshcount': 3,
            'polygons': 100,
            'p1': .1,

            'map_y': 224,
            'map_x': 224
        }

        self.device = torch.device("cpu")


    def test_each_mask_provider_uses_an_unique_mask_iterable(self):
        vmg = VoronoiHDFMapGen(
            device=self.device,
            batch_size=10,
            render_device=self.device,
            use_model_warmups=False
        )

        ps1 = {**self.paramset, **{"p1": .1}}
        ps2 = {**self.paramset, **{"p1": .8}}

        mask_provider_1 = vmg._create_mask_provider(ps1)
        mask_provider_2 = vmg._create_mask_provider(ps2)

        # act
        masks00 = mask_provider_1(5, ps1)
        masks10 = mask_provider_2(7, ps2)
        masks01 = mask_provider_1(3, ps1)

        # assert
        # check that parameters of masks obtained from each provider match their respective params
        self.assertTrue(all(.05 < m.mean() < .15  for m in torch.cat((masks00, masks01))))
        self.assertTrue(all(.7 < m.mean() < .9 for m in masks10))


class TestRISEHDFMapGen(TestCase):


    def setUp(self) -> None:
        self.paramset = {
            'renderer.fill_polygons': True,
            'renderer.outline': "halftone",
            'renderer.blur_sigma': 0.1,

            # 'meshgen.seedgen': "CheckerboardPatternSeeds",
            'gridgen.type': "perm",
            'gridgen.fixing_threshold': 0.0,

            's': 10,
            'p1': .1,

            'map_y': 224,
            'map_x': 224
        }

        self.device = torch.device("cpu")


    def test_each_mask_provider_uses_an_unique_mask_iterable(self):
        vmg = RiseHDFMapGen(
            device=self.device,
            batch_size=10,
            n_threads=1,
            use_model_warmups=False
        )

        ps1 = {**self.paramset, **{"p1": .1}}
        ps2 = {**self.paramset, **{"p1": .8}}

        mask_provider_1 = vmg._create_mask_provider(ps1)
        mask_provider_2 = vmg._create_mask_provider(ps2)

        # act
        masks00 = mask_provider_1(5, ps1)
        masks10 = mask_provider_2(7, ps2)
        masks01 = mask_provider_1(3, ps1)

        # assert
        # check that parameters of masks obtained from each provider match their respective params
        self.assertTrue(all(.05 < m.mean() < .15  for m in torch.cat((masks00, masks01))))
        self.assertTrue(all(.7 < m.mean() < .9 for m in masks10))
