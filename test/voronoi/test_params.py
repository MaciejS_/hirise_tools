import copy
from unittest import TestCase

import numpy as np
# from hirise_tools.iterative.persistence import apply_to_leaves
from parameterized import parameterized
from hirise_tools.voronoi.params import decouple_params, couple_params, gather_params_from_config, remove_params


class TestParamDecoupling(TestCase):

    def setUp(self) -> None:
        self.coupled_params = {
            'a+b': [(1, 'a'), (1, 'b'), (2, 'a')],
            'e+c+d': [('E', 'A', 'B'), ('F', 'A', None)],
        }

        self.default_expected = {
            "a": (1, 1, 2),
            "b": ('a', 'b', 'a'),
            "c": ('A', 'A'),
            "d": ('B', None),
            "e": ('E', 'F')
        }

    def test_result_for_coupled_only(self):
        decoupled = decouple_params(self.coupled_params)
        self.assertDictEqual(decoupled, self.default_expected)

    @parameterized.expand([
        (
            {'X': ['YY', 'ZZ', 'XX'], 'Y': ['R', 'V']},
            "already uncoupled"
        ),
        (
            {'X': 'YY', 'Z': None},
            "fixed"
        )
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]} params")
    def test_decoupling_doesnt_affect_non_coupled_params(self, extra_params, desc):
        expected_result = {
            **self.default_expected,
            **extra_params  # they are used as-is because they're already decoupled
        }

        decoupled = decouple_params({**extra_params, **self.coupled_params})
        self.assertDictEqual(decoupled, expected_result)


class TestParameterCoupling(TestCase):

    def setUp(self) -> None:
        self.uncoupled_params = {
            "a": (1, 1, 2),
            "b": ('a', 'b', 'a'),
            "c": ('A', 'A'),
            "d": ('B', None),
            "e": ('E', 'F')
        }

        self.coupling = ['a+b', 'e+c+d']

        self.default_expected = {
            'a+b': ((1, 'a'), (1, 'b'), (2, 'a')),
            'e+c+d': (('E', 'A', 'B'), ('F', 'A', None)),
        }

    def test_success(self):
        result = couple_params(self.uncoupled_params, self.coupling)
        self.assertDictEqual(result, self.default_expected)

    @parameterized.expand([
        (
            {'X+Y': [('YY', 'R'), ('XX', 'V')]},
            "already coupled"
        ),
        (
            {'z': ['AA', 'CC', 'BB'], 'x': ['CC', 'FF', 'EH']},
            "not listed in coupling"
        ),
        (
            {'X': 'YY', 'Z': None},
            "fixed"
        )
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]} params")
    def test_params_not_listed_in_coupling_are_included_in_results_as_is(self, extra_params, desc):
        expected_result = {
            **self.default_expected,
            **extra_params  # they are used as-is because they're already decoupled
        }

        coupled = couple_params({**extra_params, **self.uncoupled_params}, couplings=self.coupling)
        self.assertDictEqual(coupled, expected_result)


class TestParamRemoval(TestCase):

    def test_removal_when_uncoupled(self):
        removed_param_name = ["pX", "pZ", "pY"]
        grouped_params = {
            "groupA": {
                "pY": [1, 2, 3],
                "pA": [2, 3],
                "pX": ['a', 'b']
            },
            "groupB": {
                "pB": 2,
                "pC": 12,
                "pZ": 42,
            }
        }
        param_order = ["pB", "pY", "pA", "pZ"]

        expected_param_dict = copy.deepcopy(grouped_params)
        expected_param_dict['groupA'].pop('pX')
        expected_param_dict['groupA'].pop('pY')
        expected_param_dict['groupB'].pop('pZ')

        expected_param_order = ["pB", "pA"]

        # act
        result_params, result_order = remove_params(grouped_params, param_order, removed_param_name)

        # assert
        self.assertDictEqual(result_params, expected_param_dict)
        self.assertSequenceEqual(result_order, expected_param_order)

    @parameterized.expand([
        (
            ['a'],
            {
                'coupled': {
                    'a+b+c': np.asarray([[1, 2, 3], [2, 3, 4], [3, 4, 5]]),
                    'x+z': np.asarray([[9, 8], [7,6]]),
                }
            },
            ['a+b+c', 'x+z'],
            {
                'coupled': {
                    'b+c': [[2, 3], [3, 4], [4, 5]],
                    'x+z': [[9, 8], [7,6]],
                }
            },
            ['b+c', 'x+z'],
            "remove single param from one coupling",
        ),
        (
            ['a', 'z'],
            {
                'coupled': {
                    'a+b+c': np.asarray([[1, 2, 3], [2, 3, 4], [3, 4, 5]]),
                    'x+z': np.asarray([[9, 8], [7,6]]),
                }
            },
            ['a+b+c', 'x+z'],
            {
                'coupled': {
                    'b+c': [[2, 3], [3, 4], [4, 5]],
                    'x': [[9], [7]],
                }
            },
            ['b+c', 'x'],
            "remove multiple params from their couplings",
        ),
        (
            ['a', 'c'],
            {
                'coupled': {
                    'a+b+c': np.asarray([[1, 2, 3], [2, 3, 4], [3, 4, 5]]),
                    'x+z': np.asarray([[9, 8], [7,6]]),
                }
            },
            ['a+b+c', 'x+z'],
            {
                'coupled': {
                    # TODO: 'b' in this situation should/could be moved to 'cartesian' params
                    'b': [[2], [3], [4]],
                    'x+z': [[9, 8], [7,6]],
                }
            },
            ['b', 'x+z'],
            "remove multiple params from single coupling",
        ),
        (
            ['a', 'b', 'c'],
            {
                'coupled': {
                    'a+b+c': np.asarray([[1, 2, 3], [2, 3, 4], [3, 4, 5]]),
                    'x+z': np.asarray([[9, 8], [7, 6]]),
                }
            },
            ['a+b+c', 'x+z'],
            {
                'coupled': {
                    'x+z': [[9, 8], [7, 6]],
                }
            },
            ['x+z'],
            "remove all params from single coupling",
        ),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_removal_from_coupling(self, to_remove, grouped_params, param_order, expected_params, expected_order, desc):
        # act
        result_params, result_order = remove_params(grouped_params, param_order, to_remove)

        # assert
        result_params = {
            param_group: {
                param_name: v.tolist() if isinstance(v, np.ndarray) else v
                for param_name, v
                in param_group_values.items()
            }
            for param_group, param_group_values
            in result_params.items()
        }
        self.assertDictEqual(result_params, expected_params)

        self.assertSequenceEqual(result_order, expected_order)


    def test_raises_when_names_on_list_of_params_to_remove_contain_plus_signs(self):
        params = {'coupled': {'a+b+c': [[1,2,3], [2,3,4]]}}
        param_order = ["a+b+c"]
        to_remove = ['b+c']

        with self.assertRaises(ValueError):
            remove_params(params, param_order, to_remove)


class TestGatheringParamsFromConfig(TestCase):

    def test_grouping_multiple_params(self):
        params = {
            "cartA": [1, 2, 3],
            "cartB": (1, 2),
            "cou+pled": [[1,2], [3,4]],
            "fixedA": 7,
            "steps": 5,
        }
        expected_result = {
            'cartesian': {
                "cartA": [1, 2, 3],
                "cartB": (1, 2),
            },
            'coupled': {"cou+pled": [[1,2], [3,4]]},
            'fixed': {'fixedA': 7},
            'range': {'steps': 5}
        }

        # act & assert
        self.assertDictEqual(expected_result, gather_params_from_config(params))

    def test_always_groups_into_four_categories(self):
        result = gather_params_from_config({})

        expected_groups = {'cartesian', 'coupled', 'range', 'fixed'}
        self.assertSetEqual(set(result.keys()), expected_groups)

    @parameterized.expand([
        ({"foo": [1, 2, 3]}, 'cartesian', 'if value is list'),
        ({"foo": (1, 2, 3)}, 'cartesian', 'if value is tuple'),
        ({"foo": np.asarray([1, 2, 3])}, 'cartesian', 'if value is ndarray'),
        ({"a+b": [[1, 2, 3], [3, 4, 5]]}, 'coupled', "if there's a plus in the name, regardless of value"),
        ({"runs": 5}, 'range', "if param name on whitelist"),
        ({"foo": 7}, 'fixed', "if value is a scalar"),
    ],
        testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-2]} - {p.args[-1]}")
    def test_specific_group(self, params, expected_group, desc):
        grouped_params = gather_params_from_config(params)

        # assert
        self.assertDictEqual(grouped_params[expected_group], params)
        self.assertTrue(all(len(v) == 0 for k, v in grouped_params.items() if k != expected_group))

    @parameterized.expand([
        "runs", "steps", "map_y", "map_x"
    ],
        testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_range_params_names_whitelist(self, param_name):
        params = {param_name: 7}

        # act
        grouped_params = gather_params_from_config(params)

        # assert
        self.assertDictEqual(grouped_params['range'], params)
        self.assertTrue(all(len(v) == 0 for k, v in grouped_params.items() if k != 'range'))
