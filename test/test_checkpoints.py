from unittest.case import TestCase

from hirise_tools.checkpoints import npow10


class TestNPow10(TestCase):

    def test_sequence_generated(self):
        expected_values = [1, 2, 3, 4, 5, 6, 7, 8, 9,
                           10, 20, 30, 40, 50, 60, 70, 80, 90,
                           100, 200, 300, 400, 500, 600, 700, 800, 900,
                           1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000,
                           10000]

        selected_values = list(filter(npow10, range(1, 10**4+1)))

        self.assertListEqual(expected_values, selected_values)

    def test_raises_math_domain_error_on_zero(self):
        with self.assertRaises(ValueError):
            npow10(0)