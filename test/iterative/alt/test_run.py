from unittest import TestCase, mock
from unittest.mock import Mock

import torch
import numpy as np
from hirise_tools.iterative.alt.run import run_games_on_maps_tensor, run_game_for_paramsets, game_for_v2

class CommonRunnerTests:

    def test_result_contains_the_same_keys_as_games_dict(self):
        self.assertSetEqual(set(self.game_mocks.keys()), set(self.results.keys()))

class TestRunGameOnMapsTensor(CommonRunnerTests,
                                TestCase):
    def setUp(self):
        self.game_1_steps = (224, )
        self.game_2_steps = (448, )
        self.single_game_result_shape = {"game1": self.game_1_steps,
                                         "game2": self.game_2_steps}

        self.dummy_img = torch.rand(3, 10, 10)
        self.dummy_maps_tensor = torch.rand(5, 4, 10, 10)
        self.maps_tensor_hypershape = self.dummy_maps_tensor.shape[:-2]

        self.dummy_game_results = {"game1": torch.rand(np.prod(self.maps_tensor_hypershape), *self.game_1_steps),
                                   "game2": torch.rand(np.prod(self.maps_tensor_hypershape), *self.game_2_steps)}

        self.game1_result_iter = iter(self.dummy_game_results["game1"])
        self.game2_result_iter = iter(self.dummy_game_results["game2"])

        self.game_1_mock = Mock(side_effect=lambda img, smap, observed_class: next(self.game1_result_iter))
        self.game_2_mock = Mock(side_effect=lambda img, smap, observed_class: next(self.game2_result_iter))
        self.game_mocks = {"game1": self.game_1_mock,
                           "game2": self.game_2_mock}

        self.results = run_games_on_maps_tensor(maps_tensor=self.dummy_maps_tensor,
                                                img=self.dummy_img,
                                                observed_class=0,
                                                games=self.game_mocks)


    def test_values_of_results_dict_are_tensors(self):
        self.assertTrue(all(isinstance(result, torch.Tensor) for game, result in self.results.items()))

    def test_results_are_shaped_like_maps_tensor_hypershape(self):
        for game_name, game_results in self.results.items():
            self.assertSequenceEqual(game_results.shape,
                                     (*self.maps_tensor_hypershape, *self.single_game_result_shape[game_name]),
                                     msg="Shape differs from expected")

    def test_results_are_ordered_according_to_ndindex(self):
        for game_name, game_results in self.results.items():
            for idx, ndidx in enumerate(np.ndindex(self.maps_tensor_hypershape)):
                self.assertTrue(torch.allclose(game_results[ndidx], self.dummy_game_results[game_name][idx]))


class TestRunGameForParamsets(CommonRunnerTests,
                              TestCase):

    @mock.patch('hirise_tools.iterative.alt.run.run_games_on_maps_tensor')
    def setUp(self, game_runner_mock) -> None:
        self.game_mocks = {"game1": Mock(), "game2": Mock()}
        self.n_paramsets = 3
        self.dummy_paramsets = [{"img": i * torch.ones(3, 10, 10),
                                 "class": i}
                                for i
                                in range(self.n_paramsets)]


        self.dummy_maps = [torch.rand(5, 4, 10, 10) for _ in range(self.n_paramsets)]
        self.dummy_game_results = {k: [torch.rand(*m.shape[-2:], 224) for m in self.dummy_maps] for k in self.game_mocks}
        self.dummy_game_result_iterators = {k: iter(self.dummy_game_results[k]) for k in self.game_mocks}

        game_runner_mock.side_effect = lambda maps_tensor, img, observed_class, games: {k: next(self.dummy_game_result_iterators[k]) for k in games}
        self.game_runner_mock = game_runner_mock

        self.results = run_game_for_paramsets(self.dummy_maps,
                                              self.dummy_paramsets,
                                              self.game_mocks)


    def test_result_contains_the_same_keys_as_games_dict(self):
        self.assertSetEqual(set(self.game_mocks.keys()), set(self.results.keys()))

    def test_values_of_results_dict_are_lists_corresponding_to_paramsets(self):
        self.assertTrue(all(isinstance(result, list) for game, result in self.results.items()))
        self.assertTrue(all(len(result) == len(self.dummy_paramsets) for result in self.results.values()))

    def test_all_results_under_given_game_key_are_results_of_this_game(self):
        for game_name, paramset_results in self.results.items():
            for actual_result, expected_result in zip(paramset_results, self.dummy_game_results[game_name]):
                self.assertTrue(torch.allclose(actual_result, expected_result))

    def test_mock_called_once_for_each_paramset(self):
        self.assertEqual(self.game_runner_mock.call_count, len(self.dummy_paramsets))

    def test_call_args(self):
        mock_call_args = [x[1] for x in self.game_runner_mock.call_args_list]
        for call_args, paramset, maps_tensor in zip(mock_call_args, self.dummy_paramsets, self.dummy_maps):
            self.assertTrue(torch.allclose(call_args['maps_tensor'], maps_tensor))
            self.assertTrue(torch.allclose(call_args['img'], paramset['img']))
            self.assertEqual(call_args['observed_class'], paramset['class'])
            self.assertDictEqual(call_args['games'], self.game_mocks)


class TestGameForV2(CommonRunnerTests,
                    TestCase):

    @mock.patch('hirise_tools.iterative.alt.run.v2_full_param_set')
    @mock.patch('hirise_tools.iterative.alt.run.run_game_for_paramsets')
    def setUp(self, runner_mock, parambuilder_mock) -> None:
        self.n_iterations = 2
        self.dummy_paramsets = [Mock() for _ in range(self.n_iterations)]
        self.map_mocks = [Mock() for _ in self.dummy_paramsets]

        self.parambuilder_mock = parambuilder_mock
        self.parambuilder_mock.return_value = self.dummy_paramsets

        self.game_mocks = {"game1": Mock(), "game2": Mock()}

        self.dummy_runner_results = [{k: Mock() for k in self.game_mocks}
                                     for _ in range(self.n_iterations)]

        dummy_results_iterator = iter(self.dummy_runner_results)
        self.runner_mock = runner_mock
        self.runner_mock.side_effect = lambda maps, paramsets, leave_tqdm, games: next(dummy_results_iterator)

        self.dummy_archive = {"maps": self.map_mocks,
                              'params': {'iteration': Mock(),
                                         'variable': Mock(),
                                         'order': Mock(),
                                         'fixed': Mock()}}

        self.results = game_for_v2(archive=self.dummy_archive,
                                   games=self.game_mocks)

    def test_runner_called_once_for_each_iteration(self):
        self.assertEqual(self.runner_mock.call_count, self.n_iterations)

    def test_runner_called_with_each_maps_params_pair(self):
        runner_call_args = [x[1] for x in self.runner_mock.call_args_list]
        for call_args, expected_map_set, expected_paramset in zip(runner_call_args, self.map_mocks, self.dummy_paramsets):
            self.assertEqual(call_args['maps'], expected_map_set)
            self.assertEqual(call_args['paramsets'], expected_paramset)
            self.assertEqual(call_args['games'], self.game_mocks)

    def test_param_builder_arguments(self):
        call_args = self.parambuilder_mock.call_args[1]
        self.assertEqual(call_args['iteration_params'], self.dummy_archive['params']['iteration'])
        self.assertEqual(call_args['variable_params'], self.dummy_archive['params']['variable'])
        self.assertEqual(call_args['param_order'], self.dummy_archive['params']['order'])
        self.assertEqual(call_args['fixed_params'], self.dummy_archive['params']['fixed'])

    def test_results_dict_contains_lists_corresponding_to_iterations(self):
        self.assertTrue(all(len(v) == self.n_iterations for v in self.results.values()))
        for game_name, game_results_for_iteration in self.results.items():
            self.assertSequenceEqual(game_results_for_iteration, [e[game_name] for e in self.dummy_runner_results])