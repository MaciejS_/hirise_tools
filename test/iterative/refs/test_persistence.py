from datetime import datetime
from unittest.case import TestCase
from unittest.mock import Mock

from hirise_tools.iterative.refs.persistence import make_ref_archive_v1
from parameterized import parameterized


class R1ArchiveTestFixture:

    def setUp(self):
        self.dummy_source_data = {"files": [],
                                  "proper_class": [],
                                  "explained_class": []}

        self.dummy_ref_params = {"N_sets": dict(),
                                 "clusters": [],
                                 "order": [],
                                 "fixed": {}}

    @parameterized.expand([
        ("N_sets",),
        ("clusters",),
        ("fixed",),
        ("order",)
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_if_ref_params_is_missing_keys(self, key_to_remove):
        self.dummy_ref_params.pop(key_to_remove)
        with self.assertRaises(KeyError):
            make_ref_archive_v1(full_ref_params_dict=self.dummy_ref_params,
                                source_data=self.dummy_source_data,
                                all_results=[],
                                description=Mock(),
                                input_archive_file_name=Mock(),
                                start_date=datetime.now(),
                                param_index=None)

    def test_additional_keys_in_ref_params_are_not_allowed(self):
        self.dummy_ref_params["foo"] = 10
        with self.assertRaises(KeyError):
            make_ref_archive_v1(full_ref_params_dict=self.dummy_ref_params,
                                source_data=self.dummy_source_data,
                                all_results=[],
                                description=Mock(),
                                input_archive_file_name=Mock(),
                                start_date=datetime.now(),
                                param_index=None)


class TestR1ArchiveBuilder(R1ArchiveTestFixture,
                           TestCase):

    def setUp(self) -> None:
        R1ArchiveTestFixture.setUp(self)

    def test_no_validation_of_source_data_is_performed(self):
        empty_source_data_dict = {}
        make_ref_archive_v1(source_data=empty_source_data_dict,
                            all_results=[],
                            full_ref_params_dict=self.dummy_ref_params,
                            description=Mock(),
                            input_archive_file_name=Mock(),
                            start_date=datetime.now(),
                            param_index=None)


class TestR1MultiImgArchiveBuilder(R1ArchiveTestFixture,
                                   TestCase):

    def setUp(self) -> None:
        R1ArchiveTestFixture.setUp(self)

    @parameterized.expand([
        ("files",),
        ("proper_class",),
        ("explained_class",)
    ])
    def test_raises_if_source_data_is_missing_a_key(self, key_to_remove):
        self.dummy_source_data.pop(key_to_remove)
        make_ref_archive_v1(source_data=self.dummy_source_data,
                            all_results=[],
                            full_ref_params_dict=self.dummy_ref_params,
                            description=Mock(),
                            input_archive_file_name=Mock(),
                            start_date=datetime.now(),
                            param_index=None)