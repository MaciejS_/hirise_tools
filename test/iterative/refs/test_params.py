import random
from unittest import TestCase

from hirise_tools.iterative.params import paramset_to_indexing_tuple
from hirise_tools.iterative.refs.params import *


class TestDiscardIrrelevantParams(TestCase):

    def setUp(self):
        self.test_relevants = {"relevant_a", "param_r"}
        self.relevant_only_dict = {k: random.random() for k in self.test_relevants}
        self.irrelevant_only_dict = {"foo": ["a"], "bar": "y"}
        self.mixed_dict = {"relevant_a": ["xyz", 2], "quux": "c"}

    def test_contents_of_relevant_params_dict(self):
        self.assertSetEqual(set(RELEVANT_PARAMS), {"N", "s", "p1", "img", "class", "model"})

    def test_dict_of_relevant_params_is_unaltered(self):
        result = discard_irrelevant_params(self.relevant_only_dict, self.test_relevants)
        self.assertDictEqual(self.relevant_only_dict, result)

    def test_dict_of_irrelevant_params_is_emptied(self):
        filtered_dict = discard_irrelevant_params(self.irrelevant_only_dict, self.test_relevants)
        self.assertEqual(filtered_dict, dict())

    def test_params_not_on_relevants_list_are_discarded(self):
        filtered_dict = discard_irrelevant_params(self.mixed_dict, self.test_relevants)
        self.assertEqual(set(filtered_dict.keys()).difference(self.test_relevants), set())
        self.assertTrue(all(self.mixed_dict[k] == v for k, v in filtered_dict.items()))

class TestRefParamClustersFromV2(TestCase):

    def setUp(self):
        self.n_0 = [100, 500]
        self.n_1 = [400, 800]
        self.n_2 = [800, 1200]

        self.s_0 = [3, 5, 7]
        self.s_1 = [5, 9]
        self.s_2 = [7, 9, 13]

        self.iteration_params = {"N": [self.n_0, self.n_1, self.n_2],
                                 "s": [self.s_0, self.s_1, self.s_2]}
        self.variable_params = {"p1": (0.2, 0.3)}
        self.fixed_params = {"foo": ("a", "b")}

        self.param_order = ["N", "s", "p1"]

        self.result = ref_param_clusters_from_v2(iteration_params=self.iteration_params,
                                                 variable_params=self.variable_params,
                                                 param_order=self.param_order,
                                                 fixed_params=self.fixed_params)

        self.expected_N_value_sets = {500: {100, 500},
                                      1300: {500, 900, 1300},
                                      2500: {1300, 1700, 2100, 2500}}

        self.expected_param_clusters = [{"N": {100, 500},
                                         "s": self.s_0,
                                         "p1": self.variable_params["p1"]},

                                        {"N": {500, 900, 1300},
                                         "s": self.s_1,
                                         "p1": self.variable_params["p1"]},

                                        {"N": {1300, 1700, 2100, 2500},
                                         "s": self.s_2,
                                         "p1": self.variable_params["p1"]}]

        self.expected_result = {"N_sets": self.expected_N_value_sets,
                                "clusters": self.expected_param_clusters,
                                "order": self.param_order,
                                "fixed": self.fixed_params}

    def test_result_against_expected(self):
        self.assertEqual(self.result, self.expected_result)

    def test_raises_if_N_not_in_iteration_params(self):
        self.iteration_params.pop("N")

        with self.assertRaises(NotImplementedError):
            ref_param_clusters_from_v2(iteration_params=self.iteration_params,
                                       variable_params=self.variable_params,
                                       param_order=self.param_order,
                                       fixed_params=self.fixed_params)



class TestCreateParamsetsFromClusters(TestCase):

    def setUp(self) -> None:
        self.s_0 = (3, 5, 7)
        self.s_1 = (5, 9)
        self.s_2 = (7, 9, 13)
        self.variable_params = {"p1": (0.2, 0.3)}
        self.fixed_params = {"foo": (100, 200)}
        self.param_order = ["N", "s", "p1"]

        self.param_clusters = [{"N": {100, 500},
                                 "s": self.s_0,
                                 "p1": self.variable_params["p1"]},

                                {"N": {500, 900, 1300},
                                 "s": self.s_1,
                                 "p1": self.variable_params["p1"]},

                                {"N": {1300, 1700, 2100, 2500},
                                 "s": self.s_2,
                                 "p1": self.variable_params["p1"]}]

        self.paramsets = param_sets_from_ref_params(param_clusters=self.param_clusters,
                                                    param_order=self.param_order,
                                                    fixed_params=self.fixed_params)

        self.expected_paramsets = [
            {"N": 100, "s": 3, "p1": 0.2, "foo": (100, 200)},
            {"N": 100, "s": 3, "p1": 0.3, "foo": (100, 200)},

            {"N": 100, "s": 5, "p1": 0.2, "foo": (100, 200)},
            {"N": 100, "s": 5, "p1": 0.3, "foo": (100, 200)},
            {"N": 100, "s": 7, "p1": 0.2, "foo": (100, 200)},
            {"N": 100, "s": 7, "p1": 0.3, "foo": (100, 200)},

            {"N": 500, "s": 3, "p1": 0.2, "foo": (100, 200)},
            {"N": 500, "s": 3, "p1": 0.3, "foo": (100, 200)},
            {"N": 500, "s": 5, "p1": 0.2, "foo": (100, 200)},
            {"N": 500, "s": 5, "p1": 0.3, "foo": (100, 200)},
            {"N": 500, "s": 7, "p1": 0.2, "foo": (100, 200)},
            {"N": 500, "s": 7, "p1": 0.3, "foo": (100, 200)},

            #{"N": 500, "s": 5, "p1": 0.2, "foo": (100, 200)}, # eliminated by uniq
            #{"N": 500, "s": 5, "p1": 0.3, "foo": (100, 200)}, # eliminated by uniq
            {"N": 500, "s": 9, "p1": 0.2, "foo": (100, 200)}, # rearranged by sort, before duplicate elimination
            {"N": 500, "s": 9, "p1": 0.3, "foo": (100, 200)}, # rearranged by sort, before duplicate elimination

            {"N": 900, "s": 5, "p1": 0.2, "foo": (100, 200)},
            {"N": 900, "s": 5, "p1": 0.3, "foo": (100, 200)},
            {"N": 900, "s": 9, "p1": 0.2, "foo": (100, 200)},
            {"N": 900, "s": 9, "p1": 0.3, "foo": (100, 200)},

            {"N": 1300, "s": 5, "p1": 0.2, "foo": (100, 200)},
            {"N": 1300, "s": 5, "p1": 0.3, "foo": (100, 200)},

            {"N": 1300, "s": 7, "p1": 0.2, "foo": (100, 200)}, # rearranged by sort, before duplicate elimination
            {"N": 1300, "s": 7, "p1": 0.3, "foo": (100, 200)}, # rearranged by sort, before duplicate elimination

            {"N": 1300, "s": 9, "p1": 0.2, "foo": (100, 200)},
            {"N": 1300, "s": 9, "p1": 0.3, "foo": (100, 200)},

            #{"N": 1300, "s": 9, "p1": 0.2, "foo": (100, 200)}, # eliminated by uniq
            #{"N": 1300, "s": 9, "p1": 0.3, "foo": (100, 200)}, # eliminated by uniq
            {"N": 1300, "s": 13, "p1": 0.2, "foo": (100, 200)},
            {"N": 1300, "s": 13, "p1": 0.3, "foo": (100, 200)},

            {"N": 1700, "s": 7, "p1": 0.2, "foo": (100, 200)},
            {"N": 1700, "s": 7, "p1": 0.3, "foo": (100, 200)},
            {"N": 1700, "s": 9, "p1": 0.2, "foo": (100, 200)},
            {"N": 1700, "s": 9, "p1": 0.3, "foo": (100, 200)},
            {"N": 1700, "s": 13, "p1": 0.2, "foo": (100, 200)},
            {"N": 1700, "s": 13, "p1": 0.3, "foo": (100, 200)},

            {"N": 2100, "s": 7, "p1": 0.2, "foo": (100, 200)},
            {"N": 2100, "s": 7, "p1": 0.3, "foo": (100, 200)},
            {"N": 2100, "s": 9, "p1": 0.2, "foo": (100, 200)},
            {"N": 2100, "s": 9, "p1": 0.3, "foo": (100, 200)},
            {"N": 2100, "s": 13, "p1": 0.2, "foo": (100, 200)},
            {"N": 2100, "s": 13, "p1": 0.3, "foo": (100, 200)},

            {"N": 2500, "s": 7, "p1": 0.2, "foo": (100, 200)},
            {"N": 2500, "s": 7, "p1": 0.3, "foo": (100, 200)},
            {"N": 2500, "s": 9, "p1": 0.2, "foo": (100, 200)},
            {"N": 2500, "s": 9, "p1": 0.3, "foo": (100, 200)},
            {"N": 2500, "s": 13, "p1": 0.2, "foo": (100, 200)},
            {"N": 2500, "s": 13, "p1": 0.3, "foo": (100, 200)},
        ]


    def test_result_matches_expected(self):
        self.assertEqual(self.paramsets, self.expected_paramsets)

    def test_indexing_list_matches_expectation(self):
        self.index = index_from_ref_params(param_clusters=self.param_clusters,
                                           param_order=self.param_order)
        for idx, ps in enumerate(self.paramsets):
            self.assertEqual(idx, self.index[paramset_to_indexing_tuple(ps, self.param_order)])

class TestCreateMinimalParamsetForCalculation(TestCase):

    def setUp(self) -> None:
        self.s_0 = (3, 5, 7)
        self.s_1 = (5, 9)
        self.s_2 = (7, 9, 13)
        self.variable_params = {"p1": (0.2, 0.3)}
        self.fixed_params = {"foo": (100, 200)}
        self.param_order = ["N", "s", "p1"]

        self.param_clusters = [{"N": {100, 500},
                                 "s": self.s_0,
                                 "p1": self.variable_params["p1"]},

                                {"N": {500, 900, 1300},
                                 "s": self.s_1,
                                 "p1": self.variable_params["p1"]},

                                {"N": {1300, 1700, 2100, 2500},
                                 "s": self.s_2,
                                 "p1": self.variable_params["p1"]}]

        self.paramsets = minimal_ref_paramset_for_calculations(param_clusters=self.param_clusters,
                                                                param_order=self.param_order,
                                                                fixed_params=self.fixed_params)

        self.expected_paramsets = [
            {"N": 500, "s": 3, "p1": 0.2, "foo": (100, 200)},
            {"N": 500, "s": 3, "p1": 0.3, "foo": (100, 200)},
            {"N": 500, "s": 5, "p1": 0.2, "foo": (100, 200)},
            {"N": 500, "s": 5, "p1": 0.3, "foo": (100, 200)},
            {"N": 500, "s": 7, "p1": 0.2, "foo": (100, 200)},
            {"N": 500, "s": 7, "p1": 0.3, "foo": (100, 200)},

            {"N": 1300, "s": 5, "p1": 0.2, "foo": (100, 200)},
            {"N": 1300, "s": 5, "p1": 0.3, "foo": (100, 200)},
            {"N": 1300, "s": 9, "p1": 0.2, "foo": (100, 200)},
            {"N": 1300, "s": 9, "p1": 0.3, "foo": (100, 200)},

            {"N": 2500, "s": 7, "p1": 0.2, "foo": (100, 200)},
            {"N": 2500, "s": 7, "p1": 0.3, "foo": (100, 200)},
            {"N": 2500, "s": 9, "p1": 0.2, "foo": (100, 200)},
            {"N": 2500, "s": 9, "p1": 0.3, "foo": (100, 200)},
            {"N": 2500, "s": 13, "p1": 0.2, "foo": (100, 200)},
            {"N": 2500, "s": 13, "p1": 0.3, "foo": (100, 200)},
        ]


    def test_result_matches_expected(self):
        self.assertEqual(self.paramsets, self.expected_paramsets)
