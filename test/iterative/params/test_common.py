from unittest import TestCase

from hirise_tools.iterative.params import tupleize
from parameterized import parameterized


class TestTupleize(TestCase):

    @parameterized.expand([
        ((1, 0.5, "a"), (1, 0.5, "a"), "flat tuple remains unaltered"),
        ([1, 0.5, "a"], (1, 0.5, "a"), "flat list is converted to tuple"),
        ([("c",), 0.5, ["a", 12]], (("c",), 0.5, ("a", 12)), "nested lists are converted to tuples"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_tupleize(self, input, expected, desc):
        self.assertEqual(tupleize(input), expected)
