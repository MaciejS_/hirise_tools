import copy
import itertools
from unittest import TestCase

from hirise_tools.iterative.params import v2_full_param_set, paramset_to_indexing_tuple, v2_paramsets_index, \
    v2_split_iteration_param_sets, params_for_expr, convert_base_ps_to_persistence_tuples


def all_nested_collections_are_tuples(collection):
    if isinstance(collection, tuple):
        return all(map(all_nested_collections_are_tuples, collection))
    elif isinstance(collection, (list, set, dict)):
        return False
    else:
        return True

class CommonV2ParamSetBuilderTests:
    def test_all_fixed_params_occur_in_each_paramset(self):
        self.assertTrue(all(all(all(ps[k] == v
                                    for k, v
                                    in self.fixed_params.items())
                                for ps
                                in iteration_paramsets)
                            for iteration_paramsets
                            in self.result))

    def test_variable_params_recur_in_each_iteration(self):
        for iteration_paramsets in self.result:
            param_values = {param_name: {v: len(list(occurrences))
                                         for v, occurrences
                                         in itertools.groupby(sorted([ps[param_name] for ps in iteration_paramsets]))}
                            for param_name
                            in self.variable_params}

            # all values are present
            self.assertTrue(all(set(param_values[param_name].keys()) == set(values)
                                for param_name, values
                                in self.variable_params.items()))

            # each value occurs the same number of times
            self.assertTrue(all(len(set(param_values[param_name].values())) == 1
                                for param_name, values
                                in self.variable_params.items()))

    def test_keysets_in_all_paramsets_are_equal(self):
        for iteration_paramsets in self.result:
            first_keyset = set(iteration_paramsets[0].keys())
            self.assertTrue(all(set(ps.keys()) == first_keyset for ps in iteration_paramsets))

    def test_iteration_param_lengths_increase_with_iterations(self):
        for iter_idx, iteration_paramsets in enumerate(self.result):
            param_sizes = {param_name: [len(ps[param_name]) for ps in iteration_paramsets]
                           for param_name
                           in self.iteration_params}

            self.assertTrue(all(all(pv_length == iter_idx + 1
                                    for pv_length
                                    in pv_lengths)
                                for param_name, pv_lengths
                                in param_sizes.items()))

    def test_parameters_given_as_lists_are_converted_to_tuples(self):

        fixed_params = {k: list(v) for k,v in self.fixed_params.items()}

        self.result = v2_full_param_set(iteration_params=self.iteration_params,
                                          variable_params=self.variable_params,
                                          fixed_params=fixed_params,
                                          param_order=self.param_order)

        for iteration_params in self.result:
            self.assertTrue(all(all(all_nested_collections_are_tuples(v)
                                    for v
                                    in ps.values())
                                for ps
                                in iteration_params))

    def test_empty_iteration_params_cause_an_exception(self):
        with self.assertRaises(ValueError):
            param_sets = v2_full_param_set(iteration_params={},
                                           variable_params=self.variable_params,
                                           fixed_params=self.fixed_params,
                                           param_order=self.param_order)


    def test_empty_fixed_params_does_not_cause_errors(self):
        param_sets = v2_full_param_set(iteration_params=self.iteration_params,
                                       variable_params=self.variable_params,
                                       fixed_params={},
                                       param_order=self.param_order)

        expected_keyset = set(self.param_order)
        for iteration_paramsets, expected_ps in zip(param_sets, self.result):
            self.assertTrue(all(set(ps.keys()) == expected_keyset for ps in iteration_paramsets))
            self.assertEqual(len(iteration_paramsets), len(expected_ps))

    def test_empty_variable_params_are_handled(self):
        param_sets = v2_full_param_set(iteration_params=self.iteration_params,
                                       variable_params={},
                                       fixed_params=self.fixed_params,
                                       param_order=[x for x in self.param_order if x in self.iteration_params])


    def test_param_order_missing_keys_will_raise(self):
        lacking_param_order = ["N", "p1"]
        with self.assertRaises(KeyError):
            v2_full_param_set(iteration_params=self.iteration_params,
                               variable_params=self.variable_params,
                               fixed_params=self.fixed_params,
                               param_order=lacking_param_order)



class TestV2ParamSetBuilderStaticS(TestCase,
                                   CommonV2ParamSetBuilderTests):

    def setUp(self):
        self.n_0 = [100, 200]
        self.n_1 = [300, 400, 500]

        self.s_0 = [3, 5, 7]
        self.s_1 = [8, 9]

        self.iteration_params = {"N": [self.n_0, self.n_1],
                                "s": [self.s_0, self.s_1]}
        self.variable_params = {"p1": (0.2, 0.3)}
        self.fixed_params = {"foo": ("a", "b")}

        self.param_order = ["N", "s", "p1"]

        self.result = v2_full_param_set(iteration_params=self.iteration_params,
                                      variable_params=self.variable_params,
                                      fixed_params=self.fixed_params,
                                      param_order=self.param_order)

        self.expected_iteration0_paramsets = [
            {"N": (100, ), "s": (3, ), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, ), "s": (3, ), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, ), "s": (5, ), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, ), "s": (5, ), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, ), "s": (7, ), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, ), "s": (7, ), "p1": 0.3, "foo": ("a", "b")},

            {"N": (200, ), "s": (3, ), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, ), "s": (3, ), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, ), "s": (5, ), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, ), "s": (5, ), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, ), "s": (7, ), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, ), "s": (7, ), "p1": 0.3, "foo": ("a", "b")},
        ]

        # TODO: make sure that the order here is correct
        self.expected_iteration1_paramsets = [
            {"N": (100, 300), "s": (3, 8), "p1": 0.2, "foo": ("a", "b")},

            {"N": (100, 300), "s": (3, 8), "p1": 0.3, "foo": ("a", "b")},

            {"N": (100, 300), "s": (3, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 300), "s": (3, 9), "p1": 0.3, "foo": ("a", "b")},

            {"N": (100, 300), "s": (5, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 300), "s": (5, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 300), "s": (5, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 300), "s": (5, 9), "p1": 0.3, "foo": ("a", "b")},

            {"N": (100, 300), "s": (7, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 300), "s": (7, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 300), "s": (7, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 300), "s": (7, 9), "p1": 0.3, "foo": ("a", "b")},

            {"N": (100, 400), "s": (3, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 400), "s": (3, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 400), "s": (3, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 400), "s": (3, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 400), "s": (5, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 400), "s": (5, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 400), "s": (5, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 400), "s": (5, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 400), "s": (7, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 400), "s": (7, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 400), "s": (7, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 400), "s": (7, 9), "p1": 0.3, "foo": ("a", "b")},

            {"N": (100, 500), "s": (3, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 500), "s": (3, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 500), "s": (3, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 500), "s": (3, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 500), "s": (5, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 500), "s": (5, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 500), "s": (5, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 500), "s": (5, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 500), "s": (7, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 500), "s": (7, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (100, 500), "s": (7, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (100, 500), "s": (7, 9), "p1": 0.3, "foo": ("a", "b")},

            {"N": (200, 300), "s": (3, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 300), "s": (3, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 300), "s": (3, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 300), "s": (3, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 300), "s": (5, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 300), "s": (5, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 300), "s": (5, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 300), "s": (5, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 300), "s": (7, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 300), "s": (7, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 300), "s": (7, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 300), "s": (7, 9), "p1": 0.3, "foo": ("a", "b")},

            {"N": (200, 400), "s": (3, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 400), "s": (3, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 400), "s": (3, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 400), "s": (3, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 400), "s": (5, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 400), "s": (5, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 400), "s": (5, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 400), "s": (5, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 400), "s": (7, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 400), "s": (7, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 400), "s": (7, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 400), "s": (7, 9), "p1": 0.3, "foo": ("a", "b")},

            {"N": (200, 500), "s": (3, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 500), "s": (3, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 500), "s": (3, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 500), "s": (3, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 500), "s": (5, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 500), "s": (5, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 500), "s": (5, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 500), "s": (5, 9), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 500), "s": (7, 8), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 500), "s": (7, 8), "p1": 0.3, "foo": ("a", "b")},
            {"N": (200, 500), "s": (7, 9), "p1": 0.2, "foo": ("a", "b")},
            {"N": (200, 500), "s": (7, 9), "p1": 0.3, "foo": ("a", "b")},
        ]

        self.expected_param_sets = [
            self.expected_iteration0_paramsets,
            self.expected_iteration1_paramsets
        ]

    def test_paramset_is_as_expected(self):
        self.assertSequenceEqual(self.expected_param_sets, self.result)

    def test_index(self):
        index = v2_paramsets_index(iteration_params=self.iteration_params,
                                   variable_params=self.variable_params,
                                   param_order=self.param_order)

        for iteration_paramsets, index_for_iteration in zip(self.expected_param_sets, index):
            for expected_idx, paramset in enumerate(iteration_paramsets):
                pidx = paramset_to_indexing_tuple(paramset, self.param_order)
                self.assertEqual(index_for_iteration[pidx], expected_idx)



class TestV2ParamSetBuilderVariableS(TestCase,
                                     CommonV2ParamSetBuilderTests):

    def setUp(self):
        n_0 = [100]
        n_1 = [[100, 200, 300], [100, 200, 500], [100, 300, 500]] # params for 3 bands
        n_2 = [[600, 1000], [800, 1200]] # params for 2 bands

        s_0 = [3, 5, 7]
        s_1 = [[3, 7, 10], [3, 7, 12]]
        s_2 = [[8, 9]]

        self.iteration_params = {"N": [n_0, n_1, n_2],
                                 "s": [s_0, s_1, s_2]}

        self.variable_params = {"p1": (0.2, 0.3)}
        self.fixed_params = {"foo": ("a", "b")}

        self.param_order = ["N", "s", "p1"]

        self.result = v2_full_param_set(iteration_params=self.iteration_params,
                                        variable_params=self.variable_params,
                                        fixed_params=self.fixed_params,
                                        param_order=self.param_order)

    def test_all_collection_param_values_are_tuples(self):
        for iteration_paramsets in self.result:
            self.assertTrue(all(all(all_nested_collections_are_tuples(param_values)
                                    for param_name, param_values
                                    in paramset.items())
                                for paramset
                                in iteration_paramsets))


class TestIndexingTupleBuilder(TestCase):

    def setUp(self):
        self.paramset = {"N": (200, 500), "s": (3, 8), "p1": 0.2, "foo": ("a", "b")}
        self.param_order = ("N", "s", "p1")

        self.expected_tuple = ((200, 500), (3, 8), 0.2)


    def test_only_values_from_param_order_are_included(self):
        param_order = ("N", "s")
        result = paramset_to_indexing_tuple(self.paramset, param_order)

        self.assertTupleEqual(result, (self.paramset["N"], self.paramset["s"]))

    def test_indexing_tuple_follows_given_param_order(self):
        param_order = ("s", "p1", "N")
        result = paramset_to_indexing_tuple(self.paramset, param_order)

        self.assertTupleEqual(result, (self.paramset["s"], self.paramset["p1"], self.paramset["N"]))

    def test_all_nested_collections_are_converted_to_tuples(self):
        self.nested_paramset = {"N": [100, [200, 300], 500], "s": [3, [3, 8], 12], "p1": 0.2, "foo": ("a", "b")}
        param_order = ("N", "s", "p1")

        result = paramset_to_indexing_tuple(self.nested_paramset, param_order)
        self.assertTupleEqual(result, ((100, (200, 300), 500), (3, (3, 8), 12), 0.2))


class TestV2ParamsForIteration(TestCase):

    def setUp(self):
        self.n_0 = [100, 200]
        self.n_1 = [300, 400, 500]

        self.s_0 = [3, 5, 7]
        self.s_1 = [8, 9]

        self.iteration_params = {"N": [self.n_0, self.n_1],
                                 "s": [self.s_0, self.s_1],
                                 "p1": [(0.2, 0.3), (0.3, 0.2)]}
        self.variable_params = {"test": ["y", "x", "z"],
                                "test2": [1, 2, 3]}
        self.fixed_params = {"foo": ("a", "b"),
                             "quux": (9, 8)}

        self.param_order = ["N", "s", "p1", "test", "test2"]

        self.reference_full_param_sets = v2_full_param_set(iteration_params=self.iteration_params,
                                                           variable_params=self.variable_params,
                                                           fixed_params=self.fixed_params,
                                                           param_order=self.param_order)

        self.reference_full_indexes = v2_paramsets_index(iteration_params=self.iteration_params,
                                                           variable_params=self.variable_params,
                                                           param_order=self.param_order)

        self.iteration_param_sets = [v2_split_iteration_param_sets(iter_idx=i,
                                                                   iteration_params=self.iteration_params,
                                                                   variable_params=self.variable_params,
                                                                   fixed_params=self.fixed_params,
                                                                   param_order=self.param_order)
                                     for i
                                     in range(2)]

    def test_keyset_of_result(self):
        expected_keyset = {"n_values", "cur_iter_params", "base_params", "base_index"}
        for result in self.iteration_param_sets:
            self.assertSetEqual(set(result.keys()), expected_keyset)

    def test_base_params_for_first_iteration_do_not_contain_iteration_params_as_empty_collection(self):
        for paramset in self.iteration_param_sets[0]["base_params"]:
            self.assertTrue(all(len(paramset[iteration_param_key]) == 0
                                for iteration_param_key
                                in self.iteration_params.keys()))

    def test_base_params_for_second_iteration_are_equal_to_full_parameter_set_from_first_iteration(self):
        for paramset, expected in zip(self.iteration_param_sets[1]["base_params"], self.reference_full_param_sets[0]):
            self.assertSequenceEqual(paramset, expected)

    def test_base_index_for_second_iteration_is_equal_to_full_indexes_from_first_iteration(self):
        for paramset, expected in zip(self.iteration_param_sets[1]["base_index"], self.reference_full_indexes[0]):
            self.assertSequenceEqual(paramset, expected)

    def test_N_values_simply_copied(self):
        for actual, expected in zip([x["n_values"] for x in self.iteration_param_sets],
                                    self.iteration_params["N"]):
            self.assertListEqual(actual, expected)

    def test_current_iteration_params_do_not_include_N(self):
        for cur_paramsets in [x["cur_iter_params"] for x in self.iteration_param_sets]:
            self.assertTrue(all("N" not in ps for ps in cur_paramsets))

    def test_current_iteration_params_are_a_cartesian_product_of_params_for_this_iteration(self):
        for iteration_idx, cur_paramsets in enumerate([x["cur_iter_params"] for x in self.iteration_param_sets]):
            expected = params_for_expr(variable_params={k: v[iteration_idx]
                                                        for k, v
                                                        in self.iteration_params.items()
                                                        if k != "N"},
                                       param_order=[x for x in self.param_order if x in self.iteration_params and x != "N"],
                                       fixed_params={})
            self.assertListEqual(cur_paramsets, expected)


class TestConvertBasePSToIndexingTuple(TestCase):
    def setUp(self):
        self.dummy_base_paramset = {"N": (100, 300), "s": (8, 12), "p1": 0.2}
        self.dummy_base_paramset_copy = copy.deepcopy(self.dummy_base_paramset)

        self.second_iteration_s = 12
        self.second_iteration_Ns = (1000, 2000, 3000)
        self.param_order = ["N", "s", "p1"]

        self.result = convert_base_ps_to_persistence_tuples(N_values=self.second_iteration_Ns,
                                                            s_value=self.second_iteration_s,
                                                            base_paramset=self.dummy_base_paramset,
                                                            param_order=self.param_order)

    def test_returns_a_list_of_n_paramset_pairs(self):
        self.assertIsInstance(self.result, list)
        self.assertTrue(all(isinstance(ps, tuple) for n, ps in self.result))
        self.assertTrue(all(n == ps_n[-1] for n, (ps_n, ps_s, ps_p1) in self.result))

    def test_returns_one_tuple_for_each_N_value(self):
        self.assertEqual(len(self.result), len(self.second_iteration_Ns))
        self.assertSequenceEqual([n for n, ps in self.result], self.second_iteration_Ns)

    def test_second_iteration_Ns_are_appended_to_N_values_from_base_params(self):
        self.assertTrue(all(ps_n[:-1] == self.dummy_base_paramset["N"] for n, (ps_n, ps_s, ps_p1) in self.result))

    def test_s_is_appended_to_s_values_from_base_params(self):
        self.assertTrue(all(ps_s == self.dummy_base_paramset["s"] + (self.second_iteration_s, ) for n, (ps_n, ps_s, ps_p1) in self.result))

    def test_base_paramset_is_not_altered(self):
        self.assertDictEqual(self.dummy_base_paramset_copy, self.dummy_base_paramset)

    # @parameterized_expand()
    # def test_returned_indexing_tuples_follow_given_param_order(self):
