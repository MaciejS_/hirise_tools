from unittest import TestCase
from unittest.mock import Mock

import torch
from hirise_tools.iterative.common import combine_retained_checkpoints
from hirise.structures import CombinedMaskWithCheckpoints


class TestCombineRetainedCheckpoints(TestCase):
    def setUp(self) -> None:
        self.n_retainers = 3
        self.fake_checkpoints = [[(1, torch.ones(12, 12) * 1 * 1),
                                  (2, torch.ones(12, 12) * 2 * 1),
                                  (3, torch.ones(12, 12) * 3 * 1)],
                                 [(10, torch.ones(12, 12) * 1 * 10),
                                  (20, torch.ones(12, 12) * 2 * 10),
                                  (30, torch.ones(12, 12) * 3 * 10)],
                                 [(100, torch.ones(12, 12) * 1 * 100),
                                  (200, torch.ones(12, 12) * 2 * 100),
                                  (300, torch.ones(12, 12) * 3 * 100)]]

        self.expected_result = [(111, torch.ones(12, 12) * 111),
                                (222, torch.ones(12, 12) * 222),
                                (333, torch.ones(12, 12) * 333)]

        self.retainers = [Mock(spec=CombinedMaskWithCheckpoints) for _ in range(self.n_retainers)]
        for idx, r in enumerate(self.retainers):
            r.get_checkpoints = Mock(return_value=self.fake_checkpoints[idx])

        self.result = combine_retained_checkpoints(self.retainers)

    def test_n_values_in_result_are_sums_of_n_for_each_retainer(self):
        self.assertSequenceEqual([n for n, smap in self.expected_result],
                                 [n for n, smap in self.result])

    def test_nth_smap_is_a_sum_of_nth_checkpoints_from_all_retainers(self):
        self.assertTrue(torch.allclose(torch.stack([smap for n, smap in self.expected_result]),
                                       torch.stack([smap for n, smap in self.result])))
