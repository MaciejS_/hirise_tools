import itertools
from unittest import TestCase
from unittest.mock import Mock

import numpy as np
from hirise_tools.iterative.persistence import make_v1_archive, make_v2_archive, get_maps_archive_type, \
    make_source_data_dict, apply_to_leaves
from hirise_tools.iterative.refs.persistence import make_ref_archive_v1

from datetime import datetime


class TestGetMapsArchiveType(TestCase):

    def test_v1_is_recognized(self):
        archive = make_v1_archive(all_results=Mock(),
                                  source_data=make_source_data_dict([], []),
                                  fixed_params={},
                                  variable_params={},
                                  variable_param_order=[],
                                  start_date=datetime.now(),
                                  description=None)

        self.assertEqual(get_maps_archive_type(archive), "v1")

    def test_v2_is_recognized(self):
        archive = make_v2_archive(all_results=Mock(),
                                  source_data=make_source_data_dict([], []),
                                  v2_param_dict=Mock(),
                                  start_date=datetime.now(),
                                  description=None)

        self.assertEqual(get_maps_archive_type(archive), "v2")

    def test_r1_is_recognized(self):
        ref_params_dict = {"N_sets": [],
                           "clusters": [],
                           "order": [],
                           "fixed": {}}

        dummy_source_data = {"loaded_imgs": None,
                             "explained_class": None,
                             "proper_class": None}

        archive = make_ref_archive_v1(all_results=Mock(),
                                      full_ref_params_dict=ref_params_dict,
                                      source_data=dummy_source_data,
                                      input_archive_file_name="",
                                      start_date=datetime.now(),
                                      description=None,
                                      param_index=None)

        self.assertEqual(get_maps_archive_type(archive), "r1")

    def test_None_is_returned_when_archive_is_not_recognized(self):
        invalid_archive = {}
        self.assertIsNone(get_maps_archive_type(invalid_archive))


class TestApplyToLeaves(TestCase):

    def setUp(self) -> None:
        self.sample_structure = [1, (11, {"111", 112}, [121, 122]), [21, 22], 3, {(411, 412): 41, 42: [421, "422"], "43": 431}]
        self.function = str

        self.result = apply_to_leaves(self.function, self.sample_structure)
        self.expected = ["1", ("11", {"111", "112"}, ["121", "122"]), ["21", "22"], "3", {("411", "412"): "41", "42": ["421", "422"], "43": "431"}]

    def test_result_is_as_expected(self):
        self.assertEqual(self.result, self.expected)

    def test_on_ndarray(self):
        test_array = np.array([i for i in range(3*5*2)]).reshape(3, 5, 2)
        expected_arr = np.array([str(i*2) for i in range(3*5*2)]).reshape(3, 5, 2)
        test_func = lambda x: str(x*2)

        result = apply_to_leaves(test_func, test_array)

        self.assertTrue(np.array_equal(result, expected_arr))