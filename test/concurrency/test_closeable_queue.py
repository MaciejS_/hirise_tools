import queue
import unittest
import threading

from unittest import mock
from unittest.mock import Mock

from parameterized import parameterized

from hirise_tools.concurrency import CloseableQueue, Closed, Exhausted


class TestCloseableQueue(unittest.TestCase):

    def setUp(self) -> None:
        self.queue = CloseableQueue()
        self.dummy_item = Mock()

    def test_number_of_producers_must_be_greater_than_0(self):
        with self.assertRaises(ValueError):
            CloseableQueue(producers=0)

    def test_queue_has_unlimited_size_by_default(self):
        self.assertEqual(self.queue.maxsize, 0, msg=f"Default queue size should be 0, got {self.queue.maxsize}")

    def test_number_of_producers_is_1_by_default(self):
        self.assertEqual(self.queue._producer_count, 1, msg=f"Default producer count should be 1, got {self.queue._producer_count}")

    def test_maxsize_works_like_it_would_in_normal_queue(self):
        queue_size = 2
        queue = CloseableQueue(maxsize=queue_size)

        # Assert 1
        self.assertEqual(queue.maxsize, queue_size, msg=f"Queue capacity should be {queue_size}, got {queue.maxsize}")

        # Arrange 2
        for _ in range(queue_size):
            queue.put(self.dummy_item)

        # Assert 2
        self.assertTrue(queue.full(), msg="queue should be full")

    def test_producer_count_is_used_to_set_barrier_size(self):
        n_parties = 3
        with mock.patch.object(threading.Barrier, "__init__", Mock(return_value=None)) as barrier_init_mock:
            CloseableQueue(producers=n_parties)
            barrier_init_mock.assert_called_with(parties=n_parties)

    @mock.patch.object(threading.Barrier, "wait")
    def test_call_to_close_will_wait_for_barrier_first(self, barrier_wait_mock):
        self.queue.close()
        barrier_wait_mock.assert_called_once()

    def test_put_adds_item_to_queue(self):
        self.assertTrue(self.queue.empty(), msg="Queue should be empty at the beginning of the test")
        self.queue.put(self.dummy_item)
        self.assertFalse(self.queue.empty(), msg="Queue should not be empty after insertion")

    def test_raises_on_put_after_closing(self):
        self.queue.close()
        with self.assertRaises(Closed):
            self.queue.put(self.dummy_item)

    @mock.patch.object(queue.Queue, "put")
    def test_get_calls_get_from_builtin_Queue(self, mocked_base_put):
        block_value_mock = Mock()
        timeout_value_mock = Mock()
        item_mock = Mock()
        self.queue.put(item=item_mock, block=block_value_mock, timeout=timeout_value_mock)
        mocked_base_put.assert_called_once_with(item_mock, block_value_mock, timeout_value_mock)

    def test_get_works_after_queue_is_closed(self):
        self.queue.put(self.dummy_item)
        self.queue.close()
        self.queue.get() # does not raise

    def test_close_does_not_raise_on_subsequent_calls(self):
        self.queue.close()
        self.queue.close() # does not raise

    @parameterized.expand([
        (True, "true when queue is closed"),
        (False, "false when queue is open")
    ], testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_is_closed_returns(self, should_be_closed, desc):
        if should_be_closed:
            self.queue.close()

        self.assertEqual(self.queue.is_closed(), should_be_closed,
                         msg=f"Expected queue to be {'closed' if should_be_closed else 'open'}, "
                             f"but it was {'closed' if self.queue.is_closed() else 'open'}")

    @parameterized.expand([
        (False, False, 1, "false when queue is open and not empty"),
        (False, False, 0, "false when queue is open and empty"),
        (False, True,  1, "false when queue is closed and not empty"),
        (True,  True,  0, "true  when queue is closed and empty"),
    ], testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_is_exhausted_returns(self, expected_result, is_closed, queue_size, desc):
        # Arrange
        if queue_size > 0:
            self.queue.put(self.dummy_item)
        if is_closed:
            self.queue.close()

        # Assert
        self.assertEqual(self.queue.is_exhausted(), expected_result,
                         msg=f"Queue {'should' if expected_result else 'should not'} be exhausted if "
                             f"{'closed' if is_closed else 'not closed'} "
                             f"and {'empty' if queue_size == 0 else 'not empty'}")

    @mock.patch.object(queue.Queue, "get")
    def test_get_calls_get_from_builtin_Queue(self, mocked_base_get):
        block_value_mock = Mock()
        timeout_value_mock = Mock()
        self.queue.get(block=block_value_mock, timeout=timeout_value_mock)
        mocked_base_get.assert_called_once_with(block_value_mock, timeout_value_mock)

    @parameterized.expand([
        (queue.Empty, False, "Empty when open but empty"),
        (Exhausted, True, "Exhausted when closed and empty")
    ], testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_get_raises(self, expected_exception, is_closed, desc):
        self.assertTrue(self.queue.empty(), msg="The queue should be empty at the start of this test")
        if is_closed:
            self.queue.close()

        with self.assertRaises(expected_exception):
            self.queue.get(block=False)


class TestCloseableQueueMultithreading(unittest.TestCase):

    def setUp(self):
        self.n_producers = 2
        self.queue = CloseableQueue(producers=2)

    def test_close_blocks_until_all_producers_call_close(self):
        producers = [threading.Thread(target=self.queue.close)
                     for _
                     in range(self.n_producers)]

        queue_state = []
        for i, p in enumerate(producers):
            state = self.queue.is_closed()
            queue_state.append((i, state))
            p.start()

        self.assertTrue(self.queue.is_closed()) # the queue is closed now
        self.assertTrue(all(state == False for i, state in queue_state)) # before all of the producers have been started, the queue was not

        for p in producers:
            p.join()
