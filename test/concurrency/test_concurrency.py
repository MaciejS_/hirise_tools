import queue
import unittest
from unittest import mock
from unittest.mock import Mock, call

import more_itertools

import hirise_tools.concurrency
from hirise_tools.concurrency import Producer, Consumer, CloseableQueue, ConcurrentMapper



class TestProducer(unittest.TestCase):

    def setUp(self) -> None:
        self.output_queue_mock = Mock()
        self.producer = Producer(output_queue=self.output_queue_mock)

    def test_closeable_queue_is_created_if_output_queue_not_provided(self):
        with mock.patch.object(hirise_tools.concurrency.CloseableQueue, "__init__", Mock(return_value=None)) as cq_init_mock:
            producer = Producer() # output_queue is *not* given to force the use of default
            output_queue = producer.output_queue

            self.assertIsInstance(output_queue, CloseableQueue)
            cq_init_mock.assert_called_once_with() # ClosedQueue was called without arguments, creating a queue with default params

    def test_output_queue_is_set(self):
        self.assertEqual(self.producer.output_queue, self.output_queue_mock)

    def test_close_closes_the_output_queue(self):
        self.producer.close()
        self.output_queue_mock.close.assert_called_once()


class TestConsumer(unittest.TestCase):

    def setUp(self) -> None:
        self.input_queue_mock = Mock()
        self.consumer = Consumer(input_queue=self.input_queue_mock)

    def test_input_queue_must_be_provided(self):
        with self.assertRaisesRegex(TypeError, r".*? missing .*? argument: 'input_queue'") as cm:
            Consumer() # intentionally omitting the input queue

    def test_output_queue_is_set(self):
        self.assertEqual(self.consumer.input_queue, self.input_queue_mock)


class TestConcurrentMapper(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.TestedClass = ConcurrentMapper

    def setUp(self):
        self.mapper_routine_mock = Mock()
        self.mapper_routine_mock.return_value = Mock()
        self.input_queue_mock = Mock()
        self.output_queue_mock = Mock()
        self.thread_name = "dummy_thread_name"

        self.mapper = self.TestedClass(mapper=self.mapper_routine_mock,
                                       input_queue=self.input_queue_mock,
                                       output_queue=self.output_queue_mock,
                                       thread_name=self.thread_name)

    def test_is_not_daemon(self):
        self.assertFalse(self.mapper.isDaemon())

    def test_thread_has_to_be_started_manually(self):
        self.assertFalse(self.mapper.is_alive(), msg=f"Expected {self.mapper.__class__.__name__} to not be alive right after instantiation")

    def test_input_queue_initialized_correctly(self):
        self.assertEqual(self.mapper.input_queue, self.input_queue_mock)

    def test_output_queue_initialized_correctly(self):
        self.assertEqual(self.mapper.output_queue, self.output_queue_mock)

    @mock.patch("hirise_tools.concurrency.randhex")
    def test_default_thread_name(self, randhex_mock):
        randhex_mock.return_value = "<randhex>"
        default_randhex_len = 8
        mapper = ConcurrentMapper(mapper=self.mapper_routine_mock,
                                 input_queue=self.input_queue_mock,
                                 output_queue=self.output_queue_mock,
                                 thread_name=None)
        expected_default_name = f"{ConcurrentMapper.__name__}-{randhex_mock.return_value}"

        randhex_mock.assert_called_with(default_randhex_len)
        self.assertEqual(mapper.name, expected_default_name)

    @mock.patch.object(Producer, "close")
    def test_quits_when_input_queue_raises_Exhausted(self, producer_close_mock):
        self.input_queue_mock.get.side_effect = hirise_tools.concurrency.Exhausted()

        # act
        self.mapper.start()
        self.mapper.join()

        # assert
        self.input_queue_mock.get.assert_called_once()
        self.mapper_routine_mock.assert_not_called()
        self.output_queue_mock.put.assert_not_called()
        self.input_queue_mock.task_done.assert_not_called()
        producer_close_mock.assert_called_once()

    @mock.patch.object(Producer, "close")
    def test_ignores_queue_empty(self, producer_close_mock):
        # Exhausted exception is used here to exit the mapper routine and .join() the ConcurrentMapper
        self.input_queue_mock.get.side_effect = [queue.Empty(), hirise_tools.concurrency.Exhausted()]
        expected_input_queue_get_calls = [call(block=True, timeout=ConcurrentMapper._TIMEOUT)] * 2

        # act
        self.mapper.start()
        self.mapper.join()

        # assert
        self.input_queue_mock.get.assert_has_calls(expected_input_queue_get_calls)
        # no more calls because both .get() calls raise
        self.mapper_routine_mock.assert_not_called()
        self.output_queue_mock.put.assert_not_called()
        self.input_queue_mock.task_done.assert_not_called()
        producer_close_mock.assert_called_once()

    def test_calls_mapper_on_each_input_element_and_skips_when_queue_is_empty(self):
        # this is a "normal operation" test
        queue_contents_mocks = [Mock() for _ in range(6)]

        self.input_queue_mock.get.side_effect = list(more_itertools.intersperse(queue.Empty(), queue_contents_mocks)) \
                                                + [hirise_tools.concurrency.Exhausted()]
        expected_mapper_calls = [call(m) for m in queue_contents_mocks] # one call for each valid object in queue
        expected_put_calls = [call(self.mapper_routine_mock.return_value) for _ in expected_mapper_calls]
        expected_task_done_calls = [call() for _ in expected_mapper_calls]

        # Act
        self.mapper.start()
        self.mapper.join()

        # Assert
        self.mapper_routine_mock.assert_has_calls(expected_mapper_calls)
        self.output_queue_mock.put.assert_has_calls(expected_put_calls)
        self.input_queue_mock.task_done.assert_has_calls(expected_task_done_calls)

        # sanity assertions
        self.input_queue_mock.put.assert_not_called() # no puts to input
        self.output_queue_mock.get.assert_not_called() # no gets from output
        self.output_queue_mock.task_done.assert_not_called()

