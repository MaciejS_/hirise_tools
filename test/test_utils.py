import os
import random
from io import BytesIO

from unittest import mock
from unittest.case import TestCase
from parameterized import parameterized

from hirise_tools.utils import create_output_path, preallocated_file


ABSPATH_MOCK_RETURN_VALUE = "abspath:"

class TestCreateOutputPath(TestCase):
    def setUp(self) -> None:
        self.default_filename = "default_filename"

    @mock.patch("hirise_tools.utils.os.path.isdir", return_value=False)
    def test_create_output_path(self, isdir_mock):
        file = "/path/to/some_file"
        user_provided_abspath = file
        output_path = create_output_path(user_provided_abspath, self.default_filename)

        self.assertEqual(output_path, user_provided_abspath)

    @mock.patch("hirise_tools.utils.os.path.isdir", return_value=True)
    def test_create_output_path_with_directory_given_by_user(self, isdir_mock):
        user_provided_path = "/path/to/some_directory/"
        output_path = create_output_path(user_provided_path, self.default_filename)

        self.assertEqual(output_path, os.path.join(user_provided_path, self.default_filename))

    @parameterized.expand([
        ("", "empty string"),
        (None, "None object")
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    @mock.patch("hirise_tools.utils.os.path.abspath", return_value=ABSPATH_MOCK_RETURN_VALUE)
    def test_user_provided_nothing(self, user_provided_path, description, abspath_mock):
        output_path = create_output_path(user_provided_path, self.default_filename)

        abspath_mock.assert_called_once_with(self.default_filename)
        self.assertEqual(output_path, ABSPATH_MOCK_RETURN_VALUE)


class TestPreallocate(TestCase):

    def setUp(self):
        self.dummy_file = mock.MagicMock(spec=BytesIO)

        self.open_func_mock = mock.mock_open()
        self.open_func_mock.return_value = self.dummy_file

        self.open_func_patch = mock.patch("builtins.open", self.open_func_mock)
        self.open_func_patch.start()

        self.dummy_file_path = "/tmp/foo.tmpfile"

        self.requested_file_size = random.randint(100, 1000)
        self.preallocated_file = preallocated_file(self.dummy_file_path, initial_size=self.requested_file_size)
        self.x = 10

    def test_raises_when_filesize_is_negative(self):
        self.dummy_file.reset_mock()

        negative_file_size = -1
        with self.assertRaises(ValueError):
            preallocated_file(self.dummy_file_path, initial_size=negative_file_size)

        self.dummy_file.write.assert_not_called()

    def test_zero_is_a_valid_file_size(self):
        self.dummy_file.reset_mock()
        preallocated_file(self.dummy_file_path, 0)
        self.dummy_file.write.assert_called_once()

    def test_size_of_preallocation_data_is_equal_to_provided_argument(self):
        # Assert
        calls_to_write = self.dummy_file.write.call_args_list
        first_call_positional_args = calls_to_write[0][0]

        self.assertEqual(len(calls_to_write), 1)
        self.assertEqual(len(first_call_positional_args[0]), self.requested_file_size)

    def test_file_pointer_is_reset_to_zero_after_preallocation(self):
        mock_calls = self.dummy_file.mock_calls
        self.dummy_file.seek.assert_called()

        # assert that the most recent seek has been performed after the last write and that it moved the caret to 0
        last_write_call_idx = max(idx for idx, (method_name, _, _) in enumerate(mock_calls) if method_name == "write")
        last_seek_call_idx = max(idx for idx, (method_name, _, _) in enumerate(mock_calls) if method_name == "seek")
        self.assertLess(last_write_call_idx, last_seek_call_idx)
        # assert that seek has moved the caret to the beginning ofthe file
        self.dummy_file.seek.assert_called_once_with(0)

    def test_preallocation_flushes_to_disk_to_ensure_block_allocation(self):
        mock_calls = self.dummy_file.mock_calls
        self.dummy_file.flush.assert_called_once()

        last_write_call_idx = max(idx for idx, (method_name, _, _) in enumerate(mock_calls) if method_name == "write")
        last_flush_call_idx = max(idx for idx, (method_name, _, _) in enumerate(mock_calls) if method_name == "flush")
        self.assertLess(last_write_call_idx, last_flush_call_idx)

    def test_file_is_truncated_on_close(self):
        self.preallocated_file.close()
        self.dummy_file.truncate.assert_called_once()

    def tearDown(self):
        self.open_func_patch.stop()


class TestPreallocateOnRealFile(TestCase):

    def setUp(self) -> None:
        super().setUp()
        random_hex = hex(hash(os.urandom(64)))[-8:]
        self.real_file_path = f"/tmp/{random_hex}.pytesttmp"

    def test_preallocation_allocates_physical_blocks_on_disk(self):
        # this is important, because a preallocated sparse file won't fulfill its purpose
        # another, e.g. when 1GB of space is available, a sparse file won't prevent the underlying blocks
        # from being allocated to some other file, leading to an exception when "dense" gets written to the preallocated file
        # potentially rendering the preallocation useless

        initial_size = 5 * 1024**2 # size == 5 MiB
        with preallocated_file(self.real_file_path, initial_size=initial_size):
            filestat = os.stat(path=self.real_file_path)
            total_size_of_allocated_blocks = filestat.st_blksize * filestat.st_blocks
            self.assertGreaterEqual(total_size_of_allocated_blocks, initial_size)

    @parameterized.expand([
        (100, 200, "contents bigger than preallocated space"),
        (200, 100, "contents smaller than preallocated space")
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_file_is_always_trimmed_to_content_size(self, content_size, preallocation_size, desc):
        with preallocated_file(self.real_file_path, initial_size=preallocation_size) as test_file:
            test_file.write(os.urandom(content_size))

        self.assertEqual(os.path.getsize(self.real_file_path), content_size)

    def tearDown(self) -> None:
        super().tearDown()
        if os.path.exists(self.real_file_path):
            os.remove(self.real_file_path)
