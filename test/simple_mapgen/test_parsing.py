from unittest import TestCase
from hirise_tools.rise.simple_mapgen.parsing import parse_value_or_range
from parameterized import parameterized


class TestParseValueOrRange(TestCase):
    def test_parse_value(self):
        expected_value = 20
        result = parse_value_or_range(str(expected_value))
        values_in_result_range = list(result)

        self.assertIsInstance(result, range)
        self.assertEqual(values_in_result_range[0], 0)
        self.assertEqual(values_in_result_range[-1], expected_value - 1)

    @parameterized.expand([
        (-1,),
        (0,),
        (1.5,),
    ])
    def test_parse_invalid_value(self, value):
        with self.assertRaises(ValueError):
            parse_value_or_range(str(value))

    @parameterized.expand([
        (5, 15),
        (2, 3),
    ])
    def test_parse_range(self, min, max):
        result = parse_value_or_range(f"{min}-{max}")
        values_in_result_range = list(result)

        self.assertIsInstance(result, range)
        self.assertEqual(values_in_result_range[0], min)
        self.assertEqual(values_in_result_range[-1], max - 1)

    @parameterized.expand([
        (2, 2),
        (3, 1),
        (-1, 2),
        (-1, -3),
        (-3, -1),
        (0.5, 1.5),
    ])
    def test_parse_invalid_range(self, min, max):
        with self.assertRaises(ValueError):
            parse_value_or_range(f"{min}-{max}")

