from unittest.case import TestCase
from parameterized import parameterized

from hirise_tools.rise.simple_mapgen.utils import human_friendly_number


class TestHumanFriendlyNumber(TestCase):

    @parameterized.expand([
        (1, "1"), (9, "9"),
        (10, "10"), (99, "99"),
        (100, "100"), (999, "999"),
        (1000, "1K"), (2000, "2K"), (9000, "9K"), (9999, "9K"),
        (10 ** 4, "10K"), (2 * 10 ** 4, "20K"), (99000, "99K"), (10 ** 5 - 1, "99K"),
        (10 ** 5, "100K"), (2 * 10 ** 5, "200K"), (999000, "999K"), (10 ** 6 - 1, "999K"),
        (10 ** 6, "1M"), (2 * 10 ** 6, "2M"), (10 ** 7 - 1, "9M"),
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_human_friendly(self, input_value, expected_result):
        self.assertEqual(expected_result, human_friendly_number(input_value))


