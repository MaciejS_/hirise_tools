import collections
from unittest import mock
from unittest.case import TestCase
from unittest.mock import PropertyMock

from hirise_tools.rise.simple_mapgen.tensor import create_tensor_layout, create_tensor


class TestCreateTensorLayout(TestCase):

    def setUp(self) -> None:
        self.namespace_mock = mock.MagicMock()

        self.mock_mask_count_value = 10
        self.mask_count_mock = PropertyMock(return_value=self.mock_mask_count_value)
        type(self.namespace_mock).masks = self.mask_count_mock

        self.grid_size_value_mock = [2, 4, 6]
        self.grid_size_mock = PropertyMock(return_value=self.grid_size_value_mock)
        type(self.namespace_mock).grid_size = self.grid_size_mock

        self.occlusion_prob_value_mock = [0.3, 0.6]
        self.occlusion_prob_mock = PropertyMock(return_value=self.occlusion_prob_value_mock)
        type(self.namespace_mock).occlusion_prob = self.occlusion_prob_mock

        self.checkpoint_lambda = lambda x, y, z: True
        self.checkpoint_lambda_mock = mock.MagicMock(wraps=self.checkpoint_lambda)

        self.input_img_shape = (10, 20)

        self.tensor_layout = create_tensor_layout(self.namespace_mock,
                                                  self.checkpoint_lambda_mock,
                                                  self.input_img_shape)

    @staticmethod
    def make_property_mock(return_value):
        prop_mock = mock.PropertyMock()
        prop_mock.__get__ = lambda s, i, o: return_value
        return prop_mock

    def test_tensor_layout_is_ordered_dict(self):
        self.assertIsInstance(self.tensor_layout, collections.OrderedDict)

    def test_tensor_layout_contains_only_expected_fields_in_order(self):
        expected_keys = ["s", "p1", "checkpoints", "map_y", "map_x"]
        self.assertListEqual(list(self.tensor_layout.keys()), expected_keys)

    def test_certain_values_are_obtained_from_args_namespace(self):
        self.grid_size_mock.assert_called()
        self.occlusion_prob_mock.assert_called()
        self.assertListEqual(self.tensor_layout["s"], self.grid_size_value_mock)
        self.assertListEqual(self.tensor_layout["p1"], self.occlusion_prob_value_mock)

    def test_checkpoints_are_calculated_using_provided_lambda(self):
        self.assertEqual(self.checkpoint_lambda_mock.call_count, self.mock_mask_count_value)
        self.assertListEqual(self.tensor_layout["checkpoints"], list(range(1, self.mock_mask_count_value + 1)))

    def test_input_img_is_used_correctly(self):
        self.assertEqual(self.tensor_layout["map_y"], self.input_img_shape[0])
        self.assertEqual(self.tensor_layout["map_x"], self.input_img_shape[1])

    def test_raises_if_parser_returns_grid_size_as_type_other_than_collection(self):
        self.grid_size_mock.return_value = 4
        with self.assertRaises(TypeError):
            create_tensor_layout(self.namespace_mock,
                                 self.checkpoint_lambda_mock,
                                 self.input_img_shape)

    def test_raises_if_parser_returns_occlusion_prob_as_type_other_than_collection(self):
        self.occlusion_prob_mock.return_value = 4
        with self.assertRaises(TypeError):
            create_tensor_layout(self.namespace_mock,
                                 self.checkpoint_lambda_mock,
                                 self.input_img_shape)


class TestCreateTensor(TestCase):

    def setUp(self) -> None:
        self.tensor_layout = collections.OrderedDict(s=[2, 3],
                                                     p1=(0.3, 0.6, 0.9),
                                                     checkpoints=[1, 10, 100, 1000],
                                                     map_y=224,
                                                     map_x=224)
        self.tensor = create_tensor(self.tensor_layout)

    def test_tensor_follows_the_order_set_by_layout(self):
        self.assertEqual(self.tensor.shape[0], len(self.tensor_layout["s"]))
        self.assertEqual(self.tensor.shape[1], len(self.tensor_layout["p1"]))
        self.assertEqual(self.tensor.shape[2], len(self.tensor_layout["checkpoints"]))
        self.assertEqual(self.tensor.shape[3], self.tensor_layout["map_y"])
        self.assertEqual(self.tensor.shape[4], self.tensor_layout["map_x"])