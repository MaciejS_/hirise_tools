# several producers + one consumer
# make sure that perf is not impacted
# make sure that no data is lost
# make sure that no data is duplicated

from hirise_tools.concurrency import ConcurrentMapper, CloseableQueue, FanIn, ConcurrentGatherer, TqdmList, TqdmQueue

DISABLE_TQDM = True

"""
Sets up and runs the following concurrent pipeline:

                    ,-> [ worker A1 ] -> [ q1 ] -> [-.    \   ,-> [ worker B1 ] -.
[items] -> [queue] -+-> [ worker A2 ] -> [ q2 ] -> [ fanIn > -+-> [ worker B2 ] -+-> [ queue ] -> [gatherer]
                    '-> [ worker AN ] -> [ q3 ] -> [-'    /   '-> [ worker BN ] -'

This scenario tests fan-in with both multiple consumers 
* Stage A: Multiple producer queues merge using fan-in
* Stage B: Multiple consumers fetching data from fan-in 

Additionally, the CloseableQueue is tested as well:
* Input to stage A exercises the multi-consumer scenario 
* Output of stage B exercises the multi-producer scenario  

The script will exit with code 0 if the result gathered by gatherer is identical to input items (except for order of items)
"""


ITEM_COUNT = 100000
INPUT_ITEMS = list(range(ITEM_COUNT))
inputQ = TqdmQueue(CloseableQueue(), total=ITEM_COUNT, desc="inputs", disable=DISABLE_TQDM)
for e in INPUT_ITEMS:
    inputQ.put(e)
inputQ.close()

n_workers = 2
w_queues = [TqdmQueue(CloseableQueue(), desc=f"Q{i}", leave=True, total=5000, disable=DISABLE_TQDM)
            for i
            in range(n_workers)]
iter_id = [iter(range(ITEM_COUNT)) for _ in range(n_workers)]
workers = [ConcurrentMapper(mapper=lambda x: (x, (i, next(iter_id[i]))),
                            input_queue=inputQ,
                            output_queue=w_queues[i],
                            thread_name=f"Worker{i}")
            for i
           in range(n_workers)]

outputQs = [w.output_queue for w in workers]
fanIn = FanIn(input_queues=outputQs)

_2ND_STAGE_WRK = 2
final_q = TqdmQueue(CloseableQueue(producers=_2ND_STAGE_WRK), desc="F0", total=ITEM_COUNT, maxinterval=0.1, disable=DISABLE_TQDM)
second_stage_workers = [ConcurrentMapper(mapper=lambda x: (*x, i),
                                        input_queue=fanIn,
                                        output_queue=final_q,
                                        thread_name=f"Extractor{i}")
                        for i
                        in range(_2ND_STAGE_WRK)]

results = TqdmList(total=ITEM_COUNT, desc="results", maxinterval=0.1, disable=DISABLE_TQDM)
gatherer = ConcurrentGatherer(input_queues=[final_q],
                              output_container=results,
                              thread_name="Gatherer")

for w in workers:
    w.start()

for e in second_stage_workers:
    e.start()

gatherer.start()


print("Joining")
for i, w in enumerate((*workers, *second_stage_workers, gatherer)):
    w.join()
    print(f"{w.name} joined")

print("", end="\n"*10)
uniq_retrieved_items = sorted(list(x for x, i1, i2 in results))
lists_identical = uniq_retrieved_items == INPUT_ITEMS

if lists_identical:
    print("Lists identical")
else:
    print("Lengths: ", f"Reference: {len(INPUT_ITEMS)}", f"Actual: {len(uniq_retrieved_items)}")
    print("Diff: ",
          f"Ref-act: {set(INPUT_ITEMS) - set(uniq_retrieved_items)}",
          f"act-ref: {set(uniq_retrieved_items) - set(INPUT_ITEMS)}")
    print()

print("Lists", "identical" if lists_identical else "not identical")

exit(0 if lists_identical else 1)
