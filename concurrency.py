import queue
import threading
import typing
import time
from typing import Optional

import torch
from hirise_tools.utils import randhex
from tqdm.notebook import tqdm


class Closed(Exception):
    pass


class Exhausted(Exception):
    pass


class CloseableQueue(queue.Queue):

    def __init__(self,
                 maxsize: int=0,
                 producers: int=1) -> None:
        """
        A subclass of Queue which can be closed to further .put() operations.
        Once closed, the queue can still be read from, until emptied.
        Once closed and empty, the queue becomes "exhausted". Attempts to .get() will yield Exhausted exception

        `producers` determines the size of the synchronization barrier protecting the .close() operation.
        This is used to prevent premature shutdown of the queue when multiple threads are writing to the queue

        :param maxsize: int, maximum sizeof the queue
        :param producers: int, number of threads contributing to the queue - used to synchr
        """
        super().__init__(maxsize)
        self._close_event = threading.Event()
        if producers < 1:
            raise ValueError("Number of producers mut be at least 1")
        self._producer_count = producers
        self._exit_barrier = threading.Barrier(parties=self._producer_count)

    def close(self) -> None:
        """
        Closes the queue. Thread-safe.
        Subsequent calls to `.put()` will raise Closed exception.
        If this queue is used by many producers this call blocks until all of the producers close the queue.
        :return:
        """
        self._exit_barrier.wait()
        with self.mutex:
            self._close_event.set()

    def is_closed(self) -> bool:
        """
        Returns true if the queue has been closed.
        :return: bool
        """
        return self._close_event.is_set()

    def is_exhausted(self) -> bool:
        """
        Returns true if the queue is empty and closed.
        Is thread-safe because the queue cannot be refilled after being closed
        :return: bool, True if queue is exhausted
        """
        return self._close_event.is_set() and self.empty()

    def put(self, item, block: bool = False, timeout: Optional[float] = None) -> None:
        """
        Puts the `item` in queue otherwise.
        `block` and `timeout` are passed down to queue.Queue.put()
        Raises Closed exception if this queue has been closed.
        """
        if self.is_closed():
            raise Closed

        super().put(item, block, timeout)

    def get(self, block: bool = True, timeout: Optional[float] = None):
        """
        Retrieves the next item from the least recently used queue.
        Raises Empty just like a normal Queue.
        Raises Exhausted if queue is exhausted
        """
        try:
            return super().get(block, timeout)
        except queue.Empty as e:
            if self.is_exhausted():
                raise Exhausted
            else:
                raise e


class FanIn():

    def __init__(self,
                 input_queues: typing.Sequence[CloseableQueue] = ()):
        self._input_queues = list(input_queues)
        self._exhausted_queues = []
        self._queue_iter = iter(self._input_queues)

        self.mutex = threading.RLock()
        self._exhausted = threading.Event()

        self._open_tasks_count = 0

    def add_input(self, queue: CloseableQueue) -> None:
        """
        Adds `queue` to the list of contributing queues.
        If all pf the existing contributors have been exhausted, raises Exhauted exception.
        """
        with self.mutex:
            if not self.is_exhausted():
                self._input_queues += queue
            else:
                raise Exhausted

    def close(self) -> None:
        raise NotImplementedError("Dev note: This should not be implemented (or called) because the "
                                  "design assumption is that fan-in is not owned by any of the workers"
                                  "and should auto-close when all contributing queues are exhausted")

    def get(self, block: bool = True, timeout: Optional[float] = None):
        """
        Retrieves the next item from the least recently used queue.
        If all of the input queues have been exhausted, Exhausted exception is raised
        """

        start_time = time.time()
        while self._input_queues:
            try:
                with self.mutex:
                    source_q = self._get_next_queue()
            except queue.Empty:
                break # all input queues have been exhausted

            try:
                with self.mutex:
                    item = source_q.get(block=False) # blocking on this .get() would cause timeouts of other waiting threads to compound
                    source_q.task_done()
                    self._open_tasks_count += 1
                return item
            except queue.Empty:
                if block and timeout:
                    time.sleep(0.001) # 1ms backoff to prevent rapid cycling when all queues are empty
                    pass # just try another queue
                else:
                    raise queue.Empty
            except Exhausted:
                with self.mutex:
                    self._retire_queue(source_q)

            if not block or (timeout and (time.time() - start_time) > timeout):
                raise queue.Empty
        self._exhausted.set() # mark self as exhausted if no queues are left (the loop wouldn't have quit otherwise)

        raise Exhausted

    def task_done(self) -> None:
        with self.mutex:
            self._open_tasks_count -= 1

    def is_exhausted(self) -> bool:
        return self._exhausted.is_set()

    def _get_next_queue(self) -> CloseableQueue:
        """
        Returns least recently used input queue.
        """
        with self.mutex:
            try:
                return next(self._queue_iter)
            except StopIteration:
                self._refresh_iterator()
                return next(self._queue_iter)

    def _refresh_iterator(self) -> None:
        """
        Changes the
        :return:
        """
        with self.mutex:
            if len(self._input_queues) > 0:
                self._queue_iter = iter(self._input_queues)
            else:
                raise queue.Empty

    def _retire_queue(self, queue_to_retire: CloseableQueue) -> None:
        """
        Removes exhausted queue from the list of input queues.
        No-op if the queue had already been exhausted.
        """
        with self.mutex:
            if queue_to_retire in self._input_queues:
                self._input_queues.remove(queue_to_retire)
                self._exhausted_queues.append(queue_to_retire)


class Producer:
    def __init__(self, output_queue=None):
        self._output_queue = output_queue or CloseableQueue()

    def close(self):
        self._output_queue.close()

    @property
    def output_queue(self):
        return self._output_queue


class Consumer:
    def __init__(self, input_queue: CloseableQueue):
        self._input_queue = input_queue

    @property
    def input_queue(self):
        return self._input_queue


class ConcurrentMapper(Producer, Consumer, threading.Thread):
    _TIMEOUT = 0.1

    def __init__(self,
                 mapper: typing.Callable,
                 input_queue: CloseableQueue,
                 output_queue: CloseableQueue=None,
                 thread_name:str=None):
        """
        Calls `transformation` on every item in `input_queue`
        Automatically takes care of closing the ouput_queue
        """
        Producer.__init__(self, output_queue)
        Consumer.__init__(self, input_queue)
        threading.Thread.__init__(self,
                                  target=self._map,
                                  name=thread_name or f"{self.__class__.__name__}-{randhex(8)}",
                                  daemon=False,
                                  group=None)
        self._transformation = mapper

    def _map(self):
        while True:
            try:
                input = self._input_queue.get(block=True, timeout=self._TIMEOUT)
                self._output_queue.put(self._transformation(input))
                self._input_queue.task_done()
            except queue.Empty:
                pass
            except Exhausted:
                break
        Producer.close(self)


class ConcurrentLoader(Producer, threading.Thread):

    QUEUE_SIZE_INFINITE = 0

    def __init__(self,
                 file_paths: typing.Collection[str],
                 loader: typing.Callable[[str], torch.Tensor],
                 output_queue: CloseableQueue,
                 thread_name:str=None):
        Producer.__init__(self, output_queue)
        self._loader = loader
        self._all_paths = file_paths
        self._loader_name = thread_name or f"{self.__class__.__name__}-{randhex(8)}"
        threading.Thread.__init__(self,
                                  target=self._load_all,
                                  name=self._loader_name,
                                  daemon=False,
                                  group=None)

    def _load_all(self) -> None:
        for path in self._all_paths:
            try:
                loaded_img = self._loader(path)
            except:
                continue # skip this image if exception occurred
            self._output_queue.put((path, loaded_img), block=True, timeout=None)
        Producer.close(self)


class ConcurrentBatcher(Consumer, Producer, threading.Thread):

    def __init__(self,
                 input_queue: CloseableQueue,
                 output_queue=None,
                 name=None,
                 batch_size:int=50,
                 batch_timeout:float=0.1):
        """
        Extracts objects from the input queue and gathers them in lists of size equal to `batch_size`
        If input queue remains empty for too long (longer than `batch_timeout`), Batcher will return a smaller batch
        consisting of items gathered so far
        """

        Consumer.__init__(self, input_queue)
        Producer.__init__(self, output_queue)
        self._thread_name = name or f"{self.__class__.__name__}{randhex(8)}"
        threading.Thread.__init__(self,
                                  target=self._gather,
                                  name=self._thread_name)
        self.__batch_size = batch_size
        self.__batch_timeout = batch_timeout

    def _gather(self) -> None:
        while not self._input_queue.is_exhausted():
            batch = []
            while len(batch) < self.__batch_size:
                try:
                    item = self._input_queue.get(block=True, timeout=self.__batch_timeout)
                    batch.append(item)
                    self._input_queue.task_done()
                except (queue.Empty, Exhausted):
                    break  # queue is empty - proceed to evaluate what has been collected so far
            self._output_queue.put(batch)
        Producer.close(self)


class ConcurrentTransformer(Consumer, Producer, threading.Thread):
    """
    Gives full control over handling both the input and output queues to the provided `transformer`
    Makes it possible to circumvent Pauseale and Terminable, though taking advantage of this possibility is will
    lead to runtime issues if Pause or Termination is ever used.
    """

    def __init__(self,
                 input_queue: CloseableQueue,
                 transformer: typing.Callable[[CloseableQueue, CloseableQueue], None],
                 output_queue: CloseableQueue=None,
                 name=None,
                 read_timeout:float=0.1):
        Consumer.__init__(self, input_queue)
        Producer.__init__(self, output_queue)
        self._thread_name = name or f"{self.__class__.__name__}{randhex(8)}"
        self._transformer = transformer
        threading.Thread.__init__(self,
                                  target=self._transform,
                                  name=self._thread_name)
        self.__read_timeout = read_timeout

    def _transform(self):
        self._transformer(self._input_queue, self._output_queue)
        Producer.close(self)


class ConcurrentFlattener(Consumer, Producer, threading.Thread):

    def __init__(self,
                 input_queue: CloseableQueue,
                 output_queue=None,
                 name=None,
                 read_timeout:float=0.1):
        Consumer.__init__(self, input_queue)
        Producer.__init__(self, output_queue)
        self._thread_name = name or f"{self.__class__.__name__}{randhex(8)}"
        threading.Thread.__init__(self,
                                  target=self._flatten,
                                  name=self._thread_name)
        self.__read_timeout = read_timeout

    def _flatten(self) -> None:
        while True:
            try:
                collection = self._input_queue.get(block=True, timeout=self.__read_timeout)
                if not isinstance(collection, (list, tuple, set)):
                    collection = [collection]
                for element in collection:
                    self._output_queue.put(element, block=True)
                self._input_queue.task_done()
            except queue.Empty:
                pass  # queue is empty - check the loop condition again and repeat
            except Exhausted:
                break
        Producer.close(self)


class ConcurrentGatherer(Consumer, threading.Thread):

    def __init__(self,
                 input_queues: typing.Union[typing.Collection[CloseableQueue], CloseableQueue],
                 output_container: typing.Container=None,
                 thread_name:str=None): 
        Consumer.__init__(self, input_queues)
        self._thread_name = thread_name or f"{self.__class__.__name__}{randhex(8)}"
        threading.Thread.__init__(self,
                                  target=self._gather,
                                  name=self._thread_name)
        self._result_container = [] if output_container is None else output_container

    def _gather(self):
        open_queues = tuple(filter(lambda q: not q.is_exhausted(), self.input_queue))
        while open_queues:
            for q in open_queues:
                try:
                    item = q.get(block=True, timeout=0.1)
                    self._result_container.append(item)
                except queue.Empty:
                    continue
                except Exhausted:
                    open_queues = tuple(filter(lambda q: not q.is_exhausted(), self.input_queue))

    def get(self):
        self.join()
        return self._result_container


class TqdmQueue:
    def __init__(self, queue: CloseableQueue, **tqdm_kwargs) -> None:
        self.queue = queue
        self.bar = tqdm(**tqdm_kwargs)
        self.inserted_items = 0
        self.mutex = threading.RLock()
        self._closed_event = threading.Event()

    def put(self, item, block: bool = ..., timeout: typing.Optional[float] = None) -> None:
        self.queue.put(item, block, timeout)
        with self.mutex:
            self.inserted_items += 1
            if not self.bar.disable:
                self.bar.set_description(f"{self.bar.desc.split(' ')[0]} ({self.inserted_items})")
            self.bar.update(1)

    def is_exhausted(self):
        return self.queue.is_exhausted()

    def task_done(self):
        self.queue.task_done()

    def is_closed(self) -> bool:
        return self.queue.is_closed()

    def close(self) -> None:
        with self.mutex:
            if not self.is_closed() and not self.bar.disable:
                self.bar.set_description(f"(X){self.bar.desc}")
        self._closed_event.set()
        self.queue.close()

    def get(self, block: bool = True, timeout: typing.Optional[float] = None):
        item = self.queue.get(block, timeout) # if queue.Empty is raised, it will be thrown further
        with self.mutex:
            self.bar.update(-1)
        return item


class TqdmList(list):

    def __init__(self, **tqdm_kwargs) -> None:
        super().__init__()
        self.bar = tqdm(**tqdm_kwargs)

    def append(self, __object) -> None:
        super().append(__object)
        self.bar.update(1)