import itertools as it
import typing as t

import h5py
import more_itertools as mit
import numpy as np
from hirise_tools.voronoi.hdf import extract_dimension_scales
from hirise_tools.voronoi.param_def import RANGE_CLASS_PARAMS

def remove_params(
    grouped_params: t.Dict[str, t.Dict[str, t.Any]],
    param_order: t.Sequence[str],
    params_to_remove: t.Collection[str],
) -> t.Tuple[
    t.Dict[str, t.Dict[str, t.Any]],
    t.Sequence[str]
]:
    """
    Removes a parameter from param dicts, modifying them in place
    """
    couplings_on_remove_list = tuple('+' in name for name in params_to_remove)
    if any(couplings_on_remove_list):
        raise ValueError(
            f"To remove coupled params, specify their names separately. "
            f"Triggered by: {couplings_on_remove_list}"
        )

    for removed_param_name in params_to_remove:
        for param_group in grouped_params.values():
            if removed_param_name in param_group:
                param_group.pop(removed_param_name)

        # region [remove a coupled param]
        # TODO: test: for 3 coupled params. remove 1, 2 and then all 3
        param_name_coupling = mit.only(
            (coupled_params_name
            for coupled_params_name
            in grouped_params.get('coupled', {}.keys())
            if removed_param_name in coupled_params_name),
            default=None
        )
        if param_name_coupling:
            param_names = param_name_coupling.split('+')
            param_values = grouped_params['coupled'].pop(param_name_coupling)
            removed_param_idx = param_names.index(removed_param_name)
            recoupled_names = '+'.join((*param_names[:removed_param_idx], *param_names[removed_param_idx+1:]))
            if recoupled_names:  # if there are any params left in the coupling
                # TODO: if 'coupled' is an OrderedDict, make sure the `recoupled` ends up in its original place
                param_values = np.hstack((param_values[:, :removed_param_idx], param_values[:, removed_param_idx+1:]))
                grouped_params['coupled'][recoupled_names] = param_values
        # endregion

    _not_a_removed_name = lambda name: name not in params_to_remove
    new_param_order = filter(_not_a_removed_name, param_order)
    # additional step: remove from within coupled
    new_param_order = [
        '+'.join(filter(_not_a_removed_name, name.split('+'))) if '+' in name else name
        for name
        in new_param_order
    ]
    # filter out couplings which have been completely emptied
    new_param_order = list(filter(lambda x: x, new_param_order))

    return grouped_params, new_param_order


def extract_fixed_params_from_dataset(dataset: h5py.Dataset) -> t.Dict[str, t.Any]:
    return {
        name.split('.', maxsplit=1)[-1]: value
        for name, value
        in dataset.attrs.items()
        if name.startswith('param.')
    }


def gather_params_from_config(config: t.Dict[str, t.Any]) -> t.Dict[str, t.Dict[str, t.Any]]:
    """
    Use a waterfall filter to group params into "cartesian", "coupled", "fixed" and "range" categories
    Returns a dict containing 4 dicts of grouped parameters, one for each category

    """
    # TODO: I could use `hypothesis` package to quickly test validation

    # other_items, param_order = mit.partition(lambda kv: kv[0] == "_order", config.items())
    other_items, coupled_param_items = mit.partition(lambda kv: '+' in kv[0], config.items())
    other_items, cartesian_param_items = mit.partition(lambda kv: isinstance(kv[1], (tuple, list, np.ndarray)), other_items)
    fixed_params, range_param_items = mit.partition(lambda kv: kv[0] in RANGE_CLASS_PARAMS, other_items)

    return dict(
        cartesian=dict(cartesian_param_items),
        coupled=dict(coupled_param_items),
        range=dict(range_param_items),
        fixed=dict(fixed_params),
    )


def gather_params_from_dataset(dataset: h5py.Dataset):
    """
    This is an "from HDF" counterpart to the `gather_params_from_config`
    """
    # FIXME: `include_fixed` is a quick hack meant ot get this to work ASAP
    scale_params = extract_dimension_scales(dataset, include_fixed=True)

    other_scales_items, _range_items = map(tuple, mit.partition(lambda kv: kv[0] in RANGE_CLASS_PARAMS, scale_params.items()))
    cartesian_items, coupled_items = map(tuple, mit.partition(lambda kv: '+' in kv[0], other_scales_items))
    fixed_params_dict = extract_fixed_params_from_dataset(dataset)

    # # TODO: test - weird any() bug found here
    return dict(
        cartesian = dict(cartesian_items),
        coupled = dict(coupled_items),
        range = {k: v for k, v in fixed_params_dict.items() if k in RANGE_CLASS_PARAMS},
        fixed = fixed_params_dict,
    )


def decouple_params(
    params: t.Dict[str, t.Sequence[t.Sequence[t.Any]]],
) -> t.Dict[str, t.Sequence[t.Any]]:
    """
    Uncouples parameters and returns a dict containing
    Value lists are simply unzipped. No deduplication or reordering is performed.

    Example:
        >>> decouple_params({"a+b": [(1, 'a'), (1, 'b'), (2, 'a')], 'c+d': [('A', 'B')]})
        {'a': [1, 1, 2], 'b': ['a', 'b', 'a'], 'c': ['A'], 'd': ['B']}
    """

    other_params, coupled_params = map(
        dict,
        mit.partition(
            lambda kv: '+' in kv[0],
            params.items()
        )
    )

    return {
        **other_params,
        **dict(zip(
            it.chain.from_iterable(names.split('+') for names in coupled_params.keys()),
            it.chain.from_iterable(zip(*values) for values in coupled_params.values())
        ))
    }


def couple_params(
    params: t.Dict[str, t.Sequence[t.Any]],
    couplings: t.Sequence[str],
) -> t.Dict[str, t.Sequence[t.Sequence[t.Any]]]:
    """
    Couples `params` according to `couplings`. Other params from `params` are included in the
    """
    coupled_params = {
        coupling: tuple(mit.zip_equal(*(params[name] for name in coupling.split('+'))))
        for coupling
        in couplings
    }
    params_to_couple = set(it.chain.from_iterable(names.split('+') for names in couplings))
    other_params = dict(filter(lambda kv: kv[0] not in params_to_couple, params.items()))
    return {
        **coupled_params,
        **other_params
    }


def load_img_cls_mapping(
    mapping_yaml: t.Union[
        t.Sequence[t.Tuple[str, int]],
        t.Dict[str, t.Union[int, t.Sequence[int]]]
    ]
) -> t.Sequence[t.Union[str, int]]:
    # TODO: test on {img: [clss]} mapping as well as [img, cls] and (erroneous) [img, [clss]]
    if isinstance(mapping_yaml, dict):
        img_class_pairs = []
        for img, class_idx in mapping_yaml.items():
            if isinstance(class_idx, (tuple, list)):
                img_class_pairs.extend((img, cls) for cls in class_idx)
            elif isinstance(class_idx, int):
                img_class_pairs.append((img, class_idx))
            else:
                raise TypeError(f"Unexpected type of class_idx: {type(class_idx)} - {class_idx}")

        return img_class_pairs
    elif isinstance(mapping_yaml, (tuple, list)):
        return mapping_yaml