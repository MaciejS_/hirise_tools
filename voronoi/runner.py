import math
import os.path
import time
import typing as t

import h5py
import numpy as np
import torch
import more_itertools as mit
from hirise_tools.dyncfg import DynamicJSONConfig

from hirise.device import device_from_torch_model
from hirise.utils import read_tensor
from hirise.voronoi.maskgen import VoronoiMaskGen

from hirise_nbutils import convenience
from hirise_tools.models import load_model_by_name, model_warmup
from hirise_tools.voronoi.paramsets import prepare_mapgen_paramsets
from hirise_tools.voronoi.hdf import validate_path_type_params
from hirise_tools.voronoi.spawn import (
    spawn_gridgen,
    spawn_meshgen,
    spawn_raw_renderer,
)
from hirise.voronoi.renderer import FixedGaussianBlur
from hirise.occlusions.maskgen import MaskGen
from hirise.occlusions.upsampling import ParallelizedUpsampler

from tqdm.autonotebook import tqdm


def dict_symdiff(dict_1: dict, dict_2: dict) -> dict:
    return {
        k: f"{dict_1.get(k, '<missing>')} != {dict_2.get(k, '<missing>')}"
        for k
        in set(dict_1.keys()) | set(dict_2.keys())
        if (
            k not in dict_1
            or k not in dict_2
            or dict_1[k] != dict_2[k]
        )
    }

def should_generate_new_batch_of_masks(paramset: t.Dict[str, t.Any], old_paramset: t.Dict[str, t.Any]) -> bool:
    """
    Returns True if change in paramset values warrants generation of a new set of masks.
    All parameters related to the renderer, as well mesh params and number of masks must trigger generation
    This leaves 'img' and 'cls' as the only parameters for which previously used set of masks can be reused.
    """
    if old_paramset is None:
        return True

    neutral_params = {"img", "cls", "evaluator.precision", "evaluator.model"}
    relevant_params = {k: paramset[k] for k in (set(paramset.keys()) - neutral_params)}
    relevant_old_params = {k: old_paramset[k] for k in (set(old_paramset.keys()) - neutral_params)}

    dicts_differ = relevant_params != relevant_old_params
    # if dicts_differ:
    #     print(dict_symdiff(relevant_params, relevant_old_params))

    return dicts_differ


def should_reload_model(paramset: t.Dict[str, t.Any], old_paramset: t.Dict[str, t.Any]) -> bool:
    if old_paramset is None:
        return True

    relevant_params = [k for k in paramset.keys() if k.startswith('evaluator.')]
    new_params = {k: paramset[k] for k in relevant_params}
    old_params = {k: old_paramset[k] for k in relevant_params}

    return new_params != old_params


def calc_peak_memory_use(paramsets: t.Sequence[t.Dict[str, t.Any]], dtype=torch.float32):
    """
    Returns the estimated peak memory use in bytes.
    """
    max_size = 0
    for ps in paramsets:
        size = max(ps["checkpoints"]) * ps["map_y"] * ps["map_x"]
        if size > max_size:
            max_size = size

    bytes_per_value = 4
    # times 2 because during concatenation, two copies of all masks will exist - one fragmented, one concatenated
    return max_size * bytes_per_value * 2


class HDFMapGen():
    """
    This class is a generic, self-contained mapgen runner, using the target HDF5 dataset to configure itself.
    It's safe to interrupt during computation - map generation will safely resume when run() is called again.
    """

    def __init__(self,
        device: torch.device,
        batch_size: int,
        use_model_warmups: bool = False,
        throttle: float = 0.0,
        dyncfg_path: os.path = "/tmp/"
    ):
        self.device = device
        self.batch_size = batch_size
        self.use_model_warmups = use_model_warmups
        self.throttle = throttle

        dyncfg_dict = dict(
            device=str(device),
            batch_size=batch_size,
            use_model_warmups=use_model_warmups,
            throttle=throttle
        )

        self.dyncfg = DynamicJSONConfig.new_config_file(dyncfg_path, dyncfg_dict, remove_on_close=True)
        print(f"Using dyncfg file: {self.dyncfg.config_filepath}")

    def _create_mask_provider(self, paramset: t.Dict[str, t.Any]) -> t.Callable[[int, t.Dict[str, t.Any]], torch.Tensor]:
        """
        Performs initialization of the mask provider components and adds them as fields of this object
        _prepare_mask_batch should know how to use these.
        It's a pretty sloppy approach because is modifies the state of
        """
        raise NotImplementedError("Needs to be overridden by concrete implementation")

    # the only actually relevant param is the dataset.
    # By encapsulating runtime params, I would give myself a neat way of implementing plugging dyncfg here.
    def run(self,
        dataset: h5py.Dataset,
        use_tqdm: bool = False,
    ):
        # NOTE: this entire logic used to be the `restore_from_hdf5` function
        validate_path_type_params(dataset, ['img'])
        paramsets, indexing_tuples = prepare_mapgen_paramsets(dataset)
        reuse_masks = dataset.attrs.get('param.mask_reuse', default=False)

        if not paramsets:
            print(" >> Didn't find any unprocessed paramsets, exiting.")
            return

        if reuse_masks:
            print("Mask reuse mode ON")
            peak_mem_use = calc_peak_memory_use(paramsets)
            print(f"Estimated peak memory use: {peak_mem_use / 1e+9:.2f}GB")
            print("Done!")
        else:
            print("Mask reuse mode OFF")

        prev_img = None
        prev_ps = None
        for ps, ndidx in tqdm(
            tuple(zip(paramsets, indexing_tuples)),
            desc="Paramsets",
            leave=True,
            disable=not use_tqdm
        ):
            def _update_archive(dataset, results, sd):
                dataset[ndidx] = results
                if sd and 'meta.mask_sets_used' in dataset.attrs:
                    dataset.attrs['meta.mask_sets_used'] = dataset.attrs.get('meta.mask_sets_used', 0) + 1
                return dataset

            # region [update runtime config from dyncfg]
            self.batch_size = self.dyncfg['batch_size']
            self.device = torch.device(self.dyncfg['device'])
            self.use_model_warmups = self.dyncfg['use_model_warmups']
            self.throttle = self.dyncfg['throttle']
            # endregion

            retainers = {
                cls_idx: convenience.create_retainer(
                    (ps['map_y'], ps['map_x']),
                    expected_p1=None,
                    chk_indices=[x - 1 for x in ps['checkpoints']]
                )
                for cls_idx
                in ps['cls']
            }

            # optimization - load the img only if it's different from the one in previous paramset
            if prev_img != ps['img']:
                try:
                    img = read_tensor(ps['img'])
                    prev_img = ps['img']
                except Exception as e:
                    print("Failed to load image")
                    print('img_name:', ps['img'])
                    raise e

            # reconfigure evaluator
            if should_reload_model(ps, prev_ps) or self.dyncfg.has_changed('device'):
                _precision = ps['evaluator.precision']
                _model_name = ps['evaluator.model']
                model, input_shape = load_model_by_name(_model_name, self.device)
                # print(f"{_model_name} loaded, precision: {_precision}, (equal to 'fp16'?: {_precision == 'fp16'}), (type: {type(_precision)})")

                evaluator = convenience.create_evaluator(
                    device=device_from_torch_model(model),
                    model=model,
                    batch_size=self.batch_size,
                    model_name=_model_name,
                    use_tqdm=False,
                    fp16_mode=(_precision == 'fp16')
                )

                # start warmup once model has been cast to fp16/fp32 by the evaluator init
                if self.use_model_warmups:
                    model_warmup(model, input_shape, self.batch_size, use_fp16=(_precision == 'fp16'))

            N = max(ps['checkpoints'])
            _should_generate_new_mask_set = (not reuse_masks) or should_generate_new_batch_of_masks(ps, prev_ps)
            if _should_generate_new_mask_set:
                _mask_provider = self._create_mask_provider(paramset=ps)

                old_masks = []
                # NOTE: this may cause
                for _ in range(int(math.ceil(N / self.batch_size))):
                    _masks = _mask_provider(self.batch_size, ps)
                    scores = evaluator.evaluate(img=img, masks=_masks, observed_classes=ps['cls'])
                    _masks_on_cpu = _masks.cpu().squeeze()
                    for cls_idx, class_scores in zip(ps['cls'], scores.T):
                        retainers[cls_idx].add_many(_masks_on_cpu, class_scores.cpu())
                    old_masks.append(_masks_on_cpu)
                    if self.throttle > 0:
                        time.sleep(self.throttle)
                old_masks = torch.cat(old_masks, dim=0)
            else:
                # evaluate using previously generated masks
                for n_low, n_high in evaluator.batch_boundaries(len(old_masks), self.batch_size):
                    _masks = old_masks[n_low:n_high].unsqueeze(1)  # make 4D
                    scores = evaluator.evaluate(img=img, masks=_masks, observed_classes=ps['cls'])
                    for cls_idx, class_scores in zip(ps['cls'], scores.T):
                        retainers[cls_idx].add_many(_masks.cpu().squeeze(), class_scores.cpu())
                    if self.throttle > 0:
                        time.sleep(self.throttle)

            results = torch.stack(
                [
                    torch.stack(tuple(zip(*ret.get_checkpoints()))[-1])
                    for ret
                    in retainers.values()
                ]
            )

            try:
                _update_archive(dataset, results, _should_generate_new_mask_set)
            except (KeyboardInterrupt, InterruptedError) as e:
                # stall interruption to let the update finish
                _update_archive(dataset, results, _should_generate_new_mask_set)
                raise e

            prev_ps = ps


class VoronoiHDFMapGen(HDFMapGen):

    def __init__(self,
        device: torch.device,
        batch_size: int,
        render_device: torch.device,
        throttle: float = 0.0,
        use_model_warmups: bool = False,
        dyncfg_path: os.path = "/tmp/"
    ):
        HDFMapGen.__init__(self, device, batch_size, use_model_warmups, throttle, dyncfg_path=dyncfg_path)
        self.render_device = render_device
        self.dyncfg['render_device'] = str(render_device)


    def _create_mask_provider(self, paramset: t.Dict[str, t.Any]) -> t.Callable[[int, t.Dict[str, t.Any]], torch.Tensor]:

        # print("Generating a new batch of masks")
        # generate new set of masks and evaluate
        renderer = spawn_raw_renderer(paramset, torch.device(self.dyncfg['render_device']))
        maskgen = VoronoiMaskGen(
            mesh_gen=spawn_meshgen(paramset),
            occlusion_selector=spawn_gridgen(paramset),
            renderer=renderer
        )
        blur = FixedGaussianBlur(renderer=renderer, sigma=paramset['renderer.blur_sigma'])

        # sequential yield_masks is used instead of batched generate_masks
        # because yield_masks uses meshes in a round-robin fashion, whereas generate_masks
        # exhausts the mask quota for the first mesh, before creating another mesh.
        # This causes non-representative results when `checkpoint_N <= N//M`
        for int_param in ('polygons', 'meshcount'):
            if paramset[int_param] % 1 != 0:
                raise ValueError(f"Got a non-integer value of '{int_param}': {paramset[int_param]}")
        mask_iterable = maskgen.yield_masks(
            M=int(paramset['meshcount']),
            s=int(paramset['polygons']),
            p1=paramset['p1'],
            target_shape=(paramset['map_y'], paramset['map_x'])
        )

        return lambda batch_size, ps: blur._blur_many(
            torch.stack(mit.take(n=batch_size, iterable=mask_iterable)),
            sigma=ps['renderer.blur_sigma']
        ).unsqueeze(1)  # make 4D


class RiseHDFMapGen(HDFMapGen):

    def __init__(self,
        device: torch.device,
        batch_size: int,
        n_threads: int,
        throttle: float = 0.0,
        use_model_warmups: bool = False,
        dyncfg_path: os.path = "/tmp/"
    ):
        HDFMapGen.__init__(self, device, batch_size, use_model_warmups, throttle, dyncfg_path=dyncfg_path)
        self.n_threads = n_threads
        self.dyncfg['n_threads'] = n_threads

    def _create_mask_provider(self, paramset: t.Dict[str, t.Any]) -> t.Callable[[int, t.Dict[str, t.Any]], torch.Tensor]:
        maskgen = MaskGen(
            grid_gen=spawn_gridgen(paramset),
            upsampler=ParallelizedUpsampler(
                pool_size=self.dyncfg['n_threads'],
                use_tqdm=False,
            )
        )

        if paramset['s'] % 1 != 0:
            raise ValueError(f"Got a non-integer value of 's': {paramset['s']}")

        return lambda batch_size, ps: maskgen.generate_masks(
            N=batch_size,
            s=int(ps['s']),
            p1=ps['p1'],
            target_size=np.asarray((ps['map_y'], ps['map_x']))
        )
