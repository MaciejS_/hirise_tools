import itertools
import typing as t

import h5py
import more_itertools as mit
import numpy as np
from hirise_tools.voronoi.hdf import extract_dimension_scales
from hirise_tools.voronoi.params import extract_fixed_params_from_dataset

def sanitize_bytestrings(param_value: t.Any) -> t.Any:
    if isinstance(param_value, bytes):
        return param_value.decode('utf-8')
    return param_value

def prepare_mapgen_paramsets(
    dataset: h5py.Dataset
) -> t.Tuple[
    t.Sequence[t.Dict[str, t.Any]],
    t.Sequence[t.Tuple[t.Union[int, t.Sequence[int], slice]]]
]:
    """
    Returns a tuple containing:
        1. A list of full parameter set dicts (includes all types of params, from cartesian to fixed) extracted from
            scales and attrs of the `dataset`
        2. A list of indexing tuples for `dataset`, which are meant to be used when storing results of evaluation of
            the corresponding paramset. `dataset[idx_tuple] = results` guarantees correct placement of results
    """

    scale_params = extract_dimension_scales(dataset)
    scale_order = tuple(scale_params.keys())

    indices = {
        dim.label: tuple(range(dim_len))
        for dim, dim_len, dim_maxlen
        in zip(dataset.dims, dataset.shape, dataset.maxshape)
        if dim_maxlen is None
    }

    # NOTE: I don't need to separate ranged params from cartesian - all that matters is that the number of combinations
    #       is correct and
    #       If there's a param irrelevant to the evaluator, like run_idx, the evaluator can simply not read it from the
    #       paramset and ignore it. It will carry out the correct number of runs, because that exact paramset will appear
    #       N times on the list, anyway

    # step 1 - optimize the params
    # a parameter optimization can apply any transformation to any single parameter if the following rules are kept:
    # 1. an optimization of parameter A must not affect parameter B (unless they are coupled)
    # 2. the transformation applied to the list of parameter values (usually grouping) needs to be applied in the same
    #    way to the indexing tuples for this parameter (this ensures that results will be stores at correct locations
    #    in the result archive)

    # step 1.1 - apply parameter optimizations (hard-coded for now)
    # step 1.1.1 - group all classes evaluated on given img
    group_classes_by_img(scale_params, indices)

    # step 1.1.2 - wrap the list of 'chk' values in a single list (to make it appear as a single value to itertools.product)
    #  and replace "chk" with slice(None, None) in indexing tuples to mirror the change applied to the list of paramsets
    #   why slice() instead of just [1]? Because the state of each checkpoint needs to be stored separately
    #   so one parameter set evaluation will generate N maps for N checkpoints
    #   prior to this optimization, we'd get N evaluations, each generating one map for each of N checkpoints
    collapse_checkpoints(scale_params, indices)

    # step 1.2
    # convert bytestring values to strings to prevent false negatives when param values are compared to string literals
    # TODO: minor bug: byte values within couplings won't be converted to strings because they are np.ndarrays or collection types in the isinstance() call
    scale_params = {
        k: list(
            v.decode('utf-8') if isinstance(v, bytes) else v
            for v
            in values
        )
        for k, values
        in scale_params.items()
    }

    # region step 2 - convert paramset as tuples to paramsets as dicts (to make it easier for the evaluator to select the correct values)
    paramsets_as_values = list(tuple(itertools.product(*scale_params.values())))
    indexing_tuples = list(tuple(itertools.product(*indices.values())))

    paramsets = [
        {
            param_name: param_value
            for param_name, param_value
            in zip(scale_order, _paramset_values)
        }
        for _paramset_values
        in paramsets_as_values
    ]
    # endregion step 2

    # region step 3 - split up the combined params within each paramset
    # this step doesn't change the number of paramsets, only rearranges their contents to a form easier to digest
    # by the evaluator. It is carried out after the cartesian product because otherwise coupled params would be treated
    # like regular cartesian params
    # the indexing tuples don't need to be changed because the values of parameters in each parameter set don't change
    split_up_coupled_params(paramsets, scale_order)
    # region step 3

    # region step 4 - include fixed params in paramsets (fixed params are retrieved from .attrs)
    fixed_params = extract_fixed_params_from_dataset(dataset)
    fixed_params.pop('runs')  # NOTE: this is a bugfix for param.runs overwriting all run indices in paramsets
    for paramset in paramsets:
        paramset.update(fixed_params)
    # region step 4

    # region step 5 - discard paramsets which have already been evaluated
    continuation_map = create_continuation_map(dataset, single_element_ndim=2)
    if not continuation_map.any():
        return [], [] # return both outputs as empty sequences if the continuation map shows nothing to recalculate

    unevaluated_paramsets, unevaluated_indexing_tuples = tuple(zip(*filter(
        lambda ps__idx: np.any(continuation_map[ps__idx[1]]),
        zip(paramsets, indexing_tuples)
    )))
    # endregion step 5

    return unevaluated_paramsets, unevaluated_indexing_tuples


def create_continuation_map(dataset, single_element_ndim):
    return np.apply_along_axis(
        func1d=lambda x: np.any(np.isnan(x)),
        axis=-1,
        arr=dataset[:].reshape(*dataset.shape[:-single_element_ndim], -1),
    )


def split_up_coupled_params(
    paramsets: t.Sequence[t.Dict[str, t.Any]],
    param_names: t.Sequence[str]
) -> None:
    """
    Breaks up coupled params into individual values labeled with their respevtive param names.
    This form is easier to digest for the evaluator and doesn't require hard-coding knowledge about which
    parameters have benn coupled and which ones not.

    `paramset` are modified in place
    """
    for coupled_param_name in filter(lambda param_name: '+' in param_name, param_names):
        individual_param_names = coupled_param_name.split('+')

        for paramset in paramsets:
            coupled_values = paramset.pop(coupled_param_name)
            paramset.update(zip(individual_param_names, coupled_values))


def collapse_checkpoints(scale_params: t.Dict[str, t.Sequence[t.Any]], indices: t.Dict[str, t.Sequence[int]]) -> None:
    """
    wrap the list of 'chk' values in a single list (to make it appear as a single value to itertools.product)
    and replace "chk" with slice(None, None) in indexing tuples to mirror the change applied to the list of paramsets
    why slice() instead of just [1]? Because the state of each checkpoint needs to be stored separately
    so one parameter set evaluation will generate N maps for N checkpoints
    prior to this optimization, we'd get N evaluations, each generating one map for each of N checkpoints

    """
    scale_params['checkpoints'] = [scale_params['checkpoints']]
    indices['checkpoints'] = [slice(None, None)]


def group_classes_by_img(scale_params: t.Dict[str, t.Sequence[t.Any]], indices: t.Dict[str, t.Sequence[int]]) -> None:
    """
    this optimization converts a flattened list of (img, cls_idx) into a grouped list of (img, (clsidx1, clsidx2))
    where each image occurs only once. The original entries are enumerated prior to transformation to ensure that
    the results of evaluation can still be stored at the right locations in the result tuple
    advanced indexing (e.g. (1, [1, 3, 8], 0, 12)) is used to fit grouped indices of grouped params in a single
    indexing tuple

    Both arguments are modified in place
    """
    preenumerated_img_cls_list = enumerate(scale_params['img+cls'])
    grouped_img_cls = list(
        mit.groupby_transform(
            iterable=sorted(preenumerated_img_cls_list, key=lambda i__img_cls: i__img_cls[1][0]),
            keyfunc=lambda i__img_cls: i__img_cls[1][0],  # group by img name/path
            valuefunc=lambda i__img_cls: (i__img_cls[1][1], i__img_cls[0]),  # gather (cls, idx) pairs
            # repackage the (cls, idx) pairs into ([cls, ...], [idx, ...]), sorted together by `idx` in ascending order
            # indices in ascending order is a requirement of h5py
            reducefunc=lambda cls_i_list: list(zip(*sorted(cls_i_list, key=lambda x: x[1])))
        )
    )
    scale_params['img+cls'] = [(img, tuple(map(int, clsidxs))) for img, (clsidxs, idxs) in grouped_img_cls]
    indices['img+cls'] = [list(idxs) for img, (clsidxs, idxs) in grouped_img_cls]


