import typing as t

import numpy as np
import torch
from hirise.occlusions.gridgen import (
    GridGen,
    FixingGridGen,
    PermutationGridGen,
    SmartCoordinateGridGen,
    ThresholdGridGen,
)
from hirise.voronoi.meshgen import (
    CheckerboardPatternSeeds,
    UniformRandomSeeds,
    VoronoiMeshGen,
)
from hirise.voronoi.renderer import FixedGaussianBlur, PillowOcclusionRenderer

CPU = torch.device("cpu")

def spawn_meshgen(paramset: t.Dict[str, t.Any]) -> VoronoiMeshGen:

    seedgen_types = {
        "UniformRandomSeeds": UniformRandomSeeds,
        "CheckerboardPatternSeeds": CheckerboardPatternSeeds,
    }

    meshgen_class = VoronoiMeshGen
    seedgen_class = seedgen_types[paramset['meshgen.seedgen']]

    meshgen_config = dict(
        seed_coordinate_provider=seedgen_class(ndim=2),
        incremental_mesh=False,
    )

    return meshgen_class(**meshgen_config)


def spawn_gridgen(paramset: t.Dict[str, t.Any]) -> GridGen:
    gridgen_type = paramset['gridgen.type']
    if gridgen_type == "fixing":
        return FixingGridGen(
            grid_gen=ThresholdGridGen(CPU),
            fixer_grid_gen=SmartCoordinateGridGen(CPU),
            fixing_threshold=paramset["gridgen.fixing_threshold"]
        )
    else:
        primitive_gridgen_types = {
            "threshold": ThresholdGridGen,

            "coord": SmartCoordinateGridGen,
            "coordinate": SmartCoordinateGridGen,

            "perm": PermutationGridGen,
            "permutation": PermutationGridGen,
        }
        return primitive_gridgen_types[gridgen_type](device=CPU)


def spawn_renderer(paramset, device):
    return FixedGaussianBlur(
        renderer=PillowOcclusionRenderer(
            device=device,
            fill_polygon=paramset['renderer.fill_polygons'],
            outline_type=paramset['renderer.outline'],
        ),
        sigma=paramset['renderer.blur_sigma']
    )

def spawn_raw_renderer(paramset, device):
    """
    Spawns renderer without decorators
    """
    return PillowOcclusionRenderer(
        device=device,
        fill_polygon=paramset['renderer.fill_polygons'],
        outline_type=paramset['renderer.outline'],
    )