import itertools as it
import os
import typing as t

import h5py
import more_itertools as mit
import numpy as np

from hirise_tools.voronoi.param_def import RANGE_CLASS_PARAMS, FIXED_PARAM_ATTR_PREFIX


def adjust_chunksize(chunksize: t.Sequence[int], arx_shape: t.Sequence[int]) -> t.Sequence[int]:
    return tuple(
        mit.padded(
            [min(a, b) for a, b in zip(chunksize[::-1], arx_shape[::-1])],
            fillvalue=1,
            n=len(arx_shape)
        )
    )[::-1]


def merge_datasets(
    target_group: h5py.Group,
    merged_ds_name: str,
    merged_dim: str,
    part_datasets: t.Sequence[h5py.Dataset],
    new_chunksize: t.Union[None, t.Sequence[int]] = None,
) -> h5py.Dataset:
    validate_part_datasets(merged_dim, part_datasets)
    combined_shape, combined_maxshape = get_combined_ds_shape(merged_dim, *part_datasets)

    # create dataset
    combined_ds = target_group.require_dataset(
        name=merged_ds_name,
        shape=combined_shape,
        dtype=part_datasets[0].dtype,
        fillvalue=part_datasets[0].fillvalue,
        maxshape=combined_maxshape,
        chunks=(
            # TODO: it might be better to raise instead of silently ignoring a faulty user-defined chunksize
            adjust_chunksize(new_chunksize, combined_shape)
            if isinstance(new_chunksize, (tuple, list)) and all(map(lambda x:isinstance(x, int), new_chunksize))
            else True
        )
    )

    # add attributes & labels
    for attr_name, attr_value in ((k, v) for k, v in part_datasets[0].attrs.items() if k.startswith(f"{FIXED_PARAM_ATTR_PREFIX}.")):
        combined_ds.attrs.create(attr_name, attr_value)
    for new_dim, old_dim in zip(combined_ds.dims, part_datasets[0].dims):
        new_dim.label = old_dim.label

    # copy the values
    offset = 0
    merged_dim_idx = [d.label for d in combined_ds.dims].index(merged_dim)
    for source_ds in part_datasets:
        merged_dim_size = source_ds.shape[merged_dim_idx]
        idx_tuple = tuple(
            slice(offset, offset + merged_dim_size) if i == merged_dim_idx else slice(None, None)
            for i, shape
            in enumerate(source_ds.shape)
        )
        combined_ds[idx_tuple] = source_ds[:]
        offset += merged_dim_size

    # update values of merged range params
    if merged_dim in RANGE_CLASS_PARAMS:
        # Actually, this case applies to all single-value dimension-spawning params, but there's only one ATM
        # Fixed params can't be merged on (because they don't have a dimension associated with them), so that
        # leaves us with only range params
        combined_ds.attrs[f'{FIXED_PARAM_ATTR_PREFIX}.{merged_dim}'] = combined_ds.shape[merged_dim_idx]

    return combined_ds


def merge_datasets_virtual(
    target_group: h5py.Group,
    merged_ds_name: str,
    merged_dim: str,
    part_datasets: t.Sequence[h5py.Dataset],
) -> h5py.Dataset:
    """
    Perform a virtual merge instead of a "real" one. The merged dataset will link to part_datasets
    instead of holding copies of their data.
    The _scales, however, are merged-by-copy for now.

    Constraints:
    * parts must have the same dimension ordering
    * part shapes must match along all dimensions other than `merged_dim`
    """
    validate_part_datasets(merged_dim, part_datasets)
    combined_shape, combined_maxshape = get_combined_ds_shape(merged_dim, *part_datasets)

    merged_dim_idx = [x.label for x in part_datasets[0].dims].index(merged_dim)
    merged_dim_part_lengths = [part.shape[merged_dim_idx] for part in part_datasets]
    merged_dim_slices = [
        slice(start, end)
        for start, end
        in mit.pairwise(np.cumsum([0] + merged_dim_part_lengths))
    ]

    # Assemble virtual dataset
    layout = h5py.VirtualLayout(
        shape=tuple(combined_shape),
        maxshape=tuple(combined_maxshape),
        dtype=part_datasets[0].dtype
    )
    for part, target_slice in zip(part_datasets, merged_dim_slices):
        # set up the target ds slicer
        slice_idxt = [slice(None, None)] * len(combined_maxshape)
        slice_idxt[merged_dim_idx] = target_slice

        layout[tuple(slice_idxt)] = h5py.VirtualSource(part)

    virtual_ds = target_group.create_virtual_dataset(merged_ds_name, layout, fillvalue=np.nan)

    # region [add labels and attributes]
    for attr_name, attr_value in ((k, v) for k, v in part_datasets[0].attrs.items() if k.startswith(f"{FIXED_PARAM_ATTR_PREFIX}.")):
        virtual_ds.attrs.create(attr_name, attr_value)
    for new_dim, old_dim in zip(virtual_ds.dims, part_datasets[0].dims):
        new_dim.label = old_dim.label

    # update values of merged range params
    if merged_dim in RANGE_CLASS_PARAMS:
        # Actually, this case applies to all single-value dimension-spawning params, but there's only one ATM
        # Fixed params can't be merged on (because they don't have a dimension associated with them), so that
        # leaves us with only range params
        virtual_ds.attrs[f'{FIXED_PARAM_ATTR_PREFIX}.{merged_dim}'] = virtual_ds.shape[merged_dim_idx]
    # endregion

    return virtual_ds

def merge_scales(target_scales_group: h5py.Group, merged_dim: str, part_scales_groups: t.Sequence[h5py.Group]) -> None:
    """
    Merges scale values for `merged_dim` from `part_scales_group`, in order of appearance, and places the result in `target_scales_group`
    Remaining _scales (i.e. other than `merged_dim`) from the first of `part_scales_groups` are copied over to `target_scales_group`

    The call is a no-op if `merged_dim` is one of the range class parameters (e.g. runs)

    :param target_scales_group: h5py.Group object dedicated to holding scales
    :param merged_dim: name of dataset dim (scale) to merge - if it isn't one of scale dime, this operation will only populate the _scales group
    :param part_scales_groups: sequence of _scales groups from part datasets
    """
    # copy all but the merged dataset
    for scale_name in set(part_scales_groups[0]) - {merged_dim}:
        target_scales_group.copy(
            source=part_scales_groups[0][scale_name],
            dest=target_scales_group,
            name=scale_name
        )

    if merged_dim in RANGE_CLASS_PARAMS:
        # no-op, as range params have no scales associated with them
        return

    part_scales = [scales_group[merged_dim] for scales_group in part_scales_groups]
    sum_of_scale_lengths = sum(map(len, part_scales))
    combined_scale = target_scales_group.create_dataset(
        name=merged_dim,
        shape=(sum_of_scale_lengths, *part_scales[0].shape[1:]),
        dtype=part_scales[0].dtype,
    )

    N = 0
    for part in part_scales:
        part_len = len(part)
        combined_scale[N:N+part_len] = part[:]
        N += part_len

def get_default_combined_file_name(part_file_names: t.Sequence[str]):
    split_up_names = split_up_partfile_names(part_file_names)

    prefixes, partidxs, suffixes = map(tuple, it.zip_longest(*split_up_names))

    # `prefixes` and `suffixes` should onmly contain one value each
    combined_file_name = '.'.join((prefixes[0], suffixes[0]))

    return combined_file_name


def validate_part_datasets(merged_dim: str, part_datasets: t.Sequence[h5py.Dataset]):
    issues = []

    # check param order equals
    param_orders = [tuple(dim.label for dim in ds.dims) for ds in part_datasets]
    unique_param_orders = set(param_orders)
    if len(unique_param_orders) > 1:
        order_part_mapping = {
            ordering: [i for i, po in enumerate(param_orders) if po == ordering]
            for ordering
            in unique_param_orders
        }

        issues.append(
            f"Merging parts with differring param orders is not supported. "
            f"Found {len(unique_param_orders)} unique param_orders: {order_part_mapping}"
        )

    # all dimensions other than the merged one must have matching lengths
    _dim_labels = [dim.label for dim in part_datasets[0].dims]
    _dim_length_values = tuple(map(set, zip(*(ds.shape for ds in part_datasets))))
    _labelled_dim_lengths = mit.zip_equal(_dim_labels, _dim_length_values)
    _dims_with_differing_lengths = {
        name: lengths
        for name, lengths
        in _labelled_dim_lengths
        if len(lengths) > 1 and name != merged_dim # ignore length differences in merged_dim
    }
    if _dims_with_differing_lengths:
        issues.append(f"Found other dimensions varying in size across partfiles: {_dims_with_differing_lengths}")

    # check whether the the contents of all scales other than the merged one are identical
    # not doing so would lead to data corruption by mislabeling of the parts coming from differently labelled datasets
    if len(part_datasets) > 1:
        part_dataset_scales = [ds.parent['_scales'] for ds in part_datasets]
        _n_parts = len(part_datasets)
        _param_order = param_orders[0]
        conflicting_scales = tuple(it.chain.from_iterable(
            [
                f"{dim_name}, between parts: {(y, x)}"
                for y, x
                in zip(*np.triu_indices(n=_n_parts, k=1))
                if not np.all(part_dataset_scales[y][dim_name][:] == part_dataset_scales[x][dim_name][:])
            ]
            for dim_name
            in param_orders[0]
            if (dim_name in part_dataset_scales[0]) and (dim_name not in (merged_dim, 'runs'))
            # `merged_dim` is skipped because it's being merged on.
            # `runs` are skipped because they don't have a scale object associated with them
            # additionally, if there's a mismatch in the number of runs, then the earlier validation would raise (part shape mismatch)
            # however, if merged parts come from different runs (and the experiment was run-sensitive, e.g. FP16
            # experiment, which had to use just a single set of mask across the entire run and mixing results from two
            # runs would confuse the analysis), all of the parts would contain `0` as their run index, because mapgen
            # doesn't allow arbitrary run index ranges or values. In other words, if the result archive is supposed to contain
            # run 3, then this information has to be contained in the name, because nothing in the archive will indicate which
            # run it belongs to, and will only be assigned `run_idx == 3` when it's merged with archives from other runs.
        ))
        if conflicting_scales:
            formatted_list_of_conflicts = '\n'.join(conflicting_scales)
            issues.append(f"Found {len(conflicting_scales)} unmerged scale conflicts: \n{formatted_list_of_conflicts}")

    # TODO: When the trailing dimension alignment logic is added, then validator should make sure that the scales
    #  don't differ in terms of their content. Reordered entries are fine, but if there's a symmetrical diff between
    #  the lists, then merge should not be even started.
    #   * Relax the logic above to raise only if there are differences in contents (rn it checks content+ordering)

    if issues:
        raise RuntimeError(
            "Partial dataset validation failed due to the following issues:\n"
            + "\n  * ".join(issues)
        )


def get_combined_ds_shape(merged_dim, *part_datasets):
    param_orders = [tuple(dim.label for dim in ds.dims) for ds in part_datasets]
    merge_dim_idx = mit.one({x.index(merged_dim) for x in param_orders})

    total_length_after_merge = sum(ds.shape[merge_dim_idx] for ds in part_datasets)
    new_shape = list(part_datasets[0].shape)
    new_shape[merge_dim_idx] = total_length_after_merge

    new_maxshape = [
        new_shape_dim if old_maxshape_dim is not None else None
        for new_shape_dim, old_maxshape_dim
        in zip(new_shape, part_datasets[0].maxshape)
    ]

    return new_shape, new_maxshape


def split_up_partfile_names(partfile_names):
    return [
        tuple(
            map(
                lambda tokens: '.'.join(tokens),
                mit.split_at(
                    os.path.split(fname)[-1].split('.'),
                    lambda token: token.isdigit(),  # split at the part idx
                    keep_separator=True
                )
            )
        )
        for fname
        in partfile_names
    ]