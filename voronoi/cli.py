import argparse
import os.path
import socket

import torch

def add_runtime_arguments(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    parser.add_argument(
            '-b',
            '--batch_size',
            type=int,
            default=100,
            help="Number of masks evaluated on GPU in parallel. Limited by the amount of VRAM"
    )
    parser.add_argument(
            '--device',
            type=torch.device,
            default='cuda:0',
            help="Torch-compatible name of a device used for computation. E.g. 'cpu', 'cuda:0'"
    )
    parser.add_argument(
            '--throttle',
            type=float,
            default=0.0,
            help="Throttling delay (in seconds) applied after each paramset. Default is 0"
    )
    parser.add_argument(
        '--dyncfg-path',
        type=os.path.abspath,
        default=(
            os.path.expanduser("~/tmp/")
            if socket.gethostname() == "ai"
            else "/tmp/"
        ),
        help="Path to dynamic config file. "
             "If this path is a directory, a randomly named config file will be created within."
             "Defaults to system-wide /tmp on private hosts and ~/tmp on shared machines"
    )

    return parser

def add_common_arguments(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    parser.add_argument('-q', '--quiet', action='store_true', help="Do not display progress bars")
    return parser

def add_adhoc_params(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    """
    Ad-hoc parameters are frequently overridden runner params, otherwise specifiable via YAML config.
    """

    parser.add_argument(
        '--precision',
        type=str,
        choices=['fp16', 'fp32'],
        default='fp32',
        help='Floating point used during evaluation by model'
    )

    parser.add_argument(
        '--storage-precision',
        type=str,
        choices=['fp16', 'fp32'],
        default='fp32',
        help='Flaot precsion used to store subtypes. FP16 archives are ~2x smaller'
    )
    return parser

def add_mapgen_specific_runtime_args(parser: argparse.ArgumentParser):
    parser.add_argument(
        '--use-warmups',
        required=False,
        action='store_true',
        default=False,
        help="If set, each model will be warmed up right after loading byt running a single batch of random inputs."
             " Useful when profiling, to separate model initialization overhead from real mask evaluation cost."
    )

    # parser.add_argument(
    #     '--round-robin',
    #     required=False,
    #     action='store_true',
    #     default=False,
    #     help="(If meshcount > 1) Uses meshes in a round-robin manner. Affects state maps at checkpoints. "
    #          "Without round-robin, all masks from first mesh are evaluated before moving on to next mesh "
    #          "in cases where distance between checpoints is less than number of masks per mesh, initial checkpoints "
    #          "will contain results of evaluating a single mesh. With round-robin on, the result will consist of "
    #          "masks generated from `min(n_mesh, N_at_checkpoint)` meshes"
    # )
    #
    # parser.add_argument(
    #     '--mask-batch',
    #     required=False,
    #     type=int,
    #     default=1000,
    #     help="Number of masks generated before moving on to evaluation. "
    #          "Should be a multiple of --batch for optimal performance. "
    #          "Limited by amount of RAM, and in case of rendering on GPU, VRAM."
    # )