import collections
import os
import typing as t
from contextlib import contextmanager

import more_itertools as mit
import numpy as np
import h5py
from hirise_tools.voronoi.param_def import LIMITED_DIM_RANGE_PARAMS


def extract_dimension_scales(
    dataset: h5py.Dataset,
    include_fixed: bool = False,
) -> t.Dict[str, t.Any]:
    """
    Gathers parameter information from `dataset` and returns it as a dict of {param_name: param_values} pairs
    If `include_fixed` is True, returns scales for all dimensions, otherwise only for parameters associated with extensible dimensions (maxshape == None)
    Values for 'scale-less' params are provided as `np.arange(param_value)`
    Returns a dict of param_name: param_values pairs
    """
    if '_scales' not in dataset.parent:
        raise ValueError(f"The parent group of dataset {dataset.name} must contain a '_scales' group")
    scales_group = dataset.parent['_scales']

    param_value_lists = collections.OrderedDict((
        (
            dim.label,
            (
                scales_group[dim.label][:]
                if dim.label in scales_group
                else np.arange(dataset.attrs.get(f'param.{dim.label}'))
            )
        )
        for dim, dim_maxlen
        in zip(dataset.dims, dataset.maxshape)
        if include_fixed or (dim_maxlen is None)
    ))

    return param_value_lists


def create_scales(
    base_group: h5py.Group,
    scale_params: t.Dict[str, t.Any] = None,
) -> h5py.Group:
    """
    Adds each of entries in "scale_params" as a dataset to `scales_group`
    Parameters with string values are supported and converted using a special HDF5 variable len string type
    """
    scales_group = base_group.require_group('_scales')
    for param_name, param_values in scale_params.items():
        if not isinstance(param_values, (tuple, list, np.ndarray)):
            raise TypeError(f"Non-sequence param {param_name} can't be used to create a scale")
        values_ndarray = np.asarray(param_values)
        maxshape = (None, )
        if '+' in param_name:
            # this applies only to coupled params
            param_names = param_name.split('+')
            maxshape = (None, len(param_names))

        if any(map(lambda x: isinstance(x, str), values_ndarray.ravel())):
            # create a variable-length string dtype
            dtype = h5py.string_dtype(encoding='utf-8')
            values_ndarray = values_ndarray.astype(str)
        else:
            dtype = values_ndarray.dtype

        scale = scales_group.require_dataset(
            param_name,
            shape=values_ndarray.shape,
            maxshape=maxshape,
            dtype=dtype,
        )
        scale[:] = values_ndarray
        scale.make_scale(param_name)

    return scales_group


def label_dataset(
    dataset: h5py.Dataset,
    param_order: t.Sequence[str],
):
    """
    Add labels to `dataset`s dimensions, according to `param_order`
    Parameter names from `param_order` are used to label the dimensions.
    If there's a scale of the same name in `scales`, then it will be attached to corresponding dimension
    This function raises if length of `param_order` doesn't match the number of dimensions
    """
    #TODO: coverage - found a bug here
    if len(dataset.dims) != len(param_order):
        raise ValueError("Expected list of labels and list of dataset dims to be the same length, got: "
                         f"{len(dataset.dims)} and param_order: {len(param_order)} ({param_order})")

    for dim, param_name in zip(dataset.dims, param_order):
        dim.label = param_name

    return dataset


def create_attributes(
    h5py_object: h5py.HLObject,
    fixed_params: t.Dict[str, t.Union[str, int, float]],
    range_params: t.Dict[str, t.Union[str, int, float]],
    **other_attrs,
) -> h5py.HLObject:
    all_attrs = {
        **fixed_params,
        **range_params,
        **other_attrs
    }

    for name, value in all_attrs.items():
        h5py_object.attrs.create(f"param.{name}", data=value)

    return h5py_object


def create_dataset(
    group: h5py.Group,
    name: str,
    dimension_params: t.Dict[str, t.Any],
    param_order: t.Sequence[str],
    dtype: type = np.float32,
    fillvalue = np.nan,
) -> h5py.Dataset:
    """
    Creates a dataset of shape determined by `dimension_params`.
    """
    maps_dataset = group.create_dataset(
        name=name,
        shape=[
            len(param_values) if isinstance(param_values, (list, tuple, np.ndarray)) else param_values
            for param_name, param_values
            in [(k, dimension_params[k]) for k in param_order]
        ],
        dtype=dtype,
        fillvalue=fillvalue,
        maxshape=[
            None if name not in LIMITED_DIM_RANGE_PARAMS else dimension_params[name]
            for name
            in param_order
        ]
    )
    return maps_dataset


def initialize_result_dataset(
    target_group: h5py.Group,
    grouped_params: t.Dict[str, t.Dict[str, t.Union[t.Any, t.Sequence[t.Any]]]],
    dim_params_order: t.Sequence[str],
    dataset_name: str,
    result_dtype = np.float32,
) -> h5py.Dataset:
    """
    Creates and configures the dataset for results.
    Creates the dataset, dimension scales, labels dimensions, adds fixed params as dimensions.
    Returns the configured dataset.
    """

    params = grouped_params
    create_scales(
        base_group=target_group,
        scale_params={
            **params['cartesian'],
            **params['coupled'],
        }
    )

    all_provided_params = {
        **params['cartesian'],
        **params['coupled'],
        **params['fixed'],
        **params['range'],
    }

    games_dataset = create_dataset(
        group=target_group,
        name=dataset_name,
        dimension_params=dict(filter(lambda kv: kv[0] in dim_params_order, all_provided_params.items())),
        param_order=dim_params_order,
        dtype=result_dtype,
    )

    create_attributes(
        games_dataset,
        fixed_params=params['fixed'],
        range_params=params['range'],
    )

    label_dataset(
        dataset=games_dataset,
        param_order=dim_params_order,
    )

    return games_dataset


@contextmanager
def open_hdf_archives(
    results_path: t.Optional[t.Union[str, os.PathLike]],
    maps_path: t.Optional[t.Union[str, os.PathLike]],
    create_if_missing: bool = False
):
    """
    Opens `results_path` and `maps_path` HDF5 archives, which may happen to be the same archive.
    If `create_if_missing` is True, `results_path` will be created if it doesn't exist
    """
    maps_path = maps_path
    results_path = results_path

    maps_file, results_file = None, None
    # FIXME: if I pass the same file as symlink, this code will think it's not the same file
    #       this can be solved with os.path.samefile
    try:
        if maps_path and results_path:
            if maps_path != results_path:
                # maps and results are two separate files maps can be opened in read-only mode
                # SWMR mode is used to allow parallel access to maps by multiple processes
                maps_file = h5py.File(maps_path, 'r', swmr=True)
                results_file = h5py.File(results_path, 'w-' if create_if_missing else 'r+')
            else:
                # maps and results are the same file - open in write-enabled mode and return
                # the same handle as both files. File must exist.
                maps_file = h5py.File(maps_path, 'r+')
                results_file = maps_file
        elif maps_path or results_path:
            # only one path provided: implicit 'same file' mode - this file will also be the source
            # of data for the evaluation, so it must exist, hence always 'r+'
            results_file = h5py.File(maps_path or results_path, 'r+')
            maps_file = results_file
        else:
            raise ValueError("Results path must always be provided ")

        yield (results_file, maps_file)
    finally:
        if results_file:
            results_file.close()
        if maps_file:
            maps_file.close()


def validate_path_type_params(dataset: h5py, path_params: t.Sequence[t.Union[str, os.PathLike]]) -> None:
    """
    Locates all values of `path_params` (also if they're coupled with other params) and checks whether
    their values point to existing files.
    Error message contains all paths that don't exist.
    """
    missing_paths = []
    for param_name in path_params:
        if param_name in dataset.attrs:
            img_abspaths = [dataset.attrs[param_name]]
        else:
            scales = extract_dimension_scales(dataset)
            param_dim_name = mit.only(dim.label for dim in dataset.dims if param_name in dim.label)
            if param_dim_name == param_name:
                # img is a cartesian param
                img_abspaths = scales[param_dim_name][:]
            else:
                # img is coupled with other params
                img_param_idx = param_dim_name.split('+').index(param_name)
                img_abspaths = (param_values[img_param_idx] for param_values in scales[param_dim_name][:])

        missing_paths.extend(filter(lambda path: not os.path.exists(path), img_abspaths))

    if missing_paths:
        raise RuntimeError(
            f"  {len(missing_paths)} missing images: "
            + '\n    '.join(map(str, missing_paths))
        )