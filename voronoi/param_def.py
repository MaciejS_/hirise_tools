FIXED_PARAM_ATTR_PREFIX = 'param'

RANGE_CLASS_PARAMS = {"runs", "map_y", "map_x", "steps"}
LIMITED_DIM_RANGE_PARAMS = {"map_y", "map_x", "steps"}
COMMON_PARAMS = {
    # model and data
    "evaluator.model": str,  # choices: resnet50, vgg16, etc
    "evaluator.class_set": str,  # choices: imgnet, coco, pascal etc

    "runs": int,  # actually it CANNOT be a collection
    "checkpoints": int,
    # "N": int - N is inferred from max(checkpoints)

    "p1": float,

    "gridgen.type": str,  # [threshold/permutation/combination/fixing] (where fixing is threshold+"smart coord")
    "gridgen.fixing_threshold": float,

    "img": str,
    "cls": int,
}
MAP_RUNNER_PARAMS = {
    # OUTPUT size (which theoretically can be different from input size)
    # however, this will be similar for
    "map_y": int,
    "map_x": int,
}
AUC_RUNNER_PARAMS = {
    "game": str, # choices - "blur/deblur/remove/introduce"
    "steps": int,
}
RISE_RUNNER_PARAMS = {
    "s": int,
}
VRISE_RUNNER_PARAMS = {

    # VRISE-specific
    "polygons": int,
    "meshcount": int,

    # rendering - limited to default VRISE pipeline (gaussian blur, no shifts)
    "renderer.outline": str,
    "renderer.fill_polygons": bool,
    "renderer.blur_sigma": float,

    # voronoi mesh generator params
    "meshgen.seedgen": str,  # choose from class name
}

REUSE_COMPATIBLE_PARAMS = {
    "img", "cls", "evalautor.precision", "evaluator.model", "evaluator.class_set"
}