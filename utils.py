import itertools
import os
import pickle
import random
import types
from typing import Union, BinaryIO, Iterable

import numpy as np

def walk_files(root_dir: Union[str, os.PathLike]) -> Iterable[str]:
    """
    Recursively walks the directory and returns paths to all files located in `root_dir` tree
    Paths are returned as `${root_dir}/${root_relative_file_path}`
    :param root_dir:
    :return:
    """
    return itertools.chain.from_iterable(
                [[os.path.join(path, f) for f in files]
                    for path, dirs, files
                    in os.walk(root_dir)
                    if files]
    )

def create_output_path(user_provided_abspath, default_file_name):
    if user_provided_abspath:
        if os.path.isdir(user_provided_abspath):
            output_path = os.path.join(user_provided_abspath, default_file_name)
        else:
            output_path = user_provided_abspath
    else:
        output_path = os.path.abspath(default_file_name)
    return output_path


def save_npz(filepath, results_dict):
    path, extension = filepath.rsplit('.', 1)
    np.savez_compressed(path + ".npz", **results_dict)

def save_pkl(filepath, results_dict):
    path, extension = filepath.rsplit('.', 1)
    with open(path + ".pkl", 'wb') as outfile:
        pickle.dump(obj=results_dict, file=outfile)


def load_from_archive(filepath):
    path, extension = filepath.rsplit('.', 1)
    if extension in ("pkl", "pckl"):
        with open(filepath, 'rb') as infile:
            return pickle.load(infile)
    elif extension == "npz":
        arx = np.load(filepath, allow_pickle=True)
        arx_dict = {k: arx[k] if len(arx[k].shape) else arx[k].item()
                    for k
                    in arx.files}
        return arx_dict
    else:
        raise ValueError(f"\'{extension}\' is not a supported extension.")


def validate_keyset(expected_keyset: set, actual_keyset: set, allow_optionals=False):
    """
    Compares the provided sets and raises KeyError if they aren't identical.
    if `allow_optionals=True`, then additional keys in `actual_keyset` are allowed and exception won't be raised unless some of `expected_keys` are missing

    :param expected_keyset: expected set of keys
    :param actual_keyset: actual set of keys
    :param allow_optionals:
    :return:
    """

    missing_keys = expected_keyset.difference(actual_keyset)
    unexpected_keys = actual_keyset.difference(expected_keyset)

    if missing_keys or (unexpected_keys and not allow_optionals):
        missing_keys_message = f"missing expected keys: {missing_keys}" if missing_keys else ""
        unexpected_keys_message = f"unexpected keys: {unexpected_keys}" if (unexpected_keys and not allow_optionals) else ""

        raise KeyError(f"Key set doesn't match: {unexpected_keys_message} {missing_keys_message}")


def load_experiment(main_filepath, refs_filepath=None, aucs_filepath=None, ref_aucs_filepath=None, scores_only=False):
    parent_dir, archive_filename = os.path.split(main_filepath)
    main_filename, _ = archive_filename.split('.', maxsplit=1)

    score_archives = {"alt": aucs_filepath or os.path.join(parent_dir, main_filename + ".auc.pkl"),
                      "ref_alt": ref_aucs_filepath or os.path.join(parent_dir, main_filename + ".ref.auc.pkl")}

    map_archives = {"hirise": main_filepath,
                    "ref": refs_filepath or os.path.join(parent_dir, main_filename + ".ref.pkl")}

    archives = score_archives
    if not scores_only:
        archives.update(map_archives)

    return {k: load_from_archive(v) for k, v in archives.items()}


def abspath_exists(path: str) -> str:
    """
    Returns absolute path of `path` if `path` exists.
    Otherwise RuntimeError is raised

    :param path: str a filepath
    :return: str, a
    """
    abspath = os.path.abspath(path)
    if not os.path.exists(abspath):
        raise RuntimeError(f"{abspath} does not exist")
    return abspath


def parent_exists(path: str) -> str:
    abspath = os.path.abspath(path)
    parent, file = os.path.split(abspath)
    if not (os.path.exists(parent) and os.path.isdir(parent)):
        raise RuntimeError(f"Parent directory of {abspath} does not exist")
    return abspath

def preallocated_file(path: Union[str, bytes, os.PathLike, int],
                      initial_size: int,
                      buffering=-1,
                      errors=None) -> BinaryIO:
    """
    Opens a new file for writing ('wb' mode), flushes  `initial_size` bytes of random data to disk and rewinds the writing caret to the beginning of the file
    On close, the file is truncated to release the remaining preallocated space.

    :param path: str, path to file
    :param size_bytes: positive int, amount of space to preallocate (in bytes)
    :param buffering: passed directly to open() - refer to its documentation for details
    :param errors: passed directly to open() - refer to its documentation for details
    :return: open file handle
    """

    if initial_size < 0:
        raise ValueError("Preallocation size must be positive")

    def add_truncate_before_closing(file_object):
        def truncate_before_close(self):
            self.truncate()
            self.original_close()

        setattr(file_object, "original_close", file_object.close)
        setattr(file_object, "close",  types.MethodType(truncate_before_close, file_object))
        return file_object

    new_file_handle = add_truncate_before_closing(open(path, mode='wb', buffering=buffering, errors=errors))

    new_file_handle.write(os.urandom(initial_size)) # use random data to prevent creation of a sparse file
    new_file_handle.flush() # makes sure that physical disk blocks are allocated right away
    new_file_handle.seek(0)

    return new_file_handle


def randhex(len=8):
    """
    Returns a random hex string.
    :return:
    """
    return "%x" % random.getrandbits(4 * len)

def hexhash(obj, len=8):
    return ("%x" % hash(obj))[-len:]