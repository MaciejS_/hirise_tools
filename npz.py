import numpy as np


def load_from_npz(path):
    try:
        with np.load(path, allow_pickle=True) as npz:
            return {k: npz[k] for k in npz.files}
    except Exception as e:
        print(f"Exception occurred while processing {path}")
        print(e)

def add_field_to_npz(npz_file_path, key, value, overwrite=False):
    archive_copy = load_from_npz(npz_file_path)

    if overwrite or (key not in archive_copy):
        archive_copy[key] = value
    else:
        raise KeyError(f"Key \'{key}\' already exists")

    np.savez_compressed(npz_file_path, **archive_copy)