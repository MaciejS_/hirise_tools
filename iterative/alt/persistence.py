from datetime import datetime

from hirise_tools.iterative.persistence import get_maps_archive_type


def make_auc_v1_archive(alt_game_results, original_archive, input_archive_file_name, start_date, description=None):
    """

    :param ins_del_scores_dict: List of {"ins", "del"} dicts,  Tensors, following the same layout as original maps tensor (except for the last 2 dimensions, which are reduced to a vector of length equal to number of game steps)
    :param compact_param_dict: pre-cartesian parameter dictionary
    :param input_archive_file_name:
    :param start_date:
    :return:
    """
    param_dict = original_archive['params']

    return {"about": {"version": f"a1-{get_maps_archive_type(original_archive)}",
                      "type": "HiRISE game scores archive v1"},
            "source_file_name": input_archive_file_name,
            "description": description,
            "params": param_dict,
            "scores": alt_game_results,
            "layout": f'{{"game_type": {original_archive["layout"]}}}',
            "date": {"start": start_date.isoformat(),
                     "end": datetime.now().isoformat()},
            "readme": "Contains Alteration Game scores for all maps retrieved from the source archive. "
                      "Params have been copied from the original archive."}