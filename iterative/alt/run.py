from typing import List, Dict, Any

import numpy as np
import torch
from hirise_tools.iterative.params import params_for_expr, v2_full_param_set
from hirise_tools.iterative.refs.params import param_sets_from_ref_params
from tqdm.autonotebook import tqdm

def run_games_on_maps_tensor(maps_tensor: torch.Tensor,
                             img:torch.Tensor,
                             observed_class: int,
                             games: dict) -> Dict[str, torch.Tensor]:
    results = {}
    hypershape = maps_tensor.shape[:-2]
    for game_name, game in tqdm(list(games.items()), leave=False, desc="games"):
        game_results = []
        for ndidx in tqdm(list(np.ndindex(hypershape)), leave=False, desc=f"maps ({game_name})"):
            game_results.append(torch.as_tensor(game(img=img,
                                                     smap=maps_tensor[ndidx],
                                                     observed_class=observed_class)))
        results[game_name] = torch.stack(game_results).reshape(*hypershape, -1).cpu()
    return results


def run_game_for_paramsets(maps: List[torch.Tensor],
                           paramsets: List[Dict[str, Any]],
                           games:dict,
                           leave_tqdm=True) -> Dict[str, List]:
    all_results = []
    for param_set, map_tensor in tqdm(list(zip(paramsets, maps)), desc="param_sets", leave=leave_tqdm):
        games_results = run_games_on_maps_tensor(maps_tensor=map_tensor,
                                                 img=param_set["img"],
                                                 observed_class=param_set["class"],
                                                 games=games)
        all_results.append(games_results)
    rearranged_results = {game_name: [result[game_name] for result in all_results]
                          for game_name
                          in games.keys()}
    return rearranged_results


def game_for_v1(archive: Dict[str, Any], games: Dict[str, Any]) -> Dict[str, List]:
    param_sets = params_for_expr(variable_params=archive['params']['variable'],
                                 param_order=archive['params']['order'],
                                 fixed_params=archive['params']['fixed'])
    return run_game_for_paramsets(maps=archive['maps'], paramsets=param_sets, games=games)


def game_for_v2(archive: Dict[str, Any], games: Dict[str, Any]) -> Dict[str, List]:
    iteration_grouped_maps = archive['maps']
    iteration_grouped_paramsets = v2_full_param_set(iteration_params=archive['params']['iteration'],
                                                    variable_params=archive['params']['variable'],
                                                    param_order=archive['params']['order'],
                                                    fixed_params=archive['params']['fixed'])

    results = [run_game_for_paramsets(maps=maps, paramsets=iteration_param_sets, leave_tqdm=False, games=games)
               for (iteration_param_sets, maps)
               in tqdm(list(zip(iteration_grouped_paramsets, iteration_grouped_maps)), desc="iterations")]

    rearranged_results = {game_name: [iteration_games[game_name] for iteration_games in results]
                          for game_name in games.keys()}
    return rearranged_results


def game_for_r1(archive: Dict[str, Any], games: Dict[str, Any]) -> Dict[str, List]:
    param_sets = param_sets_from_ref_params(param_clusters=archive['params']['clusters'],
                                            param_order=archive['params']['order'],
                                            fixed_params=archive['params']['fixed'])

    return run_game_for_paramsets(maps=archive['maps'],
                                  paramsets=param_sets,
                                  games=games)