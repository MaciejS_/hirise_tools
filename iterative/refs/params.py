import itertools
import numpy as np
from typing import List, Dict

import torch
from hirise_tools.iterative.params import params_for_expr, v1_paramsets_index

RELEVANT_PARAMS = ("N", "s", "p1", "img", "class", "model")

def discard_irrelevant_params(param_dict, relevant_params=RELEVANT_PARAMS):
    # irrelevant_params = ["bound_gen", "band_gen", "bound_edges", "mask_budget"]
    return {k: v for k, v in param_dict.items() if k in relevant_params}


def ref_param_clusters_from_v2(iteration_params, variable_params, param_order, fixed_params):
    if "N" not in iteration_params:
        raise NotImplementedError("V2 Archives where N is not an iteration param are not supported")

    n_iterations = min(map(len, iteration_params.values()))

    n_values = {}
    param_clusters = []
    for iteration_idx in range(n_iterations):
        cumulative_n_values = set(map(sum, itertools.product(*iteration_params["N"][:iteration_idx+1])))
        max_N = max(cumulative_n_values)

        other_iter_params = {k: v[iteration_idx] for k, v in iteration_params.items() if k != "N"}
        other_iter_params.update(variable_params)

        other_iter_params["N"] = cumulative_n_values
        n_values[max_N] = cumulative_n_values

        param_clusters.append(other_iter_params)

    return {"N_sets": n_values,
            "clusters": param_clusters,
            "fixed": fixed_params,
            "order": param_order}


def param_sets_from_ref_params(param_clusters, param_order, fixed_params):
    """
    Creates a flat list of all parameter sets. This results is suitable for result indexing, but not handling


    :param param_clusters: list of variable param dicts.
    :param fixed_params:
    :return:
    """
    all_paramsets = [params_for_expr(cluster,
                                     param_order=param_order,
                                     fixed_params=fixed_params)
                      for cluster
                      in param_clusters]

    sorted_unique_params = sorted(list(itertools.chain.from_iterable(all_paramsets)),
                                  key=lambda d: tuple(d[k] for k in param_order))
    return [x for x, next in zip(sorted_unique_params, sorted_unique_params[1:] + [None]) if x != next]


def minimal_ref_paramset_for_calculations(param_clusters, param_order, fixed_params):
    """
    This paramset builder replaces N values for each cluster with the max(N).
    Full list of N values can later be retrieved from the result of `param_clusters_from_v2`

    :param param_clusters:
    :param param_order:
    :param fixed_params:
    :return:
    """
    clusters_with_max_N = [{k: {max(v)} if k == "N" else v
                            for k, v
                            in pc.items()}
                           for pc
                           in param_clusters]

    return param_sets_from_ref_params(clusters_with_max_N,
                                      param_order=param_order,
                                      fixed_params=fixed_params)


def index_from_ref_params(param_clusters, param_order):
    cluster_indexes = (v1_paramsets_index(p, param_order) for p in param_clusters)
    combined_index = set(itertools.chain.from_iterable(cluster_indexes))

    return {k: i for i, k in enumerate(sorted(combined_index))}


def calc_stats_from_ref_params(ref_paramsets: List[Dict],
                               n_reps: int,
                               source_data: Dict):
    DEFAULT_MAP_SIZE = 224 * 224
    DEFAULT_N_CHANNELS = 3
    DEFAULT_TENSOR_CELL_BYTES = 4

    if 'loaded_imgs' in source_data:
        total_imgs_cells = sum(map(lambda img: np.product(img.shape), source_data['loaded_imgs']))
    else:
        # use default
        total_imgs_cells = sum(map(len, source_data['explained_class'])) \
                          * DEFAULT_N_CHANNELS \
                          * DEFAULT_MAP_SIZE

    stored_maps = len(ref_paramsets) * n_reps * sum(map(len, source_data["explained_class"]))
    evaluated_masks = sum(x["N"] for x in ref_paramsets) \
                      * n_reps \
                      * len(source_data["explained_class"]) # len instead of sum(len()) because refs use RISE and can evaluate all classes at once

    return {"stored_maps_bytes": stored_maps * DEFAULT_MAP_SIZE * DEFAULT_TENSOR_CELL_BYTES,
            "input_imgs_bytes": total_imgs_cells * DEFAULT_TENSOR_CELL_BYTES,
            "n_evaluated_masks": evaluated_masks}


def contains_band_specific_N_values(N_value_sequences):
    """
    Band-specific N-values

    :param N_value_sequences: list of n-value sequences, imported from hirise results archive
    :return:
    """
    return any(any(isinstance(x, (list, tuple)) for x in sequence)
               for sequence
               in N_value_sequences)


def contains_band_variable_param(param_values):
    """
    Band-variable parameters are parameters, which are provided as a collection of values instaead of a single value.
    Every parameter values in the collection will be map to corresponding saliency map, ordered by saliency value.
    :param param_values:
    :param param_key:
    :return:
    """
    return any(isinstance(param_value, (tuple, list, np.ndarray, torch.Tensor))
               for param_value
               in param_values)


def is_band_variable_n_param_dict(variable_params: dict, fixed_params: dict):
    """
    Variable-N param dict can be detected (at least from the perspective of reference maps generator) by
    checking whether iteration-specific parameter values are lists or values.
    If there are only primitive values, then it's a static-n param dict
    If collections occur, then it's a variable-s param dict, or variable-N-static-s

    Variable-N HiRISE archive might also use regular N parameter values along with a custom mask budgeting scheme.
    However, this case doesn't need to be treated differently than static-N case

    :param variable_params:
    :return:
    """
    if "N" in variable_params:
        return any(map(contains_band_variable_param, variable_params["N"]))
    elif "N" in fixed_params:
        return contains_band_variable_param(fixed_params["N"])
    else:
        raise RuntimeError("N not found in parameter dicts")


def is_band_variable_s_param_dict(variable_params: dict, fixed_params: dict):
    """
    Variable-S param dict can be detected (at least from the perspective of reference maps generator) by
    checking whether iteration-specific parameter values are lists or values.
    If there are only primitive values, then it's a static-s param dict
    If collections occur, then it's a variable-s param dict, or variable-N-static-s

    :param variable_params:
    :return:
    """
    if "s" in variable_params:
        return any(map(contains_band_variable_param, variable_params["s"]))
    elif "s" in fixed_params:
        return contains_band_variable_param(fixed_params["s"])
    else:
        raise RuntimeError("s not found in parameter dicts")
