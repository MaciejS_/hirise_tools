from datetime import datetime

from hirise_tools.utils import validate_keyset


def make_ref_archive_v1(all_results,
                        full_ref_params_dict,
                        source_data,
                        description,
                        input_archive_file_name,
                        start_date: datetime,
                        param_index=None):
    validate_keyset({"clusters", "order", "N_sets", "fixed"}, set(full_ref_params_dict.keys()))

    r1_dict = {"about": {"version": "r1",
                          "type": "RISE reference SMaps archive"},
                "description": description,
                "source_data": source_data,
                "params": full_ref_params_dict,
                "source_file_name": input_archive_file_name,
                "layout": f'["ref_param_set", Tensor["repetitions", "map_y", "map_x"]]',
                "param_index": param_index,
                "maps": all_results,
                "date": {"start": start_date.isoformat(),
                         "end": datetime.now().isoformat()},
                "readme": "Contains reference RISE maps for parameter sets found in source archive "
                          "The reference archive format is semi-independent of the source archive. "
                          "Parameters are clustered, but the list of maps is a flattened, non-redundant sum of intra-cluster cartesians"}
    return r1_dict


def make_ref_archive_v1_multiimg(all_results,
                                 full_ref_params_set,
                                 source_data,
                                 description,
                                 input_archive_file_name,
                                 start_date: datetime,
                                 param_index=None):

    validate_keyset({"loaded_imgs", "proper_class", "explained_class"}, set(source_data.keys()), allow_optionals=True)

    r1_dict = make_ref_archive_v1(all_results=all_results,
                                  full_ref_params_dict=full_ref_params_set,
                                  source_data=source_data,
                                  description=description,
                                  input_archive_file_name=input_archive_file_name,
                                  param_index=param_index,
                                  start_date=start_date)

    r1_dict["about"]["version"] = "r1m"
    r1_dict["about"]["is_multi_img"] = True
    r1_dict["layout"] = '["img", "ref_param_set", Tensor["repetitions", "map_y", "map_x"]]',
    r1_dict["readme"] += "\nMulti-image version simply stacks result lists for each of the images"\
                          "'img' and 'class' keys in param dicts should be ignored. This information should be retrieved from source_data"

    return r1_dict