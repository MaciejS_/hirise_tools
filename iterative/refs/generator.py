from typing import Dict, Any, List, Tuple, Sequence

import torch

from hirise.explainer import RISE
from hirise.structures import LambdaCheckpoints, CombinedMask, ClassFilter

from hirise_tools.iterative.params import paramset_to_indexing_tuple
from hirise_tools.iterative.refs.params import discard_irrelevant_params, RELEVANT_PARAMS, ref_param_clusters_from_v2, \
    minimal_ref_paramset_for_calculations, index_from_ref_params
from tqdm.autonotebook import trange, tqdm


def run_ref_generation(full_ref_params: Dict[str, Any],
                       ref_min_paramsets: List[Dict[str, Any]],
                       ref_min_param_index: Dict[Tuple, int],
                       n_reps: int,
                       rise: RISE,
                       tqdm_leave=True):
    """
    Takes parameters strictly for reference maps and runs reference maps generation.

    :param full_ref_params:
    :param ref_min_paramsets:
    :param ref_min_param_index:
    :param tqdm_leave:
    :return:
    """
    img_shape = ref_min_paramsets[0]["img"].shape
    all_results = [torch.zeros((n_reps, *img_shape[-2:]))
                   for _
                   in ref_min_param_index]

    for rep_i in trange(n_reps, desc="repetitions", leave=tqdm_leave):
        for i, ps in tqdm(list(enumerate(ref_min_paramsets)), desc="paramsets", leave=False):
            img = ps["img"]
            p1 = ps["p1"]
            max_N = ps["N"]
            n_values = full_ref_params["N_sets"][max_N]

            retainer = LambdaCheckpoints(CombinedMask(img.shape[-2:], p1),
                                           lambda i, m, s: i in n_values)

            explained_class = ps['class']
            rise.explain(img=img,
                         N=max_N,
                         s=ps['s'],
                         p1=p1,
                         explained_class=explained_class,
                         mask_retainers=[ClassFilter(retainer, explained_class)])

            for n, smap in retainer.get_checkpoints():
                ps["N"] = n
                idxt = paramset_to_indexing_tuple(ps, full_ref_params['order'])
                all_results[ref_min_param_index[idxt]][rep_i] = smap

    return all_results


def generate_refs_for_v2(n_reps: int,
                         iteration_params:dict,
                         variable_params:dict,
                         fixed_params: dict,
                         param_order: Sequence,
                         rise,
                         tqdm_leave: bool=True):
    """
    All param dicts should be provided as retrieved from original archive

    Keys

    :param n_reps:
    :param iteration_params:
    :param variable_params:
    :param fixed_params:
    :param param_order:
    :return:
    """
    iteration_params, variable_params, fixed_params = map(discard_irrelevant_params,
                                                          (iteration_params, variable_params, fixed_params))
    param_order = list(filter(lambda x: x in RELEVANT_PARAMS, param_order))

    all_ref_params = ref_param_clusters_from_v2(iteration_params, variable_params, param_order, fixed_params)

    min_paramsets = minimal_ref_paramset_for_calculations(param_clusters=all_ref_params['clusters'],
                                                          param_order=param_order,
                                                          fixed_params=fixed_params)

    min_index = index_from_ref_params(param_clusters=all_ref_params['clusters'],
                                      param_order=param_order)

    all_results = run_ref_generation(full_ref_params=all_ref_params,
                                     ref_min_paramsets=min_paramsets,
                                     ref_min_param_index=min_index,
                                     n_reps=n_reps,
                                     rise=rise,
                                     tqdm_leave=tqdm_leave)

    return {"ref_maps": all_results,
            "all_params": all_ref_params,
            "paramsets": min_paramsets,
            "index": min_index}


def generate_refs_for_v2_multiimg(source_data,
                                  n_reps,
                                  iteration_params: dict,
                                  variable_params: dict,
                                  fixed_params: dict,
                                  rise,
                                  param_order: Sequence):

    imgs = source_data['loaded_imgs']
    classes = source_data['requested_classes']

    if len(imgs) != len(classes):
        raise ValueError(f"Expected the images and classes lists to be of the same length. Got {len(imgs)} and {len(classes)}, respectively")

    multimap_results = []
    for img, explained_class in tqdm(list(zip(imgs, classes)), desc="images"):
        if isinstance(explained_class, (tuple, list, set)):
            raise NotImplementedError("Multiimage, multiclass case is not supported")
        else:
            fixed_params["img"] = img
            fixed_params["class"] = explained_class

        multimap_results.append(generate_refs_for_v2(n_reps,
                                                     iteration_params,
                                                     variable_params,
                                                     fixed_params,
                                                     param_order,
                                                     rise,
                                                     tqdm_leave=False))

    result_dict = {"ref_maps": [d["ref_maps"] for d in multimap_results],
                   "all_params": multimap_results[0]["all_params"],
                   "paramsets": multimap_results[0]["paramsets"],
                   "index": [d["index"] for d in multimap_results] }
    return result_dict