import torch
import numpy as np
from hirise_tools.models import load_resnet50


def combine_retained_checkpoints(retainers):
    """
    Takes a list of in-band MaskRetainers and returns a list of (N_value, smap) pairs
    Each smap is a sum of in-band partial smaps for given N maps

    :param retainers:
    :return:
    """
    all_band_checkpoints = [r.get_checkpoints() for r in retainers]
    N_values = np.asarray([[n for n, smap in band_checkpoints]
                           for band_checkpoints
                           in all_band_checkpoints]).sum(axis=0)
    smaps = [(n, torch.sum(torch.stack([in_band_smaps[checkpoint_idx][1]
                                                   for in_band_smaps
                                                   in all_band_checkpoints]), dim=0))
                                for checkpoint_idx, n
                                in enumerate(N_values)]
    return smaps


def retrieve_model(arx, device:torch.device):
    model = arx['params']['fixed']['model']

    if model == "resnet50":
        model, _ = load_resnet50(device)
    else:
        raise RuntimeError(f"Unsupported model type found in the archive: {model}")
    return model


def get_single_full_band(s) -> (np.ndarray, torch.Tensor):
    bounds = np.array([(0, 1)])
    if isinstance(s, (list, tuple, torch.Tensor, np.ndarray)):
        bands = torch.ones(1, s[0], s[0])
    else:
        bands = torch.ones(1, s, s)
    return bounds, bands