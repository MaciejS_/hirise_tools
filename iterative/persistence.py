import os
from datetime import datetime

import numpy as np
import torch
from hirise_tools.utils import validate_keyset

from tqdm.autonotebook import tqdm

# TODO: move apply_to_leaves to a more suitable location (it's a very generic util, so it shouldn't sit with utils specific to archives for iterative-approach)
def apply_to_leaves(f, object):
    if isinstance(object, dict):
        return type(object)((apply_to_leaves(f, k), apply_to_leaves(f, v)) for k, v in object.items())
    elif isinstance(object, (tuple, list, set)):
        return type(object)(map(lambda element: apply_to_leaves(f, element), object))
    elif isinstance(object, np.ndarray):
        return np.array(tuple(map(lambda subarray: apply_to_leaves(f, subarray), object)))
    else:
        return f(object)

def apply_to_leaves_tqdm(f, object):
    if isinstance(object, dict):
        return type(object)((apply_to_leaves(f, k), apply_to_leaves_tqdm(f, v))
                            for k, v
                            in tqdm(object.items(), leave=False))
    elif isinstance(object, (tuple, list, set)):
        return type(object)(map(lambda element: apply_to_leaves_tqdm(f, element), tqdm(object, leave=False)))
    else:
        return f(object)

def substitute_object_params(param_dict):
    """
    Replaces object parameters (like GridGens, MaskGens, etc.) with their string representations
    :param param_dict:
    :return:
    """
    STRING_SERIALIZABLE_PARAMS = ["bound_gen", "band_gen", "mask_gen", "mask_budget", "model"]
    persistable_params = {k: apply_to_leaves(str, v) if k in STRING_SERIALIZABLE_PARAMS else v
                           for k, v
                           in param_dict.items()}

    for name, param in persistable_params.items():
        if isinstance(param, torch.Tensor):
            persistable_params[name] = param.cpu()

    return persistable_params

def make_v1_archive(all_results, source_data, fixed_params, variable_params, variable_param_order, start_date=None, description=None):
    """
    Create an output archive

    :param all_results:
    :param source_data: dict created by iterative.run_hirise.make_source_data_dict()
    :param fixed_params: dict containing parameter values common to all parameter sets
    :param variable_params: dict containing parameters combined into a cartesian product
    :param variable_param_order: list of parameter names from `variable_params` determines the position of each parameter in cartesian product
    :param start_date: datetime timestamp created at the start of the
    :param description: custom descripton string attached to the archive
    :return:
    """

    validate_keyset({"files", "proper_class"}, set(source_data.keys()), allow_optionals=True)

    return {"type": "HiRISE SMaps-only archive v1",
            "description": description,
            "source_data": source_data,
            "params": {"fixed": substitute_object_params(fixed_params),
                       "variable": substitute_object_params(variable_params),
                       "order": variable_param_order},
            "layout": '["param_set", Tensor["repetitions", "iterations", "map_y", "map_x"]]',
            "maps": all_results,
            "date": {"start": start_date.isoformat(),
                     "end": datetime.now().isoformat()},
            "readme": "Contains HiRISE saliency maps generated according to `params`. "
                      "Entries in list of maps correspond to results of cartesian product of 'variable' params, according to 'order'. "
                      "The first parameter in 'order' changes least frequently in the cartesian product results. "
                      "The last one changes the most frequently"}


def make_v2_param_dict(iteration_params, variable_params, param_order, fixed_params):
    return {"iteration": substitute_object_params(iteration_params),
            "fixed": substitute_object_params(fixed_params),
            "variable": substitute_object_params(variable_params),
            "order": param_order}


def make_v2_archive(all_results, v2_param_dict, source_data, start_date, description=None, param_index=None):
    return {"about": {"version": "v2",
                      "type": "HiRISE SMaps-only archive"},
            "description": description,
            "source_data": source_data,
            "params": v2_param_dict,
            "layout": '["iter_idx", "iteration_specific_param_set", Tensor["repetitions", "map_y", "map_x"]]',
            "maps": all_results,
            "param_index": param_index,
            "date": {"start": start_date.isoformat(),
                     "end": datetime.now().isoformat()},
            "readme": "Contains HiRISE saliency maps generated according to `params`. "
                      "Entries in list of maps correspond to results of cartesian product of 'variable' params, according to 'order'. "
                      "The first parameter in 'order' changes least frequently in the cartesian product results. "
                      "The last one changes the most frequently"
                      "Param set sizes grow with iterations. That's because param set contains information about parameters of past iterations"}


def make_v2_archive_multimap(all_results,
                             v2_param_dict,
                             source_data,
                             start_date,
                             description=None,
                             param_index=None):
    validate_keyset({"loaded_imgs", "proper_class", "explained_class"}, set(source_data.keys()), allow_optionals=True)

    v2_dict = make_v2_archive(all_results=all_results,
                              v2_param_dict=v2_param_dict,
                              source_data=source_data,
                              start_date=start_date,
                              description=description,
                              param_index=param_index)

    v2_dict["about"]["version"] = "v2m"
    v2_dict["about"]["is_multi_img"] = True
    v2_dict["layout"] = '["img", "iter_idx", "iteration_specific_param_set", Tensor["repetitions", "map_y", "map_x"]]',
    v2_dict["readme"] += "\nMulti-image version simply stacks result lists for each of the images"\
                          "'img' and 'class' keys in param dicts should be ignored. This information should be retrieved from source_data"

    return v2_dict


def make_source_data_dict(input_file_paths, expected_classes, explained_classes=None, loaded_imgs=None):
    """
    Create a dictionary describing the input data used for HiRISE experiment

    :param input_file_paths:
    :param expected_classes:
    :return:
    """
    if not all(map(lambda x: isinstance(x, (list, tuple, set)), [input_file_paths, expected_classes])):
        raise TypeError(f"Both input_file_paths and expected_classes should be Sequences, "
                        f"got {type(input_file_paths)} and {type(expected_classes)}")

    if len(input_file_paths) != len(expected_classes):
        raise ValueError("input_file_paths should be equal in length to expected_classes")

    source_data_dict = {"files": [os.path.split(path)[-1] for path in input_file_paths],
                        "proper_class": expected_classes,
                        "explained_class": explained_classes}

    if loaded_imgs is not None:
        source_data_dict["loaded_imgs"] = loaded_imgs
    
    return source_data_dict


def get_maps_archive_type(archive: dict) -> str:
    """
    :param archive: dict-like object
    :return: string version id (currently v1 or v2)
    """
    if archive.get("type", "") == "HiRISE SMaps-only archive v1":
        return "v1"
    else:
        return archive.get("about", {}).get("version", None)