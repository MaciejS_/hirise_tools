import torch
import numpy as np
from hirise.evaluation.game import BatchedDeletionGame, BatchedInsertionGame
from hirise.evaluation.runner import BatchedGameRunner

from hirise.experiment.utils import torch_normalize
from hirise.structures import ScoresRetainer, CombinedMask, BatchingCombinedMask

from tqdm.autonotebook import tqdm, trange


import contextlib

@contextlib.contextmanager
def seeded_rng(seed):
    rng_state = torch.get_rng_state()
    torch.manual_seed(seed)
    yield
    torch.set_rng_state(rng_state)



def default_mask_aggregator_provider(input_size, p1):
    saliency_map_aggregator = CombinedMask(input_size, p1)
    return BatchingCombinedMask(saliency_map_aggregator, single_op_size_limit=224 * 224 * 5000, single_mask_ndim=2)


def hirise_iteration(img, band_Ns, p1, bands, explained_class, mask_gen, evaluator, mask_aggregator_provider=default_mask_aggregator_provider):
    input_size = img.shape[-2:]
    masks = list(map(lambda x: x.cpu(), mask_gen.generate_masks(band_Ns, p1, input_size, bands))) # move masks back to RAM to prevent CUDA OOM (loses a bit of performance on copy operations, but requires less hassle - I will implement this properly (just-in-time mask generation) in HiRISE class)
    scores = list(map(lambda x: x.cpu(), evaluator.evaluate_banded(img, masks)))

    smaps = torch.empty(len(bands), *input_size)
    # TODO: zipping is incompatible with non-equal split
    for idx, (m, s) in enumerate(zip(masks, scores)):
        saliency_map_aggregator = mask_aggregator_provider(input_size, p1)
        saliency_map_aggregator.add_many(m.squeeze(), s[:, explained_class])
        smaps[idx] = saliency_map_aggregator.get_mask()

    return masks, smaps, scores


def iterative_hirise_all_data(img, N_values, s_steps, p1, bound_edges, bound_gen, band_gen, mask_gen, evaluator, explained_class, n_sample_masks=10, iteration_seeds=None):
    results = {"bands": [],
               "bounds": [],
               "scores": [],
               "smaps": [],
               "sample_masks": [],
               "partial_smaps": [],
               "coverage": []}

    for iter_idx, (s, n) in tqdm(list(enumerate(zip(s_steps, N_values))), leave=False, desc="s"):
        if iter_idx == 0:
            bounds = np.array([(0, 1)])
            if isinstance(s, (list, tuple, torch.Tensor, np.ndarray)):
                bands = torch.ones(1, s[0], s[0])
            else:
                bands = torch.ones(1, s, s)
        else:
            normalized_prev_smap = torch_normalize(prev_smap)
            bounds = bound_gen.make_contiguous_bounds(normalized_prev_smap, bound_edges)
            bands = band_gen.generate_bands(normalized_prev_smap, bounds, s)

        results["bounds"].append(bounds)
        results["bands"].append(bands)

        masks, in_band_smaps, scores = hirise_iteration(img=img,
                                                        band_Ns=int(n / len(bands)),
                                                        p1=p1,
                                                        bands=bands,
                                                        explained_class=explained_class,
                                                        mask_gen=mask_gen,
                                                        evaluator=evaluator)
        combined_smap = torch.sum(in_band_smaps, dim=0)
        coverage_densities = [m.mean(dim=0) for m in masks]

        results["sample_masks"].append([m[:n_sample_masks] for m in masks])
        results["scores"].append(scores)
        results["partial_smaps"].append(in_band_smaps)
        results["smaps"].append(combined_smap)
        results["coverage"].append(coverage_densities)
        prev_smap = combined_smap

    return results


def iterative_hirise(img, N_values, s_steps, p1, bound_edges, bound_gen, band_gen, mask_gen, evaluator, explained_class, n_sample_masks=10, iteration_seeds=None):
    results = iterative_hirise_all_data(img, N_values, s_steps, p1, bound_edges, bound_gen, band_gen, mask_gen, evaluator, explained_class, n_sample_masks=10)
    return results["smaps"][-1]


def rise_with_scores(img, N, p1, s, observed_class, rise_instance):
    scores_retainer = ScoresRetainer()
    smap = rise_instance.explain(img, N, s, p1, observed_class, [scores_retainer])
    return smap, scores_retainer.get()


def plain_rise(img, N, p1, s, observed_class, rise_instance):
    smap, scores = rise_with_scores(img, N, p1, s, observed_class, rise_instance)
    return smap


def run_deletion_game(img, smap, observed_class, step_size, batch_size, model):
    runner = BatchedGameRunner()
    kernel_game = BatchedDeletionGame(model, img, smap, batch_size, step_size, BatchedDeletionGame.default_substrate(), observed_class)
    return runner.run(kernel_game)


def run_insertion_game(img, smap, observed_class, step_size, batch_size, model):
    runner = BatchedGameRunner()
    kernel_game = BatchedInsertionGame(model, img, smap, batch_size, step_size, BatchedInsertionGame.default_substrate(), observed_class)
    return runner.run(kernel_game)


def reference_maps_and_scores(img, total_N, s_matrix, p1, n_instances, explained_class, rise_instance, auc_of_smap):
    """
    Returned structure (for both maps and scores):
    {s_value: [{hirise_all_data_results} for _ in n_instances]}
        
    """
    
    if isinstance(p1, list):
        raise NotImplementedError("Multiple p1 values are not supported yet")
        
    all_reference_maps = {s: [plain_rise(img, total_N, p1, s, explained_class, rise_instance)
                          for _
                          in trange(n_instances, desc="instances", leave=False)]
                      for s 
                      in tqdm(set([x[-1] for x in s_matrix]), desc="refs maps", leave=False)}
                  
    all_ref_aucs = {s: np.array([auc_of_smap(img, m, explained_class)
                                 for m 
                                 in tqdm(instances, desc="instances", leave=False)])
                             for s, instances 
                             in tqdm(all_reference_maps.items(), desc="refs aucs", leave=False)}

    return all_reference_maps, all_ref_aucs
