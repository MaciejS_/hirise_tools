import copy
import itertools
from typing import Sequence, List, Dict, Any, Tuple

import numpy as np
from hirise_tools.utils import validate_keyset

def tupleize(collection):
    """
    Recursively traverses collection, converting lists and sets with tuples
    :param collection:
    :return:
    """
    if isinstance(collection, (tuple, list, set)):
        return tuple(map(tupleize, collection))
    else:
        return collection

# TODO: rename to v1_paramsets
def params_for_expr(variable_params: Dict[str, Sequence[Any]],
                    param_order: Sequence[str],
                    fixed_params: Dict[str, Any]) -> List[Dict[str, Any]]:
    """
    Creates a list of paramsets from given parameter dicts.
    Each paramset is a unique element of a cartesian product of all provided parameter types.
    Each paramset contains one values of each parameter type in `variable` and `fixed` params


    :param variable_params: dict of {param_name: [param_value]} pairs. Each paramset is an unique combination of parameter values.
    :param fixed_params: dict of {param_name: param_value} pairs. parameter values from this dict are present in each paramset
    :param param_order: list of keys from `variable_params` - their order on this list determines the order of param types in cartesian product, and therefore the ordering of paramsets in the list returned by this function
    :return:
    """
    validate_keyset(set(param_order), set(variable_params.keys()))
    lengths = [len(variable_params[k]) for k in param_order]

    variable_params = {k: v if not isinstance(v, (tuple, list, set)) else tupleize(v)
                       for k, v
                       in variable_params.items()}

    fixed_params = {k: v if not isinstance(v, (tuple, list, set)) else tupleize(v)
                       for k, v
                       in fixed_params.items()}

    param_sets = []
    for ndidx in np.ndindex(*lengths):
        pdict = {param_name: variable_params[param_name][idx]
                 for idx, param_name
                 in zip(ndidx, param_order)}
        pdict.update(fixed_params)
        param_sets.append(pdict)
    return param_sets


def v1_paramsets_index(variable_params: Dict[str, Sequence[Any]],
                       param_order: Sequence[str]) -> List[Dict[Tuple, int]]:
    """
    Creates a list of paramset indices for given variable params.
    Each index is a dictionary of {indexing_tuple: paramset_index}
    * indexing_tuple is a sequence of parameter values in order determined by param_order
    * paramset_index indicates which paramset corresponds to the indexing tuple
    Indexing tuples contain only iteration and variable parameters. Fixed params are reused in every single paramset,
    so there's no point in adding them to indexing tuples

    :param iteration_params:
    :param variable_params:
    :param param_order:
    :return:
    """
    validate_keyset(set(param_order), set(variable_params.keys()))
    lengths = [len(variable_params[k]) for k in param_order]

    variable_params = {k: v if not isinstance(v, (tuple, list, set)) else tupleize(v)
                       for k, v
                       in variable_params.items()}

    index = {}
    for paramset_idx, ndidx in enumerate(np.ndindex(*lengths)):
        param_tuple = tuple([tupleize(variable_params[param_name][idx])
                               for idx, param_name
                               in zip(ndidx, param_order)])

        index[param_tuple] = paramset_idx
    return index


def v2_paramsets_index(iteration_params: Dict[str, Sequence[Any]],
                       variable_params: Dict[str, Sequence[Any]],
                       param_order: Sequence[str]) -> List[List[Dict[Tuple, int]]]:
    """
    Creates a list of lists of paramset indices for each iteration.
    Each index is a dictionary of {indexing_tuple: paramset_index}
    * indexing_tuple is a sequence of parameter values in order determined by param_order
    * paramset_index indicates which paramset corresponds to the indexing tuple
    Indexing tuples contain only iteration and variable parameters. Fixed params are reused in every single paramset,
    so there's no point in adding them to indexing tuples

    :param iteration_params:
    :param variable_params:
    :param param_order:
    :return:
    """
    n_iterations = min(map(len, iteration_params.values()))

    indexes = []
    for iteration_idx in range(n_iterations):
        params_for_iter = {name: list(itertools.product(*values[:iteration_idx + 1]))
                           for name, values
                           in iteration_params.items()}

        params_for_iter.update(variable_params)
        indexes.append(v1_paramsets_index(params_for_iter, param_order))
    return indexes


def paramset_to_indexing_tuple(paramset: dict, param_order: Sequence) -> Tuple:
    """
    Converts a dictionary paramset to a tuple containing parameter values for param names present in `param_order`,
    in order of their appearance in `param_order`
    """
    return tuple(tupleize(paramset[param_name])
                 for param_name
                 in param_order)


def v2_full_param_set(iteration_params: Dict[str, Sequence[Any]],
                      variable_params: Dict[str, Sequence[Any]],
                      fixed_params: Dict[str, Any],
                      param_order: Sequence[str]) -> List[List[Dict[str, Any]]]:
    """
    Creates a full list of parameter sets for all iterations.

    :param iteration_params: dict of nested lists of param values, keyed with param names. First list holds values for first iteration, and so on
    :param variable_params: dict of lists of param values, keyed with param names.
    :param fixed_params: dict of param values common to all param sets
    :param param_order: list of param names, which determines the order of paramsets in paramset lists. The values of last parameter on the list will change the most frequently, whereas the first parameter will change the least frequently
    :return:
    """
    n_iterations = min(map(len, iteration_params.values()))
    param_sets = []

    for iteration_idx in range(n_iterations):
        params_for_iter = {param_name: list(itertools.product(*param_values[:iteration_idx+1]))
                                for param_name, param_values
                                in iteration_params.items()}

        params_for_iter.update(variable_params)

        param_sets.append(params_for_expr(variable_params=params_for_iter,
                                          param_order=param_order,
                                          fixed_params=fixed_params))

    return param_sets


def v2_split_iteration_param_sets(iter_idx: int,
                                  iteration_params: Dict[str, Sequence],
                                  variable_params: Dict[str, Any],
                                  fixed_params: Dict[str, Any],
                                  param_order: Sequence[str]):
    """
    Returns a dictionary containing iteration parameters in ready-to-use form by run_hirise_optimized
    The `ready-to-use` form means that regular parameter sets are split into
    * "base_params" - unique parameter sets from previous iteration
    * "base_index" - dict of {indexing_tuple: int_index} pairs built from "base_params" used to determine where in the output
    * "cur_iter_params" - unique sets of parameter values specific to current iteration
    * "N_values" - mask counts to evaluate in this iteration. This parameter is returned separately, because run_hirise_optimized
    optimizes calculations over it and therefore treats it differently than all other parameters

    :param iter_idx: iteration number
    :param iteration_params: dict of iteration specific parameters (usually N and s)
    :param variable_params: dict of variable params used in each iteration (like bound edges)
    :param fixed_params: dict of fixed params, wh
    :param param_order: list of parameter names (for iteration and varaible params), which determines the parameter order.
    Fixed params names are not relevant here because their values are contant and don't create new parameter sets.
    :return:
    """
    params_of_previous_iteration = {k: tuple(itertools.product(*v[:iter_idx]))
                                     for k, v
                                     in iteration_params.items()}

    current_iter_prams_values = {k: v[iter_idx]
                                   for k, v
                                   in iteration_params.items()
                                   if k != "N"}

    if "N" in iteration_params:
        n_values = iteration_params["N"][iter_idx]
    else:
        raise NotImplementedError("N parameter values must be given via iteration params dict")

    params_of_previous_iteration.update(variable_params)

    full_prev_iteration_params = params_for_expr(variable_params=params_of_previous_iteration,
                                                 param_order=param_order,
                                                 fixed_params=fixed_params)
    prev_params_index = v1_paramsets_index(params_of_previous_iteration, param_order)

    current_paramsets = params_for_expr(current_iter_prams_values,
                                        param_order=[x for x in param_order if x in iteration_params.keys() and x != "N"],
                                        fixed_params={})

    return {"n_values": n_values,
            "cur_iter_params": current_paramsets,
            "base_params": full_prev_iteration_params,
            "base_index": prev_params_index}


def convert_base_ps_to_persistence_tuples(N_values: Sequence[int],
                                          s_value:int,
                                          base_paramset:Dict[str, Any],
                                          param_order:Sequence[str]) -> List[Tuple]:
    """
    Creates indexing tuples compliant with persistence indexing scheme.
    "Free iteration params" are those left out from base_paramset, but required in persistence paramsets. Notably "N", and sometimes "s" and "p1"
    current_iteration_parameters should follow this layout: {"param_name": [all_param_values_in_this_iteration]}

    Returns a list(zip(N_values, indexing_tuples))

    :param free_iteration_params:
    :param base_paramset:
    :param param_order:
    :return:
    """
    base_ps_copy = copy.copy(base_paramset)
    base_ps_copy["s"] = base_paramset["s"] + (s_value,)

    indexing_tuples = []
    for n in N_values:
        base_ps_copy["N"] = base_paramset["N"] + (n,)
        indexing_tuples.append(paramset_to_indexing_tuple(base_ps_copy, param_order))

    return list(zip(N_values, indexing_tuples))