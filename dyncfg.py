import json
import os

import hirise_tools.utils


class DynamicJSONConfig():

    @staticmethod
    def new_config_file(
        config_path,
        config_dict: dict,
        remove_on_close:bool=False
    ) -> "DynamicJSONConfig":
        """
        Creates a new JSON config file with mappings in `config_dict`
        `path` can either point to a directory, or a file. In the former case, a file with random name will be created in that directory.

        :param config_path:
        :param config_dict:
        :return:
        """
        if os.path.isdir(config_path):
            config_path = os.path.join(config_path, f"{hirise_tools.utils.randhex()}.cfg.json")

        try:
            with open(config_path, 'w') as config_file:
                json.dump(config_dict, config_file, indent=4)
        except Exception as e:
            raise RuntimeError("Failed to create dynamic config file", e)

        return DynamicJSONConfig(config_path, remove_on_close)


    def __init__(self, filepath, remove_on_close):
        self.config_filepath = filepath
        self.remove_on_close = remove_on_close

        self._config = {}
        self._changed = {}
        self._last_mtime = 0
        self.reload()

    def reload(self):
        # TODO: test in case the file is currently open for writing
        if os.stat(self.config_filepath).st_mtime > self._last_mtime:
            old_config = self._config
            self._config = self._load()

            self._changed = {
                k: self._config.get(k, False) or (old_config.get(k, None) != self._config.get(k, None))
                for k
                in (
                    set(old_config.keys())
                    | set(self._config.keys())
                    | set(k for k, has_changed in self._changed.items() if has_changed)
                )
            }

    def overwrite(self):
        with open(self.config_filepath, 'w') as outfile:
            json.dump(self._config, outfile, indent=2)

    def _load(self):
        # TODO: test in case the file is currently open for writing
        with open(self.config_filepath, 'r') as infile:
            new_config = json.load(infile)

        return new_config

    def has_changed(self, key):
        result = self._changed[key]
        self._changed[key] = False
        return result

    def __getitem__(self, item):
        self.reload()
        return self._config[item]

    def __setitem__(self, key, value):
        # FIXME: check if file is currently open for writing
        # FIXME: It's possible (though very unlikely) for the JSON file to change between calls to
        #  .reload() and .overwrite()
        self.reload()
        self._config[key] = value
        self.overwrite()

    def __del__(self):
        os.remove(self.config_filepath)