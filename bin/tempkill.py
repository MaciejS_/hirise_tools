import typing as t

import os
import signal
import argparse
import subprocess
from time import sleep


def get_gpu_count():
    counts = subprocess.check_output(['nvidia-smi', '--format=csv,noheader', '--query-gpu=count'])
    return int(counts.decode('utf-8')[0]) # all lines contain the same output


parser = argparse.ArgumentParser(description="Kill specified processes if temperatue of any specified GPUs exceeds it's respective thermal threshold.")

parser.add_argument('-d', '--delay', type=float, default=1.0, required=False)
parser.add_argument('-p', '--process', nargs='+', type=int, required=True)
parser.add_argument('-i', '--gpu-id', nargs='+', type=int, choices=list(range(get_gpu_count())), required=True, help="GPUs to monitor")
parser.add_argument('-t', '--temp', nargs='+', type=int, required=False, help="GPU temperature threhsold in Celsius. Number of args must match the numebr of GPUs and ")

args = parser.parse_args()

def prepare_thresholds(thresholds: t.List[int], gpu_ids: t.List[int]) -> t.Dict[int, int]:
    if len(thresholds) == len(gpu_ids):
        return dict(zip(gpu_ids, thresholds))
    elif len(thresholds) == 1:
        return {gpuid: thresholds[0] for gpuid in gpu_ids}
    else:
        raise ValueError(f"Number of temperature thresholds ({len(thresholds)})"
                         f" must either be 1 or match the number of GPUs ({len(gpu_ids)})")

gpu_ids = list(set(args.gpu_id))
thresholds = prepare_thresholds(args.temp, gpu_ids)
while True:

    # measure temps
    current_temps = [
        int(temperature_bytestr)
        for i, temperature_bytestr
        in enumerate(
            subprocess.check_output(
                ['nvidia-smi', '--format=csv,noheader', '--query-gpu=temperature.gpu']
            ).decode('utf-8').splitlines()
        )
        if i in gpu_ids
    ]

    # check thresholds and issue kill commands if needed
    for gpu_id, threshold in thresholds.items():
        cur_gpu_temp = current_temps[gpu_id]
        # print(f"GPU {gpu_id} @ {cur_gpu_temp}C, waiting for {threshold}C")
        if cur_gpu_temp >= threshold:
            for pid in args.process:
                # print(f"would kill {pid}")
                os.kill(pid, signal.SIGTERM)
            exit(0)

    sleep(args.delay)


