import argparse
import collections
import os

import numpy as np
from hirise_tools.npz import load_from_npz
from hirise_tools.utils import validate_keyset


def reshape_tensor(old_tensor):
    """
    Add s and p1 dims to the old tensor. Runs axis stays first.
    :param old_tensor:
    :return:
    """
    return old_tensor[:, np.newaxis, np.newaxis, ...]

def validate_v2_keyset(old_archive: dict):
    expected_keys = {"description",
                        "model",
                        "maps_tensor",
                        "original_image",
                        "explained_class",
                        "checkpoint_mask_counts",
                        "tensor_layout"}

    try:
        validate_keyset(expected_keys, set(old_archive.keys()))
        return True
    except KeyError as e:
        print(e)
        return False


def convert_v2_archive_to_v3(old_archive, s, p1):
    old_maps_tensor = old_archive["maps_tensor"]
    mask_shape = old_maps_tensor.shape[-2:]
    runs_count = old_maps_tensor.shape[0]
    tensor_layout = collections.OrderedDict(runs=runs_count,
                                            s=s,
                                            p1=p1,
                                            checkpoints=old_archive["checkpoint_mask_counts"],
                                            map_y=mask_shape[0],
                                            map_x=mask_shape[1])

    return {"type": "map_tensor_v3",
            "description": old_archive["description"],
            "model": old_archive["model"],
            "original_image": old_archive["original_image"],
            "explained_class": old_archive["explained_class"],
            "maps_tensor": reshape_tensor(old_maps_tensor),
            "tensor_layout": tensor_layout}


parser = argparse.ArgumentParser()
parser.add_argument('input-file', type=os.path.abspath, required=True, help="path to file to be converted")
parser.add_argument('-o', '--output-file', type=os.path.abspath, required=True, help="Result will be written here.")
parser.add_argument('-s', '--grid-shape', type=int, required=True, help="grid size used to generate maps in source archive. An archive in v2 format doesn't contain this information, so it needs to be provided manually")
parser.add_argument('-p1', '--occlusion-prob', type=float, required=True, help="occlusion probability used to generate maps in source archive. An archive in v2 format doesn't contain this information, so it needs to be provided manually")
args = parser.parse_args()

old = load_from_npz(args.input_file)
new = convert_v2_archive_to_v3(old, args.grid_size, args.occlusion_prob)
np.savez_compressed(args.output_file, **new)