import itertools
from collections import OrderedDict

import numpy as np
import os
import argparse

from joblib import Parallel, delayed

from tqdm import tqdm
from hirise_tools.rise.combiner.combine import combine_tensor_layouts
from hirise_tools.rise.combiner.validate import validate_map_tensors, validate_tensor_layouts, all_part_keysets_equal

parser = argparse.ArgumentParser()
parser.add_argument("files", nargs="+")
parser.add_argument("-o", "--output", required=True, type=os.path.abspath)
args = parser.parse_args()

sorted_file_paths = args.files

def load_archive(file_path):
    path = file_path if os.path.isabs(file_path) else os.path.join(os.getcwd(), file_path)
    with np.load(path, allow_pickle=True) as part_npz:
        loaded_archive = {k: part_npz[k] for k in part_npz.files}
        return loaded_archive

v2_fields_expected_to_be_identical  = ["description",
                                       "model",
                                       "original_image",
                                       "explained_class",
                                       "checkpoint_mask_counts"]

v3_fields_expected_to_be_identical  = ["type",
                                       "description",
                                       "model",
                                       "original_image",
                                       "explained_class"]

fields_to_combine = {"maps_tensor": {"validate": validate_map_tensors,
                                     "combine": np.concatenate},
                     "tensor_layout": {"validate": validate_tensor_layouts,
                                       "combine": combine_tensor_layouts}}

restoration_methods = {"type": lambda x: x.item(),
                       "description": lambda x: x.item(),
                       "model": lambda x: x.item(),
                       "original_image": lambda x: x,
                       "explained_class": lambda x: x,
                       "maps_tensor": lambda x: x,
                       "tensor_layout": lambda x: x.item()}

all_known_fields = set(itertools.chain(v3_fields_expected_to_be_identical, fields_to_combine.keys()))

print("This script processes files in order they were passed in: ")
for filename in sorted_file_paths:
    print(f'\t {filename}')

parts = Parallel(n_jobs=-2, backend="threading")(delayed(load_archive)(path)
                                                 for path
                                                 in tqdm(sorted_file_paths, leave=False, desc="Loading Files"))

# validate
if not all_part_keysets_equal(parts):
    raise ValueError("Some fields in partial archives differ. Parts need to be homogenous to be combined")

missing_expected_fields = all_known_fields.difference(parts[0].keys())
unknown_fields = set(parts[0].keys()).difference(all_known_fields)

if missing_expected_fields or unknown_fields:
    missing_fields_msg = f"Didn't find expected fields: {' '.join(missing_expected_fields)}"
    unknown_fields_msg = f"Found unexpected fields: {' '.join(unknown_fields)}"
    base_message = "Combined files contain some fields not known by this script. Please update to prevent damage to data."
    raise ValueError('\n'.join((base_message, missing_fields_msg, unknown_fields_msg)))

for field_name in v3_fields_expected_to_be_identical:
    if not all(np.array_equal(parts[0][field_name], parts[x][field_name])
               for x
               in range(1, len(parts))):
        encountered_values = ', '.join(set(map(lambda x: str(parts[x][field_name]),
                                               range(len(parts)))))
        raise ValueError(f"\'{field_name}\' entries were expected to be identical, but some don't match, got: {encountered_values}")

parts = [OrderedDict([(k, restoration_methods[k](v)) for k, v in part.items()]) for part in parts]

for field_name, methods in fields_to_combine.items():
    validation_method = methods["validate"]
    try:
        validation_method([part[field_name] for part in parts])
    except ValueError as e:
        raise ValueError(f"Validation of {field_name} failed", e)



# recombine
obj = {field_name: parts[0][field_name]
       for field_name
       in v3_fields_expected_to_be_identical}

for field_name, methods in fields_to_combine.items():
    combination_method = methods["combine"]
    obj[field_name] = combination_method([part[field_name] for part in parts])

print(f"Saving combined file to {args.output}")
np.savez_compressed(args.output, **obj)

print("Used value from first file for: ")
for field_name in v3_fields_expected_to_be_identical:
    print(f"\t{field_name}")

print("Combined values of:")
for field_name in fields_to_combine.keys():
    print(f"\t{field_name}")
