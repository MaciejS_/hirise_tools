import argparse
import math
import os
import threading

from hirise.occlusions.gridgen import ThresholdGridGen, CoordinateGridGen, FixingGridGen
from hirise.occlusions.maskgen import MaskGen
from hirise.occlusions.upsampling import ParallelizedUpsampler, Upsampler
from tqdm.autonotebook import tqdm, trange

from hirise.experiment.utils import top_k_classes
from hirise.explanations import RISE
from hirise.structures import CombinedMask, LambdaCheckpoints
from hirise.utils import *

from hirise_tools.rise.simple_mapgen.parsing import parse_value_or_range
from hirise_tools.rise.simple_mapgen.utils import human_friendly_number, combine_masks_async
from hirise_tools.rise.simple_mapgen.tensor import rebuild_experiment_tensor, create_tensor_layout, create_tensor
from hirise_tools.checkpoints import npow10
from hirise_tools.utils import create_output_path
from hirise_tools.models import load_resnet50

COORDS_GRID_GEN = "coords"
THRESHOLD_GRID_GEN = "threshold"
ALL_CORES_EXCEPT_ONE = -2

def provide_mask_gen(grid_gen_type: str, fix_empty: bool, device: torch.device) -> MaskGen:
    if grid_gen_type == THRESHOLD_GRID_GEN:
        base_grid_gen =ThresholdGridGen(device)
        if fix_empty:
            grid_gen = FixingGridGen(grid_gen=base_grid_gen, fixer_grid_gen=CoordinateGridGen(device))
        else:
            grid_gen = base_grid_gen
    elif grid_gen_type == COORDS_GRID_GEN:
        grid_gen = CoordinateGridGen(device)
    else:
        raise ValueError(f"{grid_gen_type} is not a valid grid generator specifier")

    if device.type == "cuda":
        upsampler = Upsampler(device)
    else:
        upsampler = ParallelizedUpsampler(ALL_CORES_EXCEPT_ONE)

    return MaskGen(grid_gen, upsampler)


parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-image', type=os.path.abspath, required=True)
parser.add_argument('-r', '--runs', type=parse_value_or_range, required=True, help="Range of runs to perform. Can either be X, in which case X runs will be performed, starting at 0, or X-Y where runs from X to Y (exclusive) will be performed.")
parser.add_argument('-N', '--masks', type=int, required=True)
# parser.add_argument('--checkpoints', type=parse_runs, required=True)
parser.add_argument('-o', '--output-path', type=os.path.abspath, required=False, default="", help='Path to file where scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
parser.add_argument('-s', '--grid-size', type=int, nargs='+', required=True, help="Length of the edge of a square mask used to occlude the input image. Several values can be provided")
parser.add_argument('-p', '--occlusion-prob', type=float, nargs='+', required=True, help="Probability of masking out a single cell in a mask. Several values can be provided")
parser.add_argument('-c', '--explained-class', default=None, type=int, help="Index of observed class. The default value is loaded from dataset, however specifying a different class ")
parser.add_argument('-b', '--batch', type=int, default=100, help="Number of masks evaluated on GPU in parallel. Limited by the amount of VRAM")
parser.add_argument('--device', type=torch.device, default='cuda:0', help="Torch-compatible name of a device used for computation. E.g. 'cpu', 'cuda:0'")
parser.add_argument('--description', type=str, help="Description string placed in the output file")
parser.add_argument('--grid-gen', type=str, choices=[THRESHOLD_GRID_GEN, COORDS_GRID_GEN], default=THRESHOLD_GRID_GEN, help="'threshold' is faster but may generate empty masks for small `s` and `p1` (unless fix-empty is specified), 'coords' is slower but guarantees fill")
parser.add_argument('--fix-empty', action="store_true", help="ThresholdGridGenerator only - add occlusions to masks which have been generated empty")
args = parser.parse_args()


model, model_input_size = load_resnet50(args.device)
img = read_tensor(args.input_image)
img_name = args.input_image.split('/')[-1].split('.')[0]

img_on_gpu = img.to(args.device)
if not args.explained_class:
    top_class_scores, top_class_idx = top_k_classes(model, img_on_gpu, 5)
    explained_class = top_class_idx[0]
else:
    explained_class = args.explained_class

BATCH = args.batch
N = args.masks

default_file_name = f"{img_name}_{human_friendly_number(args.masks)}"
output_path = create_output_path(args.output_path, default_file_name)

checkpoints_lambda = lambda i, m, s: npow10(i)
tensor_layout = create_tensor_layout(args, checkpoints_lambda, model_input_size)

mask_gen = provide_mask_gen(args.grid_gen, args.fix_empty, args.device)
explainer = RISE(model=model, input_size=model_input_size, mask_gen=mask_gen)
try:
    results_export_thread = None
    for i in tqdm(args.runs, desc="Repetitions"):
        maps_tensor = create_tensor(tensor_layout)
        parameter_sets = np.ndindex((len(tensor_layout["s"]), len(tensor_layout["p1"])))
        for s_idx, p_idx in tqdm(list(parameter_sets), desc="Parameter sets", leave=False):
            s = tensor_layout["s"][s_idx]
            p1 = tensor_layout["p1"][p_idx]
            combined_mask = LambdaCheckpoints(CombinedMask(model_input_size, p1), checkpoints_lambda)
            mask_combiner_thread = None
            for batch_idx in trange(math.ceil(N / BATCH), desc="Batches", leave=False):
                batch_size = max(0, min(BATCH, N - (batch_idx * BATCH)))
                explainer.generate_masks(N=batch_size, s=s, p1=p1)
                scores = explainer.evaluate_masks(img_on_gpu)
                if mask_combiner_thread:
                    mask_combiner_thread.join()
                mask_combiner_thread = combine_masks_async(combined_mask, explainer.masks, scores, explained_class)
            mask_combiner_thread.join()

            # save state
            single_tensor, full_checkpoints_list = rebuild_experiment_tensor([combined_mask.get_checkpoints()],
                                                                             model_input_size)
            maps_tensor[0, s_idx, p_idx] = single_tensor
            assert tensor_layout["checkpoints"] == full_checkpoints_list

        results_archive = {"type": "map_tensor_v3",
                           "description": args.description or f"Maps for \'{img_name}\'",
                           "model": "resnet50",
                           "original_image": img,
                           "explained_class": explained_class,
                           "maps_tensor": maps_tensor,
                           "tensor_layout": tensor_layout}

        if results_export_thread:
            results_export_thread.join()

        results_export_thread = threading.Thread(target=np.savez_compressed,
                                                 name="ResultExportThread",
                                                 args=(f"{output_path}_{i}",),
                                                 kwargs=results_archive)
        results_export_thread.start()
    results_export_thread.join()

except Exception as e:
    del explainer
    raise e
