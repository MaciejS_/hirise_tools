import argparse
import os
from collections import OrderedDict

from tqdm.autonotebook import tqdm

from hirise.evaluation.game import DeletionGame, InsertionGame, BatchedInsertionGame, BatchedDeletionGame
from hirise.evaluation.runner import TqdmRunner, BatchedGameRunner
from hirise.experiment.utils import normalize
from hirise.utils import *

from hirise_tools.npz import load_from_npz
from hirise_tools.models import load_resnet50
from hirise_tools.utils import create_output_path


def parse_observed_class(observed_class_param):
    if observed_class_param:
        if observed_class_param == "all":
            return Ellipsis    # To select all items in the scores vector, which is of unknows size at this point
        else:
            return list(map(int, observed_class_param.split(',')))
    return None

def parse_game_type(game_type_param):
    if game_type_param == "both":
        return ["ins", "del"]
    else:
        return [game_type_param]

def parse_runs(runs_param):
    if '-' in runs_param:
        lower_bound, upper_bound = runs_param.split('-')
        return range(int(lower_bound), int(upper_bound))
    else:
        return range(int(runs_param))

def run_game(game_template, tensor, tensor_axes_descriptions):
    if len(tensor.shape) > 3:
        return np.stack([run_game(game_template, sub_tensor, tensor_axes_descriptions[1:])
                         for sub_tensor
                         in tqdm(tensor, leave=False, desc=tensor_axes_descriptions[0])])
    else:
        all_scores = []
        for smap in tqdm(tensor, leave=False, desc=tensor_axes_descriptions[0]):
            game = game_template(smap)
            scores = BatchedGameRunner().run(game)
            if scores.ndim == 1:
                scores = scores[..., np.newaxis] # add classes axis of size 1, if single observed class
            all_scores.append(scores)
        return np.array(all_scores)

def list_of_observed_classes(observed_classes, scores):
    """
    :param observed_classes:
    :param scores:
    :return:
    """
    if observed_classes is Ellipsis:
        return scores.shape[-1] # assuming that scores has (n_steps, n_classes) shape
    elif isinstance(observed_classes, (np.ndarray, int, list)):
        return observed_classes
    else:
        raise TypeError(f"Unexpected observed_classes type: {type(observed_classes)}")


parser = argparse.ArgumentParser()
parser.add_argument('-g', '--game', required=True, type=parse_game_type)
parser.add_argument('-d', '--dataset', type=os.path.abspath, required=True)
parser.add_argument('-r', '--runs', type=parse_runs, required=True, help="Range of runs to perform. Can either be X, in which case X runs will be performed, starting at 0, or X-Y where runs from X to Y (exclusive) will be performed.")
parser.add_argument('-o', '--output', type=os.path.abspath, required=False, help='Path to file or directory where game scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
parser.add_argument('-s', '--step', type=int, required=True, help="Number of pixels to alter at every step of the game")
parser.add_argument('-c', '--observed-classes', default=None, type=parse_observed_class, help="Index of observed class. The default value is loaded from dataset. Negative values will cause top N classes to be observed. Word 'all' will cause all class scores to be recorded.")
parser.add_argument('-b', '--batch', type=int, help="Number of images to process on GPU in parallel")
parser.add_argument('--device', type=torch.device, default='cuda:0', help="Torch-compatible name of a device used for computation. E.g. 'cpu', 'gpu:0'")
args = parser.parse_args()

game_types = {"ins": BatchedInsertionGame,
              "del": BatchedDeletionGame}

if args.output and not os.path.isdir(args.output) and len(args.game) > 1:
    raise RuntimeError("Custom output file is not supported when running multiple game types")

dataset_name = args.dataset.split('/')[-1].split('.')[0]
dataset = load_from_npz(args.dataset)
dataset["tensor_layout"] = dataset["tensor_layout"].item()

if dataset['model'] == "resnet50":
    model, model_input_size = load_resnet50(args.device)
else:
    raise NotImplementedError("Models other than resnet50 are not supported yet")

selected_game_types = {k: v for k, v in game_types.items() if k in args.game}

combined_tensors = dataset['maps_tensor']
normalized_maps = normalize(combined_tensors, axes=(-1, -2))
observed_classes = args.observed_classes or dataset['explained_class']

def game_provider(explanation):
    return GameImpl(model=model,
                    image=dataset['original_image'],
                    explanation=explanation,
                    step_size=args.step,
                    batch_size=args.batch,
                    substrate_fn=GameImpl.default_substrate(),
                    observed_classes=observed_classes)

for game_name, GameImpl in tqdm(selected_game_types.items(), desc="game type"):
    all_scores = run_game(game_provider, combined_tensors, list(dataset["tensor_layout"].keys()))

    default_file_name = f"{dataset_name}_{game_name}_game_scores_normd_{args.step}.npz"
    target_file = create_output_path(args.output, default_file_name)
    tensor_layout = OrderedDict(list(dataset["tensor_layout"].items())[:-2]
                                + [("step_scores", all_scores.shape[-1])]
                                + [("observed_classes", list_of_observed_classes(observed_classes, all_scores))])

    obj = {"description": f"{GameImpl.__class__.__name__} scores for normalized maps in {dataset_name} dataset",
           "source_dataset": args.dataset,
           "step_size": args.step,
           "substrate_fn": "default",
           "scores_tensor": np.array(all_scores),
           "explained_class": observed_classes,
           "tensor_layout": tensor_layout}
    np.savez_compressed(target_file, **obj)
