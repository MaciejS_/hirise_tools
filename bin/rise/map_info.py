import os
from collections import OrderedDict

import numpy as np
import argparse

from pprint import pprint


from hirise_tools.npz import load_from_npz

parser = argparse.ArgumentParser()
parser.add_argument("path", type=os.path.abspath)

args = parser.parse_args()

archive = load_from_npz(args.path)

for key, value in archive.items():
    # if key == "tensor_layout":
    #     print(f"{key}: {value}")
    # else:
    if key == "tensor_layout" and value.shape:
        # special case for legacy tensors
        print(f"{key}: {value}")
    elif value.shape:
        print(f"{key}: ndarray of shape {value.shape}")
    else:
        item = value.item()
        if isinstance(value, (dict, OrderedDict)):
            print(f"{key}: ", end="")
            pprint(value)
        else:
            print(f"{key}: {item}")

