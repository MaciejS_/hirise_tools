import argparse

import torch
import numpy as np
from timeit import default_timer as timer
from hirise.explanations import MaskGen
from tqdm.autonotebook import trange, tqdm

# Measures the speedup of parallel RISE mask generation
# Stores the results as 

parser = argparse.ArgumentParser()
parser.add_argument('-j', '--jobs', nargs="+", type=int, required=True)
parser.add_argument('-m', '--masks', nargs="+", type=int, required=True)
parser.add_argument('-r', '--reps', type=int, default=7)
args = parser.parse_args()

pool_sizes = args.jobs
reps = args.reps
list_n = args.masks
s = 10
p1 = 0.2

perf = np.zeros((len(list_n), len(pool_sizes), reps))
for nidx, N in tqdm(list(enumerate(list_n)), desc="N values", leave=True):
    for idx, pool_size in tqdm(list(enumerate(pool_sizes)), desc="pool sizes", leave=False):
        for rep in trange(reps, desc="reps", leave=False):
            filter_gen = MaskGen(pool_size)

            start_time = timer()
            masks = filter_gen.generate_masks(N, s, p1, (224, 224))
            masks = torch.from_numpy(masks).float()
            perf[nidx, idx, rep] = N / (timer() - start_time)

        with open("perf_results.txt", 'a') as outfile:
            mean_perf = perf[nidx, idx].mean(axis=None)
            perf_stddev = perf[nidx, idx].std(axis=None) 
            outfile.write(f"{N},{pool_size}: {mean_perf:.2f} +/- {perf_stddev:.3f} it/s\n")
           
        obj = {"description": "Entries in the 'scores' array express masks generated per second with the specific combination of N masks to generate in total by n_jobs",
               "N_values": list_n,
               "njob_values": pool_sizes,
               "layout": ["N", "njobs", "repetition"],
               "scores": perf}
    
        np.savez_compressed("perf_results.npy", **obj)



