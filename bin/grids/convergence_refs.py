import contextlib
import os
import pickle
import numpy as np

from hirise.occlusions.gridgen import *

from tqdm.autonotebook import tqdm, trange

from hirise.explainer import RISE
from hirise.occlusions.maskgen import BandedMaskGen
from hirise.occlusions.upsampling import Upsampler

from hirise.mask_evaluator import MaskEvaluator
from hirise_tools.models import load_resnet50
from hirise.utils import read_tensor
from hirise.structures import ArbitraryCheckpoints, CombinedMask, ClassFilter



cpu = torch.device("cpu")
gpu = torch.device("cuda:0")
GPU_BATCH_SIZE = 200


coord_gpu = CoordinateGridGen(gpu)
smart_coord_gpu = SmartCoordinateGridGen(gpu)
perm_gpu = PermutationGridGen(gpu)
threshold_non_fixing_gpu = ThresholdGridGen(gpu)
thr_gpu = threshold_non_fixing_gpu 

fixing_thr_coord_gpu = FixingGridGen(grid_gen=threshold_non_fixing_gpu, fixer_grid_gen=smart_coord_gpu)
fixing_thr_thr_gpu = FixingGridGen(threshold_non_fixing_gpu)
fixing_thr_perm_gpu = FixingGridGen(threshold_non_fixing_gpu, perm_gpu)


fixing_thr_coord_gpu_p = FixingGridGen(grid_gen=threshold_non_fixing_gpu, fixer_grid_gen=smart_coord_gpu, fixing_threshold=10**-7)
fixing_thr_thr_gpu_p = FixingGridGen(threshold_non_fixing_gpu, fixing_threshold=10**-7)
fixing_thr_perm_gpu_p = FixingGridGen(threshold_non_fixing_gpu, perm_gpu, fixing_threshold=10**-7)
resnet50, r50_input_size = load_resnet50(gpu)


goldfish = read_tensor('./datasets/images/goldfish.jpg')
goldfish_gpu = goldfish.to(gpu)

IMG = goldfish_gpu
CLASS = 1

### generate refs

single_map_shape = (224, 224)

def gen_maps(gridgen, s, p1, checkpoint_idxs):
    mask_gen = BandedMaskGen(gridgen, Upsampler(cpu))
    evaluator = MaskEvaluator(resnet50, gpu, batch_size=GPU_BATCH_SIZE, use_tqdm=False)
    rise = RISE(mask_gen=mask_gen,
                mask_evaluator=evaluator,
                mask_batch_size=GPU_BATCH_SIZE,
                use_tqdm=True)
    
    retainer = ArbitraryCheckpoints(CombinedMask(single_map_shape, p1), checkpoint_idxs = checkpoint_idxs)
    rise.explain(img=IMG,
                 N=max(checkpoint_idxs),
                 s=s,
                 p1=p1,
                 explained_class=CLASS,
                 mask_retainers=[ClassFilter(retainer, CLASS)])
    return torch.stack([chk for i, chk in retainer.get_checkpoints()]).cpu().numpy()


def find_x_for_y(f, limits, y, atol=0.005):
    """
    Finds `x` such that `f(x) == y`, within provided `limits` using binary search algorithm.
    `f` must be monotonic within `limits`, otherwise the behaviour is undefined.
    """
    target = y
    is_ascending = (f(limits[1]) - f(limits[0])) > 0
    inp = limits[1] - limits[0] / 2
    result = f(inp)
    while True:
        step = (limits[1] - limits[0]) / 2
        too_high_on_slope = result > target
        if too_high_on_slope ^ is_ascending:
            # move right
            limits[0] = inp
            inp += step
        else:
            # move left
            limits[1] = inp
            inp -= step
        result = f(inp)
        if np.isclose(target, result, atol=atol):
            return inp


@contextlib.contextmanager
def save_on_exit(pkl_obj, path, mode="wb"):
    """
    Conte
    :param pkl_obj:
    :param path:
    :param mode: output file opening mode
    :return:
    """
    try:
       yield pkl_obj
    except Exception as e:
        path += ".brk"
        print(f"Caught {e}, results gathered so far will be saved in {path}")
    finally:
        try:
            with open(path, mode) as pkl_file:
                if 'b' in mode:
                    pickle.dump(pkl_obj, pkl_file)
                else:
                    pkl_file.write(pickle.dumps(pkl_obj))
        except Exception as e:
            print(f"Critical error: {e} while saving results in {path}")




target_pu = [0.9, 0.6, 0.3, 0.05]
ref_s = [3, 4, 5, 7]
n_reps = 3

long_lim = 50_000
short_lim = 10_000

long_checkpoint_idxs = list(filter(lambda x: x <= long_lim, sum(list(list(range(10**i, 10**(i+1), 10**i)) for i in range(5)), [])))
short_checkpoint_idxs = list(filter(lambda x: x <= short_lim, long_checkpoint_idxs))

params = [{"s": [s],
       "p": list(map(lambda pu: find_x_for_y(f=lambda p1: FixingGridGen.uninformative_grid_likelihood((s, s), p1),
                                             limits=[0, 0.5],
                                             y=pu),
                     target_pu))}
      for s 
      in ref_s]



for rep in trange(n_reps, desc="rep", leave=True):
    pkl_obj = {"paramsets": params,
               "chks_lists": (long_checkpoint_idxs, short_checkpoint_idxs),
               "pu": target_pu,
               "gens": "(fixing_thr_thr_gpu_p @ 10^-7, thr_gpu)",
               "maps": None,
               "layout": ["gen", "paramset_id", ["s", "p", "rep", "chk", "y", "x"]]}

    with save_on_exit(pkl_obj, path=f"./tmprun/conv.pkl.{rep}", mode="wb"):
        all_maps = {}
        pkl_obj["maps"] = all_maps # place `all_maps` in pickled object now, so that if an exception is thrown, everything up to this point is saved
        for gen, name, chks in tqdm(list(zip((fixing_thr_thr_gpu_p, thr_gpu),
                                             ("fixing_thr_thr", "thr"),
                                             (long_checkpoint_idxs, short_checkpoint_idxs))),
                                    desc="gen",
                                    leave=False):
            maps_for_gen = []
            all_maps[name] = maps_for_gen # again, pre-assign to prevent data loss
            for paramset in tqdm(params, desc="params", leave=False):
                s_values = paramset['s']
                p1_values = paramset['p']

                paramset_maps = []
                for s_idx, p_idx in tqdm(list(np.ndindex(len(s_values), len(p1_values))), desc='sp', leave=False):
                    maps = gen_maps(gen, s_values[s_idx], p1_values[p_idx], chks)
                    paramset_maps.append(maps)
                maps_for_gen.append(np.stack(paramset_maps).reshape((len(s_values), len(p1_values), 1, len(chks), *single_map_shape)))

