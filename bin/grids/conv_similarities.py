import os
import pickle
import re
from datetime import datetime

import numpy as np
import argparse

from skimage.metrics import structural_similarity as ssim

from hirise.experiment.utils import confusion_triu, confusion_matrix, normalize
from hirise_nbutils.metrics import spearman_rank, hog
from hirise_tools.utils import abspath_exists, parent_exists

from tqdm.autonotebook import tqdm


START_DATE = datetime.now()

def combine(parts):
    gen_names = parts[0]["maps"].keys()
    result = {gen: np.moveaxis(np.stack([p["maps"][gen]
                                         for p
                                         in parts]), 0, -3).squeeze()
              for gen
              in gen_names}
    return result


similarity_methods = {"hog": lambda x,y: spearman_rank(hog(x), hog(y)),
                      "spearman": spearman_rank,
                      "ssim": lambda x, y: ssim(x, y, window_size=5)}

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-path', type=os.path.abspath, help="Path to file containing ")
parser.add_argument('-o', '--output-path', type=parent_exists, help="Path where results will be stored")
parser.add_argument('-m', '--metrics', nargs='+', required=True, type=str, choices=list(similarity_methods.keys()), help='Similarity metrics to calculate')
parser.add_argument("--parted", action="store_true", help="Load parted result. Part files are suffixed with a dot and integer.")
# TODO: Add support for parted calculations if filesizes exceed RAM capacity (load 2 unique parts (use ndindex), calculate, repeat)
args = parser.parse_args()

if args.parted:
    ################ combiner logic - export
    input_dir, input_filename = os.path.split(args.input_path)
    part_files = list(filter(lambda x: re.search(fr"{input_filename}.*", x), os.listdir(input_dir)))

    if part_files:
        parts = []
        for partfile in part_files:
            with open(os.path.join(input_dir, partfile), mode='rb') as pkl_file:
                parts.append(pickle.load(pkl_file))

        maps = combine(parts)
        checkpoint_lists = {"fixing_thr_thr": parts[0]['chks_lists'][0], "thr": parts[0]["chks_lists"][1]}
        paramsets = {name: {'s': sum((p['s'] for p in parts[0]['paramsets']), []),
                                     'p': parts[0]['paramsets'][0]['p'],
                                     'N': chk}
                     for name, chk
                     in checkpoint_lists.items()}

        combined_layout = ["s", "p", "chk", "rep", "y", "x"]
    else:
        raise RuntimeError("reference maps archive not found. Run hirise_tools/convergence_refs.py")

    #########################
else:
    with open(args.input_path, mode='rb') as pkl_file:
        arx = pickle.load(pkl_file)

        maps = arx["maps"]
        expected_gen_set = {"fixing_thr_thr", "thr"}
        actual_gen_set = set(maps.keys())
        if actual_gen_set != expected_gen_set:
            raise NotImplementedError(
                f"For now this script supports only one specific archive instance. Expected to find maps for {expected_gen_set}, got {actual_gen_set}")

        paramsets = arx["paramsets"]
        checkpoint_lists = {"fixing_thr_thr": arx['chks_lists'][0], "thr": arx["chks_lists"][1]}

normalized_maps = {k: normalize(v, axes=(-1, -2)) for k, v in maps.items()}
unnormalized_maps = maps
maps = normalized_maps

selected_methods = dict(filter(lambda kv: kv[0] in args.metrics, similarity_methods.items()))

# calculate similarities
# direct (n->n) similarities
similarities = {}
for sim_name, sim_method in tqdm(list(selected_methods.items()), leave=True, desc="direct"):
    gen_results = {}
    similarities[sim_name] = gen_results
    for gen_name, gen_maps in tqdm(list(maps.items()), desc=sim_name, leave=False):
        gen_results[gen_name] = confusion_triu(maps=gen_maps, metric=sim_method, op_dims=2)

# to_final (n->max_n) similarities (remember to analyze diagonals separately - increased self-similarity might be a thing)
chk_similarities = {}
for sim_name, sim_method in tqdm(list(selected_methods.items()), leave=True, desc="to_converged"):
    gen_results = {}
    chk_similarities[sim_name] = gen_results
    for gen_name, gen_maps in tqdm(list(maps.items()), desc=sim_name, leave=False):
        target_shape = (*gen_maps.shape[:2], gen_maps.shape[2] - 1, gen_maps.shape[3], gen_maps.shape[3])
        checkpoints = gen_maps[:, :, :-1].reshape(*gen_maps.shape[:2], -1, *gen_maps.shape[-2:])
        converged_maps = gen_maps[:, :, -1]
        gen_results[gen_name] = confusion_matrix(checkpoints, converged_maps, metric=sim_method, op_dims=2).reshape(
            *target_shape)

# cross-similarities (both direct and to_final but comparing various generator types with one another) (remember to analyze diagonals separately)
thr_to_ref_similarities = {}
for sim_name, sim_method in tqdm(list(selected_methods.items()), leave=True, desc="cross-gen"):
    gen_results = {}
    thr_to_ref_similarities[sim_name] = gen_results

    n_thr_chk = len(checkpoint_lists["thr"])
    ref_maps = maps["fixing_thr_thr"]
    thr_maps = maps["thr"]

    converged_refs = ref_maps[:, :, -1]

    thr_flattened = thr_maps.reshape(*thr_maps.shape[:2], -1, *thr_maps.shape[-2:])

    gen_results["direct"] = confusion_matrix(thr_maps, ref_maps[:, :, :n_thr_chk], metric=sim_method,
                                             op_dims=2).reshape(*thr_maps.shape[:4], ref_maps.shape[3])

    target_shape = (*ref_maps.shape[:2], thr_maps.shape[2], ref_maps.shape[3], thr_maps.shape[3])
    gen_results["to_converged"] = confusion_matrix(thr_flattened, converged_refs, metric=sim_method,
                                                   op_dims=2).reshape(*target_shape)

# store similarities
pkl_obj = {
    "format"
    "input_file": args.input_path,
    "source_metadata": {
        "map_tensor_shapes": {k: v.shape for k, v in maps.items()},
        "checkpoint_idxs": paramsets,
        "paramsets": checkpoint_lists
        },
    "similarities": {
        "direct": similarities,
        "to-final": chk_similarities,
        "cross-gen": thr_to_ref_similarities,
    },
    "layout": {"direct": ["s", "p", "chk", "similarities_triu"],
               "to-final": ["s", "p", "chk", "n_reps", "n_reps"],
               "cross-gen": {"direct": ["s", "p", "chk", "gen1_reps", "gen2_reps"],
                             "to_converged": ["s", "p", "chk", "gen1_reps", "gen2_reps"]}},
    "date": {"start": START_DATE, "end": datetime.now()}
}

with open(args.output_path, "wb") as pklf:
    pickle.dump(pkl_obj, pklf)