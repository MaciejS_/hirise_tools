import argparse
import os
import pickle

import numpy as np
import torch
import math

import scipy.special

import more_itertools

from tqdm.autonotebook import tqdm, trange

from hirise.structures import ArbitraryCheckpoints, CombinedMask, ClassFilter
from hirise.occlusions.upsampling import ParallelizedUpsampler
from hirise.occlusions.gridgen import PermutationGridGen, ThresholdGridGen, FixingGridGen
from hirise.utils import read_tensor
from hirise.mask_evaluator import MaskEvaluator
from hirise.structures_utils import hash_tensor

from hirise_tools.models import load_resnet50
from hirise_tools.checkpoints import npow10

# utility code for this experiment
from typing import Sequence
def pad_checkpoint_tensors(checkpoint_tensors: Sequence[torch.Tensor]):
    """
    Equalizes the size of first dimension of each tensor in `checkpoint_tensors`.
    Padding is performed by duplicating the last element along the first axis enough times to match the length of the longest tensor
    """
    longest_tensor_length = max(v.shape[0] for v in checkpoint_tensors)
    result = []
    for i in range(longest_tensor_length):
        extended_tensor = torch.stack([tensor[i if len(tensor) > i else -1] # use the last checkpoint if counter has moved beyond the valid range for this `k`
                                       for tensor
                                       in checkpoint_tensors])
        result.append(extended_tensor)
    return torch.stack(result)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-path', type=os.path.abspath, required=True)
parser.add_argument('-o', '--output-path', type=os.path.abspath, required=True, default="", help='Path to file where scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
parser.add_argument('-d', '--device', type=torch.device, default='cuda:0', help="Torch-compatible name of a device used for computation. E.g. 'cpu', 'cuda:0'")
parser.add_argument('-b', '--batch', type=int, default=250, help="Number of masks evaluated on GPU in parallel. Limited by the amount of VRAM")
parser.add_argument('-r', '--reps', type=int, default=3, help="Requested number of instances of each reference mask. If not specified, it will be the same as in source archive")
parser.add_argument('-c', type=int, required=True, help="Observed class")
parser.add_argument('-s', type=int, default=None, required=True, help="s")
parser.add_argument('-N', type=int, default=None, required=True, help="Number of grids per k")
parser.add_argument('-j', '--cpu-jobs', type=int, default=-2, required=False, help="Max number of CPU threads to use for parallelizable tasks")
parser.add_argument('--debug', action="store_true")
parser.add_argument('--hybrid', action="store_true")
args = parser.parse_args()

output_filename = os.path.split(args.output_path)[-1]
# random_hash = str(hash(os.urandom(64)))[:8]
tmp_filepath = f"/tmp/tmpfs/{output_filename}.tmpstate"

if args.debug:
    print(args)
    print(f"Storing temporary state in: {tmp_filepath}")

N = args.N
N_REPS = args.reps
s = args.s
ks = list(range(2, s ** 2, math.ceil(s / 2)))
chk_idx = [i * 100 for i in range(1, 10)] + [i * 1000 for i in
                                             range(1, (N // 1000) + 1)]  # list(filter(npow10, range(100, N+1)))
IMG = read_tensor(args.input_path)
CLASS = args.c

cpu = torch.device("cpu")
gpu = torch.device(args.device)
resnet50, target_size = load_resnet50(gpu)
pgg = PermutationGridGen(cpu)
tgg = ThresholdGridGen(cpu)
hgg = FixingGridGen(tgg, tgg, fixing_threshold=10**-7)
evaluator = MaskEvaluator(resnet50, gpu, batch_size=args.batch, use_tqdm=False)
upsampler = ParallelizedUpsampler(pool_size=args.cpu_jobs)

gridgen = hgg if args.hybrid else pgg

grids = [{k: gridgen.generate_grid(min(int(scipy.special.comb(s ** 2, k) * 3), N), (s, s), k / s ** 2).cpu().numpy()
          for k
          in range(2, s ** 2, math.ceil(s / 2))}
         for i in range(N_REPS)]

unique_grid_counts = [{k: more_itertools.map_reduce(v, keyfunc=hash_tensor, valuefunc=lambda x: 1, reducefunc=sum)
                       for k, v
                       in grids[i].items()}
                      for i in range(N_REPS)]

unique_grids = [{k: more_itertools.map_reduce(v, keyfunc=hash_tensor, reducefunc=lambda x: x[0])
                 for k, v
                 in grids[i].items()}
                for i in range(N_REPS)]

fill_rates = [{k: more_itertools.map_reduce(v, keyfunc=lambda x: int(np.sum(x)), valuefunc=lambda x: 1, reducefunc=sum)
               for k, v
               in grids[i].items()}
              for i in range(N_REPS)]


checkpoint_retainers = [[ArbitraryCheckpoints(CombinedMask(target_size, k / (s ** 2)), chk_idx) for k in ks] for i in
                        range(N_REPS)]
retainers = [[ClassFilter(r, CLASS) for r in checkpoint_retainers[i]] for i in range(N_REPS)]
batch_bounds = list(range(0, N + 1, args.batch*2))
try:
    for batch_id, batch_bounds in tqdm(list(enumerate(zip(batch_bounds, batch_bounds[1:]))), desc="batches"):
        for rep_idx in trange(N_REPS, desc="reps", leave=False):
            for (k, unq_grids), retainer in tqdm(list(zip(unique_grids[rep_idx].items(), retainers[rep_idx])), desc="k",
                                                 leave=False):
                _grids = list(unq_grids.values())
                if batch_bounds[0] < len(_grids):
                    batch_bounds = (batch_bounds[0], min(batch_bounds[1], len(_grids)))
                    # upsample
                    masks = upsampler.upsample(torch.as_tensor(_grids[slice(*batch_bounds)]), target_size)

                    # evaluate current batch of grids
                    mask_scores = evaluator.evaluate(IMG, masks, MaskEvaluator.ALL_CLASSES)

                    # add to retainers
                    retainer.add_many(masks.squeeze().cpu(), mask_scores.cpu())
except Exception as e:
    print(f"Premature termination. Reason {e}")
finally:
    # extract checkpoints into tensor
    k_checkpoints = [{k: {idx: state for idx, state in retainer.get_checkpoints()}
                         for k, retainer
                         in zip(ks, checkpoint_retainers[i])}
                     for i in range(N_REPS)]

    checkpoint_tensors = [{k: torch.stack(list(map(torch.as_tensor, v.values())))
                           for k, v
                           in k_checkpoints[i].items()}
                          for i in range(N_REPS)]

    aligned_chk_t = pad_checkpoint_tensors([pad_checkpoint_tensors(list(checkpoint_tensors[i].values())) for i in range(N_REPS)])

    with open(tmp_filepath, "wb") as ofile:
        obj = {"args": args,
               "checkpoint_tensors": aligned_chk_t,
               "grid_stats": {"unique": unique_grid_counts,
                              "fill_rates": fill_rates}}
        pickle.dump(obj, ofile)

# aligned_chk_t dims: [N_REPS, checkpoints, k, y, x]


# calculate similarities
from hirise.experiment.utils import parallel_confusion_triu
from hirise_nbutils.metrics import spearman_rank, mse2, hog, pearson, ssim

similarity_methods = {"mse": mse2,
                      "hog": lambda x,y: spearman_rank(hog(x), hog(y)),
                      "spearman": spearman_rank,
                      "ssim": lambda x, y: ssim(x, y, window_size=5)}# def similarities_inter_k(maps_tensor, similarity_metric):

# similarities between pairs of varying k, within the same run (no need to calculate inter-run, because maps for each k are generated separately)
inter_k_siml = {}
for sim_name, sim_method in tqdm(list(similarity_methods.items()), leave=False):
    inter_k_siml[sim_name] = parallel_confusion_triu(aligned_chk_t.cpu().numpy(), metric=sim_method, op_dims=2, k=1, n_jobs=args.cpu_jobs)
print(list(inter_k_siml.values())[0].shape)

# similarities between maps of same k, from different runs - to gauge convergence and therefoe the trustworthiness of results obtained above
intra_k_siml = {}
for sim_name, sim_method in tqdm(list(similarity_methods.items()), leave=False):
    reps_at_op_dim = np.moveaxis(aligned_chk_t.cpu().numpy(), [0, 1, 2], [2, 0, 1]) # move reps dimension to the position where triu will be built
    intra_k_siml[sim_name] = parallel_confusion_triu(reps_at_op_dim, metric=sim_method, op_dims=2, k=1, n_jobs=args.cpu_jobs)
print(list(intra_k_siml.values())[0].shape)

# 1. calculate lower triu among maps on current checkpoint (use -1 if no further checkpoints for given k)
# optionally repeat for all
# 2. generate a heatmap plot

with open(args.output_path, "wb") as ofile:
    obj = {"args": args,
           "checkpoint_tensors": aligned_chk_t,
           "intra_k_siml": intra_k_siml,
           "inter_k_siml": inter_k_siml,
           "grid_stats": {"unique": unique_grid_counts,
                          "fill_rates": fill_rates}}
    pickle.dump(obj, ofile)

if os.path.exists(tmp_filepath):
    os.remove(tmp_filepath)
