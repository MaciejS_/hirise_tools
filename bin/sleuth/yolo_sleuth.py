import argparse
import pickle
import typing as t

import os
import cv2
import more_itertools
import torch
import numpy as np

from pprint import pformat

from chardet import detect
from pytorchyolo import models, detect, utils
from hirise_tools.utils import walk_files
from pytorchyolo.utils.transforms import DEFAULT_TRANSFORMS, Resize
from torchvision import transforms

from tqdm.autonotebook import tqdm

IMG_SIZE = 416


class YoloResult:
    def __init__(self, raw_bbox_array):
        self.bounding_boxes = raw_bbox_array[:, :-2]
        self.scores = raw_bbox_array[:, -2]
        self.classes = raw_bbox_array[:, -1]
        self.n = len(raw_bbox_array)

    def __dict__(self):
        return {"bbox":    self.bounding_boxes,
                "scores":  self.scores,
                "classes": self.classes}

    def __repr__(self):
        return pformat(self.__dict__())


parser = argparse.ArgumentParser(description="Recursively walks the `--source-dir` and returns (class_idx, confidence_score) pairs for all detected images."
                                             "Results are stored as a pickled dict."
                                             "Images raising errors on load are ignored and their names are stored in  ")
parser.add_argument('-i', '--source-dir', required=True, help="This and all subdirectories will be recursively searched for images")
parser.add_argument('-o', '--output-path', required=True, help="Path where results will be pickled.")
parser.add_argument('-e', '--extensions', nargs='+', required=False, default=["jpg", "png", "jpeg"], help="Accepted extensions of image files. Default: %(default)s")
parser.add_argument('--min-confidence', type=float, required=False, default=.5, help="Minimal confidence threshold for detection")
parser.add_argument('--nms-threshold', type=float, required=False, default=.5, help="IOU threshold for non-maximum suppression")
# parser.add_argument('-k', '--top-k', required=False, default=10, type=int, help="Number of highest confidence scores to retain, together with class IDs. Default: %(default)d")
parser.add_argument('-b', '--batch-size', required=False, default=1, type=int, help="Number of images concurrently evaluated on GPU. Default: %(default)d")
parser.add_argument('-d', '--device', required=False, default=(torch.device("cuda:0")), type=torch.device, help="Device used to run computations. Default: %(default)s")
# parser.add_argument('-t', '--task', required=True, choices=['detection', 'classification', 'c', 'd'], type=str, help="Selects the task performed by Sleuth. Imapcts the output format.")
parser.add_argument('--center-crop', required=False, default=False, action="store_true", help="If this flag is used, images will be center-cropped to a square shape before rescaling to model input shape")
parser.add_argument('--fp16', required=False, default=False, action="store_true", help="Enables half-precision mode. Use with caution, some numerical errors may follow.")
parser.add_argument('-v', '--verbose', required=False, default=False, action="store_true", help="Enables half-precision mode. Use with caution, some numerical errors may follow.")
parser.add_argument('--disable-tqdm', required=False, default=False, action="store_true", help="Enables half-precision mode. Use with caution, some numerical errors may follow.")
parser.add_argument('--yolo-root', required=True, help="Path to the directory containing YOLOv3 config and weights")
# parser.add_argument('--loaders', type=int, required=False, default=1, help="number of data loaders to spawn. Workload will be distributed evenly")


yolo_imgloader = cv2.imread

def calc_bounding_boxes(path_img_pairs_batch: t.Sequence[t.Tuple[str, torch.Tensor]],
                        model,
                        img_size=416,
                        confidence_threshold=0.5,
                        nms_threshold=0.5,
                        ):
    config = {"img_size": img_size,
              "confidence_threshold": confidence_threshold,
              "nms_threshold": nms_threshold}
    return [(path, YoloResult(detect.detect_image(model, img, **config)))
            for path, img
            in path_img_pairs_batch]


if __name__ == "__main__":
    args = parser.parse_args()

    MODEL_NAME = "YOLOv3"
    MODEL_DATASET = "MSCOCO"
    NNET_CONFIG_PATH = os.path.join(args.yolo_root, 'cfg', 'yolov3.cfg')
    YOLO_WEIGHTS_PATH = os.path.join(args.yolo_root, 'weights', 'yolov3.weights')
    yolo = models.load_model(NNET_CONFIG_PATH, YOLO_WEIGHTS_PATH).to(args.device)

    SUPPORTED_EXTENSIONS = {f".{x}" for x in args.extensions}

    # region load image list
    dataset_root = args.source_dir
    all_files = list(walk_files(dataset_root))
    print(f"{len(all_files)} files found")
    all_files = list(filter(lambda x: os.path.splitext(x)[-1].lower() in SUPPORTED_EXTENSIONS, all_files))
    print(f"{len(all_files)} are image files")
    # region load image list

    pipeline = transforms.Compose([
        DEFAULT_TRANSFORMS,
        Resize(IMG_SIZE)
    ])

    full_imgloader = lambda path: pipeline((yolo_imgloader(path), np.zeros((1, 5))))[0].unsqueeze(0)

    # region evaluate (plain)
    BATCH_SIZE = args.batch_size
    cheeky_dataloader = map(
        lambda files_imgs_tuple: ([files_imgs_tuple[0]], torch.cat(files_imgs_tuple[1])), # jit stacking logic
        zip(
            more_itertools.chunked(all_files, BATCH_SIZE),
            more_itertools.chunked(map(full_imgloader, all_files), BATCH_SIZE)
        )
    )
    """
    .detect() expects the data loader to return pairs of (list_of_filenames, 4d_stacked_img_tensor)
    and this is exactly what `cheeky_dataloader` emulates without having to put in the effort of subclassing DataLoader
    """
    bounding_boxes, used_files_list = detect.detect(yolo,
                                           cheeky_dataloader,
                                           conf_thres=args.min_confidence,
                                           nms_thres=args.nms_threshold,
                                           img_size=IMG_SIZE, # this argument is ignored anyway
                                           output_path="/tmp/foo" # this path is not used anyway
                                           )
    # endregion evaluate (plain)

    with open(args.output_path, 'wb') as outfile:
        obj = {
            "description":   f"Bounding boxes for objects detected by {MODEL_NAME} trained on {MODEL_DATASET}"
                             f"All images have been resized to {IMG_SIZE}x{IMG_SIZE}, along with bounding box coordinates",
            "bbox_canvas_shape": (IMG_SIZE, IMG_SIZE),
            "params":        args,
            "used_images":   used_files_list,
            "bboxes":        bounding_boxes,
            "tensor_layout": ["xmin", "ymin", "xmax", "ymax", "confidence", "class"],
        }
        pickle.dump(obj, outfile, protocol=pickle.HIGHEST_PROTOCOL)
    exit(0)
