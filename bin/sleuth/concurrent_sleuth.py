import typing as t

import os
import pickle
import argparse
import numpy as np
import torch
import more_itertools

import torchvision.transforms as tvt
import torchvision.transforms.functional as tvtf
from PIL import Image

from hirise.experiment.utils import top_k_classes
from hirise_tools.models import load_resnet50, model_warmup
from hirise_tools.concurrency import CloseableQueue, ConcurrentMapper, ConcurrentLoader, ConcurrentBatcher, \
    ConcurrentFlattener, \
    ConcurrentGatherer, TqdmList, TqdmQueue

from hirise_tools.utils import walk_files

parser = argparse.ArgumentParser(description="Recursively walks the `--source-dir` and returns (class_idx, confidence_score) pairs for all detected images."
                                             "Results are stored as a pickled dict."
                                             "Images raising errors on load are ignored and their names are stored in  ")
parser.add_argument('-i', '--source-dir', required=True, help="This and all subdirectories will be recursively searched for images")
parser.add_argument('-o', '--output-path', required=True, help="Path where results will be pickled.")
parser.add_argument('-e', '--extensions', nargs='+', required=False, default=["jpg", "png", "jpeg"], help="Accepted extensions of image files. Default: %(default)s")
parser.add_argument('-k', '--top-k', required=False, default=10, type=int, help="Number of highest confidence scores to retain, together with class IDs. Default: %(default)d")
parser.add_argument('-b', '--batch-size', required=False, default=200, type=int, help="Number of images concurrently evaluated on GPU. Default: %(default)d")
parser.add_argument('-d', '--device', required=False, default=(torch.device("cuda:0")), type=torch.device, help="Device used to run computations. Default: %(default)s")
parser.add_argument('-t', '--task', required=True, choices=['detection', 'classification', 'c', 'd'], type=str, help="Selects the task performed by Sleuth. Imapcts the output format.")
parser.add_argument('--center-crop', required=False, default=False, action="store_true", help="If this flag is used, images will be center-cropped to a square shape before rescaling to model input shape")
parser.add_argument('--fp16', required=False, default=False, action="store_true", help="Enables half-precision mode. Use with caution, some numerical errors may follow.")
parser.add_argument('-v', '--verbose', required=False, default=False, action="store_true", help="Enables half-precision mode. Use with caution, some numerical errors may follow.")
parser.add_argument('--disable-tqdm', required=False, default=False, action="store_true", help="Enables half-precision mode. Use with caution, some numerical errors may follow.")
parser.add_argument('--loaders', type=int, required=False, default=1, help="number of data loaders to spawn. Workload will be distributed evenly")


def calc_top_k(path_img_pairs_batch: t.Sequence[t.Tuple[str, torch.Tensor]],
               model,
               device: torch.device,
               k: int) -> t.Tuple[str, np.ndarray]:
    names, images = zip(*path_img_pairs_batch)
    stacked_imgs = torch.cat(list(images)).to(device)
    topk = tuple(top_k_classes(model, stacked_imgs, k))
    top_ks = np.swapaxes(np.stack(topk), 0, 1)
    return tuple(zip(names, top_ks))

if __name__ == "__main__":
    args = parser.parse_args()

    if args.verbose:
        from pprint import pprint
        pprint(args.__dict__)

    SUPPORTED_EXTENSIONS = {f".{x}" for x in args.extensions}

    # region load model
    DEVICE = args.device
    model, MODEL_INPUT_SIZE = load_resnet50(DEVICE)
    if args.fp16:
        model = model.half()
    model_warmup(model, MODEL_INPUT_SIZE, args.batch_size, use_fp16=args.fp16)
    # endregion load model

    # region loading pipeline setup
    read_img = tvt.Compose([
        lambda x: Image.open(x),
        lambda x: x.convert(mode="RGB") if x.mode != "RGB" else x
    ])

    imgnet_normalize = tvt.Compose([
        tvt.ToTensor(),
        tvt.Normalize(mean=[0.485, 0.456, 0.406],
                      std=[0.229, 0.224, 0.225]),
        lambda x: torch.unsqueeze(x, 0)
    ])

    crop_step = (lambda x: tvtf.center_crop(x, min(x.size))) if args.center_crop else (lambda x: x)
    precision_change_step = (lambda x: x.half()) if args.fp16 else (lambda x: x)
    img_loader = tvt.Compose([
        read_img,
        crop_step,
        tvt.Resize(MODEL_INPUT_SIZE),
        imgnet_normalize,
        precision_change_step
    ])
    # endregion loading pipeline setup

    # region load image list
    dataset_root = args.source_dir
    all_files = list(walk_files(dataset_root))
    print(f"{len(all_files)} files found")
    all_files = [x
                 for x
                 in all_files
                 if os.path.splitext(x)[-1].lower() in SUPPORTED_EXTENSIONS]
    print(f"{len(all_files)} image files")
    # region load image list

    # region evaluation pipeline setup
    DISABLE_TQDM = args.disable_tqdm
    img_queue = TqdmQueue(CloseableQueue(maxsize=5000, producers=args.loaders), desc="LoaderQ", miniters=100, leave=True, disable=DISABLE_TQDM)
    loaders = [ConcurrentLoader(list(file_list_part),
                                loader=img_loader,
                                output_queue=img_queue)
               for file_list_part
               in more_itertools.divide(args.loaders, all_files)]

    batcher_q = TqdmQueue(CloseableQueue(maxsize=50), desc="BatcherQ", leave=True, disable=DISABLE_TQDM)
    batcher = ConcurrentBatcher(input_queue=img_queue,
                                output_queue=batcher_q,
                                name="Batcher",
                                batch_size=args.batch_size)

    evalq = TqdmQueue(CloseableQueue(maxsize=50), desc="EvalQ", leave=True, disable=DISABLE_TQDM)
    evaluator = ConcurrentMapper(mapper=lambda img_batch: calc_top_k(img_batch, model, DEVICE, args.top_k),
                                      input_queue=batcher.output_queue,
                                      output_queue=evalq,
                                      thread_name="Evaluator")

    flatq = TqdmQueue(CloseableQueue(maxsize=10_000), desc="FlattenedQ", leave=True, disable=DISABLE_TQDM)
    flattener = ConcurrentFlattener(input_queue=evaluator.output_queue,
                                    output_queue=flatq,
                                    name="Flattener")

    results = TqdmList(total=len(all_files), desc="Results", leave=False, disable=False)
    gatherer = ConcurrentGatherer(input_queues=[flattener.output_queue],
                                  output_container=results,
                                  thread_name="Gatherer")
    pipeline_stages = [*loaders, batcher, evaluator, flattener, gatherer]
    # region evaluation pipeline setup

    # Launch the evaluation pipeline
    for stage in pipeline_stages:
        stage.start()

    for stage in pipeline_stages:
        stage.join()

    path_topk_pairs = gatherer.get()
    paths, topk = map(list, zip(*path_topk_pairs))
    topk = np.stack(topk)

    with open(args.output_path, 'wb') as outfile:
        obj = { "description": "Top k classes for all images in imagenet. Images, TopKs and img sizes are in the same order",
                "params": args,
                "used_images": paths,
                "top_k": topk,
                "tensor_layout": ["img", "(class_idx, confidence_score)", "values"],
                "skipped_files": set(all_files) - set(paths)}
        pickle.dump(obj, outfile, protocol=pickle.HIGHEST_PROTOCOL)

    exit(0)
