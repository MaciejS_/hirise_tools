import typing as t
from pprint import pprint

import pickle
import itertools, more_itertools

import torch
import numpy as np
import os
import argparse

from tqdm.autonotebook import tqdm

from hirise_nbutils.imagenet_labels import synset, labels as imgnet_labels
from hirise.experiment.utils import top_k_classes
from hirise_tools.models import load_resnet50
from hirise.utils import read_tensor


# region helpers
def archive_info(sleuth_archive: t.Dict[str, t.Any]) -> t.Dict[str, t.Any]:
    """
    Extract key information about the archive and return as them as a dict
    """
    topk_shape = sleuth_archive["top_k"].shape
    return {
        "description":     sleuth_archive.get("description", "<no info"),
        "layout":          sleuth_archive.get("tensor_layout", "<no info>"),
        "shape":           topk_shape,
        "imgcount":        topk_shape[0],
        "k":               topk_shape[-1],
        "collection_type": type(sleuth_archive["used_images"]),
    }


def print_mismatches(mismatches: dict) -> None:
    for idx, mismatch in mismatches.items():
        print(f"\nMistmatch for img {all_files[idx]}")
        for k, (loaded_cidx, evaluated_cidx) in enumerate(zip(mismatch["expected"], mismatch["actual"])):
            # print(f"DEBUG: loaded: {loaded_cidx}\n evaluated: {evaluated_cidx}")
            labels = {"loaded":    imgnet_labels[loaded_cidx].split(',')[0],
                      "evaluated": imgnet_labels[evaluated_cidx].split(',')[0]}
            class_pair_mismatch = loaded_cidx != evaluated_cidx
            if class_pair_mismatch:
                print(f"{k} * ({loaded_cidx:3})->({evaluated_cidx:3}) ({labels['loaded']}), ({labels['evaluated']})")
            else:
                print(f"{k}   ({loaded_cidx:3})->({evaluated_cidx:3})")


# endregion helpers

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-path', type=os.path.abspath, required=True)
parser.add_argument('-d', '--device', type=torch.device, default='cuda:0',
                    help="Torch-compatible name of a device used for computation. E.g. 'cpu', 'cuda:0'")
parser.add_argument('-s', '--samples', type=int, default=None,
                    help="Specifies how many randomly sampled images to use for validation. By default, all results are validated")
parser.add_argument('-k', '--top-k', type=int, default=None,
                    help="Will assert equality of only the top K results. By default, all results for each image are be compared")
parser.add_argument('-b', '--batch', type=int, default=1000,
                    help="Number of masks evaluated on GPU in parallel. Limited by the amount of VRAM")
parser.add_argument('-q', '--quiet', default=False, action='store_true',
                    help="Do not print, signals result with exit code. 0 if validation passes, 1 if validation fails for some images, 2 if fails for all")
parser.add_argument('--fp16', default=False, action='store_true', help="Evaluate in half precision mode")
args = parser.parse_args()

# region the load part
# "/mnt/spcc/imagenette-320/topk.fp16.pkl"
with open(args.input_path, 'rb') as sleuth_pkl:
    sleuth_arx = pickle.load(sleuth_pkl)

    if sleuth_arx["top_k"].shape[0] == 2:
        sleuth_arx["top_k"] = np.swapaxes(sleuth_arx["top_k"], 1, 0)

# TODO: if used_files is not an ordered collection, fail the validation (though it did work for imgnet in the end, once i started using correct idxs)

inv_labels = {v: k for k, v in imgnet_labels.items()}

top_k = sleuth_arx["top_k"]
all_files = list(sleuth_arx["used_images"])

# endregion  the load part
arx_info = archive_info(sleuth_arx)
if not args.quiet:
    pprint(arx_info)

if arx_info["collection_type"] is set:
    exit(2)

# region re-sample

if args.samples:
    picked_idxs = more_itertools.sample(range(len(top_k)), args.samples)
else:
    picked_idxs = list(range(len(top_k)))

MAX_K = top_k.shape[-1]
if args.top_k:
    K_SLICE = slice(0, min(args.top_k, MAX_K))
else:
    K_SLICE = slice(0, MAX_K)

# endregion re-sample

# region the re-evaluate part
model, _ = load_resnet50(args.device)
if args.fp16:
    model = model.half()

adhoc_topk = []
for i, img_idx in tqdm(enumerate(picked_idxs), desc="evaluation", total=args.samples, disable=args.quiet):
    img = read_tensor(all_files[img_idx]).to(args.device)
    if args.fp16:
        img = img.half()
    adhoc_topk.append(list(top_k_classes(model, img, K_SLICE.stop)))

adhoc_topk = np.array(adhoc_topk)
# endregion the re-evaluate part

# region compare
original_topk = top_k[picked_idxs]

mismatched = {}
for img_idx, (_, loaded_classes), (_, evaluated_classes) in zip(picked_idxs, original_topk, adhoc_topk):
    if not np.all(loaded_classes[K_SLICE] == evaluated_classes[K_SLICE]):
        mismatched[img_idx] = {"expected": loaded_classes,
                               "actual":   evaluated_classes}

# endregion compare

# region report
if len(mismatched) > 0:
    if not args.quiet:
        print_mismatches(mismatched)
        print(f"{args.samples - len(mismatched)}/{args.samples} match")
    exit(1)
else:
    if not args.quiet:
        print(f"All {len(picked_idxs)} samples match")
    exit(0)
# endregion report
