# %%
import itertools
import os
import pickle

import more_itertools
import numpy as np
import torch
import torchvision.transforms as tvt
import torchvision.transforms.functional as tvtf
from PIL import Image
from hirise.experiment.utils import top_k_classes
from hirise_tools.models import load_resnet50
from more_itertools import chunked
from tqdm.autonotebook import tqdm

import threading
import argparse

# parallel evalutation code
from utils import hexhash


def evaluate(model, images, results_container, device, k=10):
    results_container.append(np.stack(list(top_k_classes(model, images.to(device), k))))

def evaluate_async(model, images, results_container, device, k=10):
    evaluator_thread = threading.Thread(target=evaluate,
                                        name="ImgEvaluatorThread",
                                        kwargs={"model": model,
                                                    "images": images,
                                                    "results_container": results_container,
                                                    "k": k,
                                                    "device": device})
    evaluator_thread.start()
    return evaluator_thread


parser = argparse.ArgumentParser(description="Recursively walks the `--source-dir` and returns (class_idx, confidence_score) pairs for all detected images."
                                             "Results are stored as a pickled dict."
                                             "Images raising errors on load are ignored and their names are stored in  ")
parser.add_argument('-i', '--source-dir', required=True, help="This and all subdirectories will be recursively searched for images")
parser.add_argument('-o', '--output-path', required=True, help="Path where results will be pickled.")
parser.add_argument('-e', '--extensions', nargs='+', required=False, default=["jpg", "png", "jpeg"], help="Accepted extensions of image files. Default: %(default)s")
parser.add_argument('-k', '--top-k', required=False, default=10, type=int, help="Number of highest confidence scores to retain, together with class IDs. Default: %(default)d")
parser.add_argument('-b', '--batch-size', required=False, default=200, type=int, help="Number of images concurrently evaluated on GPU. Default: %(default)d")
parser.add_argument('-d', '--device', required=False, default=(torch.device("cuda:0")), type=torch.device, help="Device used to run computations. Default: %(default)s")
parser.add_argument('--center-crop', required=False, default=False, action="store_true", help="If this flag is used, images will be center-cropped to a square shape before rescaling to model input shape")
parser.add_argument('--fp16', required=False, default=False, action="store_true", help="Enables half-precision mode. Use with caution, some numerical errors may follow.")
parser.add_argument('-v', '--verbose', required=False, default=False, action="store_true", help="Enables half-precision mode. Use with caution, some numerical errors may follow.")
args = parser.parse_args()

if args.verbose:
    from pprint import pprint
    pprint(args.__dict__)

SUPPORTED_EXTENSIONS = {f".{x}" for x in args.extensions}

# %%
DEVICE = args.device
model, MODEL_INPUT_SIZE = load_resnet50(DEVICE)
if args.fp16:
    model = model.half()

# %%
read_img = tvt.Compose([
    lambda x: Image.open(x),
    lambda x: x.convert(mode="RGB") if x.mode != "RGB" else x
])

imgnet_normalize = tvt.Compose([
    tvt.ToTensor(),
    tvt.Normalize(mean=[0.485, 0.456, 0.406],
                  std=[0.229, 0.224, 0.225]),
    lambda x: torch.unsqueeze(x, 0)
])


crop_step = (lambda x: tvtf.center_crop(x, min(x.size))) if args.center_crop else (lambda x: x)
precision_change_step = (lambda x: x.half()) if args.fp16 else (lambda x: x)
img_loader = tvt.Compose([
    read_img,
    crop_step,
    tvt.Resize(MODEL_INPUT_SIZE),
    imgnet_normalize,
    precision_change_step
])

def validate_shape(img_tensor):
    if len(img_tensor.shape) != 3 or img_tensor.shape[0] != 3 :
        raise ValueError(f"Expected (3, Any, Any) got {img_tensor.shape}")
    return img_tensor


validation_pipeline = tvt.Compose([
    read_img,
    tvt.ToTensor(),
    validate_shape
])


dataset_root = args.source_dir
all_files = list(itertools.chain.from_iterable([[os.path.join(path, f) for f in files] 
             for path, dirs, files 
             in os.walk(dataset_root)
             if files]))

# %%
print(f"{len(all_files)} files found")
all_files = [x
             for x
             in all_files
             if os.path.splitext(x)[-1].lower() in SUPPORTED_EXTENSIONS]
print(f"{len(all_files)} image files")

def image_loader(paths):
    images = {}
    for path in paths:
        try:
            images[path] = img_loader(path)
        except:
            pass

    return images

# %%
top_k = []
evaluator_thread = None
used_paths = []
torch.cuda.empty_cache()
progress_bar = tqdm(leave=True, total=len(all_files))
encountered_invalid_imgs = 0
for i, img_paths_batch in list(enumerate(chunked(all_files, args.batch_size))):
    # imgs = list(map(img_loader, img_paths_batch))
    loaded_imgs = image_loader(img_paths_batch)
    img_batch = torch.cat(list(loaded_imgs.values()))
    if evaluator_thread:
        if evaluator_thread.is_alive():
            img_batch = img_batch.to(DEVICE)
        evaluator_thread.join()
        progress_bar.update()
    evaluator_thread = evaluate_async(model, img_batch, results_container=top_k, device=DEVICE, k=args.top_k)
    used_paths.extend(loaded_imgs)

    encountered_invalid_imgs += len(loaded_imgs) - len(img_paths_batch)
    progress_bar.set_description(f"Evaluated imgs ({encountered_invalid_imgs} invalid)")
    progress_bar.update(len(img_paths_batch))

evaluator_thread.join()
progress_bar.close()

top_k = np.swapaxes(np.concatenate(top_k, axis=1), 0, 1)

# %%
with open(args.output_path, 'wb') as outfile:
    obj = { "description": "Top k classes for all images in imagenet. Images, TopKs and img sizes are in the same order",
            "params": args,
            "used_images": used_paths,
            "top_k": top_k,
            "tensor_layout": ["img", "(class_idx, confidence_score)", "values"],
            "skipped_files": set(all_files) - set(used_paths)}
    pickle.dump(obj, outfile)


exit(0)
