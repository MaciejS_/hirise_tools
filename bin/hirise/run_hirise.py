import argparse
import os

import torch
from datetime import datetime

from hirise.experiment.utils import torch_normalize
from hirise.mask_evaluator import MaskEvaluator
from hirise.occlusions.bandgen import BandGen
from hirise.occlusions.bands import SaliencyValueThresholdGen, PercentileThresholdGen, EnergyThresholdGen
from hirise.occlusions.gridgen import ThresholdGridGen, FixingGridGen, CoordinateGridGen
from hirise.occlusions.maskgen import BandedMaskGen
from hirise.occlusions.mask_budget import EqualSplit
from hirise.occlusions.rescaling import AvgpoolDownsampler
from hirise.occlusions.upsampling import Upsampler
from hirise.utils import read_tensor
from hirise.structures import LambdaCheckpoints, CombinedMask

from hirise_tools.models import load_resnet50
from hirise_tools.utils import save_pkl
from hirise_tools.iterative.common import combine_retained_checkpoints, get_single_full_band
from hirise_tools.iterative.params import v2_full_param_set, paramset_to_indexing_tuple, \
    v2_split_iteration_param_sets, v2_paramsets_index, convert_base_ps_to_persistence_tuples
from hirise_tools.iterative.persistence import make_source_data_dict, make_v2_archive, \
    make_v2_param_dict

import hirise_tools.iterative.helpers as hirise_helpers

from tqdm.autonotebook import tqdm, trange

DEFAULT_GPU_BATCH_SIZE = 1000
DEFAULT_CPU_BATCH_SIZE = 5000
CPU_MAX_OP_SIZE = 50000 * (224 * 224)
GPU_MAX_OP_SIZE = 10000 * (224 * 224)
MAX_OP_SIZE = CPU_MAX_OP_SIZE

parser = argparse.ArgumentParser()
# single-image-specific args
parser.add_argument('-i', '--input-image', required=True, type=os.path.abspath)
parser.add_argument('-c', '--explained-class', required=True, type=int)
# other parameters
parser.add_argument('-o', '--output-path', required=True, type=os.path.abspath, help='Path to file where scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
parser.add_argument('-d', '--device', type=torch.device, default='cuda:0', help="Torch-compatible name of a device used for computation. E.g. 'cpu', 'cuda:0'")
parser.add_argument('-b', '--batch', type=int, default=DEFAULT_GPU_BATCH_SIZE, help="Number of masks evaluated on GPU in parallel. Limited by the amount of VRAM")
parser.add_argument('-n', '--n-reps', type=int, default=20, help="Requested number of instances of each reference mask. If not specified, it will be the same as in source archive")
parser.add_argument('--description', type=str, default=None, help="Description string placed in the output file")
args = parser.parse_args()

# set params
INPUT_PATH = args.input_image
DEVICE = args.device
OUTPUT_PATH = args.output_path
EXPECTED_CLASS = args.explained_class
USE_TQDM = True
DESCRIPTION = args.description
N_REPS = args.n_reps
BATCH_SIZE = args.batch

resnet50, input_size = load_resnet50(DEVICE)

grid_gen = FixingGridGen(grid_gen=ThresholdGridGen(DEVICE), fixer_grid_gen=CoordinateGridGen(DEVICE))
evaluator = MaskEvaluator(resnet50, DEVICE, batch_size=BATCH_SIZE, use_tqdm=USE_TQDM)
upsampler = Upsampler(DEVICE)

banded_mask_gen = BandedMaskGen(grid_gen, upsampler)
energy_bound_gen = EnergyThresholdGen()
area_bound_gen = PercentileThresholdGen()
value_bound_gen = SaliencyValueThresholdGen()
band_gen = BandGen(AvgpoolDownsampler)

n_0 = [400, 600]
n_1 = [1200, 1600, 2000, 2500, 3000, 3500]

s_0 = [3, 5, 7]
s_1 = [7, 9, 12, 18, 27]

iteration_params = {"N": [n_0, n_1],
                    "s": [s_0, s_1]}

variable_params = {"bound_gen": [area_bound_gen, value_bound_gen, energy_bound_gen]}

fixed_params = {"p1": 0.2,
                "img": read_tensor(INPUT_PATH).to(DEVICE),
                "class": EXPECTED_CLASS,
                "bound_edges": [0, 0.33, 0.66, 1],
                "band_gen": band_gen,
                "mask_gen": banded_mask_gen,
                "mask_budget": EqualSplit,
                "model": "resnet50"}

variable_param_order = ["bound_gen"]
iteration_param_order = ["N", "s"]
param_order = iteration_param_order + variable_param_order

source_data = make_source_data_dict([INPUT_PATH], [EXPECTED_CLASS])
full_param_sets = v2_full_param_set(iteration_params=iteration_params,
                                    variable_params=variable_params,
                                    fixed_params=fixed_params,
                                    param_order=param_order)
all_paramset_indexes = v2_paramsets_index(iteration_params, variable_params, param_order)

START_DATE = datetime.now()

## calculate
try:
    # set up the result container
    all_results = [[torch.zeros(N_REPS, *paramset["img"].shape[-2:])
                    for paramset in iteration_paramsets]
                   for iteration_paramsets in full_param_sets]

    for rep_idx in trange(N_REPS):
        for iteration_idx, (persistence_param_sets, persistence_index) in tqdm(list(enumerate(zip(full_param_sets, all_paramset_indexes))), desc="iteration", leave=False):
            tmp = v2_split_iteration_param_sets(iter_idx=iteration_idx,
                                                iteration_params=iteration_params,
                                                variable_params=variable_params,
                                                fixed_params=fixed_params,
                                                param_order=param_order)

            checkpoint_Ns = tmp["n_values"]
            current_iteration_paramsets = tmp["cur_iter_params"]
            base_params = tmp["base_params"]
            base_param_index = tmp["base_index"]

            for current_iter_params in tqdm(current_iteration_paramsets, desc="iter_params", leave=False):
                for base_param_set in tqdm(base_params, desc='prev_params', leave=False):
                    p1 = current_iter_params.get("p1", base_param_set["p1"])
                    s = current_iter_params.get("s", base_param_set["s"])
                    bound_edges = current_iter_params.get("bound_edges", base_param_set["bound_edges"])

                    img = base_param_set["img"]
                    bound_gen = base_param_set["bound_gen"]
                    band_gen = base_param_set["band_gen"]

                    if iteration_idx == 0:
                        bounds, bands = get_single_full_band(s)
                        domainless_bounds = bounds
                    else:
                        prev_paramset_idx = base_param_index[paramset_to_indexing_tuple(base_param_set, param_order)]
                        prev_map = all_results[iteration_idx-1][prev_paramset_idx][rep_idx]

                        normalized_prev_smap = torch_normalize(prev_map.to(DEVICE))
                        domainless_bounds = bound_gen.as_bounds(bound_edges)
                        bounds = bound_gen.make_bounds(normalized_prev_smap, domainless_bounds)
                        bands = band_gen.generate_bands(normalized_prev_smap, bounds, s)

                    mask_budget = base_param_set["mask_budget"](s_values=s,
                                                                p_values=p1,
                                                                band_spans=[u-l for l, u in domainless_bounds])

                    # TODO: iterate over an enumerated list of checkpoint_idxs for each band to add support for variable-S
                    per_band_checkpoints = tuple(zip(*map(mask_budget.split, checkpoint_Ns)))
                    retainers = [LambdaCheckpoints(CombinedMask(img.shape[-2:], p1),
                                                   lambda i, m, s: i in pbc)
                                 for pbc
                                 in per_band_checkpoints]

                    retainer_iterator = iter(retainers)
                    mask_aggr_provider = lambda x, y: next(retainer_iterator)
                    hirise_helpers.hirise_iteration(img=img,
                                                    band_Ns=[pbc[-1] for pbc in per_band_checkpoints],
                                                    p1=p1,
                                                    bands=bands,
                                                    explained_class=base_param_set["class"],
                                                    mask_gen=base_param_set["mask_gen"],
                                                    evaluator=evaluator,
                                                    mask_aggregator_provider=mask_aggr_provider)

                    # combine results
                    checkpoint_smaps = combine_retained_checkpoints(retainers)
                    if len(checkpoint_smaps) == 0:
                        raise RuntimeError("checkpoint_smaps should not be empty")

                    # build indices and insert
                    indexing_tuples = convert_base_ps_to_persistence_tuples(checkpoint_Ns, s, base_param_set, param_order)
                    for (smap_N, smap), (tuple_N, key) in zip(checkpoint_smaps, indexing_tuples):
                        if abs(smap_N - tuple_N) > len(bands):
                            raise ValueError(f"misaligned N values: smap_N ({smap_N}), indexing_tuples ({tuple_N})")
                        persistence_idx = persistence_index[key]
                        all_results[iteration_idx][persistence_idx][rep_idx] = smap
except Exception as e:
    print("Calculations have been interrupted by exception: ", e)

finally:
    param_dict = make_v2_param_dict(iteration_params=iteration_params,
                                    variable_params=variable_params,
                                    param_order=param_order,
                                    fixed_params=fixed_params)

    results_dict = make_v2_archive(all_results=all_results,
                                   source_data=source_data,
                                   v2_param_dict=param_dict,
                                   start_date=START_DATE,
                                   description=None)

    try:
        save_pkl(OUTPUT_PATH, results_dict)
    except Exception as e:
        os.rename(OUTPUT_PATH, OUTPUT_PATH + ".broken")
        print("An exception occurred while saving the results: ", e)
        raise e