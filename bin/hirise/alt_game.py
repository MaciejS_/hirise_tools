import argparse
import os

import torch
from datetime import datetime

from hirise_tools.utils import save_pkl, load_from_archive
from hirise_tools.iterative.persistence import get_maps_archive_type
from hirise_tools.iterative.alt.persistence import make_auc_v1_archive
from hirise_tools.iterative.alt.run import game_for_v1, game_for_v2, game_for_r1
from hirise_tools.iterative.common import retrieve_model

from hirise_tools.iterative.helpers import run_insertion_game, run_deletion_game

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-path', type=os.path.abspath, required=True)
parser.add_argument('-o', '--output-path', type=os.path.abspath, required=True, default="", help='Path to file where scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
parser.add_argument('-s', '--step-size', type=int, required=True, help="Number of pixels to alter in each step of the game")
parser.add_argument('-d', '--device', type=torch.device, default='cuda:0', help="Torch-compatible name of a device used for computation. E.g. 'cpu', 'cuda:0'")
parser.add_argument('-b', '--batch', type=int, default=1000, help="Number of masks evaluated on GPU in parallel. Limited by the amount of VRAM")
parser.add_argument('--description', type=str, default=None, help="Description string placed in the output file")

args = parser.parse_args()

if not os.path.exists(args.input_path):
    raise RuntimeError(f"{args.input_path} does not exist")

# set params
INPUT_PATH = args.input_path
OUTPUT_PATH = args.output_path
TARGET_DEVICE = args.device
GAME_STEP_SIZE = args.step_size
GPU_BATCH_SIZE = args.batch
DESCRIPTION = args.description
START_DATE = datetime.now()

arx = load_from_archive(INPUT_PATH)
model = retrieve_model(arx, TARGET_DEVICE)
GAMES = {"ins": lambda img, smap, observed_class: run_insertion_game(img, smap, observed_class,
                                                                    GAME_STEP_SIZE, GPU_BATCH_SIZE, model=model),
         "del": lambda img, smap, observed_class: run_deletion_game(img, smap, observed_class,
                                                                    GAME_STEP_SIZE, GPU_BATCH_SIZE, model=model)}

## calculate
try:
    archive_type = get_maps_archive_type(arx)
    if archive_type == "v1":
        all_results = game_for_v1(arx, GAMES)
    elif archive_type == "v2":
        all_results = game_for_v2(arx, GAMES)
    elif archive_type == "v2m":
        raise NotImplementedError("Not supported yet")
    elif archive_type == "r1":
        all_results = game_for_r1(arx, GAMES)
    elif archive_type == "r1m":
        raise NotImplementedError("Not supported yet")
    else:
        raise RuntimeError("Archive type not recognized")

finally:
    # all_results requires repackaging
    results_dict = make_auc_v1_archive(all_results,
                                       original_archive=arx,
                                       input_archive_file_name=os.path.split(INPUT_PATH)[-1],
                                       start_date=START_DATE,
                                       description=DESCRIPTION)

    try:
        save_pkl(OUTPUT_PATH, results_dict)
    except Exception as e:
        os.rename(OUTPUT_PATH, OUTPUT_PATH + ".broken")
        print("An exception occurred while saving the results: ", e)
        raise e
