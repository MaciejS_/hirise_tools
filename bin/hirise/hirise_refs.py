import argparse
import os

import torch
from datetime import datetime

from hirise.explainer import RISE
from hirise.mask_evaluator import MaskEvaluator
from hirise.occlusions.gridgen import ThresholdGridGen, FixingGridGen, CoordinateGridGen
from hirise.occlusions.upsampling import Upsampler
from hirise.occlusions.maskgen import BandedMaskGen

from hirise_tools.utils import save_pkl, load_from_archive
from hirise_tools.iterative.persistence import get_maps_archive_type
from hirise_tools.iterative.refs.persistence import make_ref_archive_v1, make_ref_archive_v1_multiimg
from hirise_tools.iterative.common import retrieve_model
from hirise_tools.iterative.refs.generator import generate_refs_for_v2, generate_refs_for_v2_multiimg

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-path', type=os.path.abspath, required=True)
parser.add_argument('-o', '--output-path', type=os.path.abspath, required=True, default="", help='Path to file where scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
parser.add_argument('-d', '--device', type=torch.device, default='cuda:0', help="Torch-compatible name of a device used for computation. E.g. 'cpu', 'cuda:0'")
parser.add_argument('-b', '--batch', type=int, default=1000, help="Number of masks evaluated on GPU in parallel. Limited by the amount of VRAM")
parser.add_argument('-n', '--n-reps', type=int, default=None, help="Requested number of instances of each reference mask. If not specified, it will be the same as in source archive")
parser.add_argument('--description', type=str, default=None, help="Description string placed in the output file")
args = parser.parse_args()

if not os.path.exists(args.input_path):
    raise RuntimeError(f"{args.input_path} does not exist")

# set params
INPUT_PATH = args.input_path
OUTPUT_PATH = args.output_path
TARGET_DEVICE = args.device
GPU_BATCH_SIZE = args.batch
N_REPS = args.n_reps # leave as none to infer this from the input archive
DESCRIPTION = args.description
START_DATE = datetime.now()

arx = load_from_archive(INPUT_PATH)
all_maps = arx['maps']
model = retrieve_model(arx, TARGET_DEVICE)

# References archive will have its own set of parameters, with N values differing from the original
# N value needs to be replaced in the output

grid_gen = FixingGridGen(grid_gen=ThresholdGridGen(device=TARGET_DEVICE), fixer_grid_gen=CoordinateGridGen(device=TARGET_DEVICE))
evaluator = MaskEvaluator(model, TARGET_DEVICE, batch_size=GPU_BATCH_SIZE, use_tqdm=False)
upsampler = Upsampler(TARGET_DEVICE)

banded_mask_gen = BandedMaskGen(grid_gen, upsampler)

rise = RISE(banded_mask_gen, evaluator, mask_batch_size=GPU_BATCH_SIZE)

try:
    archive_type = get_maps_archive_type(archive=arx)
    if archive_type == "v1":
        raise NotImplementedError("V1 archives are not supported")
    elif archive_type == "v2":
        n_reps = N_REPS or arx['maps'][0][0].shape[0]
        all_results = generate_refs_for_v2(n_reps=n_reps,
                                           iteration_params=arx['params']['iteration'],
                                           variable_params=arx['params']['variable'],
                                           fixed_params=arx['params']['fixed'],
                                           param_order=arx['params']['order'],
                                           rise=rise)
    elif archive_type == "v2m":
        n_reps = N_REPS or arx['maps'][0][0].shape[0]
        all_results = generate_refs_for_v2_multiimg(source_data=arx['source_data'],
                                                    n_reps=n_reps,
                                                    iteration_params=arx['params']['iteration'],
                                                    variable_params=arx['params']['variable'],
                                                    fixed_params=arx['params']['fixed'],
                                                    param_order=arx['params']['order'],
                                                    rise=rise)
    else:
        raise RuntimeError("Unknown archive type")

finally:
    archive_methods = {"v2": make_ref_archive_v1,
                       "v2m": make_ref_archive_v1_multiimg}

    ref_maps = all_results["ref_maps"]
    ref_params = all_results["all_params"]
    param_index = all_results["index"]

    archive = archive_methods[archive_type](all_results=ref_maps,
                                           full_ref_params_dict=ref_params,
                                           param_index=param_index,
                                           source_data=arx['source_data'],
                                           description=DESCRIPTION,
                                           input_archive_file_name=os.path.split(INPUT_PATH)[-1],
                                           start_date=START_DATE)

    try:
        save_pkl(OUTPUT_PATH, archive)
    except Exception as e:
        os.rename(OUTPUT_PATH, OUTPUT_PATH + ".broken")
        print("An exception occurred while saving the results: ", e)
        raise e

