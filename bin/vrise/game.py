import itertools
import typing as t

import argparse
import functools
import os
from collections import OrderedDict
from time import sleep

import numpy as np
import pandas as pd

import h5py
import torch

from hirise.evaluation.game import BatchedInsertionGame, BatchedDeletionGame
from hirise.evaluation.runner import BatchedGameRunner
from hirise.utils import read_tensor

from hirise_tools.models import load_model_by_name
from hirise_tools.voronoi.cli import (
    add_adhoc_params,
    add_common_arguments,
    add_runtime_arguments,
)
from hirise_tools.voronoi.hdf import (
    extract_dimension_scales,
    initialize_result_dataset,
    validate_path_type_params
)
from hirise_tools.voronoi.paramsets import (
    split_up_coupled_params,
    create_continuation_map,
)
from hirise_tools.voronoi.params import (
    extract_fixed_params_from_dataset,
    gather_params_from_dataset,
)

from tqdm.autonotebook import tqdm
from hirise_tools.voronoi.hdf import open_hdf_archives

GAME_TYPES = {
    "blur": functools.partial(BatchedInsertionGame, reverse=True, substrate_fn=BatchedInsertionGame.default_substrate()),
    "sharpen": functools.partial(BatchedInsertionGame, reverse=False, substrate_fn=BatchedInsertionGame.default_substrate()),

    "insert": functools.partial(BatchedDeletionGame, reverse=True, substrate_fn=BatchedDeletionGame.default_substrate()),
    "remove": functools.partial(BatchedDeletionGame, reverse=False, substrate_fn=BatchedDeletionGame.default_substrate()),
}

def prepare_paramsets(
    source_dataset: h5py.Dataset,
    target_dataset: h5py.Dataset,
) -> t.Tuple[
    t.List[t.Dict[str, t.Any]],
    t.List[t.Sequence[t.Union[int, t.Sequence[int], slice]]],
    t.List[t.Sequence[t.Union[int, t.Sequence[int], slice]]],
]:
    """
    Creates a mapping between old paramsets and new

    Returns:
        1. list of paramsets as kv dicts
        2. list of indexing tuples for source tensor
        3. list of indexing tuples for target tensor

    The lists of indexing tuples are built together to ensure that results that transformation results are placed
    in correct locations in the output tensor
    """
    source_params = extract_dimension_scales(source_dataset)
    target_params = extract_dimension_scales(target_dataset)

    # FIXME: This logic is in dire need of tests, otherwise bugs caused by incorrect operation will be very
    #  difficult to detect by analyzing the results

    # region validation
    # TODO: parameter recoupling is forbidden (coupling uncoupled params and changing existing couplings)
    old_coupled, new_coupled = map(
        lambda paramset: set(filter(lambda name: '+' in name, paramset)),
        (source_params.keys(), target_params.keys())
    )
    if old_coupled != new_coupled:
        raise ValueError(
            "Parameter recoupling is not supported. "
            f"Old couplings: {old_coupled}, new: {new_coupled}"
        )

    _old_params, _new_params = map(set, (source_params, target_params))
    _common_params = _old_params & _new_params
    # if param values for common params aren't identical - raise
    _differing_parameter_values = {
        param_name: (source_params[param_name], target_params[param_name])
        for param_name
        in _common_params
        if np.any(source_params[param_name] != target_params[param_name])
    }
    if _differing_parameter_values:
        raise ValueError(
            '\n'.join((
                "Parameter values for common params must be identical.",
                "Mismatched lists:",
                *(f"  * {k}: {source} <-> {target}"
                  for k, (source, target)
                  in _differing_parameter_values.items())
            ))
        )
    # endregion validation

    source_indices, target_indices = map(
        lambda param_dict: OrderedDict(
            (k, tuple(range(len(v))))
            for k, v
            in param_dict.items()
        ),
        (source_params, target_params)
    )

    # region [parameter optimizations (none yet)]
    #   * optimizations of comon params will need to be applied simultaneously to both source and target indexing tuples as well as the new paramsets
    # endregion

    # include abandoned params in results
    _deleted_param_names = set(source_params) - set(target_params)
    for param_name in _deleted_param_names:
        # because it's a single value, it won't affect the cartesian order
        target_params[param_name] = [source_params[param_name]]
        # this param will no longer enlarge the cartesian product, but it has to be present in source indexing tuples
        source_indices[param_name] = [slice(None, None)]

    scale_order = [dim.label for dim in target_dataset.dims]
    target_paramsets = list(tuple(itertools.product(*target_params.values())))
    target_paramsets = [
        dict(zip(scale_order, _paramset_values))
        for _paramset_values
        in target_paramsets
    ]

    # region [align source indexing tuples with target indexing tuples]
    source_idx_df, target_idx_df = map(
        lambda indices: pd.DataFrame(
            data=itertools.product(*indices.values()),
            columns=indices.keys(),
        ),
        (source_indices, target_indices)
    )

    common_columns_in_source_order = list(filter(lambda x: x in target_idx_df, source_idx_df.columns))
    source_idxs_aligned = source_idx_df.merge(
        target_idx_df,
        on=common_columns_in_source_order,
        how='right'
    )
    source_indexing_tuples = list(map(tuple, source_idxs_aligned[common_columns_in_source_order].values))
    target_indexing_tuples = list(map(tuple, target_idx_df.values))
    # endregion

    split_up_coupled_params(target_paramsets, tuple(target_params.keys()))

    fixed_params = extract_fixed_params_from_dataset(target_dataset)
    fixed_params.pop('runs')  # NOTE: this is a bugfix for param.runs overwriting all run indices in paramsets (minor bug, doesn't affect paramset alignment, nor computation, because the vlaues of 'runs' isn't used to configure the evaluator and result placement is entirely dependent on the indexing tuple (and therefore, paramset alignment))
    for paramset in target_paramsets:
        paramset.update(fixed_params)

    # filter the indexing tuples against the continuation map and return what remains
    continuation_map = create_continuation_map(target_dataset, single_element_ndim=1)
    if not continuation_map.any():
        return [], [], [] # return empty sequences if the continuation map shows nothing to recalculate

    return tuple(zip(*filter(
        lambda ps__sidx__tidx: np.any(continuation_map[ps__sidx__tidx[2]]),
        zip(target_paramsets, source_indexing_tuples, target_indexing_tuples)
    )))


def run_auc(
    source_dataset: h5py.Dataset,
    target_dataset: h5py.Dataset,

    device: torch.device,
    batch_size: int,
    use_tqdm: bool = False,
    delay: float = 0,
):
    """
    :param delay: sleep time is seconds applied after evaluation of a single map. Used as a makeshift throttling mechanism.
    """

    # TODO: models other than resnet will be ignored
    #  * loading the model *inside* the loop will be prohibitively expensive, unless I add some caching
    #  * loading the model *outside* the loop will effectively break the contract for variable `model` param
    if not 'param.evaluator.model' in target_dataset.attrs:
        raise NotImplementedError("`model` must be a fixed parameter")

    # TODO: Only 2D case is supported
    img_area = (source_dataset.attrs['param.map_y'] * source_dataset.attrs['param.map_x'])
    step_size = img_area // target_dataset.attrs['param.steps']

    runner = BatchedGameRunner()
    model, _ = load_model_by_name(target_dataset.attrs['param.evaluator.model'], device)
    _game_precision = target_dataset.attrs.get('param.game.evaluator.precision', default="fp32")
    if _game_precision == "fp16":
        model = model.half()
    elif _game_precision == "fp32":
        model = model.float()
    else:
        raise ValueError(f"Expected game precision to be either 'fp32' or 'fp16', got: {_game_precision}")

    target_paramsets, source_idxs, target_idxs = prepare_paramsets(source_dataset, target_dataset)
    if not target_paramsets:
        print(" >> Didn't find any unprocessed paramsets, exiting.")
        return

    prev_img = None
    for ps, sidxs, tidxs in tqdm(
        tuple(zip(target_paramsets, source_idxs, target_idxs)),
        desc='Paramsets',
        disable=not use_tqdm
    ):
        _cast_back = ps.get('param.game.preeval_cast_to_fp32', False)
        smap = torch.from_numpy(source_dataset[sidxs].squeeze())
        if smap.ndim != 2:
            raise NotImplementedError(f"Expected a 2D saliency map, but got {smap.shape}")

        _smap_nans = smap.flatten().isnan()
        if torch.all(_smap_nans):
            continue # Don't run game on empty inputs, skip them instead.
            # Skipping the loop will leave NaNs in the output archive as well, ensuring that game
            # runner will always check if they can be evaluated on subsequent runs


        # poor man's caching, to prevent img read on every loop
        if prev_img != ps['img']:
            img = read_tensor(ps['img']).to(args.device)
            prev_img = ps['img']

        # TODO: instead of casting here, figure out why do these bytestrings even occur
        #  and make sure that they always arrive as one specific type.
        game_name = ps["game"]
        game_name = game_name.decode('utf-8') if isinstance(game_name, bytes) else game_name
        GameClass = GAME_TYPES[game_name]
        game = GameClass(
            model=model,
            image=img,
            explanation=smap.float() if _cast_back else smap,
            batch_size=batch_size,
            step_size=step_size,
            observed_classes=int(ps["cls"]),
        )

        result = runner.run(game, use_tqdm=None)
        target_dataset[tidxs] = result

        if delay > 0:
            sleep(delay)




# TODO: just leaving this out here (to be moved where it belongs later) - REF runner will need to have a special case for `meshcount` as coupled param
#   to fully drop that param I'd have to recalculate
#   also, the same logic would have to be included in the indexer for jupyter - it would have to be able to decouple
#   params if needed
#
#   The best approach would be to put an exception if I attempt to couple meshcount

# TODO: REFs: refs will be the one archive that requires parameter substitution - polygons->s as well as
#  removal of meshcount, which may potentially lead to parameter decoupling.

# TODO: REFs: If I had the EXTEND functionality working, using a separate archive for all RISE refs would not be such
#  a bad idea (but it could lead to some sparsity issues) Perhaps I could look up what has already been computed earlier

def game_params_from_map_params(
    cartesian_params: t.Dict[str, t.Sequence[t.Any]],
    range_params: t.Dict[str, t.Any],
    param_order: t.List[str],
) -> t.Tuple[
    t.Dict[str, t.Sequence[t.Any]],
    t.Dict[str, t.Any],
    t.List[str],
]:
    """
    Converts `cartesian_params` and `range_params` taken from a (V)RISE maps archive and coverts it
    to a set of game-specific params.
    """
    if set(param_order[-2:]) != {'map_y', 'map_x'}:
        raise ValueError(f"Expected map shape dims (map_y, map_x) to be in the tail. Got: {param_order}")

    # TODO: I may just as well put the game at the very beginning
    game_param_order = param_order[:-2] + ["game", "steps"]

    range_params.pop('map_y')
    range_params.pop('map_x')
    range_params['steps'] = args.steps

    games_scale_values = list(GAME_TYPES.keys()) if 'all' in args.games else args.games
    cartesian_params['game'] = games_scale_values

    return cartesian_params, range_params, game_param_order


def run_new(args: argparse.Namespace) -> None:
    with open_hdf_archives(
        results_path=args.results,
        maps_path=args.maps,
        create_if_missing=True # because this is a continuation mode
    ) as (results_archive, maps_archive):
        maps_dataset = maps_archive[args.group]['maps']
        base_group = results_archive.require_group(args.group)

        # load params from source archive and adjust them
        params = gather_params_from_dataset(maps_dataset)
        original_param_order = [dim.label for dim in maps_dataset.dims]
        params['cartesian'], params['range'], game_param_order = game_params_from_map_params(
            cartesian_params=params['cartesian'],
            range_params=params['range'],
            param_order=original_param_order
        )

        validate_path_type_params(maps_dataset, ['img'])

        # temporarily increase the number of steps to account for the first result returned by
        # alteration game, which is the class confidence score of an unaltered image
        params['range']['steps'] += 1

        game_dataset = initialize_result_dataset(
            target_group=base_group,
            grouped_params=params,
            dim_params_order=game_param_order,
            dataset_name='game',
        )

        # region [create meta-attrs]
        game_dataset.attrs['meta.resume_count'] = 0
        # endregion

        # non-fixed game evaluator params aren't supported (e.g. you may use an archive containing maps in two
        # different precision modes, but you can't order the AltGame runner to create an extra dimension for
        # results of altgame evaluated in FP16 and FP32 modes - you can only choose one and it will be a fixed param)
        # FIXME: if any evaluator param (from mapgen archive) other than evaluator.precision is not fixed (e.g. mapgen was using two different models in one run),
        #  altgame runner will crash right here
        #    There's two potential scenarios:
        #       * single-model eval on N-model archive. This only requires providing a default model to be used by altgame evaluator
        #       * multi-model eval regardless of number of models used in the maps archive - this requires addition of an extra dimension to the game archive.
        #         Incorporation of this extra param to evalaution loop will only require addition of model reloading during evaluation, just like it was done in mapgen.py

        game_dataset.attrs['param.game.preeval_cast_to_fp32'] = args.cast_back
        game_dataset.attrs['param.game.evaluator.precision'] = args.precision

        # populate other game evaluator params from mapgen params
        for _param_name in ("class_set", "model"):
            try:
                game_dataset.attrs[f'param.game.evaluator.{_param_name}'] = (
                    maps_dataset.attrs.get(f'param.evaluator.{_param_name}', default=None)
                    or maps_dataset.attrs[f'param.{_param_name}'] # location of this param in older archives
                )
            except KeyError:
                raise RuntimeError(f"Couldn't infer the value of param '{_param_name}' from the maps archive")

        # param.steps now contains args.steps + 1, so we need to readjust that
        params['range']['steps'] -= 1
        game_dataset.attrs['param.steps'] = params['range']['steps']

        run_auc(
            source_dataset=maps_dataset,
            target_dataset=game_dataset,
            device=args.device,
            batch_size=args.batch_size,
            use_tqdm=not args.quiet,
            delay=args.throttle,
        )

# TODO: note about scales:
#   * putting all result datasets in a common group nicely ties them into a common set of scales
#     (i.e. each group is self-contained, as we don't need to look beyond it to find data)
#   * on the other hand, if we put RISE refs under /rise, then we either have to
#       * clone scales to
#       * or to move them outwards to the root level
#
# Reusing scales across related datasets (VRISE/RISE/GAME) enforces additional consistnecy on EXPAND
#  when a scale is expanded, then (in order to maintain consistency) all related datasets would need to be updated as well
#  THis is actually quite easy to accomplish, because we can check paths to scales used by each dataset
#  and apply the same expansion logic to each of them, so the expansion logic would be moved either to separate
#  script specialized in archive manipulation (merging/extending) or an unified tool that automatically runs a
#  full suite of evaluations for given set of parameters

def run_resume(args: argparse.Namespace):
    with open_hdf_archives(
        results_path=args.results,
        maps_path=args.maps,
        create_if_missing=False # because this is a continuation mode
    ) as (results_archive, maps_archive):
        maps_dataset = maps_archive[args.group]['maps']
        game_dataset = results_archive[args.group]['game']

        validate_path_type_params(game_dataset, ['img'])

        # region [update meta-attrs]
        game_dataset.attrs['meta.resume_count'] = game_dataset.attrs.get('meta.resume_count', 0) + 1
        # endregion

        run_auc(
            source_dataset=maps_dataset,
            target_dataset=game_dataset,
            device=args.device,
            batch_size=args.batch_size,
            use_tqdm=not args.quiet,
            delay=args.throttle,
        )


def run_update(args: argparse.Namespace):
    raise NotImplementedError


def run_extend(args: argparse.Namespace):
    raise NotImplementedError


# region CLI interface
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="command_name")

subparser_for_start = subparsers.add_parser('start', description="Runs evalution games on all saliency maps in the archive")
subparser_for_start.set_defaults(func=run_new)
subparser_for_start.add_argument('-m', '--maps', required=True, type=os.path.abspath, help="Path to HDF5 maps archive")
subparser_for_start.add_argument('-r', '--results', required=False, type=os.path.abspath, help='Path to file where scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
subparser_for_start.add_argument('-s', '--steps', required=True, type=int, help='Number of alteration game steps')
subparser_for_start.add_argument('-p', '--group', required=True, type=str, choices=['vrise', 'rise'], help='Internal path to the group containing the maps')
subparser_for_start.add_argument(
    '-g', '--games',
    required=True,
    choices=("all", *GAME_TYPES.keys()),
    type=str,
    nargs='+',
    help='List of games to run on the archive')
subparser_for_start.add_argument('--cast-back', action="store_true", help="Always cast sMaps to FP32 before evalaution, regardless fo storage precision")
add_runtime_arguments(subparser_for_start)
add_common_arguments(subparser_for_start)
add_adhoc_params(subparser_for_start)

subparser_for_resume = subparsers.add_parser('resume', description="Resumes an interrupted experiment")
subparser_for_resume.set_defaults(func=run_resume)
subparser_for_resume.add_argument(
    '-m', '--maps',
    required=False,
    type=os.path.abspath,
    help="Path to HDF5 saliency maps archive. If omitted, runner will look for maps in the results archive"
)
subparser_for_resume.add_argument('-r', '--results', required=True, type=os.path.abspath, help="Path to HDF5 experiment results archive")
subparser_for_resume.add_argument('-p', '--group', required=True, type=str, choices=['vrise', 'rise'], help='Internal path to the group containing the maps')
add_runtime_arguments(subparser_for_resume)
add_common_arguments(subparser_for_resume)

# TODO: In extend mode, the archive MUST exist
#   NOTE: `extend` operation can only be used on game-specifc params (must be used only in the game results archive)
#           to 'extend' common params, use extend with mapgen (or ref-gen)
subparser_for_extend = subparsers.add_parser('extend', description="Adds more parameter values to the experiment")
subparser_for_extend.set_defaults(func=run_extend)
subparser_for_extend.add_argument('-r', '--results', required=True, type=os.path.abspath, help="Path to HDF5 experiment results archive")
add_runtime_arguments(subparser_for_extend)
add_common_arguments(subparser_for_extend)

# TODO: In update mode, the archive MUST exist
#   NOTE: `update` operation is used to sync with changes in the targeted maps dataset
subparser_for_update = subparsers.add_parser('update', description="Re-run game on new maps in the source HDF5 file")
subparser_for_update.set_defaults(func=run_update)
subparser_for_update.add_argument('-m', '--maps', required=True, type=os.path.abspath, help="Path to maps archive")
subparser_for_update.add_argument('-r', '--results', required=True, type=os.path.abspath, help="Path to exising aucs archive")
add_runtime_arguments(subparser_for_update)
add_common_arguments(subparser_for_update)

args = parser.parse_args()
args.func(args)
# endregion CLI interface

