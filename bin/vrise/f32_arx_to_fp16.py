import argparse
import h5py
import numpy as np
import os

from hirise_tools.voronoi.hdf import open_hdf_archives

parser = argparse.ArgumentParser(description="Converts an FP32 archive to FP16")
parser.add_argument("-i", "--input-path", type=str)
parser.add_argument("-o", "--output-path", type=str)

args = parser.parse_args()

CONVERTIBLE_DS_NAMES = ["maps", "game"]

try:
    with open_hdf_archives(
        args.output_path,
        args.input_path,
        create_if_missing=True
    ) as (fp16_arx, fp32_arx):
        old_archive_contents = []
        fp32_arx.visititems(lambda path, h5obj: old_archive_contents.append((path, type(h5obj))))

        for path, object_type in dict(old_archive_contents).items():
            if object_type == h5py.Group:
                fp16_arx.require_group(path)
            elif object_type == h5py.Dataset:
                _base_group, _ds_name = os.path.split(path)
                if _ds_name in CONVERTIBLE_DS_NAMES:

                    # clone everything in the archive (groups and scales, except for the maps/game dataset)
                    # create the new dataset using all
                    # don't forget to label the dimensions (don't wire up the scales, though)

                    _fp32_ds = fp32_arx[path]
                    _fp16_ds = fp16_arx[_base_group].create_dataset(
                        name=_ds_name,
                        shape=_fp32_ds.shape,
                        data=_fp32_ds[:],
                        dtype=np.float16, # always FP16 in this script
                        maxshape=_fp32_ds.maxshape,
                        fillvalue=np.nan,
                    )

                    # copy labels
                    for _fp32_dim, _fp16_dim in zip(_fp32_ds.dims, _fp16_ds.dims):
                        _fp16_dim.label = _fp32_dim.label
                    # copy attributes
                    for k, v in _fp32_ds.attrs.items():
                        if k.startswith('param.'):
                            _fp16_ds.attrs[k] = v


                else:
                    # clone the dataset as-is
                    fp32_arx.copy(
                        source=path,
                        dest=fp16_arx[_base_group],
                        name=_ds_name
                    )
            else:
                raise RuntimeError(f"Encountered unexpected HDF5 object type: {object_type}")
except Exception as e:
    print("An exception occurred, rolling back...")
    os.remove(args.output_path)
    raise e
