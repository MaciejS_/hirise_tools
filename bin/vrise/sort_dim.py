import h5py
import numpy as np
import argparse

from tqdm.autonotebook import tqdm

from hirise_nbutils.vrise_helpers import build_hdf_index

parser = argparse.ArgumentParser(description='Sorts entries within a specified dimension of the archive in ascending order. Functionality is incredibly limited but this is a nearly one-off tool.')
parser.add_argument('-f', '--file', help='Archive to modify')
parser.add_argument('-p', '--ds-path', type=str, help='Internal path to target dataset')
parser.add_argument('-s', '--scale', type=str, choices=['img+cls'], help='Scale (dimension) to sort by labels, in asceding order')
parser.add_argument('--descending', default=False, action='store_true', help="Sort in descending order order")

args = parser.parse_args()

ARXPATH = args.file # '/tmp/imgnet_vgg.b9.0.h5'
DS_PATH = args.ds_path # 'vrise/maps'
SORTED_SCALE = args.scale
# SORTED_DIM = 'img'

original_arx = h5py.File(ARXPATH, 'r+')
original_ds = original_arx[DS_PATH]

_original_ds_shape = original_ds.shape
if not (
    # This composite condition ensures that the dataset shape matches the new indices built by 'reordering_index'.
    # Until some proper, auto-adapting logic is added
    original_ds.shape[0] == 1
    and original_ds.dims[1].label == SORTED_SCALE
    and original_ds.shape[2] == 1
    and len(original_ds.shape) == 5
):
    original_arx.close()
    print(" > Dataset shape wasn't exactly as expected, aborting due to risk of mangling.")
    exit(1)

imgcls_dim_len = original_ds.shape[1]
reordering_index = (
    build_hdf_index(original_ds)
    .sort_values(by=SORTED_SCALE.split('+'), ascending=not args.descending)
    # caveat - the layout of this indexing tuple will differ for each archive
    #    and will need proper tuple building logic if there's actually more than one hyperdim in the archive
    #    (this notebook was created for an imgnet+vgg run, so it only had to handle an easy, 1-hyperdim case)
    .assign(new_idx = [(0, x, 0) for x in range(imgcls_dim_len)])
    .rename(
        {"idxt": "old_idx"},
        axis=1
    )
)

# load the entire dataset into memory (if DS is too large, this could also be done by utilizing a temporary,
# barebones dataset with matching shape, within the same archive)
#
reordered_ds = np.full(original_ds.shape, dtype=np.float32, fill_value=np.nan)

for new_idx, old_idx in tqdm(reordering_index.loc[:, ['new_idx', 'old_idx']].values, desc="Load & reorder dataset"):
    reordered_ds[new_idx] = original_ds[old_idx]


original_scales = original_ds.parent[f'_scales/{SORTED_SCALE}']
new_scales = np.empty_like(original_scales)

# using tuple unpacking here because the scaleis 1D (ok, techincally 2D, because of coupling, but it's effectively 1D)
for (_, new_idx, _), (_, old_idx, _) in tqdm(reordering_index.loc[:, ['new_idx', 'old_idx']].values, desc="Load & reorder scale values"):
    new_scales[new_idx] = original_scales[old_idx]

original_ds[:] = reordered_ds[:]
original_scales[:] = new_scales[:]

original_arx.close()
print("Done!")

