import itertools as it
import os.path

import more_itertools as mit

import yaml
import argparse

from hirise_tools.voronoi.params import load_img_cls_mapping

parser = argparse.ArgumentParser(description="Groups and splits provided datasets. The tool will try to keep the class ratios in each split as close to the original as possible")
parser.add_argument('-i', '--imgcls-files', nargs='+', help="List of paths to img_cls lists (either in [img, cls] or {img: [clss]} format)")
parser.add_argument(
    '-o',
    '--output-template',
    help="Output file name template. Supportes {pct} and {i} placeholders, where {pct} will be filled with the integer"
         "percentage of original dataset placed in each file, and {i} will be replaced with part idx"
)
parser.add_argument('-n', '--n-splits', type=int, required=True, help="Number of files to split the input datasets into")

args = parser.parse_args()

imgcls_list = []
for imgcls_filepath in args.imgcls_files:
    with open(imgcls_filepath, 'r') as imgcls_file:
        imgcls_list.extend(load_img_cls_mapping(yaml.safe_load(imgcls_file)))

grouper = mit.bucket(imgcls_list, key=lambda img__cls: img__cls[1])
unique_classes = sorted(list(grouper))
grouped_imgs = {k: [img for img, cls in grouper[k]] for k in unique_classes}

interleaved = list(
    mit.interleave_longest(
        *tuple(
            zip(v, it.repeat(k, len(v)))
            for k, v
            in grouped_imgs.items()
        )
    )
)

splits = map(list, mit.divide(args.n_splits, interleaved))

raw_path = os.path.expanduser(args.output_template)
for i, split in enumerate(splits):
    part_path = raw_path.format_map({'pct': int(100/args.n_splits), 'i': i})
    with open(part_path, 'w') as ofile:
        ofile.write(f'[{list(split[0])},\n')
        ofile.writelines([f"{list(entry)},\n" for entry in split[1:-1]])
        ofile.write(f'{list(split[-1])}]\n')