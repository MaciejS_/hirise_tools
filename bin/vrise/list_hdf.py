import typing as t

import numpy as np
import h5py

import argparse
from pprint import pformat

from hirise_tools.voronoi.paramsets import create_continuation_map

ABBREVIATE_LENGTH = 20
# contains {key: single_element_ndim} pairs
CONTINUATION_MAP_KEYS = {
    "maps": 2,
    "game": 1
}

def get_completion_visitor(relevant_keys = t.Sequence[str]) -> t.Callable[[str, t.Union[h5py.Dataset, h5py.Group]], None]:
    def completion_visitor(name, obj):
        objname = name.split('/')[-1]
        parent_name = obj.parent.name.split('/')[-1]
        if objname in relevant_keys and parent_name != '_scales':
            cmap = create_continuation_map(obj, CONTINUATION_MAP_KEYS[objname])
            print(f"{name}: {(1 - cmap.flatten().mean()) * 100:.2f}%")

    return completion_visitor

def get_scales_visitor(
    relevant_keys = t.Sequence[str],
    abbreviate: bool = True,
) -> t.Callable[[str, t.Union[h5py.Dataset, h5py.Group]], None]:
    def scales_visitor(name, obj):
        objname = name.split('/')[-1]
        parent_name = obj.parent.name.split('/')[-1]
        if objname in relevant_keys and parent_name != '_scales':
            scales_group = obj.parent['_scales']
            used_scales = [dim.label for dim in obj.dims]

            # print out
            print(f"\n{name} scales:")
            for scale_key in used_scales:
                if scale_key in scales_group:
                    # param is a dimension param
                    print(f" * {scale_key}:")
                    scale_entries = scales_group[scale_key]
                    _result_limit = ABBREVIATE_LENGTH if abbreviate else None
                    contents = pformat(
                        [
                            tuple(x) if isinstance(x, np.ndarray) else x
                            for x
                            in scale_entries[:_result_limit]
                        ]
                        + (
                            [f"... {len(scale_entries) - ABBREVIATE_LENGTH} more"]
                            if abbreviate and len(scale_entries) > ABBREVIATE_LENGTH
                            else []
                        ),
                        width=120
                    )
                    contents = contents.replace('\n', '\n' + ' '*4)
                    # for entry in pformat(list(scales_group[scale_key])):
                    print(" "*4 + str(contents))
                elif f"param.{scale_key}" in obj.attrs:
                    param_key = f"param.{scale_key}"
                    print(f" * {scale_key}: {obj.attrs[param_key]}")
                else:
                    print(f" - <<{scale_key}>> not found in scales or fixed params!")

    return scales_visitor

def get_params_visitor(relevant_keys = t.Sequence[str]) -> t.Callable[[str, t.Union[h5py.Dataset, h5py.Group]], None]:
    def params_visitor(name, obj):
        objname = name.split('/')[-1]
        parent_name = obj.parent.name.split('/')[-1]
        if objname in relevant_keys and parent_name != '_scales':
            for param_key in filter(lambda x: x.startswith('param.') or x.startswith('meta.'), obj.attrs.keys()):
                print(f"  {param_key}: {obj.attrs[param_key]}")

    return params_visitor


def get_chunksize_visitor(relevant_keys = t.Sequence[str]) -> t.Callable[[str, t.Union[h5py.Dataset, h5py.Group]], None]:
    def params_visitor(name, obj):
        objname = name.split('/')[-1]
        parent_name = obj.parent.name.split('/')[-1]
        if objname in relevant_keys and parent_name != '_scales':
            print(f"  {objname}: {obj.chunks}")

    return params_visitor

parser = argparse.ArgumentParser()
parser.add_argument('path', type=str)
parser.add_argument('-s', '--scales', action='count', help="display contents of scales. By default, long scales are limited to 10 entries. Use -ss to display full contents.")
parser.add_argument('-p', '--params', action='store_true', help="display params stored in dataset attrs")
parser.add_argument('-c', '--chunksize', action='store_true', help="display chunksize information")
parser.add_argument('-C', '--completion', action='store_true', help="Calculate the percentage of non-NaN results")
# parser.add_argument('--valid-datasets', nargs='+', default=['maps', 'game'], help="Defines what keys will have a completion map calculated for them.")

args = parser.parse_args()

# region [run]
with h5py.File(args.path, 'r') as hfile:
    # print out abbreviated contents and shapes
    print("Members:")
    hfile.visititems(print)

    # display contents of scales
    if args.scales:
        print("\nScales:")
        hfile.visititems(
            get_scales_visitor(
                relevant_keys=tuple(CONTINUATION_MAP_KEYS.keys()),
                abbreviate=(args.scales < 2)
            )
        )

    if args.params:
        print("\nAttrs:")
        hfile.visititems(
            get_params_visitor(
                relevant_keys=tuple(CONTINUATION_MAP_KEYS.keys())
            )
        )

    if args.chunksize:
        print("\nChunksizes:")
        hfile.visititems(
            get_chunksize_visitor(
                relevant_keys=tuple(CONTINUATION_MAP_KEYS.keys())
            )
        )

    if args.completion:
        # display completion percentage for datasets
        print("\nCompletion:")
        hfile.visititems(get_completion_visitor(tuple(CONTINUATION_MAP_KEYS.keys())))

# endregion