import h5py
import more_itertools as mit

import argparse

import numpy as np

parser = argparse.ArgumentParser(description="Replaces image root path in specified archive")

parser.add_argument('-f', '--file-path', help="Path to HDF5 archive to modify")
parser.add_argument('-g', '--group', type=str, help='Intra-archive path to the base group')
parser.add_argument('--oldroot', type=str, help='old imgs root path')
parser.add_argument('--newroot', type=str, help='old imgs root path')
parser.add_argument('-y', '--non-interactive', action="store_true", help="If used, user won't be asked to confirm the conversion")

args = parser.parse_args()

with h5py.File(args.file_path, 'r+') as arx:
    base_group = args.group.strip('/')
    scales_group = arx[f"{base_group}/_scales"]

    image_paths_group = mit.only([k for k in scales_group.keys() if "img" in k.split('+')])

    if '+' in image_paths_group:
        idx = image_paths_group.split('+').index("img")
        selector = (slice(None, None), idx)
    else:
        selector = slice(None, None)

    images_scale = scales_group[image_paths_group]
    oldroot = f"/{args.oldroot.strip('/')}/".encode('utf-8')
    newroot = f"/{args.newroot.strip('/')}/".encode('utf-8')
    old_paths = images_scale[selector]
    new_paths = [
        path.replace(oldroot, newroot)
        for path
        in old_paths
    ]

    print(f"Old paths example:\n {images_scale[:3]} \nNew paths example: {new_paths[:3]}\n")

    if args.non_interactive:
        _perform_replacement = True
    else:
        _perform_replacement = input("Proceed? [Y/n]: ").strip() == "Y"

    if _perform_replacement:
        images_scale[selector] = np.asarray(new_paths)
        print("Done!")
    else:
        print("Aborting")
