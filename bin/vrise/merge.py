import itertools as it
import typing as t

import more_itertools as mit

import os
import sys
import h5py

import argparse

from hirise_tools.voronoi.merge import (
    merge_scales,
    merge_datasets,
    merge_datasets_virtual,
    get_default_combined_file_name,
    split_up_partfile_names
)

HIRISE_PARENT_PATH = os.path.expanduser('~/git/')
if HIRISE_PARENT_PATH not in sys.path:
    sys.path.append(HIRISE_PARENT_PATH)

MERGE_SUPPORTED_DIMS = {"runs", "img+cls"}


def validate_part_filenames(partfile_names: t.Sequence[str]):
    split_up_names = split_up_partfile_names(partfile_names)

    issues = []

    filenames_without_partidx = ['.'.join(it.chain.from_iterable(name_parts)) for name_parts in split_up_names if
                                 len(name_parts) != 3]
    if filenames_without_partidx:
        issues.append("Some filenames don't contain partidx:\n" + '\n'.join(filenames_without_partidx))

    prefixes, partidxs, suffixes = map(tuple, it.zip_longest(*split_up_names))
    print(prefixes, partidxs, suffixes)
    for token_name, token_set in (('prefix', set(prefixes)), ('suffix', set(suffixes))):
        if len(token_set) > 1:
            issues.append(f"Expected a common {token_name} for all names, found {len(token_set)}: {token_set}")

    int_partidxs = sorted(tuple(map(int, partidxs)))
    missing_partidxs = sorted(
        set(int_partidxs) - set(range(min(int_partidxs), max(int_partidxs) + (1 if min(int_partidxs) > 0 else 0)))
        )
    if missing_partidxs:
        issues.append(f"Found potentially missing partidxs: {missing_partidxs}")

    if issues:
        raise RuntimeError(
            "Partfile name validation failed due to the following issues:\n"
            + "\n  * ".join(issues)
        )

def merge_dataset_files(
    target_file: str,
    internal_dataset_path: str,
    merged_dim: str,
    virtual:bool,
    source_files: t.Sequence[str],
    chunksize: t.Union[None, tuple]
):
    """
    Acts as a slightly more high-level wrapper on merge_datasets. Takes care of opening and closing the relevant files and also copies over scales from original one of original datasets (scales are assumed to be identical in all partfiles)
    """
    target_h5 = h5py.File(target_file, 'w-', libver='latest')
    source_h5s = [h5py.File(src_path, 'r', swmr=True) for src_path in source_files]

    path_tokens = internal_dataset_path.split('/')
    base_group_path = '/'.join(path_tokens[:-1])
    ds_name = path_tokens[-1]
    group = target_h5
    for groupname in path_tokens[:-1]:
        group = group.require_group(groupname)

    base_group = target_h5[base_group_path]
    part_datasets = [src[internal_dataset_path] for src in source_h5s]
    if virtual:
        merge_datasets_virtual(base_group, ds_name, merged_dim, part_datasets)
    else:
        merge_datasets(base_group, ds_name, merged_dim, part_datasets, new_chunksize=chunksize)

    _scales_group_path = f'{base_group_path}/_scales'
    part_scales = [src[_scales_group_path] for src in source_h5s]
    # Initialize scales group in the combined dataset file by copying entire scales group from one of the part datasets.
    merged_scales_group = base_group.require_group(name='_scales')
    merge_scales(merged_scales_group, merged_dim, part_scales)

    for src_file in source_h5s:
        src_file.close()
    target_h5.close()

# merge
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--target', type=str, required=False, default=None, help="Path where result of merge will be stored")
    parser.add_argument('-s', '--sources', type=str, required=True, nargs='+', help='List of paths to files containing partial datasets to merge')
    parser.add_argument('-j', '--join-dim', type=str, required=True, choices=list(MERGE_SUPPORTED_DIMS), help='Name of dimension to merge on')
    parser.add_argument('-d', '--dataset', type=str, required=True, help="Internal archive path to the merged dataset")
    parser.add_argument('-q', '--quiet', action='store_true')

    meg = parser.add_mutually_exclusive_group()
    meg.add_argument('--virtual', action='store_true', help="Links source datasets together instead of copying their data. Requires h5py >= 2.9.0")
    meg.add_argument(
        '--chunksize',
        type=int,
        nargs='+',
        default=True, # True will trigger automatic chunk sizing
        help="Define new chunksize as space-separated ints, e.g. '64 224' would define a (64, 224) chunk along the last two dimensions. "
             "Missing leading dimensions will be filled with ones. Only applicable to physical merge."
    )

    args = parser.parse_args()

    partial_archives_paths = args.sources

    if not args.target:
        filenames = [os.path.split(path)[-1] for path in partial_archives_paths]
        try:
            base = mit.only(set(os.path.split(path)[0] for path in partial_archives_paths))
            validate_part_filenames(filenames)
        except:
            raise RuntimeError(
                "Skipping --target (target path inference mode) requires all partfiles to be located "
                "in the same dir and have consistent names"
            )

        combined_name = get_default_combined_file_name(filenames)
        combined_archive_path = os.path.join(base, combined_name)
    else:
        combined_archive_path = args.target

    if not args.quiet:
        print("Combining:")
        print('\n'.join("  * " + pth for pth in partial_archives_paths))
        print("\nOutput: " + combined_archive_path)

    merge_dataset_files(
        target_file=combined_archive_path,
        internal_dataset_path=args.dataset.strip('/'), # intra-archive path is expected to be free of leading and trailing slashes
        merged_dim=args.join_dim,
        source_files=partial_archives_paths,
        virtual=args.virtual,
        chunksize=args.chunksize,
    )
