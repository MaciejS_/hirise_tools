import argparse
import itertools as it
import math
import os

import h5py
import numpy as np

from hirise_tools.voronoi.cli import (
    add_adhoc_params,
    add_runtime_arguments,
    add_common_arguments,
    add_mapgen_specific_runtime_args,
)
from hirise_tools.voronoi.hdf import initialize_result_dataset
from hirise_tools.voronoi.params import (
    gather_params_from_dataset,
    couple_params,
    decouple_params,
    gather_params_from_config as group_params
)
from hirise_tools.voronoi.param_def import VRISE_RUNNER_PARAMS
from hirise_tools.voronoi.runner import RiseHDFMapGen


def load_params(
    source_dataset: h5py.Dataset
):
    params = gather_params_from_dataset(source_dataset)
    old_param_order = [dim.label for dim in source_dataset.dims]

    ungrouped_params = dict(it.chain.from_iterable(map(dict.items, params.values())))
    uncoupled_params = decouple_params(ungrouped_params)

    # region [remove or remap VRISE-specific params]
    polygons_param_values = uncoupled_params.pop('polygons')
    if isinstance(polygons_param_values, (tuple, list, np.ndarray)):
        s_values = tuple(round(math.sqrt(mc)) for mc in polygons_param_values)
    else:
        s_values = round(math.sqrt(polygons_param_values))
    uncoupled_params['s'] = s_values

    for param_name in VRISE_RUNNER_PARAMS:
        if param_name in uncoupled_params:
            uncoupled_params.pop(param_name)

    # remove the param name from couplings
    _name_remapping = {"polygons": "s"}
    _params_to_remove = VRISE_RUNNER_PARAMS
    coupling_mapping = {
        old_coupling: '+'.join(
            _name_remapping.get(name, name) # remap name if needed
            for name
            in old_coupling.split('+')
            if name not in _params_to_remove
        )
        for old_coupling
        in filter(lambda name: '+' in name, ungrouped_params.keys())
    }
    # remove names of deleted params from param order
    # remap standalone param names
    updated_param_order = [
        _name_remapping.get(name, name)
        for name
        in old_param_order
    ]
    # remap param names within couplings
    updated_param_order = [
        coupling_mapping[name] if '+' in name else name
        for name
        in updated_param_order
    ]
    updated_param_order = [
        name
        for name
        in updated_param_order
        if name not in VRISE_RUNNER_PARAMS
    ]

    # (both of these aren't really necessary right now, because I'm not coupling params other than img+cls)
    # TODO: another (potential) problem - a coupling can degenerate into:
    #  * a cartesian param (if only one parameter remains)
    #  * a fixed param (if only a single param with a single value remains)
    # TODO: Run uniq on params to eliminate repeated combinations (relevant for coupled params)
    #   Caveat: this logic annot be used for source params in transformer (auc), because it may not
    #           preserve the original order of parameters. That in turn would cause misalignment
    #           between the input and output archives and garble the results.

    recoupled_params = couple_params(uncoupled_params, couplings=tuple(coupling_mapping.values()))
    grouped_params = group_params(recoupled_params)
    # endregion

    return grouped_params, updated_param_order

def run_new(args: argparse.Namespace):
    with h5py.File(args.maps, 'r') as source_maps_archive:
        params, param_order = load_params(source_maps_archive['/vrise/maps'])

    # THERE MUSTN'T BE 'w' mode here unless I discriminate between the 'same path' and 'different paths'
    # otherwise refs may end up wiping the map archive out
    with h5py.File(args.refs, 'a' if args.overwrite else 'w-') as results_archive:
        # this group mustn't exist already - if it does, it means that the user is trying to overwrite
        # an earlier result
        base_group = results_archive.create_group('rise')
        dataset = initialize_result_dataset(
            target_group=base_group,
            grouped_params=params,
            dim_params_order=param_order,
            dataset_name='maps',
            result_dtype=np.float16 if args.storage_precision == "fp16" else np.float32,
        )

        # region [add meta-attrs]
        # these aren't initialized in `initialize_result_dataset` because the set of meta-attrs differs
        # between archive types, whereas `initialize...` is universal
        dataset.attrs['meta.resume_count'] = 0
        dataset.attrs['meta.mask_sets_used'] = 0
        # endregion

        dataset.attrs['param.mask_reuse'] = args.reuse_masks
        mapgen = RiseHDFMapGen(
            device=args.device,
            batch_size=args.batch_size,
            n_threads=args.threads,
            use_model_warmups=args.use_warmups,
            throttle=args.throttle,
            dyncfg_path=args.dyncfg_path,
        )

        mapgen.run(
            dataset=dataset,
            use_tqdm=not args.quiet,
        )


def run_resume(args: argparse.Namespace):
    hdf_path = args.refs
    with h5py.File(args.refs, 'r+') as results_archive:
        ds_path = '/rise/maps'
        if ds_path not in results_archive.keys():
            raise KeyError(f"Expected {ds_path} to be present in dataset {hdf_path}. Aborting.")

        dataset = results_archive[ds_path]

        if dataset.attrs.get('param.mask_reuse', False):
            print("""
            Resuming in mask reuse mode - A NEW SET OF MASKS has been generated. 
            If a single set of masks had to be used for all evaluations - the results will be invalid.   
            """)

        # region [update meta-attrs]
        dataset.attrs['meta.resume_count'] = dataset.attrs.get('meta.resume_count', 0) + 1
        # endregion

        mapgen = RiseHDFMapGen(
            device=args.device,
            batch_size=args.batch_size,
            n_threads=args.threads,
            use_model_warmups=args.use_warmups,
            throttle=args.throttle,
            dyncfg_path=args.dyncfg_path,
        )

        mapgen.run(
            dataset=dataset,
            use_tqdm=not args.quiet,
        )


def run_update(args: argparse.Namespace):
    raise NotImplementedError

def run_extend(args: argparse.Namespace):
    raise NotImplementedError

# region [CLI config]
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="command_name")

subparser_for_start = subparsers.add_parser('start', description="Runs evalution games on all saliency maps in the archive")
subparser_for_start.set_defaults(func=run_new)
subparser_for_start.add_argument('-m', '--maps', required=True, type=os.path.abspath, help="Path to HDF5 maps archive")
subparser_for_start.add_argument('-r', '--refs', required=False, type=os.path.abspath, help='Path to file where scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
# subparser_for_start.add_argument('-p', '--group', required=True, type=str, choices=['vrise', 'rise'], help='Internal path to the group containing the maps')
subparser_for_start.add_argument('--threads', required=False, default=1, type=int, help='Number of threads to use. Negative values mean "all available cores minus the value given"')
subparser_for_start.add_argument('--overwrite', action='store_true', help="If used, the script won't fail if the target archive already exists")
subparser_for_start.add_argument(
    '--reuse-masks',
    required=False,
    action='store_true',
    help="If used, a single set of masks will be reused for all evaluations, as long as their maskgen parameters remain compatible with current mask set"
         " Each run will use a new set of masks."
         " It's recommended to put 'img' and 'cls' params at the end of param order to maximize speedup offered by this option."
)
add_mapgen_specific_runtime_args(subparser_for_start)
add_runtime_arguments(subparser_for_start)
add_common_arguments(subparser_for_start)
add_adhoc_params(subparser_for_start)

subparser_for_resume = subparsers.add_parser('resume', description="Runs evalution games on all saliency maps in the archive")
subparser_for_resume.set_defaults(func=run_resume)
subparser_for_resume.add_argument('-r', '--refs', required=True, type=os.path.abspath, help='Path to file where scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
# subparser_for_resume.add_argument('-p', '--group', required=True, type=str, choices=['vrise', 'rise'], help='Internal path to the group containing the maps')
subparser_for_resume.add_argument('--threads', required=False, default=1, type=int, help='Number of threads to use. Negative values mean "all available cores minus the value given"')
add_mapgen_specific_runtime_args(subparser_for_resume)
add_runtime_arguments(subparser_for_resume)
add_common_arguments(subparser_for_resume)

args = parser.parse_args()
args.func(args)
# endregion