import socket
import typing as t

import os

import h5py
import numpy as np
import yaml
import argparse

import torch
import itertools
from hirise_tools.voronoi.cli import (
    add_runtime_arguments,
    add_common_arguments,
    add_adhoc_params,
    add_mapgen_specific_runtime_args
)
from hirise_tools.voronoi.hdf import initialize_result_dataset
from hirise_tools.voronoi.params import (
    gather_params_from_config,
    load_img_cls_mapping
)
from hirise_tools.voronoi.param_def import (
    COMMON_PARAMS,
    MAP_RUNNER_PARAMS,
    VRISE_RUNNER_PARAMS,
    REUSE_COMPATIBLE_PARAMS,
)
from hirise_tools.voronoi.runner import VoronoiHDFMapGen

CPU = torch.device("cpu")

# REQUIRED PARAMS is a mapping because in case of coupling string and numerical parameters together
#  they will be all stored as strings, so I will need a way to map them back to their original types
#  plus I can use this information to validate paramsets provided in config
#  of course, any of these can be an iterable, but all of the values in the iterable need to be of the specified type
REQUIRED_PARAMS = {
    **COMMON_PARAMS,
    **VRISE_RUNNER_PARAMS,
    **MAP_RUNNER_PARAMS,
    **{"evaluator.precision": str, "mask_reuse": bool},
}


def validate_params(
    cartesian_params: t.Dict[str, t.Any],
    coupled_params: t.Dict[str, t.Any],
    fixed_params: t.Dict[str, t.Any],
    range_params: t.Dict[str, int],
    param_order: t.Sequence[str],
):
    extracted_params = {
        **cartesian_params,
        **coupled_params,
        **fixed_params,
        **range_params,
    }
    all_param_names = set(itertools.chain.from_iterable(x.split('+') for x in extracted_params.keys()))


    issues = []
    # validation 1.1 - the config must provide a value for each of the params required by this runner
    missing_params = set(REQUIRED_PARAMS.keys()) - all_param_names
    if missing_params:
        issues.append(f"missing parameters: {missing_params}")
    # validation 1.2 - no extra params other than expected
    unknown_params = all_param_names - set(REQUIRED_PARAMS.keys())
    if unknown_params:
        issues.append(f"unknown params found: {unknown_params}")

    dim_spawning_params = {*cartesian_params.keys(), *coupled_params.keys(), *range_params.keys()}
    # validation 2 - all dimension-spawning params must appear in param order
    # in case of coupled params, they need to appear in their coupled for in param_order!
    cartesian_and_coupled = dim_spawning_params
    if not all(p in param_order for p in cartesian_and_coupled):
        params_without_order = cartesian_and_coupled.difference(set(param_order))
        issues.append(f"params defined without order: {params_without_order}")

    # validation 3 - `order` must be a subset of all defined params
    #  in case of coupled params, the name in order must be in coupled form (names joined with '+')
    params_existing_only_in_order_list = set(param_order) - dim_spawning_params
    if params_existing_only_in_order_list:
        issues.append(f"param_order contains undefined or fixed params: {params_existing_only_in_order_list}")

    # validation 4
    #   * all `coupled` params must contain a '+' in name - otherwise they should be placed in `cartesian`
    #   * fixed and cartesian may not contain '+'
    coupled_param_names_without_plus = ['+' not in param_name for param_name in coupled_params.keys()]
    if any(coupled_param_names_without_plus):
        issues.append(f"coupled param without a '+' in name: {coupled_param_names_without_plus}")

    for set_label, param_names in {"cartesian": cartesian_params, "fixed": fixed_params}.items():
        param_names_with_plus = ['+' in param_name for param_name in param_names]
        if any(param_names_with_plus):
            issues.append(f"'+' found in {set_label} param names: {param_names_with_plus}")

    # validation 5 - the "width" of a coupled param value must match the number of params in coupling
    for param_names, param_values in coupled_params.items():
        expected_n_values = param_names.count('+') + 1
        if not all(len(param_tuple) == expected_n_values for param_tuple in param_values):
            issues.append(f"not all values of {param_names} contain {expected_n_values} values")

    # TODO: Validation 6 - mutually exclusive param groups aren't mixed (e.g. no mixing of VRISE and RISE params, or AUC with MAP params)
    # this validation is already covered by validation 1

    # TODO: Validation 7 - each param contains values of correct type

    # validation 8 - `map_y`, `map_x` and `steps` must be trailing dimensions
    _trailing_params = {'map_y', 'map_x', 'steps'}
    first_trailing_param_idx = min(
        param_order.index(p) if p in param_order else len(param_order)
        for p
        in _trailing_params
    )
    # head is free of trailing params by definition - otherwise a smaller idx would be found by listcomp above
    tail = param_order[first_trailing_param_idx:]
    non_trailing_params_in_param_order_tail = set(tail).difference(_trailing_params)
    if non_trailing_params_in_param_order_tail:
        issues.append(f"{_trailing_params} may only be at the end of param_order (got: {param_order})")

    if issues:
        raise RuntimeError(
            f"  Config validation failed, {len(issues)} issues found:\n"
            + '\n    '.join([f'* {issue}' for issue in issues])
        )


def validate_config(full_config: t.Dict[str, t.Any]) -> None:
    """
    Raises if there's anything wrong with the config and prints out detected issues
    """
    params = full_config['params']
    param_order = full_config['param_order']

    validate_params(
        cartesian_params=params.get('cartesian', dict()),
        coupled_params=params.get('coupled', dict()),
        fixed_params=params.get('fixed', dict()),
        range_params=params.get('range', dict()),
        param_order=param_order,
    )

# TODO: copied to eval_loop_utils
def calc_peak_memory_use(paramsets: t.Sequence[t.Dict[str, t.Any]], dtype=torch.float32):
    """
    Returns the estimated peak memory use in bytes.
    """
    max_size = 0
    for ps in paramsets:
        size = max(ps["checkpoints"]) * ps["map_y"] * ps["map_x"]
        if size > max_size:
            max_size = size

    bytes_per_value = 4
    # times 2 because during concatenation, two copies of all masks will exist - one fragmented, one concatenated
    return max_size * bytes_per_value * 2


def start_experiment(args: argparse.Namespace):
    """
    A fully self-contained version of new experiment creation.
    Takes only the set of `args` extracted by ArgumentParser and does the rest automatically
    """
    with open(args.config, 'r') as cfgyaml:
        config = yaml.safe_load(cfgyaml)

    if hasattr(args, 'imgcls_list') and (args.img or args.cls):
        raise ValueError("--img/--cls and --imgcls_list are mutually exclusive")

    if vars(args).get('imgcls_list', None):
        imgcls_list = []
        for imgcls_filepath in args.imgcls_list:
            with open(imgcls_filepath, 'r') as imgcls_file:
                imgcls_list.extend(load_img_cls_mapping(yaml.safe_load(imgcls_file)))
        config['params']['img+cls'] = imgcls_list
    else:
        config['params']['img+cls'] = [(args.img, cls) for cls in args.cls]

    grouped_params = gather_params_from_config(config['params'])

    sanitized_config = {**config}
    sanitized_config['params'] = grouped_params
    param_order = sanitized_config['param_order']

    # add 'precision' CLI param to config as fixed param and inform user about selected precision
    # mode(s) and potential costs of multi-precision mode
    if 'evaluator.precision' not in config['params']:
        precision_value = args.precision
        print(f"Using precision provided via CLI: {precision_value}")
        grouped_params['fixed']['evaluator.precision'] = precision_value
    else:
        precision_values = config['params']['evaluator.precision']
        print(f"Using precision mode(s) {precision_values} from config")

    # region [warn user about impact of evaluator reconfiguration on performance]
    _variable_evaluator_params = {
        k: v
        for k, v
        in config['params'].items()
        if k.startswith('evaluator.') and isinstance(v, (tuple, list, np.ndarray))
    }
    if _variable_evaluator_params:
        # warn about performance and frequent model reloading
        _param_order_idxs = {k: param_order.index(k) for k in _variable_evaluator_params}
        print(f"Found {len(_variable_evaluator_params)} variable evaluator params: {_variable_evaluator_params.keys()}")
        print("Warning: Evaluator reconfiguration will negatively impact the performance."
              " To minimize the impact, evaluator params should occur as early in the param_order as possible, to reduce the number of reconfigurations.")

    # warn user against exceedingly frequent mask reloading
    if args.reuse_masks:
        _mask_neutral_params_idxs = [
            i for i, param_name
            in enumerate(param_order)
            if param_name in REUSE_COMPATIBLE_PARAMS
        ]
        if _mask_neutral_params_idxs and (min(_mask_neutral_params_idxs) < (len(param_order) - len(_mask_neutral_params_idxs))):
            _mask_params_mixed_with_neutrals = [
                name for name
                in param_order[min(_mask_neutral_params_idxs):]
                if name not in REUSE_COMPATIBLE_PARAMS
            ]
            print(f"""
                REDUCED MASK REUSE WARNING: params incompatible with reuse {_mask_params_mixed_with_neutrals} are
                interspersed among other in `param_order` - masks will be generated more frequently than probably intended.
            """)
    # endregion

    grouped_params['fixed']['mask_reuse'] = args.reuse_masks
    validate_config(sanitized_config)
    with h5py.File(args.output_path, mode='w-') as hfile:
        base_group = hfile.create_group('vrise')
        dataset = initialize_result_dataset(
            target_group=base_group,
            grouped_params=grouped_params,
            dim_params_order=param_order,
            dataset_name="maps",
            result_dtype=np.float16 if args.storage_precision == "fp16" else np.float32,
        )

        dataset.attrs['meta.resume_count'] = 0
        dataset.attrs['meta.mask_sets_used'] = 0

        vrise_mapgen = VoronoiHDFMapGen(
            device=args.device,
            batch_size=args.batch_size,
            render_device=args.render_device,
            use_model_warmups=args.use_warmups,
            dyncfg_path=args.dyncfg_path,
        )

        # restore_from_hdf5
        vrise_mapgen.run(
            dataset=dataset,
            use_tqdm=not args.quiet,
        )

def resume_experiment(args: argparse.Namespace) -> None:
    """
    Analyzes an existing HDF5 and resumes the experiment
    Takes only the set of `args` extracted by ArgumentParser and does the rest automatically
    """
    hdf_path = args.archive
    with h5py.File(hdf_path, 'r+') as hfile:
        ds_path = '/vrise/maps'
        if ds_path not in hfile.keys():
            raise KeyError(f"Expected {ds_path} to be present in dataset {hdf_path}. Aborting.")

        dataset = hfile[ds_path]
        if dataset.attrs.get('param.mask_reuse', False):
            print("""
            Resuming in mask reuse mode - A NEW SET OF MASKS has been generated. 
            If a single set of masks had to be used for all evaluations - the results will be invalid.   
            """)

        # update resume count
        # .get() with 0 as default is used b/c older archives don't contain this attrib (in which ca
        _resume_count_attrname = 'meta.resume_count'
        dataset.attrs[_resume_count_attrname] = dataset.attrs.get(_resume_count_attrname, 0) + 1

        vrise_mapgen = VoronoiHDFMapGen(
            device=args.device,
            batch_size=args.batch_size,
            render_device=args.render_device,
            use_model_warmups=args.use_warmups,
            throttle=args.throttle,
            dyncfg_path=args.dyncfg_path,
        )

        # restore_from_hdf5
        vrise_mapgen.run(
            dataset=dataset,
            use_tqdm=not args.quiet,
        )


def extend_experiment(args: argparse.Namespace) -> None:
    # NOTE: `extend` should not be compatible (or should at least warn the user when used) with archives created
    # in `mask reuse` mode if extension is carried out along "neutral" dims (like img, cls, evaluator.*).
    # That's because results along these dimensions will no longer be based on a single set of masks.
    # Experiment extension will have to generate a new set of masks because old ones aren't stored anywhere
    raise NotImplementedError("This won't be implemented anytime soon")

def add_vrise_specific_runtime_args(parser: argparse.ArgumentParser):
    parser.add_argument(
        '--render-device',
        required=False,
        type=torch.device,
        default=torch.device('cpu'),
        help="torch device used by the renderer"
    )


# region CLI arg parsing
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="command_name")

subparser_for_start = subparsers.add_parser('start', description="Create a new map archive and start saliency map generation for provided params")
subparser_for_start.set_defaults(func=start_experiment)
subparser_for_start.add_argument('-c', '--config', required=True, type=os.path.abspath, help="Path to a JSON file containing experiment config")
subparser_for_start.add_argument('-o', '--output-path', required=True, type=os.path.abspath, help='Path to file where scores archive will be stored. By default, results will be stored in the current working directory and the name of the archive will be based on the input file name.')
subparser_for_start.add_argument('-f', '--imgcls-list', required=False, nargs='*', default=None, type=os.path.abspath, help="Path to a list of (img_path, class_idx) pairs")
subparser_for_start.add_argument('--img', required=False, type=os.path.abspath, help="image to evaluate")
subparser_for_start.add_argument('--cls', required=False, type=int, nargs='+', help="integer index(es) of object classes to generate maps for")
subparser_for_start.add_argument(
    '--reuse-masks',
    required=False,
    action='store_true',
    help="If used, a single set of masks will be reused for all evaluations, as long as their maskgen parameters remain compatible with current mask set"
         " Each run will use a new set of masks."
         " It's recommended to put 'img' and 'cls' params at the end of param order to maximize speedup offered by this option."
)
add_vrise_specific_runtime_args(subparser_for_start)
add_mapgen_specific_runtime_args(subparser_for_start)
add_runtime_arguments(subparser_for_start)
add_common_arguments(subparser_for_start)
add_adhoc_params(subparser_for_start)

subparser_for_resume = subparsers.add_parser('resume', description="Resume an interrupted experiment")
subparser_for_resume.set_defaults(func=resume_experiment)
subparser_for_resume.add_argument('-a', '--archive', required=True, type=os.path.abspath, help="Path to HDF experiment file")
add_vrise_specific_runtime_args(subparser_for_resume)
add_mapgen_specific_runtime_args(subparser_for_resume)
add_runtime_arguments(subparser_for_resume)
add_common_arguments(subparser_for_resume)

subparser_for_extend = subparsers.add_parser('extend', description="Expand parameter space, and generate maps for these parameters")
subparser_for_extend.set_defaults(func=extend_experiment)
add_vrise_specific_runtime_args(subparser_for_extend)
add_mapgen_specific_runtime_args(subparser_for_extend)
add_runtime_arguments(subparser_for_extend)
add_common_arguments(subparser_for_extend)

args = parser.parse_args()
args.func(args)

# endregion cli arg parsing


