import typing as t
from xml.etree import ElementTree as ET
import pandas
import pandas as pd


def load_bbox_as_dict(xml: str) -> t.Sequence[t.Dict[str, t.Any]]:
    """
    Parses annotations in XML format and returns a sequence of bounding boxes as dicts, ome per bounding box.
    Format of a single dict:
    {
        "img": (str: image name)
        "synset": (str: synset ID of the object class)
        "xmin|xmax|ymin|ymax": (int: pixel coordinate of each corner of the bounding box)
    }
    """

    root = ET.fromstring(xml)
    img_name = root.find("filename").text
    bounding_boxes = root.findall("object")

    return [
        {
            "img":    img_name,
            "synset": bbox.find("name").text,
            **{
                coord_node.tag: int(coord_node.text.strip())
                for coord_node
                in bbox.find("bndbox")
            }
        }
        for bbox
        in bounding_boxes
    ]


def load_bbox_as_rows(xml: str) -> t.List[t.Tuple]:
    """
    Parses annotations in XML format and returns a sequence of bounding boxes in compressed row format
    Format of a single row:
    (
        img name (str),
        synset ID (str),
        xmin (int), ymin (int), xmax (int), ymax(int)
    )
    """
    root = ET.fromstring(xml)
    img_name = root.find("filename").text
    bounding_boxes = root.findall("object")
    return [
        (
            img_name,
            bbox.find("name").text,
            *(
                int(bbox.find("bndbox").find(coord_name).text.strip())
                for coord_name
                in ("xmin", "ymin", "xmax", "ymax")
            )
        )
        for bbox
        in bounding_boxes
    ]


def load_to_dataframes(annotation_xmls: t.Iterable[str]) -> t.Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Loads all annotations found in `annotation_xmls` strings and returns a tuple of two pandas dataframes:
    1. image info dataframe: ['folder', 'dataset', 'file', 'height', 'width']
    2. bounding boxes dataframe ['img', 'cls', 'synset', 'bbox', 'xmin', 'xmax', 'ymin', 'ymax']

    """
    img_df_mapping = {
        'folder': 'folder',
        'source/database': 'dataset',
        'filename': 'img',
        'size/width': 'width',
        'size/height': 'height',
    }

    img_records = []
    obj_records = []
    for xml_string in annotation_xmls:
        et = ET.fromstring(xml_string)
        img_record = {
            df_col: et.find(xml_key).text
            for xml_key, df_col
            in img_df_mapping.items()
        }
        img_records.append(img_record)

        for instance in et.iterfind('object'):
            objects_record = {
                'img': img_record['img'],
                'synset': instance.find('name').text.strip(),
                **{
                    elem.tag: int(elem.text)
                    for elem
                    in instance.find('bndbox')
                }
            }
            obj_records.append(objects_record)

    img_df = (
        pandas.DataFrame.from_records(img_records)
        .assign(
            height=lambda df: df['height'].apply(int),
            width=lambda df: df['width'].apply(int),
        )
    )
    bbox_df = pandas.DataFrame.from_records(obj_records)
    return img_df, bbox_df