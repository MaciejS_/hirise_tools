import typing as t

import numpy as np
import pandas as pd


def scale_bboxes(bbox_df: pd.DataFrame, img_df: pd.DataFrame, scale: t.Tuple[int, int] = (1,1)) -> pd.DataFrame:
    """
    Scales bounding box data to image read from `img_df`.
    the returned dataframe has identical structure to bbox_df, but the bbox coord values are
    replaced with values in range (0, 1) expressing the ratio of bbox coord to img edge length.
    """
    expected_bbox_cols = {'xmin', 'ymin', 'xmax', 'ymax'}
    expected_img_cols = {'height', 'width'}
    if not expected_bbox_cols.issubset(bbox_df.columns):
        common_bbox_cols = expected_bbox_cols.intersection(bbox_df.columns)
        raise KeyError(
            f"bbox_df must contain all of the following columns: {expected_bbox_cols}, got {common_bbox_cols}"
        )

    if not expected_img_cols.issubset(img_df.columns):
        common_img_cols = expected_img_cols.intersection(img_df.columns)
        raise KeyError(
            f"img_df must contain all of the following columns: {expected_img_cols}, got {common_img_cols}"
        )


    return (
        bbox_df
        .merge(img_df, how='left', on='img')
        .assign(
            **{
                'xmin': lambda df: (df['xmin'] / df['width']) * scale[1],
                'xmax': lambda df: (df['xmax'] / df['width']) * scale[1],
                'ymin': lambda df: (df['ymin'] / df['height']) * scale[0],
                'ymax': lambda df: (df['ymax'] / df['height']) * scale[0],
            },
            axis=0
        )
        .loc[:, bbox_df.columns]
    )

def add_bbox_arrays(bbox_df: pd.DataFrame) -> pd.DataFrame:
    """
    Adds a 'bbox' column containing y(min/max) x(min/max) coords packed into
    (2,2) shape arrays, as expected by `poninting_game` implementation
    """
    return (
        bbox_df.assign(
            bbox=list(
                bbox_df
                .loc[:, ('ymin', 'ymax', 'xmin', 'xmax')]
                .values
                .reshape(-1, 2, 2)
            )
        )
    )

def render_bbox(bbox_tensor: np.ndarray, map_shape: t.Union[np.ndarray, t.Sequence[int]], scale_bbox:bool=True) -> np.ndarray:
    """
    Returns a binary tensor of shape (len(bbox_tensor), *map_shape).
    Each slice along the first dimension contains a "render" of a corresponding bbox from bbox_tensor.

    if `scale_bbox` is true, bbox_tensor is assumed to contain values in range (0, 1) and will be scaled to `map_shape` (ZYX dim order)
    """
    if bbox_tensor.ndim != 3:
        raise NotImplementedError(f"bbox_tensor ndim != 3 is not supported (got {bbox_tensor.ndim})")
    if len(map_shape) != bbox_tensor.shape[-2]:
        raise ValueError(f"bbox_tensor ndim != 3 is not supported (got {bbox_tensor.ndim})")

    if scale_bbox:
        bbox_tensor = (bbox_tensor * np.array(map_shape).reshape(1, len(map_shape), 1)).round()
    bbox_tensor = bbox_tensor.astype(int)

    bbox_renders = np.zeros((bbox_tensor.shape[0], *map_shape), dtype=bool)
    for i, bbox_coords in enumerate(bbox_tensor):
        selector = (slice(*dim_coords) for dim_coords in bbox_coords)
        bbox_renders[(i, *selector)] = True

    return bbox_renders

def bbox_union(bbox_tensor: np.ndarray, map_shape: t.Union[np.ndarray, t.Sequence[int]], scale_bbox:bool=True) -> np.ndarray:
    """
    Convenience function - renders bboxes and returns their union as a tensor of shape - (map_shape)
    """
    bbox_renders = render_bbox(bbox_tensor, map_shape, scale_bbox)
    return np.sum(bbox_renders, axis=0).astype(bool)


def bbox_intersection(bbox_tensor: np.ndarray, map_shape: t.Union[np.ndarray, t.Sequence[int]], scale_bbox:bool=True) -> np.ndarray:
    """
    Convenience function - renders bboxes and returns their intersection as a tensor of shape - (map_shape)
    """
    bbox_renders = render_bbox(bbox_tensor, map_shape, scale_bbox)
    return np.prod(bbox_renders, axis=0).astype(bool)

