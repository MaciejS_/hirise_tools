import time

import torch
from hirise_tools.dyncfg import DynamicJSONConfig

from hirise.mask_evaluator import MaskEvaluator

from tqdm import tqdm

from hirise_tools.models import load_resnet50
from hirise.experiment.utils import parallel_confusion_triu

CONFIG_FILE = "/tmp/dyn.cfg"
## real-time support of parallelism

def configurable_cpu_parallelism_spike(array):

    config = DynamicJSONConfig(CONFIG_FILE)

    n_reps = 30
    bar = tqdm(total=n_reps, desc=f"Job")
    for i in range(n_reps):
        n_jobs = config["cpu_threads"]
        bar.set_description(f"Job ({n_jobs} threads)")
        parallel_confusion_triu(array, lambda x, y: (x**2 - y**2).mean(), op_dims=2, n_jobs=n_jobs)
        bar.update(1)


def split_merge_computations_on_multiple_gpus():
    """
    Give up on this one for now. It's way too complex.
    :return:
    """


    def devices_changed():
        return hash(tuple(prev_devices.items())) != hash(tuple(devices.items()))

    config = DynamicJSONConfig(CONFIG_FILE)

    n_reps = 30
    bar = tqdm(total=n_reps, desc=f"Job")
    prev_devices = None

    for i in range(n_reps):
        devices = config["torch_devices"]
        if devices_changed():
            # rebuild evaluators
            evaluators = {device: MaskEvaluator(resnet50, torch.device(device), batch_size=batch_size, use_tqdm=False)
                          for device, batch_size
                          in devices.items()}
            # adjust batch ratios
            raise NotImplementedError
        devices = config["torch_devices"]


def dynamic_device_and_batch_size_change():
    config = DynamicJSONConfig(CONFIG_FILE)

    IMG = torch.rand(3, 224, 224)
    MASKS = torch.rand(200, 3, 224, 224)

    n_reps = 200
    device = torch.device(config["torch_device"])
    resnet50, target_size = load_resnet50(device)
    evaluator = MaskEvaluator(resnet50, device, batch_size=50, use_tqdm=False)

    bar = tqdm(total=n_reps, desc=f"Job")
    for i in range(n_reps):
        evaluator.evaluate(IMG, MASKS, 0)
        bar.update(1)

        device = config["torch_device"]
        batch_size = config["batch_size"]
        if device != str(evaluator.device) or batch_size != evaluator.batch_size:
            if device is not None and batch_size != 0:
                device = torch.device(device)
                resnet50 = resnet50.to(device)
                torch.cuda.empty_cache() # necessary in order to free up memory for other processes when batch is downsized
                evaluator = MaskEvaluator(resnet50, device, batch_size=batch_size, use_tqdm=False)
            else:
                with tqdm(desc=f"Paused, {str(device)} {batch_size}", bar_format="{desc} {elapsed}",
                          leave=False) as waiting_bar:
                    while config["torch_device"] is "None" or config["batch_size"] == 0:
                        bar.set_description("No device")
                        waiting_bar.update(1)
                        time.sleep(1)

        bar.set_description(f"{str(device)} {batch_size}")

def intermittent_calculations_on_gpu():
    """
    Adds pauses between iterations and allows breaks

    :return:
    """

    raise NotImplementedError


# configurable_cpu_parallelism_spike(np.random.rand(4, 3, 80, 100, 100))

dynamic_device_and_batch_size_change()