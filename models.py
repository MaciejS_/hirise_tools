import typing as t

import torch
from hirise.device import device_from_torch_model
from torch import nn
from torch.backends import cudnn
from torchvision import models


def _prepare_model(model: torch.nn.Sequential) -> torch.nn.Sequential:
    """
    Configures model for operation in evaluation mode
    """
    model = nn.Sequential(model, nn.Softmax(dim=1))
    model = model.eval()

    for p in model.parameters():
        p.requires_grad = False

    cudnn.benchmark = True
    return model

def load_resnet50(target_device: torch.device) -> t.Tuple[torch.nn.Sequential, t.Tuple[int, int]]:
    model_input_size = (224, 224)
    model = models.resnet50(pretrained=True).to(target_device)
    return _prepare_model(model), model_input_size

def load_vgg16(target_device: torch.device) -> t.Tuple[torch.nn.Sequential, t.Tuple[int, int]]:
    model_input_size = (224, 224)
    model = models.vgg16(pretrained=True).to(target_device)
    return _prepare_model(model), model_input_size

def load_model_by_name(model_name: str, device: torch.device) -> t.Tuple[torch.nn.Sequential, t.Tuple[int, int]]:
    if model_name.startswith('resnet'):
        n_layers = int(model_name[6:].rstrip())
        # TODO: remove this constraint
        if n_layers != 50:
            raise NotImplementedError("ResNet versions other than ResNet50 aren't supported")
        return load_resnet50(device)
    elif model_name.startswith('vgg'):
        n_layers = int(model_name[3:].rstrip())
        if n_layers != 16:
            raise NotImplementedError("VGG versions other than VGG16 aren't supported")
        return load_vgg16(device)
    else:
        raise ValueError(f"Provided model type is not supported: {model_name}")


def model_warmup(model, input_size, batch_size: int=100, use_fp16:bool=False):
    warmup_data = torch.rand(batch_size, 3, *input_size)
    if use_fp16:
        warmup_data = warmup_data.half()

    device = device_from_torch_model(model)
    model(warmup_data.to(device))
    del warmup_data