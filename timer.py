import time


class Timer():

    def __init__(self):
        self._start_time = None
        self._end_time = None

    def __enter__(self):
        self.start()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def start(self):
        self._start_time = time.perf_counter()
        self._end_time = None

    def stop(self):
        self._end_time = time.perf_counter()

    @property
    def elapsed(self):
        return (self._end_time if self._end_time else time.perf_counter()) - self._start_time


class RecordingTimer(Timer):

    def __init__(self):
        Timer.__init__(self)
        self._recorded_times = []

    def stop(self):
        Timer.stop(self)
        self._recorded_times.append(self.elapsed)

    def clear(self):
        self._recorded_times = []

    @property
    def recorded_times(self):
        return self._recorded_times


class PrintingTimer(Timer):
    def __init__(self, op_name=None):
        super().__init__()
        self.operation_name = op_name or "Elapsed"

    def __str__(self):
        return f"{self.operation_name}: {self.elapsed:3f}s"

    def __exit__(self, exc_type, exc_val, exc_tb):
        super().__exit__(exc_type, exc_val, exc_tb)
        print(self)
