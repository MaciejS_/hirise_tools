from math import floor, log10


def npow10(index: int) -> bool:
    """
    Checkpoint lambda which returns true on single-digit multiples of power of ten, e.g. 1,2,3 ... 9, 10, 20, 30 ... 90, 100, etc.
    Raises ValueError when 0 is passed

    x * (10^n), where x is an int in range [1, 9] and n is an int in range [0, +inf)

    :param index:
    :return: boolean
    """
    return not (index % 10**(floor(log10(index))))