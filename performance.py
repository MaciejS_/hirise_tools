import contextlib
import logging
from timeit import default_timer as timer


def get_file_logger(logger_name, log_file):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
    fh.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    return logger


@contextlib.contextmanager
def log_performance(name, logger):
    start = timer()
    yield
    elapsed = timer() - start
    logger.info(f'{name} took: {elapsed:.3f}s')

@contextlib.contextmanager
def measure_performance(results_list):
    start = timer()
    yield
    elapsed = timer() - start
    results_list.append(elapsed)

